package com.test.models.gpdecorate.login;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 10:25
 * @description
 */
public interface ILoginForThirdService extends ILoginService {

    String loginForQQ(String qq);


    String loginForPhone(String phone);

    String loginForWC(String id);
}
