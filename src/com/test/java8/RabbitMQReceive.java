package com.test.java8;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQReceive {
    public static final String RABBITMQ_NAME="produce_people";

    public static final String EXCHANGE_NAME="exchage_basic";
    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection=ConnectionConfig.getConnection();
        Channel channel=connection.createChannel();
        //获取消息队列 生命消费者
//        channel.queueDeclare(RABBITMQ_NAME,true,false,false,null);
//        channel.queueBind(RABBITMQ_NAME,EXCHANGE_NAME,"key1",null);
        //channel.queueBind(RABBITMQ_NAME,EXCHANGE_NAME,"key",null);

        Consumer consumer=new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                super.handleDelivery(consumerTag, envelope, properties, body);
                System.out.println(envelope.getRoutingKey()+"接收生产消息："+new String(body,"utf-8"));
            }
        };
//        channel.basicConsume(RABBITMQ_NAME,false,consumer);

    }
}
