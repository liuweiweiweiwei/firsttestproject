package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/8/8 15:27
 * @description
 */
public class LeetCodeDpThree {
    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int end = getMaxChar(nums);
        System.out.println(end);
        //
        TreeNode treeNode = new TreeNode(5);
        TreeNode right = new TreeNode(12);
        TreeNode left = new TreeNode(3);
        treeNode.right = right;
        treeNode.left = left;

        //TreeNode treeNode1 = convertBST(treeNode);
        System.out.println(romanToInt("MCMXCIV"));

        System.out.println(Arrays.toString("(asd((yyy".split("\\(",-1)));
        //第一题
        String s = reverseParentheses("(ed(et(oc))el)");
        System.out.println("第一题的答案是："+s);

    }
    public static int romanToInt(String s) {
        Map<String, Integer> map = new LinkedHashMap<>();
            map.put("M",1000);
            map.put("CM",900);
            map.put("D",500);
            map.put("CD",400);
            map.put("C",100);
            map.put("XC",90);
            map.put("L",50);
            map.put("XL",40);
            map.put("X",10);
            map.put("IX",9);
            map.put("V",5);
            map.put("IV",4);
            map.put("I",1);
            Integer end=0;
            for(String k:map.keySet()) {
                while (s.startsWith(k)) {
                    end+=map.get(k);
                    int index=k.length()==1?s.indexOf(k)+1:s.indexOf(k)+2;
                    s=s.substring(index);
                    if (s.equals("")) return end;
                }
            }
            return end;
        }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public static TreeNode convertBST(TreeNode root) {
        int num=0;
        if(root!=null){
            root=convertBST(root.right);
            root.val+=num;
            num=root.val;
            root=convertBST(root.left);
            return root;
        }
        return null;
    }
    /**
     * 连续最大子字符串
     * int[] nums = [-2,1,-3,4,-1,2,1,-5,4]
     * 1, 总和与下一个数字比较：如果加上此数字与总和相比小于此数据，则从此数据从新计数 2,
     */
    public static int getMaxChar(int[] arr){
        if(arr.length==0){
            return 0;
        }
        int num=arr[0];
        int next=arr[0];
        for(int i=1;i<arr.length;i++){
            next+=arr[i];
            next=Math.max(arr[i],next);
            num=Math.max(next,num);
        }
        return num;
    }

    /**
     * 赢家是否
     * @param nums 数组
     * @return
     */
    public boolean PredictTheWinner(int[] nums) {

        return false;
    }

    public static int findMaximumXOR(int[] nums) {
        int t=0;
        for (int i = 0; i < nums.length-1; i++) {
            for (int j = 0; j < i; j++) {
                t=Math.max(t,nums[i]^nums[j]);
            }
        }
        return t;
    }

    public static String reverseParentheses(String s) {
        StringBuilder temp=new StringBuilder();
        List<String> splitHead = Arrays.asList(s.split("\\(",-1));
        int start=splitHead.size()-2;
        String tem=splitHead.get(splitHead.size()-1);
        String mid=tem.substring(0,tem.indexOf(")"));
        List<String> splitTail = Arrays.asList(tem.split("\\)",-1));
        int end=0;
        if(splitHead.get(start)!=null&&!"".equals(splitHead.get(start))&&
                splitTail.get(end)!=null&&!"".equals(splitTail.get(end))){
            temp.append(mid);
        }else{
            char[] c = temp.toString().toCharArray();
            StringBuilder te= new StringBuilder();
            for (char c1:c) {
                te.insert(0, c1);
            }
            temp.append(te.toString());
        }

        return temp.toString();
    }

    public static int hammingDistance(int x, int y) {
        int result=x^y;
        int m=0;
        while (result!=0){
            m=result%2==1? ++m :m;
            result/=2;
        }
        return m;
    }

    public static boolean isPowerOfTwo(int n) {
        while (n>1){
            if(n%2!=0){
                return false;
            }
            n/=2;
        }
        return true;
    }


    public static boolean isPowerOfFour(int n) {
        if(n<=0)return false;
        while (n>1){
            if(n%4!=0)return false;
            n/=4;
        }
        return true;
    }
}
