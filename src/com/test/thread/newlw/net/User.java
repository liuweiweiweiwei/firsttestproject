package com.test.thread.newlw.net;

import javassist.SerialVersionUID;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 14:46
 * @description
 */
public class User implements Serializable{

    //版本号
    private final static long serialVersionUID = 1L;
    private String name;
    private transient Integer age;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 序列化重写方法
     * @param objectOutputStream
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream objectOutputStream)throws IOException{
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(age);
    }

    /**
     * 读序列化
     * @param objectInputStream
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        age = (Integer) objectInputStream.readObject();
    }
    
}
