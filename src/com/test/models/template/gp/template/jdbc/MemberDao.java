package com.test.models.template.gp.template.jdbc;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 14:24
 * @description
 */
public class MemberDao extends JdbcTemplate{

    public MemberDao(DataSource dataSource) {
        super(dataSource);
    }
    
    public List<?> selectAll(){
        String sql = "select * from user";
        return super.executeQuery(sql, new RowMapper<Member>() {
            @Override
            public Member mapRow(ResultSet rs, int rowNum) throws Exception {
                Member member = new Member();
                member.setName(rs.getString("username"));
                member.setWork(rs.getString("hobby"));
                member.setId(rs.getInt("id"));
                return member;
            }
        },null);
    }
}
