package com.test.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/24 20:35
 * @description
 */
public class ExcuteThread {
    /**
     * single execute
     */
    public static final ExecutorService EXECUTOR_SERVICE_SINGLE = new ThreadPoolExecutor(
            1,
            1,
            30,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100),
            new ThreadPoolExecutor.DiscardPolicy());

    /**
     * catch execute
     */
    public static final ExecutorService EXECUTOR_SERVICE_CATCH = new ThreadPoolExecutor(
            8,
            8,
            30,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100000),
            new ThreadPoolExecutor.DiscardPolicy());
}
