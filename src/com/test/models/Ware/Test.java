package com.test.models.Ware;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/15 15:00
 * @description 桥
 */
public class Test {

    public static void main(String[] args){
        Phone phone=new Oppo();
        phone.setSoftWare(new AppStore());
        phone.run();
        phone.setSoftWare(new Camera());
        phone.run();
        Phone phone1=new Vivo();
        phone1.setSoftWare(new Camera());
        phone1.run();
        phone1.setSoftWare(new AppStore());
        phone1.run();
    }
}

/**
 * 软件
 */
interface SoftWare{
    void run();
}
class AppStore implements SoftWare{
    @Override
    public void run() {
        System.out.println("run with AppStore!");
    }
}
class Camera implements SoftWare{
    @Override
    public void run() {
        System.out.println("run with Camera!");
    }
}
/**
 * 手机
 */
abstract class Phone{
    protected SoftWare softWare;
    public void setSoftWare(SoftWare softWare){this.softWare=softWare;}
    public abstract void run();
}
class Oppo extends Phone{
    @Override
    public void run() {
        System.out.print("Oppo 手机：");
        softWare.run();
    }
}
class Vivo extends Phone{
    @Override
    public void run() {
        System.out.print("Vivo 手机：");
        softWare.run();
    }
}
