package com.test.models.delegite.simple;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 16:36
 * @description
 */
public interface IWorkers {

    void work();
}
