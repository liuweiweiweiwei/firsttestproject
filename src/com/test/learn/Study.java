package com.test.learn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/3/2 12:47
 * @description
 */
public class Study {

    public static void main(String[] args) {

    }
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        ArrayList<List<Integer>> result = new ArrayList<>();
        HashSet<List<Integer>> hashSet = new HashSet<>();
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] >  0) break;
            int low = i + 1;
            int high = nums.length - 1;
            while (low < high) {
                if (nums[low] + nums[high] + nums[i] == 0) {
                    if (!hashSet.contains(Arrays.asList(nums[i], nums[low], nums[high]))) {
                        hashSet.add(Arrays.asList(nums[i], nums[low], nums[high]));
                        result.add(Arrays.asList(nums[i], nums[low], nums[high]));
                    }
                    while (low < high && nums[low] == nums[low + 1]) low++;
                    while (low < high && nums[high] == nums[high - 1]) high--;
                    low++;
                    high--;
                } else if (nums[low] + nums[high] + nums[i] > 0 ) {
                    high--;
                } else {
                    low++;
                }
            }
        }
        return result;
    }
}
