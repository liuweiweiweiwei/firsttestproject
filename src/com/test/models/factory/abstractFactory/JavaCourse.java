package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 11:18
 * @description
 */
public class JavaCourse implements ICourse {
    @Override
    public void operateCourse() {
        System.out.println("java的 course 操作！");
    }
}
