package com.test.autimatic;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/11/21 9:17
 * @description
 */
public class LeetCodeFour {
    public static void main(String[] args) {
        ListNode listNode = new ListNode(3);
        ListNode listNode1 = new ListNode(4);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(1);
        listNode.next = listNode1;
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        sortList(listNode);
        //
        int[] end = {1, 2, 4};
        int mo = maxProfit(end);
        System.out.println(mo);
        //3
        int m = fourSumCount(new int[]{1, 2}, new int[]{-2, -1}, new int[]{-1, 2}, new int[]{0, 2});
        System.out.println(m);
        //4.
        int[] tem = {2147483647, 2147483647, 2147483647, 2147483647, 2147483647, 2147483647};

        int result = reversePairs(tem);
        System.out.println(result);
        //5.
        int[] merge = {2, 6, 4, 9, 2, 4, 5, 1, 6, 12, 145, 2, 1, 10, 12, 25, 7, 9, 8, 13};
        int[] mergeResult = getMerge(merge, 0, merge.length - 1);
        Arrays.stream(mergeResult).forEach(System.out::println);
        //6.
        int[][] ints = {{1, 3, 1}, {1, 5, 1}, {4, 2, 1}};
        System.out.println(maxValue(ints));
        //7.
        int[] nuseven = {1, 1, 1, 1, 1, 1};
        System.out.println(Arrays.toString(searchRange(nuseven, 1)));
        //8.
        char[] chars = {'A', 'A', 'A', 'B', 'B', 'B'};
        int end8 = leastInterval(chars, 2);
        System.out.println(end8);
        //9.
        long t1 = System.currentTimeMillis();
        int end9 = countPrimes(10000);
        long t2 = System.currentTimeMillis();
        System.out.println(end9 + ":9:" + (t2 - t1));
        //10
        System.out.println(0b1111 + ";；；；10");
        int[][] en10t = {{0, 1}, {1, 1}};
        int end10 = matrixScore(en10t);
        System.out.println("10:::" + end10);
        //11.
        String S = "17522";
        List<Integer> integerList = splitIntoFibonacci(S);
        System.out.println("11:>>>>>>>>>>>>>>>>>>>>>>");
        integerList.forEach(System.out::println);
        //12
        int[] ints12 = {5, 5, 10};
        boolean b = lemonadeChange(ints12);
        System.out.println(b);
        //13
        String str = "DDRRRR";
        //String s13=predictPartyVictory(str);
        //System.out.println(s13);
        //移动机器人
        int[][] ints1 = {{1, 2, 3}, {1, 1, 1}, {1, 1, 1}, {1, 2, 1}};
        int lenMoveRabit = getMoveOn(ints1);
        System.out.println("移动机器人:" + lenMoveRabit);
        //14
        String round = "aasdcaasdxzb2jd";
        Kmp kmp = new Kmp("aasdx");
        System.out.println(kmp.search(round));
        //15
        System.out.println("15:" + max(new int[]{2, 5, 6, 3, 1, 7}, 1));
        //16
        TreeNode treeNode1 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(7);
        TreeNode treeNode4 = new TreeNode(1);
        TreeNode treeNode5 = new TreeNode(3);
        treeNode1.left = treeNode2;treeNode1.right = treeNode3;treeNode2.left = treeNode4;treeNode2.right = treeNode5;
        TreeNode treeNode = searchBST(treeNode1, 2);
        System.out.println(treeNode);
        //leetcode
        Solution solution = new Solution(3,1);
        solution.flip();
        solution.flip();
        solution.reset();
        solution.flip();
        solution.flip();
        solution.flip();
        //leetcode
        System.out.println(Arrays.toString(kthSmallestPrimeFraction1(new int[]{1, 2, 3, 5}, 3)));
        //leetcode
        System.out.println(isPalindrome("race a car"));
        System.out.println('a'-'0');
    }



    public static boolean isPalindrome(String s) {
        int i = 0,j = s.length() - 1;
        while (i <= j){
            char ac = Character.toLowerCase(s.charAt(i));
            char bc = Character.toLowerCase(s.charAt(j));
            if(ac - '0' < 0 || ac - '0' > 74 || (ac - '0' > 9 && ac - '0' < 49)){
                i++;
                continue;
            }
            if(bc - '0' < 0 || bc - '0' > 74  || (bc - '0' > 9 && bc - '0' < 49)){
                j--;
                continue;
            }
            if(ac != bc){
                return false;
            }
            i++;j--;
        }
        return true;
    }


    /**
     *
     * @param arr
     * @param k
     * @return
     */
    public static int[] kthSmallestPrimeFraction(int[] arr, int k) {
        int temp = 0;
        //统计数目
        for (int i = 0; i < arr.length; i++) {
            temp += i;
        }//构造存储数据
        int[][] sort = new int[temp][2];
        //利用类似于选择排序
        for (int i = 1,index = 0; i < arr.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                //记录对应位置的数据
                sort[index][0] = arr[j];
                sort[index][1] = arr[i];
                index++;
            }
        }
        //排序二维数组
        Arrays.sort(sort, (o1, o2) -> o1[0] * o2[1] - o2[0] * o1[1] > 0 ? 1 : -1);
        //返回目标值
        return new int[]{sort[k-1][0],sort[k-1][1]};
    }

    /**
     * 优先队列
     * @param arr
     * @param k
     * @return
     */
    public static int[] kthSmallestPrimeFraction1(int[] arr, int k) {
        //统计数据---按照arr的数据相除（转化为相乘）进行排序
        PriorityQueue<int[]> priorityQueue = new PriorityQueue<>((a,b)->arr[a[0]] * arr[b[1]] - arr[b[0]] * arr[a[1]]);
        //先将最小的数据集合放进去--就是类似于1/2,1/3,1/5,1/7...但是他们的下标为0,1；0,2,0,3...(注意不能包括自己)
        for (int i = 1; i < arr.length; i++) {
            priorityQueue.offer(new int[]{0,i});
        }
        //将数据每次取出最小值进行分子加一操作(注意不能包括自己)-此时取到第k个后存到队列内
        // （队列自动排序）再进行下一次取最小值，进行分子加一即可保证最新加入的是比后续加入的要小
        for (int i = 1; i < k; i++) {
            //因为已经取到过这个数据---所以这个数据后边不会再取到了--因此可以直接删掉
            int[] poll = priorityQueue.poll();
            if(poll[0] + 1 < poll[1]){//将取到的值放进去
                priorityQueue.offer(new int[]{poll[0] + 1 , poll[1]});
            }
        }
        //返回目标值
        return new int[]{arr[priorityQueue.peek()[0]], arr[priorityQueue.peek()[1]]};
    }

    public static List<Integer> findAnagrams(String s, String p) {
        //如果p的长度比s大则直接结束
        if (p.length()>s.length()){return Collections.emptyList();}
        //定义用于比较的字符数组
        int[] temp = new int[26];
        int[] create = new int[26];
        //统计temp的p中字母数
        for (char c:p.toCharArray()) {
            temp[c - 'a']++;
        }
        //循环s的长度
        List<Integer> index = new ArrayList<>();
        for (int i = 0,j=0; j < s.length(); i++) {
            while (j<s.length()&&j-i<p.length()){
                //增加右节点长度使他达到p的长度
                create[s.charAt(j++)- 'a']++;
            }//如果满足条件记录i的下标位置
            if(juNumSim(temp,create)){
                index.add(i);
            }//移除i位置所对应的字母数
            create[s.charAt(i)-'a']--;
        }
        return index;
    }

    /**
     * 判断是否相等
     * @param a
     * @param b
     * @return
     */
    private static boolean juNumSim(int[] a,int[] b){
        for (int i = 0; i < 26; i++) {
            if(a[i] != b[i]){
                return false;
            }
        }
        return true;
    }

    public static class Solution {

        Map<Integer, Integer> map = new HashMap<>();
        int m, n, total;
        Random rand = new Random();

        public Solution(int m, int n) {
            this.m = m;
            this.n = n;
            this.total = m * n;
        }

        public int[] flip() {
            int x = rand.nextInt(total);
            total--;
            // 查找位置 x 对应的映射
            int idx = map.getOrDefault(x, x);
            // 将位置 x 对应的映射设置为位置 total 对应的映射
            map.put(x, map.getOrDefault(total, total));
            return new int[]{idx / n, idx % n};
        }

        public void reset() {
            total = m * n;
            map.clear();
        }
    }

    /**
     * Definition for a binary tree node.
     */
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static TreeNode searchBST(TreeNode root, int val) {
        if (root == null || root.val == val) {
            return root;
        }
        TreeNode treeNodeL = searchBST(root.left, val);
        if(treeNodeL != null){return treeNodeL;}
        return searchBST(root.right, val);
    }

    /**
     * 2进制神器
     *
     * @param buckets
     * @param minutesToDie
     * @param minutesToTest
     * @return
     */
    public int poorPigs(int buckets, int minutesToDie, int minutesToTest) {
        return (int) Math.ceil(Math.log(buckets) / Math.log(minutesToTest / minutesToDie));
    }


    /**
     * 处理单交易
     *
     * @param price
     * @return
     */
    public static int doublePrice(int[] price, int k) {
        int dp_i_0 = 0;
        int dp_i_1 = -price[0];//定义初始值0为交易，1交易完
        for (int i = 1; i < price.length; i++) {
            int temp = k == 1 ? 0 : dp_i_0;//存储前一个为0的值
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + price[i]);//替换未交易
            dp_i_1 = Math.max(dp_i_1, temp - price[i]);//替换交易
        }
        return dp_i_0;
    }

    /**
     * 价格数据
     *
     * @param price
     * @return
     */
    public static int max(int[] price, int k) {
        //如果k为1或者k>price.length/2则调用doubleprice
        if (k == 1 || k >= price.length) {
            return doublePrice(price, k);
        }
        //定义中间状态转移值dp[0][0][0]--k表示交易次数，price表示价格，0表示出售，1表示购买
        int[][][] dp = new int[price.length][k + 1][2];
        //定义初始值
        dp[0][k][0] = 0;
        dp[0][k - 1][1] = -price[0];
        for (int i = 1; i < price.length; i++) {
            for (int j = k; j > 0; j--) {
                //如果当前为已经出售的状态：从上一次没有购买转移过来，也可以从上一次到这次卖出后转移过来
                dp[i][j][0] = Math.max(dp[i - 1][j][1] + price[i], dp[i - 1][j][0]);
                //如果当前为购买状态：则可以从上一次出售状态本次购买转移过来，也可以从上一次未购买状态转移过来
                dp[i][j][1] = Math.max(dp[i - 1][j - 1][0] - price[i], dp[i - 1][j][1]);
            }
        }
        return dp[price.length - 1][k][0];
    }


    static class Kmp {
        private int[][] dp;
        private String string;

        public Kmp(String chars) {
            this.dp = new int[chars.length()][256];
            this.string = chars;
            dp[0][string.charAt(0) - '0'] = 1;
            int x = 0;
            for (int i = 1; i < string.length(); i++) {
                for (int j = 0; j < 256; j++) {
                    dp[i][j] = dp[x][j];
                }
                dp[i][string.charAt(i) - '0'] = i + 1;
                x = dp[x][string.charAt(i) - '0'];
            }
        }

        /**
         * 查找数据
         *
         * @param search
         * @return
         */
        public int search(String search) {
            int index = 0;
            for (int i = 0; i < search.length(); i++) {
                index = dp[index][search.charAt(i) - '0'];
                if (string.length() == index) {
                    return i - string.length() + 1;
                }
            }
            return -1;
        }

    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static ListNode sortList(ListNode head) {
        if (head == null) {
            return new ListNode();
        }
        List<Integer> list = new ArrayList<>();
        ListNode headTem = head;
        while (headTem != null) {
            list.add(headTem.val);
            headTem = headTem.next;
        }
        List<Integer> integerList = list.stream().sorted().collect(Collectors.toList());
        ListNode listNode = new ListNode(integerList.get(0));
        ListNode temp = listNode;
        ListNode endChange;
        for (int i = 1; i < integerList.size(); i++) {
            ListNode temp1 = new ListNode(integerList.get(i));
            temp.next = temp1;
            endChange = temp;
            temp = endChange.next;
        }
        return listNode;
    }

    /**
     * 买卖股票
     */
    public static int maxProfit(int[] prices) {
        if (prices.length < 2) {
            return 0;
        }
        int t = 0;
        int min = prices[0];
        int max = prices[1];
        for (int i = 0; i < prices.length - 1; i++) {
            //校验降序
            if (prices[i] < prices[i + 1]) {
                t = 1;
            }
            //比较最大,最小值
            min = Math.min(prices[i], min);

            max = i != 0 ? Math.max(max, prices[i]) : max;
        }
        if (t == 0) {
            return 0;
        }
        return max - min;
    }

    /**
     * 输入: [3,6,9,1]
     * 输出: 3
     * 解释: 排序后的数组是 [1,3,6,9], 其中相邻元素 (3,6) 和 (6,9) 之间都存在最大差值 3。
     */
    public static int maximumGap(int[] nums) {
        int end = 0;
        if (nums.length < 2) {
            return end;
        }
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            end = Math.max(end, nums[i + 1] - nums[i]);
        }
        return end;
    }

    /**
     * 何为0
     * 输入:
     * A = [ 1, 2]
     * B = [-2,-1]
     * C = [-1, 2]
     * D = [ 0, 2]
     * <p>
     * 输出:
     * 2
     * <p>
     * 解释:
     * 两个元组如下:
     * 1. (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
     * 2. (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0
     *
     * @param A
     * @param B
     * @param C
     * @param D
     * @return
     */
    public static int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        List<Integer> other = new LinkedList<>();
        getmax(A, B, C, D, 0, 0, other);
        return other.size();
    }

    public static void getmax(int[] A, int[] B, int[] C, int[] D, int step, int num, List<Integer> list) {
        if (step == 4) {
            if (num == 0) {
                list.add(0);
            }
            return;
        }
        for (int i = 0; i < A.length; i++) {
            int stem = i;
            switch (step) {
                case 0:
                    stem = A[i];
                    break;
                case 1:
                    stem = B[i];
                    break;
                case 2:
                    stem = C[i];
                    break;
                case 3:
                    stem = D[i];
                    break;
                default:
                    break;
            }
            num += stem;
            step++;
            getmax(A, B, C, D, step, num, list);
            num -= stem;
            step--;
        }
    }

    /**
     * 给定一个数组 nums ，如果 i < j 且 nums[i] > 2*nums[j] 我们就将 (i, j) 称作一个重要翻转对。
     * 你需要返回给定数组中的重要翻转对的数量。
     * 示例 1:
     * 输入: [1,3,2,3,1]
     * 输出: 2
     * 示例 2:
     * 输入: [2,4,3,5,1]
     * 输出: 3
     * 注意:
     * 给定数组的长度不会超过50000。
     * 输入数组中的所有数字都在32位整数的表示范围内。
     */
    public static int reversePairs(int[] nums) {
        if (nums.length < 2) {
            return 0;
        }
        int num = 0;
        int tail = nums.length - 1;
        for (int i = tail; i > 0; i--) {
            long tem = nums[i];
            for (int j = i - 1; j >= 0; j--) {
                if (nums[j] > tem * 2) {
                    num++;
                }
            }
        }

        return num;
    }

    /**
     * 归并排序
     * 2,6,4,9,2,4,5,1,6,12,145,2,1,10,12,25,7,9,8,13
     */

    public static int[] getMerge(int[] nums, int start, int end) {
        if (start == end) {
            return new int[]{nums[start]};
        }
        int middle = (start + end) / 2;
        int[] left = getMerge(nums, start, middle);
        int[] right = getMerge(nums, middle + 1, end);
        int l = left.length;
        int r = right.length;
        int[] result = new int[l + r];
        int i = 0, j = 0, m = 0;
        while (l > i && r > j) {
            if (left[i] >= right[j]) {
                result[m++] = right[j++];
            } else {
                result[m++] = left[i++];
            }
        }
        while (i < l) {
            result[m++] = left[i++];
        }
        while (j < r) {
            result[m++] = right[j++];
        }
        return result;
    }


    /**
     * 给定一个字符串S，检查是否能重新排布其中的字母，使得两相邻的字符不同。
     * 若可行，输出任意可行的结果。若不可行，返回空字符串。
     * 示例 1:
     * 输入: S = "aab"
     * 输出: "aba"
     * 示例 2:
     * 输入: S = "aaab"
     * 输出: ""
     * 注意:
     * S 只包含小写字母并且长度在[1, 500]区间内。
     */
    public static String reorganizeString(String S) {
        int len = S.length();
        char[] index = {'a', 'b',
                'c', 'd', 'e', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z'};
        List<Object> list = Arrays.asList(index);
        //存储数量的字典
        Integer[][] dict = new Integer[26][1];
        for (char c : S.toCharArray()) {
            int m = list.indexOf(c);
            dict[m][0]++;
        }
        List<Integer> tempList = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < 26; i++) {
            if (dict[i][0] != 0) {
                tempList.add(dict[i][0]);
            }
            map.put(i, dict[i][0]);
        }
        List<Integer> tempListTem = tempList.stream().sorted().collect(Collectors.toList());
        int max = tempListTem.get(tempListTem.size() - 1);
        int num = 0;
        for (int i = 0; i < tempListTem.size() - 1; i++) {
            num += tempListTem.get(i);
        }
        if (max > num) {
            return "";
        }
        Map<Integer, Integer> mapLink = new LinkedHashMap<>();
        List<Map.Entry<Integer, Integer>> mapList = new ArrayList<>(map.entrySet());
        mapList.sort(new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        mapList.forEach(en -> mapLink.put(en.getKey(), en.getValue()));
        StringBuilder stringBuilder = new StringBuilder("");
        boolean judge = true;
        while (judge) {
            Set<Integer> entries = mapLink.keySet();
            Iterator<Integer> integerIterator = entries.iterator();

            judge = false;
        }
        return null;
    }

    /**
     * 输入:
     * [
     * [1,3,1],
     * [1,5,1],
     * [4,2,1]
     * ]
     * 输出: 12
     * 解释: 路径 1→3→5→2→1 可以拿到最多价值的礼物
     */
    public static int maxValue(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                if (i == 0) {
                    grid[i][j] += grid[i][j - 1];
                } else if (j == 0) {
                    grid[i][j] += grid[i - 1][j];
                } else {
                    grid[i][j] = Math.max(grid[i - 1][j], grid[i][j - 1]) + grid[i][j];
                }
            }
        }
        return grid[m - 1][n - 1];
    }

    /**
     * 给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。
     * 如果数组中不存在目标值 target，返回 [-1, -1]。
     * 进阶：
     * 你可以设计并实现时间复杂度为 O(log n) 的算法解决此问题吗？
     * 示例 1：
     * 输入：nums = [5,7,7,8,8,10], target = 8
     * 输出：[3,4]
     * 示例 2：
     * 输入：nums = [5,7,7,8,8,10], target = 6
     * 输出：[-1,-1]
     * 示例 3：
     * 输入：nums = [], target = 0
     * 输出：[-1,-1]
     */
    public static int[] searchRange(int[] nums, int target) {
        int[] result = {-1, -1};
        if (nums.length == 0) {
            return result;
        }
        int start = 0;
        int end = nums.length;
        int mid = (start + end) / 2;
        long tem = mid - 1;
        while (tem != mid) {
            tem = mid;
            if (nums[mid] > target) {
                end = mid;
            } else if (nums[mid] < target) {
                start = mid;
            } else {
                int midf;
                int mida;
                int temmid;
                if (nums.length == 1) {
                    return new int[]{mid, mid};
                }
                temmid = mid;
                while ((temmid = temmid - 1) >= 0) {
                    if (nums[temmid] != nums[mid]) {
                        break;
                    }
                }
                midf = ++temmid;
                temmid = mid;
                while ((temmid = temmid + 1) < nums.length) {
                    if (nums[temmid] != nums[mid]) {
                        break;
                    }
                }
                mida = --temmid;
                return new int[]{midf, mida};
            }
            mid = (start + end) / 2;
        }
        return result;
    }

    /**
     * 统计所有小于非负整数 n 的质数的数量。
     * 示例 1：
     * 输入：n = 10
     * 输出：4
     * 解释：小于 10 的质数一共有 4 个, 它们是 2, 3, 5, 7 。
     * 示例 2：
     * 输入：n = 0
     * 输出：0
     * 示例 3：
     * 输入：n = 1
     * 输出：0
     */
    public static int countPrimes(int n) {
        if (n <= 2) {
            return 0;
        }
        if (n == 3) return 1;
        int num = 2;
        /*for (int i = 4; i <n; i++) {
            boolean k=false;
            int s=i/2;
            for (int j =2; j <=s ; j++) {
                if(i%j==0){
                    k=true;
                    break;
                }
            }
            if(!k){
                num++;
            }
        }*/
        for (int i = 4; i < n; i++) {
            int temNum = num;
            num = countCheckBack(i, 2, temNum);
        }
        return num;
    }

    /**
     * @return
     */
    public static int countCheckBack(int n, int tem, int num) {
        if (tem > n / 2) {
            num = num + 1;
            return num;
        }
        if (n % tem == 0) {
            return num;
        }
        return countCheckBack(n, ++tem, num);
    }

    /**
     * 给你一个用字符数组 tasks 表示的 CPU 需要执行的任务列表。其中每个字母表示一种不同种类的任务。任务可以以任意顺序执行，
     * 并且每个任务都可以在 1 个单位时间内执行完。在任何一个单位时间，CPU 可以完成一个任务，或者处于待命状态。
     * 然而，两个 相同种类 的任务之间必须有长度为整数 n 的冷却时间，因此至少有连续 n 个单位时间内 CPU 在执行不同的任务，或者在待命状态。
     * 你需要计算完成所有任务所需要的 最短时间 。
     * 示例 1：
     * 输入：tasks = ["A","A","A","B","B","B"], n = 2
     * 输出：8
     * 解释：A -> B -> (待命) -> A -> B -> (待命) -> A -> B
     * 在本示例中，两个相同类型任务之间必须间隔长度为 n = 2 的冷却时间，而执行一个任务只需要一个单位时间，所以中间出现了（待命）状态。
     * 示例 2：
     * 输入：tasks = ["A","A","A","B","B","B"], n = 0
     * 输出：6
     * 解释：在这种情况下，任何大小为 6 的排列都可以满足要求，因为 n = 0
     * ["A","A","A","B","B","B"]
     * ["A","B","A","B","A","B"]
     * ["B","B","B","A","A","A"]
     * ...
     * 诸如此类
     * 示例 3：
     * 输入：tasks = ["A","A","A","A","A","A","B","C","D","E","F","G"], n = 2
     * 输出：16
     * 解释：一种可能的解决方案是：
     * A -> B -> C -> A -> D -> E -> A -> F -> G -> A -> (待命) -> (待命) -> A -> (待命) -> (待命) -> A
     */
    public static int leastInterval(char[] tasks, int n) {
        if (n == 0) {
            return tasks.length;
        }
        // char[] chars={'a' , 'b' , 'c' , 'd' , 'e' , 'f' , 'g' , 'h' , 'i' , 'j' , 'k' , 'l' , 'm' ,
        // 'n' ,'o' , 'p' , 'q' , 'r' , 's' , 't' , 'u' , 'v' , 'w' , 'x' , 'y' , 'z'};
        List<Object> list = new LinkedList<>();
        list = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        int[] ints = new int[26];
        for (char c : tasks) {
            char charTem = Character.toLowerCase(c);
            int tem = list.indexOf(charTem);
            System.out.println(tem + ":::IMG::" + charTem);
            ints[tem]++;
        }
        //总数
        List<Integer> listInt = new LinkedList<>();
        int max = 0;//最多数的字符
        for (Integer in : ints) {
            if (in != 0) {
                listInt.add(in);
            }
            max = Math.max(max, in);
        }
        listInt = listInt.stream().sorted().collect(Collectors.toList());
        //最终的list长度
        int end = 0;
        for (int i = 0; i < n; i++) {
            for (int s = 0; s < listInt.size(); s++) {
                for (int j = 0; j < listInt.get(s); j++) {
                    end++;
                }
            }
        }
        return end;
    }

    /**
     * 有一个二维矩阵 A 其中每个元素的值为 0 或 1 。
     * 移动是指选择任一行或列，并转换该行或列中的每一个值：将所有 0 都更改为 1，将所有 1 都更改为 0。
     * 在做出任意次数的移动后，将该矩阵的每一行都按照二进制数来解释，矩阵的得分就是这些数字的总和。
     * 返回尽可能高的分数。
     * 示例：
     * 输入：[[0,0,1,1],[1,0,1,0],[1,1,0,0]]
     * 输出：39
     * 解释：
     * 转换为 [[1,1,1,1],[1,0,0,1],[1,1,1,1]]
     * 0b1111 + 0b1001 + 0b1111 = 15 + 9 + 15 = 39
     */
    public static int matrixScore(int[][] A) {
        if (A.length == 0 || A[0].length == 0) {
            return 0;
        }
        int lenW = A.length;
        int lenN = A[0].length;
        int[][] AA = new int[lenW][lenN];
        AA = A;
        //1
        for (int i = 0; i < lenW; i++) {
            if (A[i][0] == 0) {
                changex(A, i, lenN);
            }
        }
        int max = merge(A, lenW, lenN);
        for (int i = 1; i < lenW && i < lenN; i++) {
            changey(A, i, lenW);
            int tem = merge(A, lenW, lenN);
            if (max > tem) {
                changey(A, i, lenW);
            }
            max = Math.max(tem, max);
        }
        //2
        for (int i = 0; i < lenW; i++) {
            changey(AA, 0, lenW);
        }
        for (int i = 0; i < lenW; i++) {
            if (AA[i][0] == 0) {
                changex(AA, i, lenN);
            }
        }
        max = Math.max(merge(AA, lenW, lenN), max);
        for (int i = 1; i < lenW && i < lenN; i++) {
            changey(AA, i, lenW);
            int tem = merge(AA, lenW, lenN);
            if (max > tem) {
                changey(AA, i, lenW);
            }
            max = Math.max(tem, max);
        }
        return max;
    }

    public static void changex(int[][] ints, int m, int nums) {
        for (int i = 0; i < nums; i++) {
            ints[m][i] = ints[m][i] == 0 ? 1 : 0;
        }
    }

    public static void changey(int[][] ints, int m, int nums) {
        for (int i = 0; i < nums; i++) {
            ints[i][m] = ints[i][m] == 0 ? 1 : 0;
        }
    }

    public static int merge(int[][] ints, int numy, int numx) {
        int eng = 0;
        for (int i = 0; i < numy; i++) {
            int tem = 0;
            for (int j = 0; j < numx; j++) {
                tem += ints[i][j] * Math.pow(2, j);
            }
            eng += tem;
        }
        return eng;
    }

    /**
     * 给定一个数字字符串 S，比如 S = "123456579"，我们可以将它分成斐波那契式的序列 [123, 456, 579]。
     * 形式上，斐波那契式序列是一个非负整数列表 F，且满足：
     * 0 <= F[i] <= 2^31 - 1，（也就是说，每个整数都符合 32 位有符号整数类型）；
     * F.length >= 3；
     * 对于所有的0 <= i < F.length - 2，都有 F[i] + F[i+1] = F[i+2] 成立。
     * 另外，请注意，将字符串拆分成小块时，每个块的数字一定不要以零开头，除非这个块是数字 0 本身。
     * 返回从 S 拆分出来的任意一组斐波那契式的序列块，如果不能拆分则返回 []。
     * 示例 1：
     * 输入："123456579"
     * 输出：[123,456,579]
     * 示例 2：
     * 输入: "11235813"
     * 输出: [1,1,2,3,5,8,13]
     * 示例 3：
     * 输入: "112358130"
     * 输出: []
     * 解释: 这项任务无法完成。
     */
    public static List<Integer> splitIntoFibonacci(String S) {
        int len = S.length();
        List<Integer> list = new LinkedList<>();
        StringBuilder stringBuilder = null;
        for (int i = 1; i <= len / 3 + 1; i++) {
            long zero = Long.parseLong(S.substring(0, i));
            if (zero >= Integer.MAX_VALUE) {
                return Collections.emptyList();
            }
            for (int j = i; j <= i + len / 3; j++) {
                stringBuilder = new StringBuilder("");
                list = new LinkedList<>();
                stringBuilder.append(zero);
                list.add(Integer.parseInt(zero + ""));
                long one = Long.parseLong(S.substring(i, j + 1));
                if (one >= Integer.MAX_VALUE) {
                    return Collections.emptyList();
                }
                list.add(Integer.parseInt(one + ""));
                stringBuilder.append(one);
                while (stringBuilder.toString().length() < len) {
                    long tem = zero + one;
                    if (tem >= Integer.MAX_VALUE) {
                        return Collections.emptyList();
                    }
                    stringBuilder.append(tem);
                    list.add(Integer.parseInt(tem + ""));
                    zero = one;
                    one = tem;
                }
                if (stringBuilder.toString().equals(S)) {
                    return list;
                }
                zero = Long.parseLong(S.substring(0, i));
            }
        }
        if (!S.equals(stringBuilder.toString())) {
            return Collections.emptyList();
        }
        return list;
    }

    /**
     * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
     * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
     * 问总共有多少条不同的路径？
     * 输入：m = 3, n = 7
     * 输出：28
     * 示例 2：
     * 输入：m = 3, n = 2
     * 输出：3
     * 解释：
     * 从左上角开始，总共有 3 条路径可以到达右下角。
     * 1. 向右 -> 向右 -> 向下
     * 2. 向右 -> 向下 -> 向右
     * 3. 向下 -> 向右 -> 向右
     */
    public static int uniquePaths(int m, int n) {
        int end = 0;
        return end;
    }

    /**
     * 在柠檬水摊上，每一杯柠檬水的售价为 5 美元。
     * 顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。
     * 每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。
     * 注意，一开始你手头没有任何零钱。
     * 如果你能给每位顾客正确找零，返回 true ，否则返回 false 。
     * 示例 1：
     * 输入：[5,5,5,10,20]
     * 输出：true
     * 解释：
     * 前 3 位顾客那里，我们按顺序收取 3 张 5 美元的钞票。
     * 第 4 位顾客那里，我们收取一张 10 美元的钞票，并返还 5 美元。
     * 第 5 位顾客那里，我们找还一张 10 美元的钞票和一张 5 美元的钞票。
     * 由于所有客户都得到了正确的找零，所以我们输出 true。
     * 示例 2：
     * 输入：[5,5,10]
     * 输出：true
     * 示例 3：
     * 输入：[10,10]
     * 输出：false
     * 示例 4：
     * 输入：[5,5,10,10,20]
     * 输出：false
     * 解释：
     * 前 2 位顾客那里，我们按顺序收取 2 张 5 美元的钞票。
     * 对于接下来的 2 位顾客，我们收取一张 10 美元的钞票，然后返还 5 美元。
     * 对于最后一位顾客，我们无法退回 15 美元，因为我们现在只有两张 10 美元的钞票。
     * 由于不是每位顾客都得到了正确的找零，所以答案是 false。
     * 提示：
     * 0 <= bills.length <= 10000
     * bills[i] 不是 5 就是 10 或是 20 
     */
    public static boolean lemonadeChange(int[] bills) {
        int len = bills.length;
        int[] ints = new int[2];
        boolean judge = false;
        for (int i = 0; i < len; i++) {
            int cions = bills[i];
            switch (cions) {
                case 5:
                    ints[0]++;
                    break;
                case 10:
                    ints[0]--;
                    ints[1]++;
                    if (ints[0] < 0) {
                        judge = true;
                    }
                    break;
                case 20:
                    if (ints[1] > 0) {
                        ints[1]--;
                        ints[0]--;
                    } else {
                        ints[0] -= 3;
                    }
                    if (ints[0] < 0 || ints[1] < 0) {
                        judge = true;
                    }
                    break;
                default:
                    break;
            }
            if (judge) {
                return false;
            }
        }
        return true;
    }

    /**
     * Dota2 的世界里有两个阵营：Radiant(天辉)和 Dire(夜魇)
     * Dota2 参议院由来自两派的参议员组成。现在参议院希望对一个 Dota2 游戏里的改变作出决定。他们以一个基于轮为过程的投票进行。在每一轮中，每一位参议员都可以行使两项权利中的一项：
     * 禁止一名参议员的权利：
     * 参议员可以让另一位参议员在这一轮和随后的几轮中丧失所有的权利。
     * 宣布胜利：如果参议员发现有权利投票的参议员都是同一个阵营的，他可以宣布胜利并决定在游戏中的有关变化。
     * 给定一个字符串代表每个参议员的阵营。字母 “R” 和 “D” 分别代表了 Radiant（天辉）和 Dire（夜魇）。然后，如果有 n 个参议员，给定字符串的大小将是 n。
     * 以轮为基础的过程从给定顺序的第一个参议员开始到最后一个参议员结束。这一过程将持续到投票结束。所有失去权利的参议员将在过程中被跳过。
     * 假设每一位参议员都足够聪明，会为自己的政党做出最好的策略，你需要预测哪一方最终会宣布胜利并在 Dota2 游戏中决定改变。输出应该是 Radiant 或 Dire。
     * 示例 1：
     * 输入："RD"
     * 输出："Radiant"
     * 解释：第一个参议员来自 Radiant 阵营并且他可以使用第一项权利让第二个参议员失去权力，因此第二个参议员将被跳过因为他没有任何权利。然后在第二轮的时候，第一个参议员可以宣布胜利，因为他是唯一一个有投票权的人
     * 示例 2：
     * 输入："RDD"
     * 输出："Dire"
     * 解释：
     * 第一轮中,第一个来自 Radiant 阵营的参议员可以使用第一项权利禁止第二个参议员的权利
     * 第二个来自 Dire 阵营的参议员会被跳过因为他的权利被禁止
     * 第三个来自 Dire 阵营的参议员可以使用他的第一项权利禁止第一个参议员的权利
     * 因此在第二轮只剩下第三个参议员拥有投票的权利,于是他可以宣布胜利
     * 提示：
     * 给定字符串的长度在 [1, 10,000] 之间.
     */
    public static String predictPartyVictory(String senate) {
        String m = getPeople(senate, senate, 0, senate);
        if (m.contains("D")) {
            return "Dire";
        } else {
            return "Radiant";
        }
    }

    public static String getPeople(String people, String result, int index, String yuan) {
        if (people.indexOf("D") < 0 || people.indexOf("R") < 0) {
            return result;
        }
        char c = people.charAt(index);
        StringBuilder tem = new StringBuilder(people.substring(0, index + 1));
        if (c == 'D') {
            for (int ma = index + 1; ma != index; ) {
                if (people.charAt(ma) == 'R') {
                    tem.append(people.substring(ma + 1, people.length()));
                    break;
                } else {
                    tem.append("D");
                }
                ma = ma >= people.length() ? 0 : ma + 1;
            }
        } else {
            for (int ma = index + 1; ma != index; ) {
                if (people.charAt(ma) == 'D') {
                    tem.append(people.substring(ma + 1, people.length()));
                    break;
                } else {
                    tem.append("R");
                }
                ma = ma >= people.length() ? 0 : ma + 1;
            }
        }
        people = tem.toString();
        index++;
        if (index >= people.length()) {
            index = 0;
        }
        result = people;
        return getPeople(result, result, index, yuan);
    }

    /**
     * 如果连续数字之间的差严格地在正数和负数之间交替，则数字序列称为摆动序列。第一个差（如果存在的话）可能是正数或负数。少于两个元素的序列也是摆动序列。
     * 例如， [1,7,4,9,2,5] 是一个摆动序列，因为差值 (6,-3,5,-7,3) 是正负交替出现的。相反, [1,4,7,2,5] 和 [1,7,4,5,5] 不是摆动序列，第一个序列是因为它的前两个差值都是正数，第二个序列是因为它的最后一个差值为零。
     * 给定一个整数序列，返回作为摆动序列的最长子序列的长度。 通过从原始序列中删除一些（也可以不删除）元素来获得子序列，剩下的元素保持其原始顺序。
     * 示例 1:
     * 输入: [1,7,4,9,2,5]
     * 输出: 6
     * 解释: 整个序列均为摆动序列。
     * 示例 2:
     * 输入: [1,17,5,10,13,15,10,5,16,8]
     * 输出: 7
     * 解释: 这个序列包含几个长度为 7 摆动序列，其中一个可为[1,17,10,13,10,16,8]。
     * 示例 3:
     * 输入: [1,2,3,4,5,6,7,8,9]
     * 输出: 2
     * 进阶:
     * 你能否用 O(n) 时间复杂度完成此题?
     */
    public static int wiggleMaxLength(int[] nums) {
        return 0;
    }

    public static int getMax(int[][] arr) {
        //累计和max[i+1][j+1]=max的max[i][j]+max[i][j+1]+arr[i][j];
        int aLen = arr.length;
        int yLen = arr[0].length;
        int[][] max = new int[aLen][yLen];
        for (int i = 1; i < aLen; i++) {
            for (int j = 1; j < yLen; j++) {
                max[i + 1][j + 1] = Math.max(max[i][j], max[i][j + 1]) + arr[i][j];
            }
        }
        return max[1][1];
    }

    /**
     * 移动机器人
     */
    public static int getMoveOn(int[][] arr) {
        //定义一个移动数组和
        int lenW = arr.length;
        int lenY = arr[0].length;
        int[][] len = new int[lenW + 1][lenY + 1];
        len[0][0] = 2;
        for (int i = 0; i < lenW; i++) {
            for (int j = 0; j < lenY; j++) {
                if (i == 0 && j != 0) {
                    len[i][j] = 1;
                } else if ((i != 0 && j == 0)) {
                    len[i][j] = 1;
                } else if (i > 0) {
                    len[i][j] = len[i - 1][j] + len[i][j - 1];
                }
            }
        }
        return len[lenW - 1][lenY - 1];
    }
}
