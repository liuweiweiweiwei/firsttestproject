package com.test.models.agent.dbsource;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:36
 * @description
 */
public class DynaticDataSourceEntity {

    private static final String DEFAULT_DB = null;
    private final static ThreadLocal<String> db = new ThreadLocal<>();

    public DynaticDataSourceEntity() { }

    public static String dbGet(){
        return db.get();
    }
    public static void set(String dbInfo){
        db.set(dbInfo);
    }
    public static void set(int dbInfo){
        db.set("DB_" + dbInfo);
    }
    public static void restore(){
        db.set(DEFAULT_DB);
    }
}
