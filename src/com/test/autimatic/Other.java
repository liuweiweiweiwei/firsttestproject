package com.test.autimatic;

import java.util.Arrays;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/24 20:48
 * @description
 */
public class Other {


    public static void main(String[] args) {
        int[] arr = {3,2,8,7,5,9,0,1,4,6,5,6,1,1,1,2,6,4,3,2};//Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        int middle = getMiddle(arr, 12);
        System.out.println(middle);
    }

    /**
     * 查询第k个位置的数
     */
    private static int getMiddle(int[] arr,int k){
        //定义初试结束位置
        int start = 0;int end = arr.length - 1;
        while (start < end){
            int j = prepare(arr,start,end);
            if(j == k){
                return arr[j];
            }else if(j < k){
                start = j + 1;
            }else{
                end = j - 1;
            }
        }
        return arr[k];
    }

    private static int prepare(int[] arr,int i,int j){
        //将数组切分
        int start = i;int end = j;
        int temp = arr[start];
        while (start < end){
            while (arr[start] <= temp){//小于左侧值的时候
                start++;
            }
            while (arr[end] > temp){//大于右侧值的时候
                end--;
            }
            if(start >= end)break;//如果开始值大于结束值说明后边的都比标准值达可以停止
            swap(arr, start, end);
        }
        swap(arr,end,i);//交换确定位置的数据结束值
        return end;
    }

    private static void swap(int[] arr,int s,int e){
        int temp = arr[s];
        arr[s] = arr[e];
        arr[e] = temp;
    }
}
