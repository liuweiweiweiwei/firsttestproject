package com.test.autimatic.week;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/10 10:32
 * @description
 */
public class leet0410 {
    public static void main(String[] args) {
        System.out.println(largestInteger(4173));
        System.out.println(minimizeResult("5+22"));
        System.out.println(maximumProduct(new int[]{24,5,64,53,26,38}, 54));
    }

    public static int largestInteger(int num) {
        String result = String.valueOf(num);
        char[] chars = result.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] % 2 == chars[j] % 2 && chars[i] < chars[j]) {
                    char temp = chars[i];
                    chars[i] = chars[j];
                    chars[j] = temp;
                }
            }
        }
        return Integer.parseInt(String.valueOf(chars));
    }

    /**
     * add 括號
     *
     * @param expression e
     * @return min
     */
    public static String minimizeResult(String expression) {
        String[] split = expression.split("\\+");
        long start = Integer.parseInt(split[0]) + Integer.parseInt(split[1]);
        String result = '(' + expression + ')';
        for (int i = 0; i < split[0].length(); i++) {
            int leftNum = i == 0 ? 0 : Integer.parseInt(split[0].substring(0, i));
            int leftYu = Integer.parseInt(split[0].substring(i));
            for (int j = 1; j <= split[1].length(); j++) {
                int rightLeftNum = Integer.parseInt(split[1].substring(0, j));
                int rightYuNum = j == split[1].length() ? 0 : Integer.parseInt(split[1].substring(j));
                long cur = (leftYu + rightLeftNum);
                if (leftNum != 0){ cur *= leftNum;}
                if (rightYuNum != 0) { cur *= rightYuNum; }
                if (cur < start) {
                    start = cur;
                    result = String.valueOf(leftNum == 0 ? "" : leftNum) + '(' + leftYu + '+' + rightLeftNum + ')'
                            + (rightYuNum == 0 ? "" : rightYuNum);
                }
            }
        }
        return result;
    }

    /**
     * 累加
     * @param nums arr
     * @param k l
     * @return max
     */
    public static int maximumProduct(int[] nums, int k) {
        int mode = (int)Math.pow(10,9) + 7;
        int result = 1;
        TreeMap<Integer, Integer> treeMap = new TreeMap<>(Comparator.comparingInt(o -> o));
        int max = 1;
        Arrays.sort(nums);
        for (int i:nums) {
            int calNum = treeMap.getOrDefault(i, 0);
            treeMap.put(i, calNum + 1);
            max = Math.max(max, i);
        }
        int[] keys = new int[treeMap.size()];
        int in = 0;
        for (int kt : treeMap.keySet()) {
            keys[in] = kt;
            in++;
        }
        if (k < treeMap.getOrDefault(0, -1)) { return 0; }
        for (int i = 0; i < keys.length; i++) {
            Integer valNum = treeMap.get(keys[i]);
            int tempResult = 1;
            if (k >= valNum) {
                if (i == keys.length - 1 || ((keys[i + 1] - keys[i]) * valNum >= k)) {
                    int increa = k / valNum;
                    int yu = k % valNum;
                    tempResult = add(result, keys[i] + increa, valNum - yu, mode);
                    result = add(tempResult, keys[i] + increa + 1, yu, mode);
                    k = 0;
                } else {
                    k -= ((keys[i + 1] - keys[i]) * valNum);
                    treeMap.put(keys[i + 1], treeMap.getOrDefault(keys[i + 1], 0) + valNum);
                }
            }else {
                if (k > 0) { tempResult = add(result, keys[i] + 1, k, mode); }else {tempResult = result;}
                result = add(tempResult, keys[i], valNum - k, mode);
                k = 0;
            }
        }
        return result;
    }

    /**
     * 相乘計算
     * @return num
     */
    private static int add(int res, int cur, int num, int mod) {
        int curRes = res % mod;
        long kCur = 1;
        while (num-- > 0) {
            kCur = kCur * cur % mod;
        }
        return  (curRes * ((int)kCur % mod )) % mod;
    }

    public int maximumProduct1(int[] nums, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        for (int i:nums) {
            priorityQueue.offer(i);
        }
        while (k-- > 0) {
            Integer poll = priorityQueue.poll();
            priorityQueue.offer(poll + 1);
        }
        long result = 1;
        while (!priorityQueue.isEmpty()) {
            Integer poll = priorityQueue.poll();
            result = (result * poll) % 1000000007;
        }
        return (int)result;
    }
}
