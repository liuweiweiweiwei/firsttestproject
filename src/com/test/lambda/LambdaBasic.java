package com.test.lambda;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

/**
 * lambda表达式基础版
 */
public class LambdaBasic {
    private Integer integer;

    public LambdaBasic(){
        System.out.println("测试四：调用工厂方法！");
    }

    public static void main(String[] args) {
        getLambda();
        getLambdaBasic();
        List<Integer> list=Arrays.asList(12,20,13,15,8);
    }

    /**
     * basic
     */
    public static void getLambda(){
        try {
            //1.
            File file= Paths.get("E://fileTest//files//").toFile();
            File[] files=file.listFiles(fi->fi.getName().endsWith(".txt"));
            assert files != null;
            System.out.println(Arrays.asList(files).toString());
            //2.
            ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(1,2,3, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(2),new ThreadPoolExecutor.AbortPolicy());
            final String msg="这是tom！！！";
            threadPoolExecutor.submit(()-> System.out.println(msg));
            //3.
            String[] msgs=new String[]{"hello","world"};
            msgs[0]="hi";
            threadPoolExecutor.submit(()-> System.out.println(msgs[0]));
            threadPoolExecutor.shutdown();
            //4.
            Comparator<File> fileComparator= Comparator.comparing(File::getName);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * java8底层
     */
    public static void getLambdaBasic(){
        try {
            List<Integer> integerList=Arrays.asList(12,14,2,5,8,3,10,12,11,6,7,9,8,10,22);
            //1.filter里运用predictare -test测试是否满足条件
            Predicate<Integer> predicate=da->da>10;
            List<Integer> end=new LinkedList<>();
            integerList.forEach(integer -> {
                if(predicate.test(integer)){
                    end.add(integer);
                }
            });
            System.out.println("测试一："+end.toString());
            //2.function 类型转换 R apply(T)
            Function<Integer,String> function=lo->(lo+12)+"末尾";
            List<String> stringList=new LinkedList<>();
            integerList.forEach(da->{
                stringList.add(function.apply(da));
            });
            System.out.println("测试二："+stringList.toString());
            //3.consumer- void Accept(T)消费者
            Consumer<Integer> consumer=da-> System.out.print(da+",");
            System.out.print("测试三：");
            integerList.forEach(integer -> consumer.accept(integer));
            System.out.println("");
            //4.supplier -T get()工厂方法???
            Supplier<LambdaBasic> integerSupplier=LambdaBasic::new;
            LambdaBasic lambdaBasic=integerSupplier.get();
            final Integer integer = lambdaBasic.integer;
            //5.UnaryOperator-T apply() 函数转换特例
            UnaryOperator<String> stringUnaryOperator=da->da+"t";
            List<String> stringList1=new LinkedList<>();
            integerList.forEach(da->{
                stringList1.add(stringUnaryOperator.apply(da+""));
            });
            System.out.println("测试五："+stringList1.toString());
            //6.BiFunction 接受俩参数函数转换- R apply(T,U) ---两参数类型转化
            BiFunction<Integer,Integer,String> biFunction=(d1,d2)->d1+d2+"多参数";
            List<String> stringList2=new LinkedList<>();
            integerList.forEach(d1->{
                stringList2.add(biFunction.apply(d1,d1));
            });
            System.out.println("测试六："+stringList2.toString());
            //7.BinaryOperator-接受俩参数 T apply(T,T)
            BinaryOperator<Integer> binaryOperator=(d1,d2)->d1+d2;
            List<Integer> stringList3=new LinkedList<>();
            integerList.forEach(d1->{
                stringList3.add(binaryOperator.apply(d1,d1));
            });
            System.out.println("测试七："+stringList3.toString());
            //8.BiConsumer-消费者俩参数 void accept(R,T)--使用
            System.out.print("测试八：");
            BiConsumer<Integer,String> biConsumer=(t1,u1)-> System.out.print(t1+u1);
            integerList.forEach(d1->{
                biConsumer.accept(d1,d1+"a，");
            });
            //9.BiPredicate-谓词过滤条件俩参数-Boolean test();
            System.out.println("");
            BiPredicate<Integer,Integer> biPredicate=(t1,t2)->t1>t2;
            List<Integer> integerList1=new LinkedList<>();
            integerList.forEach(d1->{
                if(biPredicate.test(d1,8)){
                    integerList1.add(d1);
                }
            });
            System.out.println("测试九："+integerList1.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
