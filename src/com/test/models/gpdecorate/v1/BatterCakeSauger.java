package com.test.models.gpdecorate.v1;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:18
 * @description
 */
public class BatterCakeSauger extends BatterCakeEgg {

    @Override
    protected String name() {
        return "加了个香肠的煎饼！";
    }

    @Override
    protected int price() {
        System.out.println("捡了个香肠");
        return super.price() + 2;
    }
}
