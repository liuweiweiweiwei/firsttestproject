package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 11:18
 * @description
 */
public class JavaNode implements INode {
    @Override
    public void operateNode() {
        System.out.println("java node 的笔记操作！");
    }
}
