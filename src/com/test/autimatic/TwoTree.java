package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/17 10:28
 * @description
 */
public class TwoTree {

    public static void main(String[] args) {
        /**
         *          1
         *        81  2
         *      115 23  15
         *    10
         *
         *          30
         *       16     81
         *     15  23
         *       22 25
         */
        TwoTree tree = new TwoTree();
        TreeNode<Integer> treeNode = new TreeNode<Integer>(30);
        TreeNode<Integer> treeNode1 = new TreeNode<Integer>(16);
        TreeNode<Integer> treeNode2 = new TreeNode<Integer>(81);
        TreeNode<Integer> treeNode3 = new TreeNode<Integer>(15);
        TreeNode<Integer> treeNode4 = new TreeNode<Integer>(23);
        TreeNode<Integer> treeNode5 = new TreeNode<Integer>(25);
        TreeNode<Integer> treeNode6 = new TreeNode<Integer>(22);
        treeNode.left = treeNode1;treeNode.right = treeNode2;
        treeNode1.left = treeNode3; treeNode1.right = treeNode4;
        treeNode4.right = treeNode5;treeNode4.left = treeNode6;
        tree.middleErgodic(treeNode);
        System.out.println("分隔符");
        tree.prefixErgodic(treeNode);
        System.out.println("分隔符");
        tree.tailErgodic(treeNode);
        System.out.println("分隔符");
        System.out.println(tree.fullTreeNodeNum(treeNode4));
        System.out.println(tree.isSimple(treeNode2,treeNode2));
        System.out.println(tree.judgeBTS(treeNode1,null,null));
        System.out.println(tree.isExist(treeNode3,23));
        System.out.println("分隔符");
        tree.tailErgodic(treeNode);
        System.out.println("分隔符");
        tree.tailErgodic(tree.delBST(treeNode,23));
        System.out.println("分隔符");
        System.out.println(tree.fullTreeNodeNum(treeNode));
        //employee
        System.out.println(createBinaryTree(new int[][]{{20,15,1},{20,17,0},{50,20,1},{50,80,0},{80,19,1}}));
    }

    /**
     * 二叉树的节点数
     * @param treeNode
     * @return
     */
    public int fullTreeNodeNum(TreeNode treeNode){
        TreeNode left = treeNode;TreeNode right = treeNode;
        //标记左右节点数是否相等做判断
        int hl = 0,hr = 0;
        while (left != null){
            hl++;
            left = left.left;
        }
        while (right != null){
            hr++;
            right = right.right;
        }
        //相等的时候返回2的次方数
        if(hl == hr) {
            return (int) Math.pow(2, hl) - 1;
        }
        //如果不相等则继续向下遍历
        return fullTreeNodeNum(treeNode.left)+fullTreeNodeNum(treeNode.right)+1;
    }

    /**
     * 计算深度
     * @param treeNode
     * @return 节点个数--全遍历
     */
    public int treeNodeNum(TreeNode treeNode){
        if(treeNode == null){return 0;}
        return 1 + treeNodeNum(treeNode.left) + treeNodeNum(treeNode.right);
    }

    /**
     * BST树删除操作
     * @param treeNode
     */
    public <T> TreeNode delBST(TreeNode treeNode,T t){
        if(treeNode == null){return treeNode;}
        int i = treeNode.data.compareTo(t);
        if(i == 0){
            //删除操作
            treeNode = delete(treeNode);
        }else if(i < 0){
            treeNode.right = delBST(treeNode.right,t);
        }else {
            treeNode.left = delBST(treeNode.left,t);
        }
        return treeNode;
    }

    private TreeNode delete(TreeNode rootDel){
        //如果左右节点都为空，直接删除
        if(rootDel.left == null && rootDel.right == null){ return null; }
        //如果左右节点有一个不为空
        if(rootDel.left == null){return rootDel.right;}
        if(rootDel.right == null){return rootDel.left;}
        //如果均不为空--可以查找左子树最大值或右子树最小值：
        TreeNode temp = rootDel.left;
        while (temp.right != null){
            temp = temp.right;
        }
        TreeNode treeNode = new TreeNode(temp.data);
        treeNode.left = delBST(rootDel.left,temp.data);
        treeNode.right = rootDel.right;
        return treeNode;
    }

    /**
     * 是否存在
     * @param treeNode
     * @param target
     * @return 变形-儿茶
     */
    public boolean isExist(TreeNode treeNode,int target){
        if(treeNode == null){ return false; }
        int ju = treeNode.data.compareTo(target);
            //比较相等时候处理流程
        if(ju == 0){
            return true;
            //比目标值小的向右子树查找
        }else if(ju < 0){
            return isExist(treeNode.right,target);
            //比目标值大的向左子树查找
        }else{
            return isExist(treeNode.left,target);
        }
    }




    /**
     * 判断BST树
     *          5
     *       3     8
     *     2     7    9
     * @param root 父节点
     * @param min 最小值
     * @param max 最大值
     * @return 是否为BST
     */
    public boolean judgeBTS(TreeNode root,TreeNode min,TreeNode max){
        if(root==null){return true;}
        //比较左子树数值的大小
        if(min != null && root.data.compareTo(min.data)<0){
            return false;
        }
        //比较数据值的大小
        if(max != null && root.data.compareTo(max.data)>0){
            return false;
        }
        //循环判断左右子树节点数据
        return judgeBTS(root.left,min,root)&&judgeBTS(root.right,root,max);
    }

    /**
     * 两个树是否相同
     * @param treeNodeA
     * @param treeNodeB
     * @return
     */
    public boolean isSimple(TreeNode treeNodeA,TreeNode treeNodeB){
        //如果均为空，则相等
        if(treeNodeA == null && treeNodeB == null){
            return true;
        }
        //如果有一个不为空则不相等
        if(treeNodeA == null || treeNodeB == null){
            return false;
        }
        //如果两个数值不相等，则不相等
        if(treeNodeA.data != treeNodeB.data){
            return false;
        }
        //递归左右子节点
        return isSimple(treeNodeA.left,treeNodeB.left)&&isSimple(treeNodeA.right,treeNodeB.right);
    }


    /**
     * 前序遍历
     * @param treeNode
     */
    public void prefixErgodic(TreeNode treeNode){
        if(treeNode == null){
            return;
        }
        //前序遍历操作
        System.out.print(treeNode.data+",");
        //分别遍历左右节点
        prefixErgodic(treeNode.left);
        prefixErgodic(treeNode.right);
    }

    /**
     * 后序遍历
     * @param treeNode
     */
    public void tailErgodic(TreeNode treeNode){
        if(treeNode == null){
            return;
        }
        tailErgodic(treeNode.left);
        tailErgodic(treeNode.right);
        //后序遍历操作
        System.out.print(treeNode.data+",");
    }

    /**
     * 中序遍历
     * @param treeNode
     */
    public void middleErgodic(TreeNode treeNode){
        if(treeNode == null){
            return;
        }
        middleErgodic(treeNode.left);
        //中序遍历操作
        System.out.print(treeNode.data+",");
        middleErgodic(treeNode.right);
    }

    /**
     * 二叉树
     * @param <T>
     */
    public static class TreeNode<T extends Comparable<T>>{
        private T data;
        private TreeNode left;
        private TreeNode right;

        public TreeNode() {
        }

        public TreeNode(T data) {
            this.data = data;
        }

        public TreeNode(TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
        }

        public TreeNode(T data, TreeNode left, TreeNode right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }



    public static TreeNode createBinaryTree(int[][] descriptions) {
        Map<Integer,int[]> tree = new HashMap<>();
        Set<Integer> set = new HashSet<>();
            for (int[] desc : descriptions) {
            int[] treeOrDefault = tree.getOrDefault(desc[0],new int[2]);
            if (desc[2] == 0) {
                treeOrDefault[0] = desc[1];
            }else {
                treeOrDefault[1] = desc[1];
            }
            tree.put(desc[0], treeOrDefault);
            set.add(desc[1]);
        }
        int root = 0;
            for (int k :tree.keySet()) {
            if (!set.contains(k)) {
                root = k;
            }
        }
        TreeNode treeNode = new TreeNode(root);
        getTreeNode(treeNode, tree, root);
        return treeNode;
    }

    private static TreeNode getTreeNode(TreeNode root, Map<Integer,int[]> map, int key) {
        int[] ints = map.get(key);
        if(ints != null) {
            TreeNode left = new TreeNode(ints[1]);
            TreeNode right = new TreeNode(ints[0]);
            if (ints[1] != 0){
                root.left = left;
                getTreeNode(root.left, map, ints[1]);
            }
            if (ints[0] != 0) {
                root.right = right;
                getTreeNode(root.right, map, ints[0]);
            }
        }
        return root;
    }




    public static class Info{
        public Integer distinct;
        public Integer height;

        public Info(Integer distinct, Integer height) {
            this.distinct = distinct;
            this.height = height;
        }
        public Info() { }
    }

    /**
     * 计算树之间最大距离
     * @param treeNode
     * @return
     */
    public static Info getDisHeight(TreeNode<Integer> treeNode){
        if(treeNode == null){
            return new Info(0,0);
        }
        Info left = getDisHeight(treeNode.left);
        Info right = getDisHeight(treeNode.right);
        //计算最大距离
        int d1 = left.distinct;
        int d2 = right.distinct;
        int d3 = left.height + right.height + 1;
        int d = Math.max(d1,Math.max(d2,d3));
        int h = Math.max(left.height, right.height) + 1;
        return new Info(d,h);
    }

    public static class Employee{
        public int happy;
        List<Employee> emplyeeList;

        public Employee(int happy, List<Employee> emplyeeList) {
            this.happy = happy;
            this.emplyeeList = emplyeeList;
        }

        public Employee() {
        }
    }

    public static class EmployeeInfo{
        private int lai;
        private int bu;

        public EmployeeInfo(int lai, int bu) {
            this.lai = lai;
            this.bu = bu;
        }
    }

    /**
     * 没毛病
     * @param emplyee
     * @return
     */
    public static EmployeeInfo maxHappy(Employee emplyee){
        if(emplyee.emplyeeList.isEmpty()){
            return new EmployeeInfo(emplyee.happy,0);
        }
        int lai = emplyee.happy;
        int bu = 0;
        for (Employee e:emplyee.emplyeeList) {
            EmployeeInfo ee = maxHappy(e);
            lai += ee.bu;
            bu += Math.max(ee.bu,ee.lai);
        }
        return new EmployeeInfo(lai,bu);
    }

}
