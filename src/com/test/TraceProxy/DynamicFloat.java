package com.test.TraceProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicFloat {
    interface IService{
        void sayHello();
        String getName();
    }
    static class IserviceAImpl implements IService{
        @Override
        public void sayHello() {
            System.out.println("hi!A");
        }

        @Override
        public String getName() {
            return "serviceNameA";
        }
    }
    static class IserviceBImpl implements IService{
        @Override
        public void sayHello() {
            System.out.println("hello ! B");
        }
        @Override
        public String getName() {
            return "serviceNameB";
        }
    }

    /**
     *
     */
    static class SimpleDynamicHander implements InvocationHandler{
        private Object object;

        public SimpleDynamicHander(Object obj){
            this.object=obj;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("InvocationHandler enter start with:"+object.getClass().getSimpleName()+"::"+method.getName());
            Object result = method.invoke(object,args);
            System.out.println("InvocationHandler leave end with:"+object.getClass().getSimpleName()+"::"+method.getName());
            return result;
        }
    }

    /*
     * 通过Proxy的newProxyInstance方法来创建我们的代理对象，我们来看看其三个参数
     * 第一个参数 handler.getClass().getClassLoader() ，我们这里使用handler这个类的ClassLoader对象来加载我们的代理对象
     * 第二个参数realSubject.getClass().getInterfaces()，我们这里为代理对象提供的接口是真实对象所实行的接口，
     * 表示我要代理的是该真实对象，这样我就能调用这组接口中的方法了
     * 第三个参数handler， 我们这里将这个代理对象关联到了上方的 InvocationHandler 这个对象上
     */
    private static <T> T getProxy(Class<?> classes,T realObj){
        return (T) Proxy.newProxyInstance(
                classes.getClassLoader(),
                new Class[]{classes},
                new SimpleDynamicHander(realObj));
    }

    public static void main(String[] args) {
        IService a1= getProxy(IService.class,new IserviceAImpl());
        a1.sayHello();
        System.out.println(a1.getName());
        IService b1 = getProxy(IService.class, new IserviceBImpl());
        b1.sayHello();
        System.out.println("=======");
        IService a=new IserviceAImpl();
        a.sayHello();
        IService b=new IserviceBImpl();
        b.sayHello();
    }

}
