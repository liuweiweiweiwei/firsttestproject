package com.test.TwnatyFoyur;

public class ClassLoaderAnaly {
    public static void main(String[] args) {
        ClassLoader classLoader=ClassLoaderAnaly.class.getClassLoader();
        while (classLoader!=null){
            System.out.println(classLoader.getClass().getName());
            classLoader=classLoader.getParent();
        }
        System.out.println(String.class.getClassLoader());
        getInst();
    }
    //自定义加载顺序，网状加载顺序，父加载器为牌子加载器加载
    public static void getInst(){
        ClassLoader cls=ClassLoader.getSystemClassLoader();//系统加载器
        try {
            Class<?> cl=cls.loadClass("java.util.ArrayList");
            ClassLoader inst=cl.getClassLoader();
            System.out.println("加载器："+inst);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    static class CLtInitDemo{
        public static class sayHello{
            static {
                System.out.println("hi!boy");
            }
        }

        public static void main(String[] args) throws ClassNotFoundException {
            ClassLoader classLoader=ClassLoader.getSystemClassLoader();
            String className=CLtInitDemo.class.getName()+"$sayHello";
            //Class<?> clasinit=classLoader.loadClass(className);//不会加载静态代码块里的代码
            Class<?> clasLoad=Class.forName(className);//会加载静态代码块里的代码
        }
    }
}
