package com.test.models.Strategy.old;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 10:47
 * @description 策
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }
    public int getRealData(){
        return this.strategy.getData();
    }

    public static void main(String[] args) {
        int realDataA = new Context(new ContractStrategyA()).getRealData();
        int realDataB = new Context(new ContractStrategyB()).getRealData();
        System.out.println(realDataA+":"+realDataB);
        System.out.println(getMethod(new ContractStrategyB(),"getData"));
    }

    private static <T> String getMethod(T t,String method){
        Class<?> aClass = t.getClass();
        try {
            Method method1 = aClass.getMethod(method);
            Object invoke = method1.invoke(aClass.newInstance());
            return invoke+"";
        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
