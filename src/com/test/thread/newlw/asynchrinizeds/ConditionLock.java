package com.test.thread.newlw.asynchrinizeds;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/22 16:39
 * @description
 */
public class ConditionLock {

    public static class CondWait implements Runnable{
        private Condition condition;
        private Lock lock;

        public CondWait(Condition condition, Lock lock) {
            this.condition = condition;
            this.lock = lock;
        }

        @Override
        public void run() {
            try {
                lock.lock();
                System.out.println("CondWait is notify:......");
                TimeUnit.SECONDS.sleep(3);
                //唤醒阻塞的线程
                condition.signal();
                System.out.println("CondWait is single");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public static class CondNoti implements Runnable{

        private Condition condition;
        private Lock lock;

        public CondNoti(Condition condition, Lock lock) {
            this.condition = condition;
            this.lock = lock;
        }

        @Override
        public void run() {
            //枷锁
            lock.lock();
            try {//阻塞
                System.out.println("CondNoti is lock");
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
            System.out.println("CondNoti is over!");
        }
    }

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        new Thread(new CondNoti(condition,lock)).start();
        new Thread(new CondWait(condition,lock)).start();
    }
}
