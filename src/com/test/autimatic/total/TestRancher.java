package com.test.autimatic.total;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/1/8 19:27
 */
public class TestRancher {

    public static void main(String[] args) {
        System.out.println(rancher("aacabdkacaa"));
        System.out.println(rancher1("cbbd"));
        System.out.println("------kmp----------");
        System.out.println(getKmp("aasdcaasdxzb2jd", "aasdx"));
        System.out.println(kmpIndex("aasdcaasdxzb2jd", "aasdx"));
    }

    /**
     * 回文字符串
     * @param s
     */
    private static String rancher(String s){
        // 存储臂长的集合
        List<Integer> armList = new ArrayList<>(16);
        StringBuffer buffer = new StringBuffer();
        for (char c:s.toCharArray()) {
            buffer.append("#").append(c);
        }
        buffer.append("#");
        int index = -1;
        int len = -1;
        int start = -1;
        int end = -1;
        for (int i = 0; i < buffer.length(); i++) {
            int arm = 0;
            if(len >= i){
                arm = Math.min(armList.get(2 * index - i), len - i);
            }
            while (i - arm - 1 >= 0 && i + arm + 1 < buffer.length()
                    && buffer.charAt(i - arm - 1) == buffer.charAt(i + arm + 1)){arm++;}
            armList.add(arm);
            if(i + arm >= len){
                len = i + arm;
                index = i;
            }
            if((end - start) < (arm * 2 + 1)){
                start = i - arm;
                end = i + arm;
            }
            if((i + arm) == (buffer.length() - 1)){
                break;
            }
        }
        StringBuffer result = new StringBuffer();
        for (int i = start; i <= end; i++) {
            if(buffer.charAt(i) != '#'){
                result.append(buffer.charAt(i));
            }
        }
        return result.toString();
    }

    private static String rancher1(String str){
        boolean[][] judge = new boolean[str.length()][str.length()];
        for (int i = 0; i < judge.length; i++) {
            judge[i][i] = true;
        }
        int start = 0;int len = 0;
        for (int i = 1; i < str.length(); i++) {
            for (int j = 0; j < i; j++) {
                if(str.charAt(j) == str.charAt(i)) {
                    judge[j][i] = i - j < 3 || judge[j + 1][i - 1];
                }
                if(judge[j][i] && i - j > len){
                    start = j;
                    len = i - j;
                }
            }

        }

        return str.substring(start, start + len + 1);
    }

    private static int[][] KMP;

    private static void KMP(String s){
        KMP = new int[s.length()][26];
        int n = 0;
        for (int i = 0; i < s.length(); i++) {
            System.arraycopy(KMP[n], 0, KMP[i], 0, 'z' + 1 - 'a');
            n = KMP[i][s.charAt(i) - 'a'];
            KMP[i][s.charAt(i) - 'a'] = i + 1;
        }
    }

    private static int getKmp(String kmp, String target){
        KMP(target);
        int end = -1;
        int state = 0;
        for (int i = 0; i < kmp.length(); i++) {
            state = KMP[state][kmp.charAt(i) - 'a'];
            if(state == target.length()){
                return i - target.length() + 1;
            }
        }
        return end;
    }

    private static int[] kmpArr(String target){
        int[] kmp = new int[target.length()];
        kmp[0] = -1; kmp[1] = 0; int index = 2; int kmpIndex = 0;
        while (index < target.length()){
            if(target.substring(index - 1, index).equals(target.substring(kmpIndex, kmpIndex + 1))){
                kmp[index++] = ++kmpIndex;
            }else if(kmpIndex > 0){
                kmpIndex = kmp[kmpIndex];
            }else{
                kmp[index++] = 0;
            }
        }
        return kmp;
    }

    public static int kmpIndex(String total, String target){
        int[] kmpArr = kmpArr(target);
        int indexT = 0;
        int indexR = 0;
        while (indexT < total.length() && indexR < target.length()){
            if(total.substring(indexT, indexT + 1).equals(target.substring(indexR, indexR + 1))){
                indexR++;indexT++;
            }else if(kmpArr[indexR] == -1){
                indexT++;
            }else{
                indexR = kmpArr[indexR];
            }
        }
        return indexR == target.length() ? indexT - target.length() : -1;
    }

}
