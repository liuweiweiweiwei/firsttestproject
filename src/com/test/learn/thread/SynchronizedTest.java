package com.test.learn.thread;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/24 8:43
 * @description
 */
public class SynchronizedTest {

    private static double times;
    public void run(){

        //wait()和notify（）都是object的方法，必须搭配synchronized使用wait会释放锁，notify不会
        //线程A做a要做的事
        Thread threada=new Thread(()->{
           synchronized (this){
               for (times = 0; times <=5; times+=0.5) {
                   System.out.println("已经进行的次数是："+times+"活动继续；");
                   try {
                       Thread.sleep(1000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   if(times==2.5){
                       System.out.println("+++++++++++++++++++++++++++++++++");
                       this.notify();
                   }
               }
           }
        });

        //线程b继续做另一件事
        Thread threadb=new Thread(()->{
           while (true){
               synchronized (this){
                   try {
                       this.wait();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   System.out.println("进行另一个活动！");
               }
               break;
           }
        });

        //启动线程
        threadb.start();
        threada.start();
    }

    //测试
    public static void main(String[] args) {
        SynchronizedTest test=new SynchronizedTest();
        test.run();
    }
}
