package com.test.models.agent.proxy;


import com.test.models.agent.dynamicProxy.jdkProxy.Person;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:26
 * @description
 */
public class Son implements Person {

    public void find(){
        System.out.println("需要肤白貌美大长腿！");
    }

    @Override
    public void findLove() {
        System.out.println("小娃娃找对象！");
    }
}
