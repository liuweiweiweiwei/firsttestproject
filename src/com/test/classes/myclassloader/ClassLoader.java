package com.test.classes.myclassloader;

import com.test.MyLog.LwLog;

import java.util.Arrays;
import java.util.Locale;
import java.util.StringJoiner;

/**
 * @author 25338
 * @date 2022/5/12 21:55
 * @since 1.0
 * description
 */
public class ClassLoader {
    public static void main(String[] args) {
        ClassLoader classLoader = new ClassLoader();
        classLoader.testClassLoader();
        LwLog.logInfo("分隔符");
        classLoader.testExtClassLoader();
        LwLog.logInfo("分隔符");
    }

    /**
     * classloader
     */
    public void testClassLoader() {
        // 跟加载器获取不到
        LwLog.logInfo(String.format(Locale.ROOT, "bootstrapt classloader: %s", Object.class.getClassLoader()));
        // sun.boot.class.path 可获取跟加载器路径
        LwLog.logInfo(splitBySymbleToStr(System.getProperty("sun.boot.class.path"),";","\n"));
    }

    /**
     * ext class loader
     */
    public void testExtClassLoader() {
        //  扩展类
        LwLog.logInfo(ClassLoader.class.getClassLoader().toString());
        // ext
        LwLog.logInfo(splitBySymbleToStr(System.getProperty("java.ext.dirs"), ";", "\n"));
    }

    /**
     * 3. application class loader
     */
    public void classPathLoader() {
        // java.class.path
        LwLog.logInfo(System.getProperty("java.class.path"));
        // applicationclassloader
        LwLog.logInfo("applicationClassLoader");
    }

    /**
     * 分割字符串
     * @param param param
     * @param split aplit
     * @param interval insert
     * @return str
     */
    private String splitBySymbleToStr(String param, String split, String interval) {
        StringJoiner joiner = new StringJoiner(interval);
        String[] split1 = param.split(split);
        Arrays.stream(split1).forEach(joiner::add);
        return joiner.toString();
    }
}
