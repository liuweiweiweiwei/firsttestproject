package com.test.Task;

public class Chicken {
    private Integer age;
    private String name;
    private String result;

    public Chicken() {
    }

    public Chicken(Integer age, String name, String result) {
        this.age = age;
        this.name = name;
        this.result = result;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getResult() {
        return result;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
