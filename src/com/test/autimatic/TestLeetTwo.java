package com.test.autimatic;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 栈实现相关的操作
 */
public class TestLeetTwo {
    public static void main(String[] args) {
        int[] A = {0, 0, 0, 0, 0, 0};//,1,0,0,0
        int[] B = {0, 0, 0, 0, 0, 0};//,0,1,0,0
        int num = findLength(A, B);
        System.out.println("查找到匹配的两个数组相似长度为：" + num);
        //2.
        int[][] nums = new int[3][3];
        nums[0] = new int[]{0, 0, 0};
        nums[1] = new int[]{0, 1, 0};
        nums[2] = new int[]{0, 0, 0};
        int path = uniquePathsWithObstacles(nums);
        System.out.println("最终的路径数量为：" + path);
        //3.
        TreeNode treeNode = new TreeNode(5);
        TreeNode treeNode1l = treeNode.left = new TreeNode(4);
        TreeNode treeNode1r = treeNode.right = new TreeNode(8);
        TreeNode treeNode1 = treeNode1r.left = new TreeNode(13);
        TreeNode treeNode1rr = treeNode1r.right = new TreeNode(4);
        treeNode1rr.right = new TreeNode(1);
        TreeNode treeNode1ll = treeNode1l.left = new TreeNode(11);
        treeNode1ll.left = new TreeNode(7);
        treeNode1ll.right = new TreeNode(2);
        hasPathSum(treeNode, 22);
        //4.
        int shorts = 1;
        int longs = 2;
        int k = 3;
        int[] intArr = divingBoard(shorts, longs, k);
        System.out.println(Arrays.toString(intArr));

        //5.
        String[] strings={"looked","just","like","her","brother"};
        String sentence = "jesslookedjustliketimherbrother";
        List<String> list=new ArrayList<>();
        //tracBack(list,stringList);
        for (List<String> sl:listList) {
            System.out.println("------");
            for (String s:sl) {
                System.out.print(s+",");
            }
        }
        int min=respace(strings,sentence);
        System.out.println("获取到的最小数据为："+min);
        //6.
        int[] arr={1,3,5,7,2,4,6,8};
        int[] a=smallestK(arr,4);
        System.out.println("第六题数据为"+Arrays.toString(a));
    }
    private static List<List<String>> listList=new ArrayList<>();
    class CQueue {
        int val;
        CQueue next;
        CQueue head;
        CQueue tail;

        public CQueue() {

        }

        public CQueue(int val) {
            this.val = val;
        }

        public void appendTail(int value) {
            tail.next = new CQueue(val);
        }

        public int deleteHead() {
            return 0;
        }
    }

    /**
     * 获取数组的公共部分
     *
     * @param A 啊
     * @param B b
     * @return
     */
    public static int findLength(int[] A, int[] B) {
        int max = 0;
        int[][] judge = new int[A.length + 1][B.length + 1];
        judge[0][0] = 0;
        for (int i = 1; i < A.length + 1; i++) {
            for (int j = 1; j < B.length + 1; j++) {
                if (A[i - 1] == B[j - 1]) {
                    judge[i][j] = judge[i - 1][j - 1] + 1;
                    max = Math.max(judge[i][j], max);
                }
            }
        }
        /*for (int i = 0; i < A.length; i++) {
            int mid=0;
            int m=i;
            for (int j = 0,b=0; j <B.length ; j++) {
                if (m<A.length&&A[m] == B[j]) {
                    mid++;
                    m++;
                } else if (mid != 0 ) {
                    max = Math.max(max, mid);
                    mid = 0;
                    m = i;
                    if(A[m] == B[++b]){
                        j=b-1;
                    }else{
                        j--;
                    }
                }
                max = Math.max(max, mid);
            }
        }*/
        return max;
    }

    /**
     * 不同路径
     *
     * @param obstacleGrid 所有路径
     * @return 路径数量
     */
    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int path = 0;
        boolean[][] judge = new boolean[obstacleGrid.length + 1][obstacleGrid[0].length + 1];
        judge[0][0] = true;
        for (int i = 1; i < judge[0].length; i++) {
            judge[0][i] = true;
        }
        for (int i = 1; i < obstacleGrid.length + 1; i++) {
            for (int j = 1; j < obstacleGrid[0].length + 1; j++) {
                if (judge[i - 1][j - 1] && obstacleGrid[i - 1][j - 1] == 0) {
                    judge[i][j] = true;
                } else if (judge[obstacleGrid.length][obstacleGrid[0].length]) {
                    path++;
                } else if (obstacleGrid[i - 1][j - 1] == 1) {
                    break;
                }
            }
        }
        return path;
    }


    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    /**
     * 路径之和是否为固定值
     *
     * @param root
     * @param sum
     * @return
     */
    public static boolean hasPathSum(TreeNode root, int sum) {
        List<Integer> list = new LinkedList<>();
        int num = 0;
        tracBack(root, list, 0, root, root);
        System.out.println(list.toString());
        return false;
    }

    public static void tracBack(TreeNode treeNode, List<Integer> list, int num, TreeNode root, TreeNode laRoot) {
        if (treeNode.left == null && treeNode.right == null) {
            list.add(num);
            treeNode = root;
            return;
        }
        while (treeNode.left != null || treeNode.right != null) {
            laRoot = treeNode;
            if (treeNode.left != null) {
                treeNode = treeNode.left;
                num += treeNode.val;
            }
            tracBack(treeNode, list, num, root, laRoot);
            //treeNode=laRoot;
            if (treeNode.right != null) {
                treeNode = treeNode.right;
                num += treeNode.val;
            }
        }
    }

    /**
     * 查询用两个的最多数量
     */
    public static int[] divingBoard(int shorter, int longer, int k) {
        if (k == 0) {
            return new int[0];
        }
        int m = 0;//定义short出现的次数
        List<Integer> integerList = new ArrayList<>();
        while (m <= k) {
            integerList.add(shorter * m + longer * (k - m));
            m++;
        }
        List<Integer> objects = integerList.stream().distinct().collect(Collectors.toList());
        int[] numArr = new int[objects.size()];
        for (int i = 0; i < objects.size(); i++) {
            numArr[objects.size() - i - 1] = objects.get(i);
        }
        return numArr;
    }

    /**
     * 断开语句
     * @param dictionary 字典
     * @param sentence 句子
     * @return 未识别的字符数
     */
    public static int respace(String[] dictionary, String sentence) {
        List<String> stringList=new ArrayList<>();
        for (String str:dictionary) {
            if(sentence.contains(str)){
                stringList.add(str);
            }
        }
        List<String> list=new ArrayList<>();
        tracBack(list,stringList);
        int min=sentence.length();
        for (List<String> ls:listList) {
            String midStr=sentence;
            for (String str:ls) {
                if(midStr.contains(str)){
                    midStr=midStr.replaceAll(str,"");
                }
            }
           min=Math.min(min,midStr.length());
        }
        return min;
    }

    public static void tracBack(List<String> list,List<String> pre){
        if(pre.size()==list.size()){
            listList.add(new ArrayList<>(list));
            return;
        }
        for (String s:pre) {
            if(list.contains(s)){
                continue;
            }
            list.add(s);
            tracBack(list,pre);
            list.remove(list.size()-1);
        }
    }

    /**
     * 最小的k个数据？？？
     * @param arr 请求数组
     * @param k 最小的k个数据
     * @return k个数据的数组
     */
    public static int[] smallestK(int[] arr, int k) {
        if(k==0||arr.length==0){
            return new int[0];
        }
        int[] newArr=new int[k];
        for (int i = 0; i <k ; i++) {
            newArr[i]=arr[i];
        }

        for (int i = k; i <arr.length ; i++) {
            for (int j = 0; j <k ; j++) {
                if(arr[k]<newArr[j]){
                    int temp=newArr[j];
                    newArr[j]=arr[k];
                    arr[k]=temp;
                }
            }
        }
        return newArr;
    }

    /**
     * 最大利润
     * @param prices 价格数组
     * @return 最大利润
     */
    public int maxProfit(int[] prices) {
        if(prices==null||prices.length==0){
            return 0;
        }
        //利润有三种状态：0不持股，1持股，2冷冻期
        int[][] price=new int[prices.length][3];
        price[0][0]=0;  //卖出
        price[0][1]=-prices[0]; //持股
        price[0][2]=0;  //冷冻期
        for (int i = 1; i <prices.length ; i++) {
            //假设第i天不持股则他为i-1天
            price[i][0]=Math.max(price[i-1][0],price[i-1][2]);
            //
            price[i][1]=Math.max(price[i-1][1],price[i-1][0]-prices[i]);

            price[i][2]=price[i-1][1]+prices[i];
        }
        return Math.max(price[prices.length-1][0],price[prices.length-1][2]);
    }




}