package com.test.thread.newlw.net;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 15:51
 * @description xs序列化
 */
public class XStreamSericial implements IService {

    private XStream xStream = new XStream(new DomDriver());

    {
        //安全机制
        xStream.allowTypesByRegExp(new String[] { ".*" });
    }
    @Override
    public <T> byte[] read(T t) {
        return xStream.toXML(t).getBytes();
    }

    @Override
    public <T> T write(byte[] bytes, Class<T> tClass, String name) {
        return (T)xStream.fromXML(new String(bytes));
    }
}
