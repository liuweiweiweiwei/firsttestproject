package com.test.thread.javaconcurrencyprogram.lock;

import com.test.MyLog.LwLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.IntStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/1 17:10
 * @description
 */
public class BooleanLock {
    private final BooleanMyLock lock = new BooleanMyLock();

    @SuppressWarnings("unused")
    public void syncMethod() {
        try {
            lock.lock();
            int random = ThreadLocalRandom.current().nextInt(50);
            System.out.println(Thread.currentThread() + " get the lock!");
            /*System.out.println(lock.getBlockThreads());*/
            TimeUnit.SECONDS.sleep(random);
        } catch (InterruptedException e) {
            LwLog.errorInfo(Thread.currentThread() + " is error : " + e.getMessage());
//        } catch (TimeoutException e) {
//            LwLog.errorInfo(Thread.currentThread() + " is error : time out " );
//            e.printStackTrace();
        } finally {
            lock.unLock();
        }
    }

    public static void main(String[] args) {
        BooleanLock booleanLock = new BooleanLock();
        IntStream.range(1,10).mapToObj(t -> new Thread(booleanLock::syncMethod)).forEach(Thread::start);
//        new Thread(booleanLock::syncMethod,"WEIZHUANG").start();
//        new Thread(booleanLock::syncMethod,"GENIE").start();
    }

    private interface LockMine{
        /**
         * lock
         * @throws InterruptedException e
         */
        void lock() throws InterruptedException;

        /**
         * lock in time
         * @param milltime time
         * @throws InterruptedException e
         * @throws TimeoutException t
         */
        void lock(long milltime) throws InterruptedException, TimeoutException;

        /**
         * unlock
         */
        default void unLock(){ }

        /**
         * blockthreads
         * @return th
         * @throws InterruptedException e
         */
        List<Thread> getBlockThreads() throws InterruptedException;
    }

    private static class BooleanMyLock implements LockMine{
        private Thread currentThread;

        private boolean lock;

        private final List<Thread> threadList = new ArrayList<>();

        @Override
        public void lock() throws InterruptedException {
            synchronized (this) {
                System.out.println(Thread.currentThread() + " : start lock!!!" );
                while (lock) {
                    final Thread currentThread1 = Thread.currentThread();
                    try {
                        if (!threadList.contains(currentThread1)) {
                            threadList.add(currentThread1);
                        }
                        this.wait();
                    }catch (Exception e) {
                        threadList.remove(currentThread1);
                        throw e;
                    }
                }
                System.out.println(Thread.currentThread() + " : two start lock!!!" );
                threadList.remove(Thread.currentThread());
                this.lock = true;
                this.currentThread = Thread.currentThread();
                System.out.println(Thread.currentThread() + " : end lock!!!" );
            }
        }

        @Override
        public void lock(long milltime) throws InterruptedException, TimeoutException {
            synchronized (this) {
                if (milltime < 0) {
                    this.lock();
                } else {
                    long readMillimTime = milltime;
                    long endTime = System.currentTimeMillis() + readMillimTime;
                    while (lock) {
                        if (readMillimTime < 0) {
                            throw new TimeoutException("lock with time failed !");
                        }
                        if (!threadList.contains(Thread.currentThread())) {
                            threadList.add(Thread.currentThread());
                        }
                        this.wait(readMillimTime);
                        readMillimTime = endTime - System.currentTimeMillis();
                    }
                    threadList.remove(Thread.currentThread());
                    this.lock = true;
                    this.currentThread = Thread.currentThread();
                }
            }
        }

        @Override
        public void unLock() {
            synchronized (this) {
                if (currentThread == Thread.currentThread()) {
                    this.lock = false;
                    LwLog.logInfo(Thread.currentThread() + " release the lock!");
                    this.notifyAll();
                }
            }
        }

        @Override
        public List<Thread> getBlockThreads() throws InterruptedException {
            return Collections.unmodifiableList(threadList);
        }
    }
}
