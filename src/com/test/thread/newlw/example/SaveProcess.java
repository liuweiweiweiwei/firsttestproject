package com.test.thread.newlw.example;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/16 21:10
 * @description
 */
public class SaveProcess implements Runnable,IProcess {

    /**
     * 使用阻塞队列维护任务
     */
    LinkedBlockingQueue<User> blockingQueue = new LinkedBlockingQueue<>(8);

    private boolean key = false;

    public void setKey(boolean key) {
        this.key = key;
    }

    /**
     * 下一个责任链任务的对象
     */
    private final IProcess process;

    public SaveProcess(IProcess process) {
        this.process = process;
    }

    /**
     * 操作任务
     * @param user
     */
    @Override
    public void process(User user) {
        System.out.println("保存操作开始......添加任务");
        blockingQueue.add(user);
    }


    @Override
    public void run() {
        System.out.println("阻塞队列--打印任务>>>>>>");
        while (!key){
            try {
                //取出队列中的数据
                User take = blockingQueue.take();
                System.out.println("开始保存操作>>>>>>");
                System.out.println("线程：【" + Thread.currentThread().getName() + "】保存任务【" + take.toString() + "】完成！");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
