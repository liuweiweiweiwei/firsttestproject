package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:29
 * @description
 */
public class SpringVideo implements IVideo {
    @Override
    public void operateVideo() {
        System.out.println("spring 的 录像机操作！");
    }
}
