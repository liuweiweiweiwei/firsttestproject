package com.test.Files.bio.tomcat.http;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http2.HttpUtil;

import java.io.OutputStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 22:26
 * @description
 */
public class LwResponse {

    private ChannelHandlerContext context;
    private HttpRequest request;

    private OutputStream outputStream;

    public LwResponse(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public LwResponse(ChannelHandlerContext context, HttpRequest request) {
        this.context = context;
        this.request = request;
    }

    public void write(String outputStream)throws Exception{
        try {
            if(outputStream == null || outputStream.length() == 0){
                return;
            }
            //设置http协议以及请求信息
            FullHttpResponse response = new DefaultFullHttpResponse(
                    //设置http版本
                    HttpVersion.HTTP_1_1,
                    //设置响应状态吗
                    HttpResponseStatus.OK,
                    //将输出编码为UTF-8
                    Unpooled.wrappedBuffer(outputStream.getBytes("UTF-8"))
                    );
            //设置格式
            response.headers().set("Content-Type","text/html");

            //当前是否支持场链接
            context.write(response);
//
//            StringBuilder response = new StringBuilder();
//            response.append("HTTP/1.1 200 OK\n")
//                    .append("Content-Type: text/html;\n")
//                    .append("\r\n")
//                    .append(outputStream);
//            this.outputStream.write(response.toString().getBytes());
        }finally {
            context.flush();
            context.close();
        }
    }
}
