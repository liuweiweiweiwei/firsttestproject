package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:07
 * @description
 */
public class TestStrategy {

    public static void main(String[] args) {
        PromotionActivity promotionActivity618 = new PromotionActivity(new CashBackStrategy());
        promotionActivity618.execute();
        PromotionActivity promotionActivity1111 = new PromotionActivity(new CouponStrategy());
        promotionActivity1111.execute();

        PromotionActivity group = new PromotionActivity(PromotionStrategyFactory.getInstance("GROUP"));
        group.execute();

    }
}
