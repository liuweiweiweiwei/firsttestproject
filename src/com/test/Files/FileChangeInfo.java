package com.test.Files;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FileChangeInfo {
    public static void main(String[] args) throws IOException {
        String path="E://a.txt";
        writeInfoData(path);
        getFile(path);
        //模拟前端获取到的数据stringBuilder
        StringBuilder stringBuilder=getChar("E:\\electorder\\image\\timg1.jpg");
        //处理base64数组
        getBytes(stringBuilder);
        readerS();
    }
    public static void writeInfoData(String path){
        try(DataOutputStream dataOutputStream=new DataOutputStream(new FileOutputStream(path))){
            List<Student> studentList= Arrays.asList(
                    new Student(12,"肖勉",true,"描述"),
                    new Student(15,"肖勉",true,"描述1"),
                    new Student(19,"tom",true,"描述2"),
                    new Student(17,"jane",true,"描述3"),
                    new Student(21,"walk",true,"描述4"));
            studentList.forEach(stu->{
                try {
                    dataOutputStream.writeBoolean(stu.isSa());
                    dataOutputStream.writeInt(stu.getAge());
                    dataOutputStream.writeUTF(stu.getName());
                    dataOutputStream.writeUTF(stu.getDetail());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void getFile(String path){
        try(DataInputStream dataInputStream=new DataInputStream(new FileInputStream(path))) {
            List<Student> list=new LinkedList<>();
            for (int i = 0; i < 5; i++) {
                Student s=new Student();
                s.setSa(dataInputStream.readBoolean());
                s.setAge(dataInputStream.readInt());
                s.setDetail(dataInputStream.readUTF());
                s.setName(dataInputStream.readUTF());
                list.add(s);
            }
            list.forEach(student -> System.out.println(student.toString()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static StringBuilder getChar(String path){
        StringBuilder stringBuilder=new StringBuilder("");
        BASE64Encoder base64Encoder=new BASE64Encoder();
        try (FileInputStream fileInputStream=new FileInputStream(path)){
            byte[] bytes=new byte[1024];
            int a=0;
            while ((a=fileInputStream.read(bytes))!=-1){
                stringBuilder.append(base64Encoder.encode(bytes));
            }
            System.out.println(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder;
    }
    public static void getBytes(StringBuilder stringBuilder) throws IOException {
        BASE64Decoder base64Decoder=new BASE64Decoder();
        byte[] bytes=base64Decoder.decodeBuffer(stringBuilder.toString());
        System.out.println("从前端获取到的字节数组："+bytes.length);
        try(ByteArrayOutputStream writer=new ByteArrayOutputStream()) {
            writer.write(bytes);
            writer.flush();
            FileOutputStream fileOutputStream=new FileOutputStream("E:\\newsImage.jpg");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void readerS(){
        StringBuilder stringBuilder=new StringBuilder("");
        try(BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream("E:\\a.txt"),"utf-8"))) {
            String str="";
            while ((str=reader.readLine())!=null){
                stringBuilder.append(str);
            }
            System.out.println(stringBuilder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
