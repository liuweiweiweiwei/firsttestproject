package com.test.learn.Interface;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/25 9:00
 * @description
 */
public interface InterfaceT {
    public String get();

}
