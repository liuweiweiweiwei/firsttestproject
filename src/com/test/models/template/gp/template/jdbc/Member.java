package com.test.models.template.gp.template.jdbc;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 14:23
 * @description
 */
public class Member {

    private String name;
    private int id;
    private String work;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
}
