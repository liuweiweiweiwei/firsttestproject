package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/24 17:04
 * @description
 */
public class SingleQueueAndSingleStack {


    public static void main(String[] args) {
        //mystack--获取比当前值大的最近的元素
        int[] arr = {2,1,3,4,3};
        List<Integer> list = new ArrayList<>();//最终数据
        MyStack<Integer> myStack = new MyStack<>();
        for (int i = arr.length-1; i >= 0; i--) {
            Integer add = myStack.add(arr[i]);
            list.add(add==null?-1:add);
        }
        System.out.println(list);
        System.out.println("---------------------------");
        //myqueue---单调队列，获取一为3的长度段区间内的最大值
        int[] arrQue = {2,1,3,4,3};
        MyQueue<Integer> queue = new MyQueue<>();
        List<Integer> endMax = new ArrayList<>();
        for (int i = 0; i < arrQue.length; i++) {
            queue.add(arrQue[i]);
            if(i > 2) {
                queue.delete(arrQue[i-3]);
            }
            endMax.add(queue.max());
        }
        System.out.println(endMax);
    }


    /**
     * 单调栈--递增
     */
    public static class MyStack<Key extends Comparable<Key>>{

        private Stack<Key> stack;   //存储数据的队列

        private Key max;    //存储最大元素

        /**
         * 构造函数初始化集合
         */
        public MyStack() {
            this.stack = new Stack<>();
        }

        /**
         * 添加数据---主要(--)外加输出比当前值大的数据
         * @param key
         */
        public Key add(Key key){
            //如果stack不为空且栈顶元素比目标值小，删除栈顶元素-维持低增粘
            while (!stack.isEmpty() && stack.peek().compareTo(key) < 0){
                stack.pop();
            }
            //设置最大值
            if(stack.isEmpty()){
                max = key;
            }
            //获取倒数第二个数
            Key end = stack.isEmpty()? null:stack.peek();
            //添加元素
            stack.push(key);
            return end;
        }

        /**
         * 删除最小元素---一般用不到
         * @return
         */
        public Key del(){
            if(!stack.isEmpty()){
                return stack.pop();
            }else{
                max = null;
            }
            return null;
        }

        /**
         * 获取最大值 --- 一般用不到
         * @return
         */
        public Key max(){
            return this.max;
        }

    }

    /**
     * 单调队列---递增
     */
    public static class MyQueue<key extends Comparable<key>>{

        //  数据集合
        private Deque<key> queue;

        public MyQueue() {
            this.queue = new LinkedList<>();
        }

        /**
         * 添加元素
         * @param key
         */
        public void add(key key){
            //先进先出--从头进去数据
            while (!queue.isEmpty() && queue.peekFirst().compareTo(key) < 0 ){
                queue.pollFirst();  //如果比队头大，则删去队头元素
            }
            //添加元素
            queue.addFirst(key);
        }

        /**
         * 末尾元素即为最大值
         * @return
         */
        public key max(){
            if(queue.isEmpty()){
                return null;
            }
            //返回最后一个元素就是最大值
            return queue.peekLast();
        }

        /**
         * 删除元素
         * @param key
         * @return
         */
        public key delete(key key){
            if(!queue.isEmpty() && queue.peekLast().compareTo(key) == 0){
                return queue.pollLast();  //如果最后一个数据为当前要删除的数据则删除
            }else{
                return null;
            }
        }

    }

}
