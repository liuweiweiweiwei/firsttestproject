package com.test.models.agent;

import com.test.models.agent.dbsource.IOrderService;
import com.test.models.agent.dbsource.Order;
import com.test.models.agent.dbsource.OrderService;
import com.test.models.agent.dbsource.OrderServiceProxy;
import com.test.models.agent.dbsource.dynamic.DynamicProxy;
import com.test.models.agent.dynamicProxy.cglibProxy.CGlibMeiPo;
import com.test.models.agent.dynamicProxy.cglibProxy.Custmer;
import com.test.models.agent.dynamicProxy.gpproxy.LwMeiPo;
import com.test.models.agent.dynamicProxy.jdkProxy.Girl;
import com.test.models.agent.dynamicProxy.jdkProxy.JdkMeiPo;
import com.test.models.agent.dynamicProxy.jdkProxy.Person;
import com.test.models.agent.proxy.Father;
import com.test.models.agent.proxy.Son;
import net.sf.cglib.core.DebuggingClassWriter;
import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.util.Date;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:25
 * @description
 */
public class TestProxy {
    public static void main(String[] args){
        Father father = new Father(new Son());
        father.findLove();
        System.out.println("---------------next------------------");
        //媒婆
        Order order = new Order();
        order.setTime(System.currentTimeMillis());
        IOrderService iOrderService =(IOrderService) new DynamicProxy().getInstance(new OrderService());
        iOrderService.createOrder(order);
        iOrderService.dealOrderService(order);
        System.out.println("---------------next------------------");

        //婚介所
        System.out.println("【jdk】动态代理！");
        Person person = null;
        try {
            person = (Person) new JdkMeiPo().getInstance(new Girl());
            person.findLove();

            //反向代理
            byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{Person.class});
            FileOutputStream outputStream = new FileOutputStream("F:\\tools\\testFile\\$proxy0.class");
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //每天都在用的静态代理-threadlocal动态数据源切换
        System.out.println("---------------next------------------");

        //婚介所
        System.out.println("自己编译写的【jdk】动态代理！");
        Person person1;
        try {
            person1 = (Person) new LwMeiPo().getInstance(new Son());
            person1.findLove();
        }catch (Exception e){
            e.printStackTrace();
        }
        //cglibtest测试类
        System.out.println("-------------next------------");
        System.out.println("cglib测试类！");
        try {

            System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY,"F:\\tools\\testFile");
            Custmer instance = (Custmer)new CGlibMeiPo().getInstance(Custmer.class);
            instance.findLove();
            System.out.println(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
