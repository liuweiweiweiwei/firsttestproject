package com.test.models.adapt.gpnew.power;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 15:17
 * @description
 */
public class PowerAdapter implements DC5{
    private AC220 ac220;

    public PowerAdapter(AC220 ac220) {
        this.ac220 = ac220;
    }

    @Override
    public int outputDC5() {
        System.out.println("获取直流电220v---输入；");
        int ac220 = this.ac220.output220();
        System.out.println("转换为交流电5V---输出；");
        return ac220/22;
    }
}
