package com.test.thread.javaconcurrencyprogram.syncthred;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/24 21:24
 */
public class SyncThread implements Runnable{
    public static int index = 1;

    private static final Object MUTEX = new Object();

    public static final int MAX = 100;

    @Override
    public void run() {
        synchronized (MUTEX) {
            while (index <= MAX) {
                System.out.println(Thread.currentThread().getName() + " : " + (++index));
            }
        }
    }

    public static void main(String[] args) {
        SyncThread syncThread = new SyncThread();
        new Thread(syncThread).start();
        new Thread(syncThread).start();
        new Thread(syncThread).start();
    }
}
