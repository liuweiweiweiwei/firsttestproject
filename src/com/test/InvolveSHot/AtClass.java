package com.test.InvolveSHot;

import com.sun.deploy.security.ValidationState;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AtClass {
    public static void main(String[] args) {

    }
    static class SimpleContainer{
        private static Map<Class<?>,Object> map=new ConcurrentHashMap<>();
        public static <T> T getInstance(Class<T> tClass){
            boolean sing=tClass.isAnnotationPresent(SimpleSingleton.class);
            if (!sing){
                return null;
            }
            Object o=map.get(tClass);
            if(null!=o){
                return (T)o;
            }
            synchronized (tClass){
                o=map.get(tClass);
                map.put(tClass,o);
            }
            return (T)o;
        }
    }
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface SimpleSingleton{

    }
}
