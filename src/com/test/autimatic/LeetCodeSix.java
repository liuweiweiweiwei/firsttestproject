package com.test.autimatic;

import com.test.autimatic.type.PowAndHashString;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/22 9:18
 * @description
 */
public class LeetCodeSix {

    public static void main(String[] args) {
        //leetcode-1
        int[][] ints = colorBorder(new int[][]{{1, 2, 2}, {2, 3, 2}}, 0, 1, 3);
        System.out.println(ints);
        //leetcode-2
        System.out.println(' ' - '0');
        //leetcode-3
        System.out.println(shortestCompletingWord("iMSlpe4", new String[]
                {"claim", "consumer", "student", "camera", "public", "never", "wonder", "simple", "thought", "use"}));

        TreeMap<Integer, Integer> treeMapOrder = new TreeMap<>((o1, o2) -> o1 - o2);
        treeMapOrder.put(1, 2);
        System.out.println(treeMapOrder.get(1));
        //leetcode-4
        System.out.println('a' - '0');
        System.out.println('A' - '0');
        //leetcode-4
        System.out.println("www.baidu.com/servlet/selectAfrom/user".hashCode());
        System.out.println("/servlet/selectArm/user".hashCode());
        //leetcode-5
        System.out.println(numWaterBottles(9, 3));
        //leetcode-6
        System.out.println(countBattleships(new char[][]{{'X', '.', 'X', 'X'}, {'.', 'X', '.', '.'}, {'.', '.', '.', '.'}}));
        //leetcode-7
        System.out.println(findRadius(new int[]{1, 2, 3, 4, 1}, new int[]{2}));
        //leetcode - 8
        System.out.println(dayOfYear("2004-03-01"));

        //leetcode -9
        System.out.println(repeatedStringMatch("gdeb", "ebg"));
        //leetcode--10
        System.out.println(longestDupSubstring("banana"));
        //          1
        //      10      4
        //    3       7    9
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(10);
        TreeNode treeNode3 = new TreeNode(4);
        TreeNode treeNode4 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(7);
        TreeNode treeNode6 = new TreeNode(9);
        treeNode1.left = treeNode2;treeNode2.left = treeNode4;treeNode1.right = treeNode3;
        treeNode3.left = treeNode5;treeNode3.right = treeNode6;
        System.out.println(isEvenOddTree(treeNode1));
        //leetcode - 11
        System.out.println(numFriendRequests(new int[]{73,106,39,6,26,15,30,100,71,35,46,112,6,60,110}));
    }

    public static int countQuadruplets(int[] nums) {
        int num = 0;
        Map<Integer,Integer> map = new HashMap<>(16);
        for (int i = nums.length - 3; i > 0 ; i--) {
            for (int j = i + 2; j < nums.length; j++) {
                map.put(nums[j] - nums[i + 1]  ,map.getOrDefault(nums[j] - nums[i + 1],0) +1);
            }
            for (int j = 0; j < i; j++) {
                num += map.getOrDefault(nums[j] + nums[i],0);
            }
        }
        return num;
    }

    static PowAndHashString.Trie trie = new PowAndHashString.Trie();
    public static List<String> findAllConcatenatedWordsInADict(String[] words) {
        Arrays.sort(words, Comparator.comparingInt(String::length));
        List<String> result = new ArrayList<>();
        for (String t:words) {
            if(t.length() != 0) {
                if (find(t, 0)) {
                    result.add(t);
                } else {
                    trie.insert(t);
                }
            }
        }
        return result;
    }

    private static boolean find(String word,int start){
        if(word.length() == start){return true;}
        PowAndHashString.Trie child = trie;
        for (int i = start; i < word.length(); i++) {
            child = child.children[word.charAt(i) - 'a'];
            if(child == null){
                return false;
            }
            if(child.end){
                if(find(word, i + 1)){
                    return true;
                }
            }
        }
        return false;
    }



    public static int numFriendRequests(int[] ages) {
        //总数量以及排序
        Arrays.sort(ages);
        int num = 0;int end ;int start;
        for (int i = ages.length - 1; i > 0; i--) {
            if (ages[i] < 15) { continue; }
            start = 0;end = i - 1;int tempIndex = -1;int lEnd = i;
            while (end > -1 && ages[end] == ages[i]){
                num += 2;end--;lEnd--;
            }
            if(end >= 0) {
                int temp = ages[i] / 2 + 7;
                while (end > -1 && start <= end) {
                    int mid = start + (end - start) / 2;
                    if (temp >= ages[mid]) {
                        start = mid + 1;
                    } else {
                        if (mid == 0 || ages[mid - 1] <= temp) {
                            tempIndex = mid;
                            break;
                        } else {
                            end = mid - 1;
                        }
                    }
                }//计算
                if (tempIndex != -1) {
                    start = tempIndex;
                    num += (lEnd - start);
                }
            }
        }
        return num;
    }

    public static String[] findOcurrences(String text, String first, String second) {
        List<String> list = new ArrayList<>();
        String[] split = text.split(" ");
        for (int i = 0; i + 1 < split.length; i++) {
            if(split[i].equals(first)&& split[i + 1].equals(second)){
                if(i + 2 < split.length){
                    list.add(split[i + 2]);
                }
            }
        }
        String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }

        public TreeNode(TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public static boolean isEvenOddTree(TreeNode root) {
        List<TreeNode> rootT = new ArrayList<>();
        rootT.add(root);
        return isEvent(rootT,true);
    }

    private static boolean isEvent(List<TreeNode> nodeList,boolean j){
        if(nodeList == null || nodeList.size() == 0){
            return true;
        }
        List<TreeNode> treeNodeList = new ArrayList<>();
        int first = j ? -1 : 1000001;
        int li;
        for (TreeNode aNodeList : nodeList) {
            if (aNodeList.left != null) {
                treeNodeList.add(aNodeList.left);
            }
            if (aNodeList.right != null) {
                treeNodeList.add(aNodeList.right);
            }
            li = aNodeList.val;
            if (j) {
                if (first >= li || li % 2 == 0) {
                    return false;
                }
                first = li;
            } else {
                if (first <= li || li % 2 != 0) {
                    return false;
                }
                first = li;
            }
        }
        return isEvent(treeNodeList,!j);
    }

    public static int eatenApples(int[] apples, int[] days) {
        int[][] p = new int[apples.length][3];
        for (int i = 0; i < apples.length; i++) {
            p[i][0] = i + 1;
            p[i][1] = i + days[i];
            p[i][2] = apples[i];
        }
        Arrays.sort(p, (o1, o2) -> {
            if(o2[1] != o1[1]){
                return o2[1] - o1[1];
            }
            return o2[0] - o1[0];
        });
        int start = Math.max(p[0][1]- p[0][2],p[0][1]- p[0][0]);int num = Math.min(p[0][2], p[0][1] - p[0][0]);
        for (int i = 1; i < apples.length; i++) {
            if(start <= 1){ break; }
            if(start < p[0][0]){ continue; }
            if(start > p[i][1]){start = p[i][1];}
            start = Math.max(start - p[i][2],start - p[i][0]);
            num += Math.min(p[i][2], start - p[i][0]);
        }
        return num;
    }

    static long[] hashA;static long[] hashB;static int mode = 2333333;
    public static String longestDupSubstring(String s) {
        String result = "";
        hashA = new long[s.length() + 1];hashB = new long[s.length() + 1];hashB[0] = 1;
        for (int i = 0; i < s.length(); i++) {
            hashB[i + 1] = hashB[i] * mode;
            hashA[i + 1] = hashA[i] * mode + s.charAt(i);
        }
        int l = 0;int r = s.length();
        //将字符串按照 len/2 位一组统计分割---不满足 len/2/2 继续分割满足则按照 (len + len/2)/2分割
        while (l < r){
            int mid = l + (r - l)/2;
            String temp = check(s,mid);
            if(!"".equals(temp)){
                l = mid + 1;
            }else{
                r = mid - 1;
            }
            result = result.length() < temp.length() ? temp : result;
        }
        return result;
    }

    /**
     * 校验数据
     * @param str
     * @param mid
     * @return
     */
    private static String check(String str,int mid){
        Set<Long> hashEqualStr = new HashSet<>();
        for (int i = 1,j = i + mid -1; j <= str.length() ; i++,j++) {
            //统计当前从i-1 到 j位置之间的字符串hash
            Long hash = hashA[j] - hashA[i - 1] * hashB[mid];
            //比较操作
            if(hashEqualStr.contains(hash)){
                return str.substring(i-1,j);
            }
            hashEqualStr.add(hash);
        }
        return "";
    }

    public static int repeatedStringMatch(String a, String b) {
        if (a.contains(b)) {
            return 1;
        }
        int[] aarr = new int[26];
        int[] barr = new int[26];
        for (char i : a.toCharArray()) {
            aarr[i - 'a']++;
        }
        for (char i : b.toCharArray()) {
            barr[i - 'a']++;
        }
        for (int i = 0; i < 26; i++) {
            if (aarr[i] == 0 && barr[i] != 0) {
                return -1;
            }
        }
        dp(a);
        //统计a在b中的数量
        int num = 0;
        int start = -1;
        int state = 0;
        for (int i = 0; i < b.length(); i++) {
            int temp = dpKmp[state][b.charAt(i) - 'a'];
            if (start == -1) {
                if (num == 0 && temp == a.length()) {
                    start = i - a.length();
                }
            } else {
                if (start >= a.length() || (temp != 0 && temp < state) || (temp == 0 && state != a.length())) {
                    return -1;
                }
            }
            state = temp;
            if (state == a.length()) {
                num++;
                state = 0;
            }
        }
        for (int i = start, j = a.length() - 1; i >= 0; i--, j--) {
            if (a.charAt(j) != b.charAt(i)) {
                return -1;
            }
        }
        //把初始和结束位置加上
        if(num != 0) {
            for (int i = start + num * a.length() + 1, j = 0; i < b.length(); i++, j = (j + 1) % a.length()) {
                if (a.charAt(j) != b.charAt(i)) {
                    return -1;
                }
            }
        }else{
            for (int i = b.length() - state - 1, j = a.length() - 1; i >= 0; i--, j = (j - 1) % a.length()) {
                if (a.charAt(j) != b.charAt(i)) {
                    return -1;
                }
            }
        }
        if (state != 0) {
            num = num == 0 ? 2 : ++num;
        }
        if (start != -1) {
            num++;
        }
        return num == 0 ? -1 : num;
    }

    public static int[][] dpKmp;

    private static void dp(String s) {
        dpKmp = new int[s.length()][26];
        int shard = 0;
        for (int i = 0; i < s.length(); i++) {
            //拷贝影子状态
            System.arraycopy(dpKmp[shard], 0, dpKmp[i], 0, 26);
            //影子状态更新
            shard = dpKmp[i][s.charAt(i) - 'a'];
            //状态值等于位置加一
            dpKmp[i][s.charAt(i) - 'a'] = i + 1;
        }
    }


    public static int dayOfYear(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).getDayOfYear();
    }

    public static int findRadius(int[] houses, int[] heaters) {
        //先排序--再按照滑动窗口处理
        Arrays.sort(heaters);
        Arrays.sort(houses);
        int min = 0;
        int index = 0;
        for (int h : houses) {
            int compare = Math.abs(heaters[index] - h);
            //找出house中与heater中差的绝对值最小的---记录绝对值差
            while (index + 1 < heaters.length && compare >= Math.abs(heaters[index + 1] - h)) {
                index++;
                compare = Math.abs(heaters[index] - h);
            }
            //替换所需要的最大步数
            min = compare != 0 ? Math.max(min, compare) : min;
        }
        return min;
    }

    public static int findJudge(int n, int[][] trust) {
        if (trust.length == 0 && n == 1) {
            return 1;
        }
        //记录每个坐标以及坐标上的标记数量
        int[][] result = new int[n + 1][n + 1];
        for (int[] t : trust) {
            result[t[0]][0]++;
            result[t[1]][1]++;
        }
        for (int i = 0; i < result.length; i++) {
            if (result[i][0] == (n - 1) && result[i][1] == 0) {
                return i;
            }
        }
        return -1;
    }

    public static int countBattleships(char[][] board) {
        int numOt = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 'X') {
                    if (i > 0 && board[i - 1][j] == 'X') {
                        continue;
                    }
                    if (j > 0 && board[i][j - 1] == 'X') {
                        continue;
                    }//记录存在位置的数目
                    numOt++;
                }
            }
        }
        return numOt;
    }

    public static int numWaterBottles(int numBottles, int numExchange) {
        int num = numBottles;
        int divisor;
        while ((divisor = numBottles / numExchange) > 0) {
            num += divisor;
            numBottles = numBottles % numExchange + divisor;
        }
        return num;
    }

    public int visiblePoints(List<List<Integer>> points, int angle, List<Integer> location) {
        int sameCnt = 0;
        List<Double> polarDegrees = new ArrayList<>();
        int locationX = location.get(0);
        int locationY = location.get(1);
        for (List<Integer> point : points) {
            int x = point.get(0);
            int y = point.get(1);
            if (x == locationX && y == locationY) {
                sameCnt++;
                continue;
            }
            Double degree = Math.atan2(y - locationY, x - locationX);
            polarDegrees.add(degree);
        }
        Collections.sort(polarDegrees);

        int m = polarDegrees.size();
        for (int i = 0; i < m; ++i) {
            polarDegrees.add(polarDegrees.get(i) + 2 * Math.PI);
        }

        int maxCnt = 0;
        int right = 0;
        double toDegree = angle * Math.PI / 180;
        for (int i = 0; i < m; ++i) {
            Double curr = polarDegrees.get(i) + toDegree;
            while (right < polarDegrees.size() && polarDegrees.get(right) <= curr) {
                right++;
            }
            maxCnt = Math.max(maxCnt, right - i);
        }
        return maxCnt + sameCnt;
    }

    private static Map<Integer, Integer> map = new HashMap<>();

    public static int[] loudAndRich(int[][] richer, int[] quiet) {
        Map<Integer, List<Integer>> listMap = new HashMap<>();
        //将数据存入到map中
        for (int[] arr : richer) {
            List<Integer> orDefault = listMap.getOrDefault(arr[1], new ArrayList<>());
            orDefault.add(arr[0]);
            listMap.put(arr[1], orDefault);
        }
        //初始化最终数据
        int[] end = new int[quiet.length];
        for (int i = 0; i < end.length; i++) {
            end[i] = i;
        }
        //将数据回填int【】
        for (Integer key : listMap.keySet()) {
            int min = getMin(listMap, key, quiet);
            end[key] = min;
        }
        return end;
    }

    private static int getMin(Map<Integer, List<Integer>> listMap, int key, int[] quiet) {
        if (!listMap.containsKey(key)) {
            return key;
        }
        int min = quiet[key];
        int index = key;
        //dfs全遍历--利用map存储中间数据节省时间
        for (Integer i : listMap.get(key)) {
            if (map.containsKey(i)) {
                int temp = map.get(i);
                if (min >= quiet[temp]) {
                    index = temp;
                    min = quiet[temp];
                }
            } else {
                if (listMap.containsKey(i)) {
                    int min1 = getMin(listMap, i, quiet);
                    if (quiet[min1] <= min) {
                        index = min1;
                        min = quiet[min1];
                    }
                }
                if (min >= quiet[i]) {
                    index = i;
                    min = quiet[i];
                }
            }
        }//将计算过的存到map备用
        map.put(key, index);
        return index;
    }


    public static int scheduleCourse(int[][] courses) {
        //首先排序
        Arrays.sort(courses, (o1, o2) -> {
            if (o1[1] - o2[1] != 0) {
                return o1[1] - o2[1];
            }
            return o1[0] - o2[0];
        });
        //记录当前开始位置
        int index = 0;
        //按照顺序依次进入队列--如果 此时的 ①开始位置+此处的数据占用长度满足要求或者②队列中最长长度大于此时入站的课程长度的话
        // ①直接加入课程，并累加起始位置②则将长度占用最大的去除累加并减去起始位置
        PriorityQueue<Integer> queue = new PriorityQueue<>((o1, o2) -> o2 - o1);
        for (int[] course : courses) {
            if (course[0] + index <= course[1]) {
                queue.offer(course[0]);
                index += course[0];
            } else if (!queue.isEmpty() && queue.peek() > course[0]) {
                queue.offer(course[0]);
                index = index - queue.poll() + course[0];
            }
        }
        return queue.size();
    }


    public static int maxIncreaseKeepingSkyline(int[][] grid) {
        int[] w = new int[grid.length];
        int[] h = new int[grid[0].length];
        int oldNum = 0;
        int newNum = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                w[i] = Math.max(grid[i][j], w[i]);
                h[i] = Math.max(grid[j][i], h[i]);
                oldNum += grid[i][j];
            }
        }
        for (int aW : w) {
            for (int aH : h) {
                newNum += Math.min(aW, aH);
            }
        }
        return newNum - oldNum;
    }


    public static String toLowerCase(String s) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c >= 'A' && c <= 'Z') {
                chars[i] |= 32;
            }
        }

        return new String(chars);
    }


    public static class TopVotedCandidate {

        private int[] orders;
        private int[] times;
        private Map<Integer, Integer> treeMapPerson = new HashMap<>();


        public TopVotedCandidate(int[] persons, int[] times) {
            this.orders = new int[persons.length];
            this.times = times;
            //临时最大值
            int maxIndex = -1;
            //存储数据
            for (int i = 0; i < persons.length; i++) {
                treeMapPerson.put(persons[i], treeMapPerson.getOrDefault(persons[i], 0) + 1);
                //获取第一个key---person
                if (maxIndex == -1 || treeMapPerson.get(persons[i]) >= treeMapPerson.get(maxIndex)) {
                    maxIndex = persons[i];
                }
                orders[i] = maxIndex;
            }
        }

        public int q(int t) {
            return orders[twoChar(t, times)];
        }

        private int twoChar(int time, int[] times) {
            int l = 0, r = times.length - 1, mid = 0;
            while (true) {
                mid = l + (r - l) / 2;
                if (mid >= orders.length) {
                    break;
                }
                if (times[mid] == time) {
                    return mid;
                } else if (times[mid] < time) {
                    l = mid + 1;
                } else {
                    if (times[mid - 1] <= time) {
                        return mid - 1;
                    } else {
                        r = mid - 1;
                    }
                }
            }
            return orders.length - 1;
        }
    }


    /**
     * @param licensePlate
     * @param words
     * @return
     */
    public static String shortestCompletingWord(String licensePlate, String[] words) {
        //转换小写
        String lowerCase = licensePlate.toLowerCase();
        //保存统计数目--最后一位保存总数量
        int[] arr = new int[27];
        for (char c : lowerCase.toCharArray()) {
            if (c - 'a' >= 0 && c - 'a' <= 25) {
                arr[c - 'a']++;
                arr[26]++;
            }
        }
        //将word数组排序---可省略
//        insertSort(words);
        //遍历数组--比较-记录下标位置
        int[] arrTemp = new int[27];
        int index = -1;
        for (int j = 0; j < words.length; j++) {
            String word = words[j];
            System.arraycopy(arr, 0, arrTemp, 0, 27);
            for (int i = 0; i < word.length(); i++) {
                int wi = word.charAt(i) - 'a';
                if (arrTemp[wi] > 0) {
                    arrTemp[wi]--;
                    arrTemp[26]--;
                }
                if (arrTemp[26] <= 0) {
                    index = (index == -1 || words[j].length() < words[index].length()) ? j : index;
                }
            }
        }
        return words[index];
    }


    private static void insertSort(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j].length() < arr[j - 1].length()) {
                    String temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                } else {
                    break;
                }
            }
        }
    }


    public static boolean validTicTacToe(String[] board) {
        //chars的下标，x的数量，o的数量
        int lenX = 0, lenO = 0;
        for (String str : board) {
            for (char c : str.toCharArray()) {
                if (c == 'X') {
                    lenX++;
                }
                if (c == 'O') {
                    lenO++;
                }
            }
        }
        //1.如果o的数量等于x或x-1；否则false
        if (lenO != lenX && (lenO + 1) != lenX) {
            return false;
        }
        //2.统计成行数量
        int h = 0, l = 0, j = -1;
        if (board[0].charAt(0) == board[0].charAt(1) && board[0].charAt(1) == board[0].charAt(2)
                && (board[0].charAt(0) == 'O' || board[0].charAt(0) == 'X')) {
            j = (board[0].charAt(0) == 'X') ? 1 : 0;
            h++;
        }
        if (board[1].charAt(0) == board[1].charAt(1) && board[1].charAt(1) == board[1].charAt(2)
                && (board[1].charAt(0) == 'O' || board[1].charAt(0) == 'X')) {
            j = (board[1].charAt(0) == 'X') ? 1 : 0;
            h++;
        }
        if (board[2].charAt(0) == board[2].charAt(1) && board[2].charAt(1) == board[2].charAt(2)
                && (board[2].charAt(0) == 'O' || board[2].charAt(0) == 'X')) {
            j = (board[2].charAt(0) == 'X') ? 1 : 0;
            h++;
        }
        if (board[0].charAt(0) == board[1].charAt(0) && board[1].charAt(0) == board[2].charAt(0)
                && (board[0].charAt(0) == 'O' || board[0].charAt(0) == 'X')) {
            j = (board[0].charAt(0) == 'X') ? 1 : 0;
            l++;
        }
        if (board[0].charAt(1) == board[1].charAt(1) && board[1].charAt(1) == board[2].charAt(1)
                && (board[0].charAt(1) == 'O' || board[0].charAt(1) == 'X')) {
            j = (board[0].charAt(1) == 'X') ? 1 : 0;
            l++;
        }
        if (board[0].charAt(2) == board[1].charAt(2) && board[1].charAt(2) == board[2].charAt(2)
                && (board[0].charAt(2) == 'O' || board[0].charAt(2) == 'X')) {
            j = (board[0].charAt(2) == 'X') ? 1 : 0;
            l++;
        }
        if (board[0].charAt(2) == board[1].charAt(1) && board[1].charAt(1) == board[2].charAt(0)
                && (board[0].charAt(2) == 'O' || board[0].charAt(2) == 'X')) {
            j = (board[0].charAt(2) == 'X') ? 1 : 0;
        }
        if (board[0].charAt(0) == board[1].charAt(1) && board[1].charAt(1) == board[2].charAt(2)
                && (board[0].charAt(0) == 'O' || board[0].charAt(0) == 'X')) {
            j = (board[0].charAt(0) == 'X') ? 1 : 0;
        }
        return (j != 1 || lenO != lenX) && (j != 0 || lenO + 1 != lenX) && h <= 1 && l <= 1;
    }

    public static int[] maxSumOfThreeSubarrays(int[] nums, int k) {
        //计算总情况---三个滑动窗口
        int[] numsT = new int[3];
        int sum1 = 0, sum2 = 0, sum3 = 0;
        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        int i1 = 0, i21 = 0, i22 = 0;
        for (int i = 2 * k; i < nums.length; i++) {
            num1 += nums[i - 2 * k];
            num2 += nums[i - k];
            num3 += nums[i];
            if (i > 3 * k - 1) {
                if (num1 > sum1) {
                    i1 = i - 3 * k + 1;
                    sum1 = num1;
                }
                if (num2 + sum1 > sum2) {
                    i21 = i1;
                    i22 = i - 2 * k + 1;
                    sum2 = num2 + sum1;
                }
                if (num3 + sum2 > sum3) {
                    numsT[0] = i21;
                    numsT[1] = i22;
                    numsT[2] = i - k + 1;
                    sum3 = sum2 + num3;
                }
                num1 -= nums[i - 3 * k + 1];
                num2 -= nums[i - 2 * k + 1];
                num3 -= nums[i - k + 1];
            }
        }
        return numsT;
    }


    private static List<int[]> listColor = new ArrayList<>();
    private static Set<String> setColor = new HashSet<>();

    public static int[][] colorBorder(int[][] grid, int row, int col, int color) {
        getRowCol(grid, row, col, grid.length, grid[0].length, grid[row][col]);
        for (int[] arr : listColor) {
            grid[arr[0]][arr[1]] = color;
        }
        return grid;
    }

    private static void getRowCol(int[][] grid, int row, int col, int lenX, int lenY, int target) {
        if (setColor.contains(row + "" + col)) {
            return;
        }//记录走过的路径
        setColor.add(row + "" + col);
        //记录四个相邻节点数据
        int x1 = row > 0 ? grid[row - 1][col] : -1;
        int x2 = row < lenX - 1 ? grid[row + 1][col] : -1;
        int y1 = col > 0 ? grid[row][col - 1] : -1;
        int y2 = col < lenY - 1 ? grid[row][col + 1] : -1;
        //如果有一个相邻接点不和target相同的话记录数据
        if ((x1 != target || x2 != target || y1 != target || y2 != target)) {
            listColor.add(new int[]{row, col});
        }
        //递归遍历四个子节点
        if (x1 != -1 && x1 == target) {
            getRowCol(grid, row - 1, col, lenX, lenY, target);
        }
        if (x2 != -1 && x2 == target) {
            getRowCol(grid, row + 1, col, lenX, lenY, target);
        }
        if (y1 != -1 && y1 == target) {
            getRowCol(grid, row, col - 1, lenX, lenY, target);
        }
        if (y2 != -1 && y2 == target) {
            getRowCol(grid, row, col + 1, lenX, lenY, target);
        }
    }


    public static String originalDigits(String s) {
        //按照规则将字母存到数组中
        int[] letterArr = new int[26];
        for (char letter : s.toCharArray()) {
            letterArr[letter - 'a']++;
        }
        int[] numberArr = new int[10];
        //数字0 e,o,r
        numberArr[0] = letterArr[25];
        letterArr[4] -= letterArr[25];
        letterArr[14] -= letterArr[25];
        letterArr[17] -= letterArr[25];
        //数字6 s
        numberArr[6] = letterArr[23];
        letterArr[18] -= letterArr[23];
        //数字2 o,t
        numberArr[2] = letterArr[22];
        letterArr[14] -= letterArr[22];
        letterArr[19] -= letterArr[22];
        //数字4 f,o,r
        numberArr[4] = letterArr[20];
        letterArr[5] -= letterArr[20];
        letterArr[14] -= letterArr[20];
        letterArr[17] -= letterArr[20];
        //数字5 e
        numberArr[5] = letterArr[5];
        letterArr[4] -= letterArr[5];
        //数字1 e
        numberArr[1] = letterArr[14];
        letterArr[4] -= letterArr[14];
        //数字7 e
        numberArr[7] = letterArr[18];
        letterArr[4] -= (2 * letterArr[18]);
        //数字3 e,t
        numberArr[3] = letterArr[17];
        letterArr[4] -= (2 * letterArr[17]);
        letterArr[19] -= letterArr[17];
        //数字8
        numberArr[8] = letterArr[19];
        letterArr[4] -= letterArr[19];
        //数字9
        numberArr[9] = letterArr[4];
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < numberArr[i]; j++) {
                builder.append(i);
            }
        }
        return builder.toString();
    }

    public static boolean buddyStrings(String s, String goal) {
        if (s.length() != goal.length() || s.length() == 1) {
            return false;
        }
        int[] data = new int[29];//判断的数据
        data[26] = -1;
        data[27] = -1;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != goal.charAt(i)) {//如果不相等
                data[28] = 1;
                if (data[26] == -1) {//第一个下标
                    data[26] = i;
                } else if (data[27] == -1) {///第二个下标
                    data[27] = i;
                } else {
                    return false;
                }
            }
            data[s.charAt(i) - 'a']++;
        }
        for (int i = 0; i < 26; i++) {
            if (data[i] > 1 && data[28] != 1) {
                return true;
            }
        }
        return (data[26] != -1 && data[27] != -1
                && s.charAt(data[26]) == goal.charAt(data[27])
                && s.charAt(data[27]) == goal.charAt(data[26]));
    }

    public static class Solution {

        private int[] data;
        private int[] shuffleData;
        private Random random;

        public Solution(int[] nums) {
            //初始化长度
            this.data = new int[nums.length];
            this.shuffleData = new int[nums.length];
            random = new Random();
            //赋值
            System.arraycopy(nums, 0, data, 0, nums.length);
            System.arraycopy(nums, 0, shuffleData, 0, nums.length);
        }

        public int[] reset() {
            System.arraycopy(data, 0, shuffleData, 0, data.length);
            return this.data;
        }

        public int[] shuffle() {
            //打乱顺序
            for (int i = 0; i < shuffleData.length; i++) {
                int rand = this.random.nextInt(shuffleData.length);
                int temp = shuffleData[rand];
                shuffleData[rand] = shuffleData[i];
                shuffleData[i] = temp;
            }
            return this.shuffleData;
        }
    }
}
