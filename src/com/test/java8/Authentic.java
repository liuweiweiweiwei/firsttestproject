package com.test.java8;

import java.util.Arrays;

public class Authentic {
    public static final String EXCHANGE_NAME="exchage_basic";
    public static final String EXCHANGE_TYPE="exchage";

    public static void main(String[] args) {
        int[] a={12,7,9,2,20,16,15,25,2,0,12,21,21,13,11,7,5,8,4};
        //int[] a={12,36,47,29,98,76,23,36,29};
        //insertSort(a);
        //choiceSort(a);
        //int[] b=mergeSort(a,0,a.length-1);
        //Arrays.stream(b).forEach(System.out::println);
        //MaoPaoSort(a);
        //quickSort(a,0,a.length-1);
        //start(a,a.length);
        xiEr(a);
        //int[] b=getDigitSor(a,2);
        Arrays.stream(a).forEach(System.out::println);
    }

    public static void insertSort(int[] arr){
        for (int i = 1; i <arr.length ; i++) {
            for (int j=i; j > 0; j--) {
                if(arr[j]<arr[j-1]){
                    int temp=arr[j];
                    arr[j]=arr[j-1];
                    arr[j-1]=temp;
                }else {
                    break;
                }
            }
        }
    }

    public static void choiceSort(int[] a){
        for (int i = 0; i < a.length-1; i++) {
            int temp=a[i];
            for (int j = i; j < a.length; j++) {
                if(a[j]<temp){
                    int tem=temp;
                    temp=a[j];
                    a[j]=tem;
                }
            }
            a[i]=temp;
        }
    }

    public static int[] mergeSort(int[] arr,int st,int end){
        if(st==end){//当始末位置相同说名只有一个元素
            return new int[]{arr[st]};
        }
        int middle=(st+end)/2;//取中间值并按照左右拆分
        int[] left=mergeSort(arr,st,middle);
        int[] right=mergeSort(arr,middle+1,end);
        //定义新的返回数组
        int[] newArr=new int[left.length+right.length];
        int i = 0,j=0,m=0;//将左右两个数组的值放到新数组中
        while (left.length>i&&right.length>j){
            if(left[i]>right[j]){
                newArr[m++]=right[j++];
            }else {
                newArr[m++]=left[i++];
            }
        }
        //如果左边或右边数组有遗漏则直接加到新数组末尾
        while (left.length>i){
            newArr[m++] = left[i++];
        }
        while (right.length>j) {
            newArr[m++] = right[j++];
        }
        return newArr;//返回
    }

    public static void quickSort(int[] arr,int start,int end){
        //当始末位置不足时结束递归调用
        if(start>end){
            return;
        }
        //定义·中间标准值
        int temp=arr[start];
        int i=start;
        int j=end;
        //当首位不相同或头比尾小时，进入循环必须要从后向前对比
        while(i<j){
            while ((i<j&&arr[j]>=temp)){
                j--;
            }
            while (i<j&&arr[i]<=temp){
                i++;
            }

            if(i<j){
                int tem=arr[i];
                arr[i]=arr[j];
                arr[j]=tem;
            }
        }
        //将标准值定义重新到i,交换初始值和最终这轮循环确定的标准值位置
        arr[start]=arr[i];
        arr[i]=temp;

        quickSort(arr,start,j-1);
        quickSort(arr,j+1,end);
    }

    public static void MaoPaoSort(int[] arr){
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j <arr.length-1-i ; j++) {
                if(arr[j]>arr[j+1]){
                    int temp=arr[j+1];
                    arr[j+1]=arr[j];
                    arr[j]=temp;
                }
            }
        }
    }

    public static void chang(int[] arr,int st,int end){
        int tem=arr[st];
        arr[st]=arr[end];
        arr[end]=tem;
    }

    public static void compares(int[] arr,int length){
        //构建初始
        int j=(length/2)-1;
        for (int i = j; i >=0 ; i--) {
            if (length - 1 >= (2 * i + 2) && arr[2 * i + 1] > arr[2*i+2]) {
                chang(arr,2 * i + 2,2 * i + 1);
            }
            if(arr[i]>arr[2 * i + 1]){
                chang(arr,i,2 * i + 1);
            }
        }
    }
    public static void start(int[] arr,int length){
        for (int i = length; i >0 ; i--) {
            compares(arr,i);
            chang(arr,0,i-1);
        }
    }

    public static int[] getDigitSor(int[] arr,int site){
        int n=10;
        int m=0;
        int[] ints=new int[10];
        int[][] ints1=new int[10][arr.length];
        while (site>0) {
            for (int i = 0; i < arr.length; i++) {
                int tem = (arr[i]%n)*10/n;
                int mid = ints[tem]++;
                ints1[tem][mid] = arr[i];
            }
            for (int j = 0; j < 10 ; j++) {
                int t=0;
                while (ints[j]>0){
                    arr[m]=ints1[j][t];
                    ints[j]--;
                    t++;
                    m++;
                }
            }
            n*=10;
            m=0;
            site--;
        }
        return arr;
    }

    public static void xiEr(int[] arr){
        int interval=arr.length/2;
        for (int i = interval; i >0 ; i/=2) {
            for (int j = i; j <arr.length ; j++) {
                for (int k =j ; k <arr.length ; k++) {
                    if(arr[k]>arr[k-i]){
                        int tem=arr[k];
                        arr[k]=arr[k-i];
                        arr[k-i]=tem;
                    }
                }
            }
        }
    }
}
