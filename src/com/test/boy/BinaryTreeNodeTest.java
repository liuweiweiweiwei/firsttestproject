package com.test.boy;

import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/5 10:04
 * @description
 */
public class BinaryTreeNodeTest {
    public static void main(String[] args) {
        BinaryTreeNode Fnode = new BinaryTreeNode("F", null, null);
        BinaryTreeNode Cnode = new BinaryTreeNode("C", Fnode, null);
        BinaryTreeNode Enode = new BinaryTreeNode("E", null, null);
        BinaryTreeNode Dnode = new BinaryTreeNode("D", null, null);
        BinaryTreeNode Bnode = new BinaryTreeNode("B", Dnode, Enode);
        BinaryTreeNode root = new BinaryTreeNode("A", Bnode, Cnode);
        getNodePrint( root);
        postOrderOneStack(root);
    }

    /**
     * 输出内容
     */
    public static void getNodePrint(BinaryTreeNode root){
        if(root!=null){
            Stack<BinaryTreeNode> stackTemp=new Stack<>();
            Stack<BinaryTreeNode> stack=new Stack<>();
            while (root!=null||!stackTemp.isEmpty()){
                while (root!=null){
                    stackTemp.push(root);
                    stack.push(root);
                    root=root.getRightNode();
                }
                if(!stackTemp.isEmpty()){
                    root=stackTemp.pop();
                    root=root.getLeftNode();
                }
            }
            while (!stack.isEmpty()){
                System.out.print(stack.pop().getValue()+"\t");
            }
        }
    }

    public static void postOrderOneStack(BinaryTreeNode root){
        Stack<BinaryTreeNode> stack=new Stack<>();
        while (true){
            if(root!=null){
                stack.push(root);
                root=root.getLeftNode();
            }else{
                if(stack.isEmpty()){
                    return;
                }
                if(null==stack.peek().getRightNode()){
                    root=stack.pop();
                    //输出第一个进入的最左侧的数据
                    System.out.print(root.getValue()+"\t");
                    while (root==stack.peek().getRightNode()){
                        root=stack.pop();
                        //输出第二阶段里的数据
                        System.out.print(root.getValue()+"\t");
                        if(stack.isEmpty()){
                            break;
                        }
                    }
                }
                if(!stack.isEmpty()){
                    root=stack.peek().getRightNode();
                }else {
                    root=null;
                }
            }
        }
    }
}
