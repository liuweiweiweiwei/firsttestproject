package com.test.models.Observer.event;

import java.lang.reflect.Method;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:43
 * @description 监听器---
 */
public class EventL {

    private String name;
    private Object target;
    private Object source;
    private String trigger;
    private Method callback;
    private long time;

    public long getTime() {
        return time;
    }

    public String getTrigger() {
        return trigger;
    }

    public EventL setTrigger(String trigger) {
        this.trigger = trigger;
        return this;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public EventL(Object target, Method callback) {
        this.target = target;
        this.callback = callback;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Method getCallback() {
        return callback;
    }

    public void setCallback(Method callback) {
        this.callback = callback;
    }

    @Override
    public String toString() {
        return "EventL{" +
                "name='" + name + '\'' +
                ", target=" + target +
                ", source=" + source +
                ", trigger='" + trigger + '\'' +
                ", callback=" + callback +
                ", time=" + time +
                '}';
    }
}
