package com.test.autimatic;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/17 9:47
 * @description
 */
public class HeapTwo {

    public static void main(String[] args) {
        HeapPQ<Integer> pq = new HeapPQ<>();
        pq.insert(3);
        System.out.println(pq.max());
        pq.insert(9);
        System.out.println(pq.max());
        System.out.println(pq.delMax());
        pq.insert(6);
        System.out.println(pq.max());
        System.out.println(pq.delMax());
    }

    /**
     * 优先队列
     * @param <Key>
     */
    public static class HeapPQ<Key extends Comparable<Key>>{
        private Key[] data;//存储数据
        private int num = 1;//数据量
        private int total = 3;//数据的最大数量长度 默认3

        public HeapPQ(int num) {
            this.num = num + 1;
            this.total = 2*num;
            data = (Key[])new Comparable[this.total];
        }

        public HeapPQ() {
            data = (Key[])new Comparable[this.total];
        }

        /**
         * 是否为空
         * @return
         */
        public boolean isEmpty(){
            return num == 1;
        }

        /**
         * 输出最大值
         * @return
         */
        public Key max(){
            if(num<2){
                throw new RuntimeException("数据为空！");
            }
            return data[1];
        }
        /**
         * 插入数据
         * @param key
         */
        public void insert(Key key){
            if(total<1.5*num){
                Key[] temp = (Key[])new Comparable[total*2];
                System.arraycopy(this.data,0,temp,0,num);
                this.data = temp;
            }
            //赋值数据
            data[num++] = key;
            //上浮数据
            swim(num-1);
        }

        /**
         * 删除最大值
         * @return
         */
        public Key delMax(){
            Key t = data[1];
            //赋值
            data[1] = data[num-1];//替换位置
            data[--num] = null;//为空赋值
            sink(1);//下沉操作
            return t;
        }

        /**
         * 上浮操作维护数据
         * @param n
         */
        private void swim(int n){
            while (n/2>0&&comp(n,n/2)){
                swap(n,n/2);//交换数据
                n /= 2;//重新赋值
            }
        }

        /**
         * 下沉操作维护数据6
         * @param n
         */
        private void sink(int n){
            while (2*n>num){
                int temp =2*n;
                if(temp+1<num&&comp(temp+1,temp)){temp++;}
                if(comp(n,temp)){break;}
                swap(n,temp);
                n = temp;
            }
        }

        /**
         * 比较大小
         * @param i1
         * @param i2
         * @return i1》i2
         */
        private boolean comp(int i1,int i2){
            return data[i1].compareTo(data[i2])>0;
        }

        /**
         * 交换下标数据
         * @param index1
         * @param index2
         */
        private void swap(int index1,int index2){
            Key datum = data[index1];
            data[index1] = data[index2];
            data[index2] = datum;
        }
    }
}
