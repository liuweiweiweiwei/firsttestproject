//package com.test.java8;
//
//import com.ibm.mq.*;
//
//public class Mq {
//    static MQQueueManager qMgr;
//    static int CCSID = 1381;
//    static String queueString = "LOCALQUEUE";
//    MQQueueManager manager;
//    public static void connect() throws MQException {
//        MQEnvironment.hostname = "127.0.0.1";
//        MQEnvironment.channel = "SERVERCONN";
//        MQEnvironment.port = 8927;
//        //MQEnvironment.CCSID = CCSID;
//        //MQ中拥有权限的用户名
//        //MQEnvironment.userID = "MUSR_MQADMIN";
//        //用户名对应的密码
//        MQEnvironment.password = "123456";
//
//        qMgr = new MQQueueManager("MyTest");
//
//    }
//
//    public static void sendMsg(String msgStr) {
//        int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
//        MQQueue queue = null;
//        try {
//            // 建立Q1通道的连接
//            queue = qMgr.accessQueue(queueString, openOptions, null, null, null);
//            MQMessage msg = new MQMessage();// 要写入队列的消息
//            msg.format = MQC.MQFMT_STRING;
//            msg.characterSet = CCSID;
//            msg.encoding = CCSID;
//            // msg.writeObject(msgStr); //将消息写入消息对象中
//            msg.writeString(msgStr);
//            MQPutMessageOptions pmo = new MQPutMessageOptions();
//            msg.expiry = -1; // 设置消息用不过期
//            queue.put(msg, pmo);// 将消息放入队列
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } finally {
//            if (queue != null) {
//                try {
//                    queue.close();
//                } catch (MQException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    public static void receiveMsg() {
//        int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
//        MQQueue queue = null;
//        try {
//            queue = qMgr.accessQueue(queueString, openOptions, null, null, null);
//            System.out.println("该队列当前的深度为:" + queue.getCurrentDepth());
//            System.out.println("===========================");
//            int depth = queue.getCurrentDepth();
//            // 将队列的里的消息读出来
//            while (depth-- > 0) {
//                MQMessage msg = new MQMessage();// 要读的队列的消息
//                MQGetMessageOptions gmo = new MQGetMessageOptions();
//                queue.get(msg, gmo);
//                System.out.println("消息的大小为：" + msg.getDataLength());
//                System.out.println("消息的内容：\n" + msg.readStringOfByteLength(msg.getDataLength()));
//                System.out.println("---------------------------");
//            }
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } finally {
//            if (queue != null) {
//                try {
//                    queue.close();
//                } catch (MQException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    //读取ecm中消息数据
//    public void receiveMsg1(){
//        int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
//        MQQueue queue = null;
//        try {
//            queue = manager.accessQueue(queueString, openOptions, null, null, null);
//            System.out.println("该队列当前的深度为:" + queue.getCurrentDepth());//？？？队列内数据消息数量
//
//            MQMessage mqMessage=new MQMessage();// 要读的队列的消息
//            MQGetMessageOptions mqGetMessageOptions=new MQGetMessageOptions();//读取数据操作
//            queue.get(mqMessage,mqGetMessageOptions);
//            mqMessage.getDataLength();//长度
//            String s=mqMessage.readUTF();//获取数据
//            //mqMessage.readStringOfByteLength(mqMessage.getDataLength());//获取数据
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//
//    public static void main(String[] args) throws MQException {
//        connect();
//        sendMsg("我来测试一下");
//        receiveMsg();
//    }
//}
