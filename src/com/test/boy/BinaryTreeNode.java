package com.test.boy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/5 10:02
 * @description
 */
public class BinaryTreeNode {

    private String value;

    private BinaryTreeNode leftNode;

    private BinaryTreeNode rightNode;

    public void setValue(String value) {
        this.value = value;
    }

    public void setLeftNode(BinaryTreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public void setRightNode(BinaryTreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public String getValue() {
        return value;
    }

    public BinaryTreeNode getLeftNode() {
        return leftNode;
    }

    public BinaryTreeNode getRightNode() {
        return rightNode;
    }

    public BinaryTreeNode(String value, BinaryTreeNode leftNode, BinaryTreeNode rightNode) {
        this.value = value;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }
}
