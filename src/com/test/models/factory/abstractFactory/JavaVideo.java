package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 11:18
 * @description
 */
public class JavaVideo implements IVideo{
    @Override
    public void operateVideo() {
        System.out.println("java 的 video 笔记操作");
    }
}
