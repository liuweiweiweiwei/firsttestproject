package com.test.lambda.lambdamethod;

import com.test.MyLog.LwLog;
import net.sf.cglib.core.Local;

import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Function;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/8 17:11
 * @description
 */
public class LambdaFunction {
    public static void main(String[] args) {
        LambdaFunction lambdaFunction = new LambdaFunction();
        LwLog.logInfo("please input a num : ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        Function<String, Integer> function = lambdaFunction::intToString;
        Integer apply = function.apply(input);
        LwLog.logInfo(String.format(Locale.ROOT, "integer is : %d", apply));
        // my
        LwLog.splitLogInfo("分割线");
        LwLog.logInfo("2.please input a num : ");
        String input2 = scanner.next();
        MyFunctionInterface<String, Integer> function2 = lambdaFunction::intToString;
        Integer apply2 = function2.apply(input2);
        LwLog.logInfo(String.format(Locale.ROOT, "2.integer is : %d", apply2));
        // 2
        LwLog.splitLogInfo("分割线");
        LwLog.logInfo("3.please input a num : ");
        String input3 = scanner.next();
        MyFunctionInterface<Person, Double> personIntegerMyFunctionInterface = lambdaFunction::dooler;
        MyFunctionInterface<Integer, Man> manIntegerMyFunctionInterface = lambdaFunction::manMoney;
        MyFunctionInterface<Integer, Double> integerDoubleMyFunctionInterface
                = personIntegerMyFunctionInterface.beforeApply(manIntegerMyFunctionInterface);
        Double apply1 = integerDoubleMyFunctionInterface.apply(function2.apply(input3));
        System.out.println(apply1);
        // 3
        LwLog.splitLogInfo("分割线");
        LwLog.logInfo("4.please input a num : ");
        String input4 = scanner.next();
        MyFunctionInterface<String, Integer> stringIntegerMyFunctionInterface = Integer::parseInt;
        MyFunctionInterface<Integer, Double> doubleMyFunctionInterface = lambdaFunction::intToDooler;
        MyFunctionInterface<String, Double> stringDoubleMyFunctionInterface
                = doubleMyFunctionInterface.beforeApply(stringIntegerMyFunctionInterface);
        Double apply3 = stringDoubleMyFunctionInterface.apply(input4);
        LwLog.logInfo(String.format(Locale.ROOT, "money to doler is -> %f", apply3));
    }

    public Integer intToString(String s) {
        return Integer.parseInt(s);
    }

    public Double intToDooler(Integer integer) {
        return 6.5 * integer;
    }

    public Man manMoney(Integer integer) {
        Man person = new Man(String.valueOf(integer));
        person.setMoney(integer);
        return person;
    }

    public Double dooler(Person person) {
        return 1.5 * person.money();
    }

    private static class Person{
        private String name;

        private Integer money;

        public Integer getMoney() {
            return money;
        }

        public void setMoney(Integer money) {
            this.money = money;
        }

        public Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        protected Integer money() {
            return 10000;
        }
    }

    private static class Man extends Person{
        private String hobby;

        public Man(String name) {
            super(name);
        }

        public Man(String name, String hobby) {
            super(name);
            this.hobby = hobby;
        }

        public String getHobby() {
            return hobby;
        }

        public void setHobby(String hobby) {
            this.hobby = hobby;
        }

        @Override
        protected Integer money() {
            return super.money() + 10000;
        }
    }

    @FunctionalInterface
    public interface MyFunctionInterface<T, R> {
        R apply(T t);

        default <V> MyFunctionInterface<V, R> beforeApply(MyFunctionInterface<? super V, ? extends T> before) {
            Objects.requireNonNull(before);
            return (V v) -> apply(before.apply(v));
        }
    }
}