package com.test.models.single;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 11:49
 * @description
 */
public class ThreadLocalSingle {

    private ThreadLocalSingle(){}

    private static final ThreadLocal<ThreadLocalSingle> THREAD_LOCAL = new ThreadLocal<ThreadLocalSingle>(){
        @Override
        protected ThreadLocalSingle initialValue() {
            return new ThreadLocalSingle();
        }
    };

    public static ThreadLocalSingle getInstance(){
        return THREAD_LOCAL.get();
    }

}