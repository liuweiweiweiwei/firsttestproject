package com.test.models.gpdecorate.v3;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:38
 * @description
 */
public abstract class BatterDecorator extends BatterCake {

    private BatterCake batterCake;

    public BatterDecorator(BatterCake batterCake) {
        this.batterCake = batterCake;
    }

    @Override
    protected String name() {
        return this.batterCake.name();
    }

    @Override
    protected int price() {
        return this.batterCake.price();
    }
}
