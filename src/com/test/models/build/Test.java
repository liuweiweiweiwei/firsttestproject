package com.test.models.build;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 11:05
 * @description
 */
public class Test {

    public static void main(String[] args) {
        BuildModel buildModel= new BuildModel.BuildFactory()
                .setAge(12)
                .setHobby("bool")
                .setName("小明")
                .build();
        System.out.println(buildModel.toString());
    }
}
