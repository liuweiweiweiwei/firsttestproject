package com.test.thread.mythreadpool;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 20:55
 * @description
 */
public interface RunnablePool {
    void execute(Runnable runnable);

    void shutdown();

    int getInitSize();

    int getMaxSize();

    int getCoreSize();

    int getQueueSize();

    int getActivitySize();

    boolean isShutdown();
}
