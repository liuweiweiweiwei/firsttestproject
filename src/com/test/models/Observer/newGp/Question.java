package com.test.models.Observer.newGp;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:26
 * @description
 */
public class Question {

    private String content;
    private String name;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
