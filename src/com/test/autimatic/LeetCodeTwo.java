package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * leetcode 2
 */
public class LeetCodeTwo {
    public static void main(String[] args) {
        int[] nums1 = {1,2,2,1};
        int[] nums2 = {2,2};
        int[] re=intersect(nums1,nums2);
        System.out.println(Arrays.toString(re));
        //随机排序方法
        int[] paramOne={9,8,7,6,5,4,3,2,1};
        long start=System.currentTimeMillis();
        System.out.println(getSort(paramOne));
        System.out.println(Arrays.toString(paramOne));
        long end=System.currentTimeMillis();
        System.out.println("花费的时间为："+(end-start));
        //亦或
        int[] twoParam={12,2,6,5,7,8,9,5,3,2,7,8,9,12,3};
        System.out.println("亦或"+getSingle(twoParam));
        //前赘述

    }

    /**
     * Jiaoji
     * @param nums1
     * @param nums2
     * @return
     */
    private static int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> end=new LinkedList<>();
        List<Integer> list=new ArrayList<>();
        for (int n2:nums2) {
            list.add(n2);
        }
        for (int n1:nums1) {
            Iterator<Integer> integerIterator=list.iterator();
            while (integerIterator.hasNext()){
                int mid=integerIterator.next();
                if(mid==n1){
                    end.add(n1);
                    integerIterator.remove();
                    break;
                }
            }
        }
        int[] ints=new int[end.size()];
        for (int i = 0; i < ints.length; i++) {
            ints[i]=end.get(i);
        }
        return ints;
    }

    /**
     * 随机排序
     */
    private static int getSort(int[] arr){
        int end=0;
        while (!isSort(arr)){
            sort(arr);
            end++;
        }
        return end;
    }

    /**
     * 是否有序
     * @param arr
     * @return
     */
    private static boolean isSort(int[] arr){
        for (int i = 1; i < arr.length; i++) {
            if(arr[i]<arr[i-1]){
                return false;
            }
        }
        return true;
    }

    /**
     * 排序操作
     * @param arr
     */
    private static void sort(int[] arr){
        int temp;
        for (int i = 0; i <arr.length ; i++) {
            int t= (int) (Math.random() * arr.length);
            temp=arr[i];
            arr[i]=arr[t];
            arr[t]=temp;
        }
    }

    /**
     * 数组中有一个只出现一次的数字找出
     * @param arr 数组
     * @return 数字
     */
    private static int getSingle(int[] arr){
        final int[] result = {0};
        Arrays.stream(arr).forEach(a-> result[0] ^=a);
        return result[0];
    }

    static class Trie {
        private static List<String> strings;

        /** Initialize your data structure here. */
        public Trie() {
            strings=new ArrayList<>();
        }

        /** Inserts a word into the trie. */
        public void insert(String word) {
            strings.add(word);
        }

        /** Returns if the word is in the trie. */
        public boolean search(String word) {
            return strings.contains(word);
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        public boolean startsWith(String prefix) {
            for (String var1:strings) {
                if(var1.startsWith(prefix)){
                    return true;
                }
            }
            return false;
        }
    }

}
