package com.test.models.template.gp.template.course;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 13:44
 * @description
 */
public abstract class NetWorkCourse {

    protected final void createCourse(){
        //发布预习资料
        this.createPreCourse();
        //制作ppt
        this.createPPT();
        //在线直播
        this.liveVideo();
        //提交课件，课堂笔记
        this.postNodte();
        //提交源码
        this.postSource();
        //布置作业--有些有作业有些没作业
        if(this.work()){//钩子方法work
            checkWork();

        }
    }

    /**
     * 钩子方法
     * @return
     */
    protected boolean work(){
        System.out.println("没有作业！！！");
        return false;
    }

    protected abstract void checkWork();

    private void postSource() {
        System.out.println("提交源码");
    }

    private void postNodte() {
        System.out.println("提交课程笔记！");
    }

    private void liveVideo() {
        System.out.println("在线直播授课！");
    }

    private void createPPT() {
        System.out.println("创建备课PPT！");
    }

    private void createPreCourse() {
        System.out.println("分发预习资料！");
    }
}
