package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:01
 * @description 策略的实现----无优惠
 */
public class EmptyStrategy implements PromotionStrategy{


    @Override
    public void doPromotion() {
        System.out.println("无优惠的活动！");
    }
}
