package com.test.classes.classnamespace;

import com.test.MyLog.LwLog;
import com.test.classes.myclassloader.MyClassLoader;

/**
 * @author 25338
 * @date 2022/5/14 11:29
 * @since 1.0
 * description
 */
public class ClassNamespace {
    public static void main(String[] args) throws ClassNotFoundException {
        ClassNamespace classNamespace = new ClassNamespace();
        classNamespace.sampleLoadClass();
        LwLog.splitLogInfo("分隔符");
        classNamespace.differentClassLoad();
    }

    /**
     * 相同类加载器
     * @throws ClassNotFoundException
     */
    private void sampleLoadClass() throws ClassNotFoundException {
        // 获取系统类加载器
        ClassLoader classLoader = ClassNamespace.class.getClassLoader();
        Class<?> aClass = classLoader.loadClass("com.test.classes.classnamespace.TestNameSpace");
        Class<?> bClass = classLoader.loadClass("com.test.classes.classnamespace.TestNameSpace");
        System.out.println(aClass.hashCode());
        System.out.println(bClass.hashCode());
        System.out.println(aClass == bClass);
    }

    /**
     * different class load
     * @throws ClassNotFoundException
     */
    private void differentClassLoad() throws ClassNotFoundException {
        MyClassLoader myClassLoader = new MyClassLoader(null, "F:/file/java/");
        Class<?> aClass = myClassLoader.loadClass("com.test.classes.classnamespace.TestNameSpace");
        MyClassLoader myClassLoaderc = new MyClassLoader(null, "F:/file/java/");
        Class<?> cClass = myClassLoaderc.loadClass("com.test.classes.classnamespace.TestNameSpace");
        ClassLoader classLoader = ClassNamespace.class.getClassLoader();
        Class<?> bClass = classLoader.loadClass("com.test.classes.classnamespace.TestNameSpace");

        //compare
        System.out.println(aClass.getClassLoader());
        System.out.println(bClass.getClassLoader());
        System.out.println(cClass.getClassLoader());
        System.out.println(aClass.hashCode());
        System.out.println(bClass.hashCode());
        System.out.println(cClass.hashCode());
        System.out.println(aClass == bClass);
        System.out.println(aClass == cClass);
    }
}
