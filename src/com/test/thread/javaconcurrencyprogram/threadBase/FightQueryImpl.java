package com.test.thread.javaconcurrencyprogram.threadBase;

import com.test.MyLog.LwLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * join 保证当前线程执行完毕
 * @author 25338
 * @version 1.0
 * @date 2022/4/24 20:19
 */
public class FightQueryImpl extends Thread implements FightQuery {
    private final List<String> fightList = new ArrayList<>();

    private String origin;

    private String destination;

    public FightQueryImpl(String threadName, String origin, String destination) {
        super(threadName);
        this.origin = origin;
        this.destination = destination;
    }

    @Override
    public List<String> getQuery() {
        return this.fightList;
    }

    @Override
    public void run() {
        System.out.printf("%s- query from %s to %s \n", Thread.currentThread().getName(), origin, destination);
        int randomVal = ThreadLocalRandom.current().nextInt(10);
        try {
            TimeUnit.SECONDS.sleep(randomVal);
            this.fightList.add(Thread.currentThread().getName() + "-" + randomVal);
            System.out.printf("thread query %s find success!\n", Thread.currentThread().getName());
        } catch (InterruptedException e) {
            LwLog.errorInfo("find fight failed");
        }
    }

    private static List<String> airport = Arrays.asList("CHINA", "AMERICAN", "INDIA");
    public static void main(String[] args) throws InterruptedException {
        LwLog.splitLogInfo("分割线");
        List<String> resultFightLine = findFromTo("NEYWORK", "SHANGHAI");
        resultFightLine.forEach(System.out::println);
        LwLog.splitLogInfo("分割线");
    }

    private static List<String> findFromTo(String origin, String target) {
        final List<String> result = new ArrayList<>();
        // task
        List<FightQueryImpl> fightQueryList = airport.stream().map(f -> createFight(f, origin, target))
                .collect(Collectors.toList());
        // start task
        fightQueryList.forEach(FightQueryImpl::start);
        // join thread
        fightQueryList.forEach(f -> {
            try {
                f.join();
            } catch (InterruptedException e) {
                LwLog.errorInfo(Thread.currentThread().getName() + "join failed");
            }
        });
        // add query
        fightQueryList.stream().map(FightQuery::getQuery).forEach(result::addAll);
        return result;
    }

    private static FightQueryImpl createFight(String name, String origin, String target) {
        return new FightQueryImpl(name, origin, target);
    }
}
