package com.test.thread.javaconcurrencyprogram.synclinklist;

import com.test.MyLog.LwLog;
import org.apache.poi.ss.formula.functions.Even;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/26 21:57
 * @description
 */
public class EventList {
    private static final int DEFAULT_MAX = 10;

    private final int max_queue;

    private static class Event{}

    private final LinkedList<Event> eventLinkedList = new LinkedList<>();

    public EventList() {
        this(DEFAULT_MAX);
    }

    public EventList(int max_queue) {
        this.max_queue = max_queue;
    }

    /**
     * offer event thing
     *
     * @param event event
     */
    public void offer(Event event) {
        synchronized (eventLinkedList) {
            while (eventLinkedList.size() >= max_queue) {
                LwLog.logInfo(Thread.currentThread().getName() + " : event is full ! place wait amount.");
                try {
                    eventLinkedList.wait();
                } catch (InterruptedException e) {
                    LwLog.errorInfo(Thread.currentThread().getName() + " wait is error!");
                }
            }
            LwLog.logInfo(Thread.currentThread().getName() +  " the new event is add eventLinkList!");
            eventLinkedList.add(event);
            eventLinkedList.notify();
        }
    }

    /**
     * event take
     *
     * @return event e
     */
    public Event take() {
        synchronized (eventLinkedList) {
            while (eventLinkedList.isEmpty()) {
                LwLog.logInfo(Thread.currentThread().getName() + " : event is empty ! place wait amount.");
                try {
                    eventLinkedList.wait();
                } catch (InterruptedException e) {
                    LwLog.errorInfo(Thread.currentThread().getName() + " wait is error!");
                }
            }
            LwLog.logInfo(" the new event is take from eventLinkList!");
            Event event = eventLinkedList.removeFirst();
            eventLinkedList.notifyAll();
            LwLog.logInfo(Thread.currentThread().getName() + " the event " + event + "  is has done eventLinkList!");
            return event;
        }
    }

    public static void main(String[] args) {
        final EventList eventList = new EventList();
        new Thread(() -> {
            while (true) {
                eventList.offer(new EventList.Event());
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    LwLog.errorInfo(" sleep thread is failed : interrupt!");
                }
            }
        }, "PRODUCTS").start();

        new Thread(() -> {
            while (true) {
                eventList.offer(new EventList.Event());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    LwLog.errorInfo("sleep two thread is failed : interrupt!");
                }
            }
        }, "PRODUCTS-TWO").start();

        new Thread(() -> {
            while (true) {
                Event event = eventList.take();
                System.out.println(Thread.currentThread().getName() + " : " + event);
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    LwLog.errorInfo("sleep thread is failed : interrupt!");
                }
            }
        }, "CUSTOMERS").start();
    }
}
