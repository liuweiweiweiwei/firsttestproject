package com.test.models.YuanXing;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 14:45
 * @description 客户端调用
 */
public class Client {

    private ProtoType protoType;

    public Client(ProtoType protoType){
        this.protoType = protoType;
    }

    public ProtoType getProtoType() {
        return protoType.clone1();
    }
}
