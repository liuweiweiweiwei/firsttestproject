package com.test.autimatic;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/4 10:37
 * @description
 */
public class DpLeetCode {

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{6,1,6,4,3,0,2}));
        //dp - 1
        System.out.println(change(5,new int[]{1, 2, 5}));
    }

    public static int maxProfit(int[] prices) {
        int[][][] dp = new int[prices.length][4][2];
        dp[0][1][1] = -prices[0];
        dp[0][0][1] = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            dp[i][0][0] = Math.max(dp[i - 1][0][0], dp[i- 1][3][0]);
            dp[i][0][1] = Math.max(dp[i - 1][0][1], dp[i- 1][1][1]);
            dp[i][3][0] = dp[i- 1][2][0];
            dp[i][1][1] = Math.max(dp[i - 1][3][0],Math.max(dp[i - 1][0][0],0)) - prices[i];
            dp[i][2][0] = Math.max(dp[i - 1][0][1],dp[i - 1][1][1]) + prices[i];
        }
        return Math.max(dp[prices.length - 1][0][0],Math.max(dp[prices.length - 1][2][0],dp[prices.length - 1][3][0]));
    }

    public static int change(int amount, int[] coins) {
        if (amount == 0) {
            return 1;
        }
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int icon : coins) {
            for (int i = 1; i <= amount; i++) {
                if (i >= icon){
                    dp[i] += dp[i - icon];
                }
            }
        }
        return dp[amount];
    }
}
