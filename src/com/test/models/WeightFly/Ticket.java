package com.test.models.WeightFly;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/20 16:07
 * @description 票信息
 */
public interface Ticket {
    void showTicketInfo(String pumb);
}
