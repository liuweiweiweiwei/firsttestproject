package com.test.autimatic;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/3 10:23
 * @description
 */
public class TwoDevide {

    public static void main(String[] args) {
        System.out.println(findMinFibonacciNumbers(19));
    }

    public static int findMinFibonacciNumbers(int k) {
        if(k == 1 || k == 2 || k == 3){return 1;}
        int result = 0;
//        int[] arr = new int[k + 1];
//        arr[0] = 1;arr[1] = 1;int index = 2;
//        for (int i = 2; arr[i - 1] <= k && i <= k; i++,index++) {
//            arr[i] = arr[i - 1] + arr[i - 2];
//            if(arr[i] > k){break;}
//        }
//        int[] arrnew = new int[index];
//        System.arraycopy(arr,0,arrnew,0,index);
//        k = k - arrnew[index - 1];
        while (k != 0){
            k = getNumTwo(k);
            result++;
        }
        return result;
    }

    private static int getNumTwo(int k){
        if(k == 1 || k == 2){ return 0; }
        int a = 1,b = 1,c = 2;
        while (c <= k){
            c = a + b;
            a = b;
            b = c;
        }
        return k - a;
    }

    /**
     *
     * @param arr
     * @param target
     * @return
     */
    private static int getNum(int[] arr,int target){
        int start = 0,end = arr.length;
        while (true){
            int mid = start + (end - start)/2;
            if(arr[mid] == target){
                return arr[mid];
            }else if(arr[mid] > target){
                if(arr[mid - 1] <= target){
                    return arr[mid - 1];
                }else{
                    end = mid - 1;
                }
            }else{
                if(arr[mid + 1] > target){
                    return arr[mid];
                }else if(arr[mid + 1] == target){
                    return arr[mid + 1];
                }else {
                    start = mid + 1;
                }
            }
        }
    }


}
