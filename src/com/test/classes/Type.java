package com.test.classes;

import com.alibaba.fastjson.JSON;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/11/6 11:09
 * @description
 */
public class Type {
    private String name;
    private Integer age;
    private String title;

    public Type() {
    }

    public Type(String name, Integer age, String title) {
        this.name = name;
        this.age = age;
        this.title = title;
    }

    public String getName() {
        JSON.toJSONString(name);
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
