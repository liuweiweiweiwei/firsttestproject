package com.test.Files;

import java.io.*;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/10 14:28
 * @description
 */
public class FileDealManage {
    public static ExecutorService service = new ThreadPoolExecutor(2,2,30, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100),new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
//        FileDealManage manage = new FileDealManage();
//        manage.flow("E:\\wecharfile","E:\\weFile");
//        service.shutdown();
        fileDeal("E:\\wecharfile\\data.txt");
    }

    public final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static ThreadLocal<AtomicInteger> local = new InheritableThreadLocal<AtomicInteger>();



    public void flow(String filepath,String newPath){
        File file = new File(newPath);
        File fileOld = new File(filepath);
        if(!file.exists()||!file.isDirectory()){
            throw new RuntimeException("生成地址---所选文件夹路径不存在！--->>>>>>" + newPath);
        }
        if(!fileOld.exists()||!fileOld.isDirectory()){
            throw new RuntimeException("目标路径---所选文件夹路径不存在！--->>>>>>" + fileOld);
        }
        //调用生成监听-流程启动
        local.set(new AtomicInteger(0));
        dealFlow(filepath,null,newPath);
        System.out.println("生成完成！");
        //监听替换完成状态
        while (local.get().get()!=0){
            System.out.println("等待5秒监听："+local.get());
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        dealZipYasuo(null,newPath);
        System.out.println("压缩完成");
        //删除操作
        File[] fileT = new File(newPath).listFiles();
        if(fileT != null && fileT.length>0) {
            for (File f:fileT){
                if(f.isDirectory()){
                    System.out.println(deleteFile(f));
                }
            }
        }
        System.out.println("删除完成！");
        //监听执行
        System.out.println("执行完成！");
    }

    private void dealFlow(String filepath,String zipMidPath,String newPath){
        //1.文件夹
        File file = new File(filepath);
        if(!file.exists()){
            System.out.println("所选文件、文件夹不存在！");
            return;
        }//如果是文件夹
        if(file.isDirectory()) {
            //获取文件集合
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File childFile : files) {
                    //为生成的文件创建文件夹-判断文件格式---也可以后边递归处理--暂时不处理
                    if (childFile.isDirectory()) {
                        System.out.println("文件夹不处理!");
                        continue;
                    }
                    //处理文件txt，csv，zip
                    deal(childFile,zipMidPath,newPath);
                }
            }
            //如果是文件
        }else{
            deal(file,zipMidPath,newPath);
        }
    }

    /**
     * 处理流程！
     * @param childFile
     * @param zipMidPath
     */
    private void deal(File childFile,String zipMidPath,String newPath){
        //txt流程处理
        if (childFile.getName().endsWith(".txt")||childFile.getName().endsWith(".csv")) {
            String pathNewTxt = createFile(childFile, newPath);
            copy(childFile.getPath(),newPath+File.separator+childFile.getName());
            //写入文件
            dealFile(childFile.getName(), pathNewTxt,local.get());
        //csv流程处理
        }  else if (childFile.getName().endsWith(".zip")) {
            //解压文件-到文件夹zip中-->原filepath/date/zip/压缩文件夹名zipName/解压后的全部文件
            String zipFilePath = newPath + File.separator + childFile.getName().replace(".zip","");
            //创建文件夹
            createMkdir(zipFilePath);
            //解压文件到目录内
            zipJieYa(zipFilePath,childFile.getPath());
            //处理流程
            dealFlow(zipFilePath, null, zipFilePath + File.separator +
                    childFile.getName().replace(".zip",""));
        }
    }

    private static void copy(String source,String end){
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;
        try{
            outputStream = new FileOutputStream(end);
            inputStream = new FileInputStream(source);
            byte[] bytes = new byte[1024];
            int b;
            while ((b = inputStream.read(bytes))!=-1){
                outputStream.write(bytes,0,b);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeI(inputStream);
            closeO(outputStream);
        }
    }



    /**
     * 最后的压缩操作
     * @param newFile
     */
    private void dealZipYasuo(String type,String newFile){
        String zipPath = newFile;
        //监听线程全部执行完成后------压缩文件filepath/date/zip/--判断zip文件夹是否存在---存在则将内部文件夹压缩
        File fileZip = new File(zipPath);
        if(!fileZip.exists()){
            return;
        }
        File[] files = fileZip.listFiles();
        //将zip文件夹内的所有文件夹压缩
        if(files != null && files.length>0) {
            for (File fTemp:files) {
                if(fTemp.isDirectory()){
                    String name = zipPath + File.separator + fTemp.getName() + File.separator + fTemp.getName();
                    //压缩文件夹
                    zipYaSuo(name,newFile);
                }
            }
        }
    }

    /**
     * 创建文件
     * @param childFile
     * @param filePath
     */
    private String createFile(File childFile,String filePath){
        //创建文件--1,2
        String pathNameNew = filePath;
//        if(fileType.equals("txt")||fileType.equals("csv")){
//            pathNameNew = filePath + File.separator + childFile.getName() ;
//        }else{
//            pathNameNew = childFile.getPath().replace(childFile.getName(),"") + File.separator + fileType ;
//        }
        //创建文件夹
        createMkdir(pathNameNew);
        //创建文件
        pathNameNew = pathNameNew + File.separator + childFile.getName();
        createFile(pathNameNew);
        //返回文件名
        return pathNameNew;
    }

    /**
     *  文件内的替换生成操作
     * @param fileName
     * @param endName
     */
    private void dealFile(String fileName,String endName,AtomicInteger integer){
        service.execute(new Runnable() {
            @Override
            public void run() {
                //自增
                integer.incrementAndGet();
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //复制生成文件
                System.out.println(fileName + ":" + endName);
                //自减
                integer.decrementAndGet();
            }
        });
    }


    //工具类--------------------------------------------------------------------------------------------------------------------


    public static boolean judge(String param){
        return param==null||param.length()==0;
    }

    /**
     * 创建文件夹
     * @param path
     */
    public static void createMkdir(String path){
        File newTxtFile = new File(path);
        if(!newTxtFile.exists()){
            boolean mkdir = newTxtFile.mkdirs();
            System.out.println("创建状态："+mkdir+":"+newTxtFile.getPath());
        }
    }

    /**
     * 创建文件
     * @param filename
     */
    public static void createFile(String filename){
        File file = new File(filename);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("创建文件异常:" + filename);
                e.printStackTrace();
            }
        }
    }

    /**
     * zip解压缩path
     * @param zipPath
     * @return
     */
    public static void zipJieYa(String zipPath,String old){
        ZipInputStream inputStream = null;
        ZipEntry nextEntry;
        try{
            inputStream = new ZipInputStream(new FileInputStream(old));
            while ((nextEntry = inputStream.getNextEntry())!= null) {
                FileOutputStream outputStream = null;
                try {
                    //解压后的文件夹
                    String tempName = zipPath + File.separator + nextEntry.getName();
                    //创建文件
                    createFile(tempName);
                    outputStream = new FileOutputStream(tempName);
                    byte[] bytes = new byte[1024];
                    int b;
                    while ((b = inputStream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, b);
                    }
                }finally {
                    closeO(outputStream);
                }
            }
            inputStream.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            closeI(inputStream);
        }
    }



    /**
     * 压缩zip文件
     */
    public static void zipYaSuo(String filePath,String endPath){
        //创建文件
        endPath += filePath.substring(filePath.lastIndexOf(File.separator)) + ".zip";
        createFile(endPath);
        ZipOutputStream outputStream = null;
        try{
            //压缩操作
            outputStream = new ZipOutputStream(new FileOutputStream(endPath));
            ysSuo(outputStream,new File(filePath));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeO(outputStream);
        }
    }

    /**
     * 压缩具体操作
     * @param outputStream
     * @param file
     */
    public static void ysSuo(ZipOutputStream outputStream,File file){
        if(!file.exists()){
            return;
        }
        try {
            //如果是文件--则遍历压缩
            if(file.isDirectory()){
                File[] files = file.listFiles();
                if(files != null && files.length > 0) {
                    for (File ft : files) {
                        ysSuo(outputStream,ft);
                    }
                }
            }else{
                FileInputStream inputStream = null;
                try {
                    //具体压缩操作
                    inputStream = new FileInputStream(file.getPath());
                    outputStream.putNextEntry(new ZipEntry(file.getName()));
                    byte[] bytes = new byte[1024];
                    int b;
                    while ((b = inputStream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, b);
                    }
                    outputStream.closeEntry();
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    closeI(inputStream);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 删除遍历
     */
    public static void delete(File newPath){
        for (File ftemp : newPath.listFiles()) {
            if (ftemp.isDirectory()) {
                delete(ftemp);
            } else {
                ftemp.delete();
            }
        }
        newPath.delete();
    }

    public boolean deleteFile(File file){
        if(!file.exists()){
            return false;
        }
        if(file.isFile()){
            return file.delete();
        }else{
            for (File t:file.listFiles()) {
                return deleteFile(t);
            }
            return file.delete();
        }
    }

    /**
     * 关闭
     * @param inputStream
     */
    public static void closeI(InputStream inputStream){
        if(inputStream != null){
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 关闭
     * @param outputStream
     */
    public static void closeO(OutputStream outputStream){
        if(outputStream != null){
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 查询数据编码信息
     * @param file
     */
    private static void judgeFileType(String file){
        File fileType = new File(file);
        if(!fileType.exists()){
            return;
        }

    }

    private static final String[] CHAR_SET = {"ASCII","GB2312","UTF-8","GB18030","GBK","UTF-16","Big5","Windows-1252","ISO-8859-1"};

    /**
     * 文件处理
     * @param filepath 文件路径
     */
    private static void fileDeal(String filepath){
        for (String char_set:CHAR_SET) {
            judgeFileType(filepath,char_set);
        }
    }

    /**
     * 查询编码格式
     * @param file
     * @param charset
     */
    private static void judgeFileType(String file,String charset){
        File fileType = new File(file);
        if(!fileType.exists()){
            return;
        }
        BufferedReader inputStream = null;
        StringBuilder builder = new StringBuilder();
        try{
            inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(fileType), Charset.forName(charset)));
            builder.append(inputStream.readLine());
            //输出数据
            System.out.println(charset + ":" + builder.toString());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeR(inputStream);
        }
    }

    /**
     * 关闭流
     */
    private static void closeR(Reader reader){
        if(reader != null){
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
