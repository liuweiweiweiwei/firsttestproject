package com.test.java8;

import redis.clients.jedis.Jedis;

import java.util.concurrent.TimeUnit;

public class RedisTest {
    public static void main(String[] args) throws InterruptedException {
        Jedis jedis=RedisConfig.getConnection();
        jedis.set("key","0");
        System.out.println(jedis.get("key"));
        Coun coun=new Coun(Integer.valueOf(jedis.get("key")));

        for (int i = 0; i <= 50; i++) {
            ThreadArem threadArem=new ThreadArem(coun);
            Thread thread=new Thread(threadArem);
            thread.start();
            jedis.set("key",coun.getCon()+"");
        }
       /* for (int i = 0; i < 50; i++) {
            threadArem.join();
        }*/
        System.out.println(jedis.get("key"));
    }
    static class ThreadArem extends Thread{
        private Coun num;//测试数据
        ThreadArem(Coun num){
            this.num=num;
        }

        @Override
        public void run() {
            for (int i = 0; i <100 ; i++) {
                num.addCon();
            }
            try {
                TimeUnit.MILLISECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Coun{
        private int con=0;
        public Coun(int con){
            this.con=con;
        }
        public void addCon(){
            con++;
        }
        public int getCon(){
            return con;
        }
    }
}
