package com.test.thread.newlw.asynchrinizeds;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/19 10:54
 * @description
 */
public class SyncDemo {

    public static void main(String[] args) {
        SyncDemo demo1 = new SyncDemo();
        SyncDemo demo2 = new SyncDemo();
        //TODO get1与get2等价---两种代码表示--两种作用范围
        //不互斥
        new Thread(()-> demo1.get0()).start();
        new Thread(()-> demo2.get0()).start();
        //互斥
        new Thread(()-> SyncDemo.get2()).start();
        new Thread(()-> SyncDemo.get2()).start();


    }

    public synchronized void get0(){
        //TODO 方式一实例之内 -- 只锁定当前《对象》级别 -- 等价于 -- 下边这一行
//        synchronized (this){} -可以控制粒度--保护存在线程安全的代码 -- 对象的生命周期
        System.out.println("sync的实例锁定！");
    }


    public synchronized void get1(){
        //TODO 方式二静态代码块 -- -- 类级别 --跨对象
        synchronized (SyncDemo.class) {
            System.out.println("sync的实例锁定！");
        }
    }

    public synchronized static void get2(){
        //TODO 方式二静态方法 -- 类级别 --跨对象
        System.out.println("sync的实例锁定！");

    }

}
