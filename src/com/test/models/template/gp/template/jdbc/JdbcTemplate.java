package com.test.models.template.gp.template.jdbc;

import javax.lang.model.element.VariableElement;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 14:03
 * @description
 */
public abstract class JdbcTemplate {

    private DataSource dataSource;

    public JdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * 查询模板
     * @param sql
     * @param rowMapper
     * @param value
     * @return
     */
    public List<?> executeQuery(String sql,RowMapper<?> rowMapper,Object[] value){
        try {
            //获取连接
            Connection connection = this.getConnection();
            //创建与剧集
            PreparedStatement statement = this.createPrepareStatement(connection,sql);
            //执行语句
            ResultSet resultSet = this.executeSql(statement,value);
            //参数转换
            List<?> list = this.paresResultSet(resultSet,rowMapper);
            //关闭连接结果集
            this.closeSet(resultSet);
            //关闭statement
            this.closeStatement(statement);
            //关闭连接
            this.closeConnection(connection);
            //返回数据
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void closeConnection(Connection connection) throws SQLException {
        //数据库连接池
        connection.close();
    }

    protected void closeStatement(PreparedStatement statement) throws SQLException {
        statement.close();
    }

    protected void closeSet(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }

    private List<?> paresResultSet(ResultSet resultSet, RowMapper<?> rowMapper) throws Exception {
        List<Object> result = new ArrayList<>();
        int rowNum = 1;
        while (resultSet.next()){
            result.add(rowMapper.mapRow(resultSet, rowNum++));
        }
        return result;
    }

    private ResultSet executeSql(PreparedStatement ps,Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            ps.setObject(i,values[i]);
        }
        return ps.executeQuery();
    }

    protected PreparedStatement createPrepareStatement(Connection connection, String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }
}
