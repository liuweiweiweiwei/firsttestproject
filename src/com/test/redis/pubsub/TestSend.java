package com.test.redis.pubsub;

import redis.clients.jedis.Jedis;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/31 14:27
 * @description
 */
public class TestSend {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.175.1.196",6379);
        System.out.println(jedis.publish("tw1","123"));
        System.out.println(jedis.publish("tw2","我命由我不由天！"));
    }
}
