package com.test.models.delegite.simple;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 16:37
 * @description
 */
public class EmployeeB implements IWorkers {
    @Override
    public void work() {
        System.out.println("我是员工B：我擅长加工！");
    }
}
