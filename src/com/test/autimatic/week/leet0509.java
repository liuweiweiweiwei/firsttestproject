package com.test.autimatic.week;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/8 10:33
 * @description
 */
public class leet0509 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left = new TreeNode(8);
        TreeNode right = new TreeNode(5);
        TreeNode rightl = new TreeNode(1);
        TreeNode rightr = new TreeNode(0);
        root.left = left;
        root.right = right;
        left.left = rightl;
        left.right = rightr;
        right.right = new TreeNode(6);
        averageOfSubtree(root);
    }

    public static String largestGoodInteger(String num) {
        Integer result = -1;
        for (int i = 0; i < num.length() - 2; i++) {
            if (num.charAt(i) == num.charAt(i + 1) && num.charAt(i) == num.charAt(i + 2)) {
                result = Math.max(result, Integer.parseInt(num.substring(i, i + 3)));
            }
        }
        return result == -1 ? "" : result == 0 ? "000" : String.valueOf(result);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    private static int result = 0;
    public static int averageOfSubtree(TreeNode root) {
        if (root == null) {
            return result;
        }
        int[] cur = new int[2];
        getNumAndValueRoot(root, cur);
        if (root.val == cur[1] / cur[0]) {
            result++;
        }
        averageOfSubtree(root.left);
        averageOfSubtree(root.right);
        return result;
    }

    private static void getNumAndValueRoot(TreeNode root, int[] node) {
        if (root == null) {
            return;
        }
        node[0]++;
        node[1] += root.val;
        getNumAndValueRoot(root.left, node);
        getNumAndValueRoot(root.right, node);
    }

    public static boolean hasValidPath(char[][] grid) {
        List[][] sets = new ArrayList[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                int aChar = grid[i][j] == '(' ? -1 : 1;
                List<Integer> list = new ArrayList<>();
                if (i == 0 && j == 0) {
                    list.add(aChar);
                } else if (i == 0) {
                    sets[i][j - 1].forEach(s -> list.add(Integer.parseInt(String.valueOf(s)) + aChar));
                } else if (j == 0) {
                    sets[i - 1][j].forEach(s -> list.add(Integer.parseInt(String.valueOf(s)) + aChar));
                } else {
                    sets[i][j - 1].forEach(s -> list.add(Integer.parseInt(String.valueOf(s)) + aChar));
                    sets[i - 1][j].forEach(s -> list.add(Integer.parseInt(String.valueOf(s)) + aChar));
                }
                sets[i][j] = list;
            }
        }
        return sets[grid.length - 1][grid[0].length - 1].contains(0);
    }

}
