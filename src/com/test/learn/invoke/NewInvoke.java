package com.test.learn.invoke;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/23 8:39
 * @description
 */
public class NewInvoke {
    public static void main(String[] args) {

    }
    /**
     * test
     */
    public static void getInstance(){
        //实例化对象Class 调用instance实例化---默认无参构造
        try {
            Class<?> hashClass=Class.forName("java.util.HashMap");
            HashMap hashMap=(HashMap)hashClass.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        //constructor构造实例对象---可以有参构造传入constructor中
        try {
            Class<?> hashClassTwo=Class.forName("java.util.HashMap");
            Constructor<?> constructor=hashClassTwo.getConstructor();
            constructor.setAccessible(true);//懒汉式额韩式强制加载对象
            HashMap hashMap=(HashMap) constructor.newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        //注释：1.field类变量包括public和非public变量--还包括集成的变量
        //2.constructor类变量（构造器）包括public和非public变量可以通过setaccessible方法强制访问构造器，实例化对象，破坏懒汉恶汉方式
        //3.Method方法包括public和非public 不能访问父类protect方法
        Field[] fields=HashMap.class.getFields();//获取变量
    }
}
