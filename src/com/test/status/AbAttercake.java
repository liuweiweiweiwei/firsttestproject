package com.test.status;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/9/29 15:41
 * @description 组件类
 */
public abstract class AbAttercake {
    protected abstract String getDescr();
    protected abstract int scope();

    public AbAttercake() {
    }
    public static void main(String[] args) {
        AbAttercake abAttercake=new Battercake();
        abAttercake=new EggDecorator(abAttercake);
        abAttercake=new EggDecorator(abAttercake);
        abAttercake=new SausageDecorator(abAttercake);
        //结果：
        System.out.println(abAttercake.getDescr()+"销售价格为："+abAttercake.scope());
    }
}
//具体组件实现类
 class Battercake extends AbAttercake{

    @Override
    protected String getDescr() {
        return "煎饼";
    }

    @Override
    protected int scope() {
        return 5;
    }
}
//抽象装饰器类
 class AbstractDecorate extends AbAttercake{
    private AbAttercake abAttercake;
    public AbstractDecorate(AbAttercake abAttercake) {
        this.abAttercake=abAttercake;
    }

    @Override
    protected String getDescr() {
        return this.abAttercake.getDescr();
    }

    @Override
    protected int scope() {
        return this.abAttercake.scope();
    }
}
//具体装饰器实现类egg
  class EggDecorator extends AbstractDecorate{

    public EggDecorator(AbAttercake abAttercake) {
        super(abAttercake);
    }

    @Override
    protected String getDescr() {
        return super.getDescr()+"加一个鸡蛋";
    }

    @Override
    protected int scope() {
        return super.scope()+1;
    }
}
//具体装饰器实现类egg
 class SausageDecorator extends AbstractDecorate{

    public SausageDecorator(AbAttercake abAttercake) {
        super(abAttercake);
    }

    @Override
    protected String getDescr() {
        return super.getDescr()+"加两个香肠";
    }

    @Override
    protected int scope() {
        return super.scope()+2;
    }
}


