package com.test.thread.javaconcurrencyprogram.threadexception;

import com.test.MyLog.LwLog;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/8 18:30
 * @description 异常 ：main-system-system error
 */
public class ThreadException {

    public static void main(String[] args) throws IOException {
//        catchThreadException();
//        testUncatchException();
//        hookThread();
        testHook();
    }

    private static void catchThreadException() {
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            LwLog.outErr(Thread.currentThread().getName() + "has a exception : " + e.getMessage());
            e.printStackTrace();
        });
        final Thread thread = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
            }
            // throw exception
//            int error = 1 / 0;
        }, "CURRENT_THREAD");
        thread.start();
    }

    private static void testUncatchException() {
        // wu全局thread 捕获
        ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
        LwLog.logInfo(threadGroup.getName());
        LwLog.logInfo(threadGroup.getParent().getName());
        LwLog.logInfo(threadGroup.getParent().getParent().getName());
        final Thread thread = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
            }
            // throw exception
            int error = 1 / 0;
        }, "UN_CHECK_CURRENT_THREAD");
        thread.start();
    }

    private static void hookThread() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                LwLog.logInfo(Thread.currentThread().getName() + " the hook thread is running");
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "HOOK_THREAD"));
        LwLog.logInfo(Thread.currentThread().getName() + " the hook thread is exit!");
    }

    private static void testHook() throws IOException {
        final String pathLockBase = "F:/file/";
        final String pathEnd = "test.lock";
        Path path = Paths.get(pathLockBase, pathEnd);
        // 校验文件存在
        if (path.toFile().exists()) {
            throw new RuntimeException("file is exit,please delete it");
        }
        Files.createFile(path);
        //程序退出删除文件
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            boolean delete = path.toFile().delete();
            LwLog.logInfo("delete temp lock file end status :" + delete);
        }));
        // 运行
        try {
            LwLog.logInfo(Thread.currentThread().getName() + " is running");
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
