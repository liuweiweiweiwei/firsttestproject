package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:30
 * @description
 */
public class CourseFactory {


    /**
     * 通过保全路径名获取对象
     * @param className
     * @return
     */
    public ICourse getBeanCourseByName(String className) {
        if(className != null && !"".equals(className.trim())){
            try {
                ICourse o = (ICourse)Class.forName(className).newInstance();
                return o;
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    /**
     * 通过类名获取对象
     * @param tClass
     * @param <T>
     * @return
     */
    public <T,E extends T> T getBeanByClass(Class<E> tClass){
        if(tClass != null){
            try {
                return tClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return null;
    }
}
