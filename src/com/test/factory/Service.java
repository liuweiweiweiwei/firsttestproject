package com.test.factory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 10:09
 * @description
 */
public class Service {

    public static void carry(String head){
        try {
            Class<?> aClass = Class.forName("com.test.factory." + Factory.name(head));
            Factory o = (Factory) aClass.newInstance();
            System.out.println(o.getSomeThing());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        carry("Date");
        carry("Time");
        carry("Int");
    }
}
