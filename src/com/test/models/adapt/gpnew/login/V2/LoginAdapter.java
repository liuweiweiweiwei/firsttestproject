package com.test.models.adapt.gpnew.login.V2;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 15:38
 * @description 适配器里此接口可有可无
 * 木板木事一定是抽象类，而这里仅仅是个接口
 */
public interface LoginAdapter {

    boolean support(Object adapter);

    String login(String id,String name);
}
