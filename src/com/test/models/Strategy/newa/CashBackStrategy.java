package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:03
 * @description
 */
public class CashBackStrategy implements  PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("直接返现给客户账户120元！！！");
    }
}
