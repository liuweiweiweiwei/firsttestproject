package com.test.models.agent.dbsource.dynamic;

import com.test.models.agent.dbsource.DynaticDataSourceEntity;
import com.test.models.agent.dbsource.OrderServiceProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Date;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 10:31
 * @description
 */
public class DynamicProxy implements InvocationHandler {
    private Object object;


    /**
     * 代理对象的生产
     * @param proxyObj
     * @return
     */
    public Object getInstance(Object proxyObj) {
        this.object = proxyObj;
        Class<?> aClass = proxyObj.getClass();
        return Proxy.newProxyInstance(aClass.getClassLoader(),
                aClass.getInterfaces(),this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before(args[0]);
        Object invoke = method.invoke(object, args);
        after();
        return invoke;
    }

    /**
     * target为order
     * @param target
     */
    public void before(Object target){
        System.out.println("proxy before method");
        //切换数据源
        long time = 0;
        try {
            time = (Long) target.getClass().getMethod("getTime").invoke(target);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        int year = Integer.parseInt(OrderServiceProxy.simpleDateFormatThreadLocal.get().format(new Date(time)));
        DynaticDataSourceEntity.set(year);
        System.out.println("动态代理对象自动分配到【" + year + "】中！");

    }

    public void after(){
        System.out.println("after proxy invoke method");
        //还原
        DynaticDataSourceEntity.restore();
    }

}
