package com.test.java8;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TouLianThree {
    public enum files{
        //地址
        PATHS("E:\\writeEfile\\");
        private String path;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        files(String path) {
            this.path = path;
        }
    }
    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime=LocalDateTime.parse("2020-04-28 14:30:26",dateTimeFormatter);
        LocalDateTime localDateTime1=LocalDateTime.now();
        System.out.println(localDateTime1.compareTo(localDateTime));
        System.out.println(localDateTime.getDayOfWeek());
        List<Item> items = Arrays.asList(
                new Item("apple", 10, new BigDecimal(20.5)),
                new Item("apple", 20, new BigDecimal(32.5)),
                new Item("orange", 30, new BigDecimal(13.5)),
                new Item("orange", 20, new BigDecimal(32.5)),
                new Item("orange", 10, new BigDecimal(63.5)),
                new Item("orange", 50, new BigDecimal(41.5)),
                new Item("peach", 20, new BigDecimal(26.5)),
                new Item("peach", 30, new BigDecimal(32.5)),
                new Item("peach", 40, new BigDecimal(24.5)),
                new Item("peach", 10, new BigDecimal(12.5))
        );
        BigDecimal bigDecimal=BigDecimal.valueOf(1.25);
        BigDecimal bigDecimal1=BigDecimal.valueOf(2.15);
        DecimalFormat decimalFormat=new DecimalFormat("0.000");
        System.out.println(1+":");
        System.out.println(bigDecimal1.subtract(bigDecimal));

        System.out.println(Optional.ofNullable(null).orElse('0'));
        LocalDate localDate=LocalDate.parse("2020-05-10 11:20:35",dateTimeFormatter);
        System.out.println(localDate);
        // 分组
        Map<BigDecimal, List<Item>> groupByPriceMap = items.stream()
                .collect(Collectors.groupingBy(Item::getPrice));
        groupByPriceMap.forEach((key1, value1) -> System.out.println(decimalFormat.format(key1) + ":123" + value1));
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
        String s=null;
        Arrays.asList(Optional.ofNullable(s).orElse("").split(",")).forEach(e-> System.out.println(e+"000"));

        Map<Person,String> stringMap=new TreeMap<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return (o2.getAge()+"").compareTo(o1.getAge()+"");
            }
        });
        stringMap.put(new Person(12,"min"),"123");
        stringMap.put(new Person(2,"mid"),"234");
        stringMap.put(new Person(10,"max"),"345");
        stringMap.entrySet().forEach(e-> System.out.println(e.getKey().getName()+":"+e.getValue()));

        Path path= Paths.get(files.PATHS.getPath());
        path.forEach(p->{
            System.out.println(p.getFileName());
        });

       /* List<byte[]> stringList= Lists.newArrayListWithExpectedSize(5);
        try (Stream<Path> stream= java.nio.file.Files.walk(path,1)){
            stream.forEach(fil->{
                if(!fil.toFile().isDirectory()){
                    try {
                        byte[] bytes=java.nio.file.Files.readAllBytes(fil);
                        stringList.add(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        /*stringList.forEach(e -> {
            try {
                System.out.print(new String(e, "GBK"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        });*/
        String str="gasoline txt";
        //String patter=".*up.*";
        String patter="[a-z]{8}[\\u3000|\\u0020|\\u00A0][a-z]{3}";
        Pattern pattern=Pattern.compile(patter);
        System.out.println(pattern.toString());
        Matcher matcher=pattern.matcher(str);
        if(matcher.matches()){
            System.out.println(1);
        }
        String s1="测试数据";
        String reg="[^\\x00-\\xff]{4}";
        Pattern pattern1=Pattern.compile(reg);
        Matcher matcher1=pattern1.matcher(s1);
        System.out.println(matcher1.matches());

        String value="1.566";
        BigDecimal bigDecimal2=new BigDecimal(value).setScale(1,BigDecimal.ROUND_UP);
        BigDecimal bigDecimal3=new BigDecimal(value).setScale(1,BigDecimal.ROUND_DOWN);
       // BigDecimal bigDecimal9=new BigDecimal(value).setScale(1,BigDecimal.ROUND_CEILING);
       // BigDecimal bigDecimal4=new BigDecimal(value).setScale(1,BigDecimal.ROUND_FLOOR);
        BigDecimal bigDecimal5=new BigDecimal(value).setScale(1,BigDecimal.ROUND_HALF_UP);
        //BigDecimal bigDecimal6=new BigDecimal(value).setScale(1,BigDecimal.ROUND_HALF_DOWN);
        //BigDecimal bigDecimal7=new BigDecimal(value).setScale(1,BigDecimal.ROUND_HALF_EVEN);
        //BigDecimal bigDecimal8=new BigDecimal(value).setScale(1,BigDecimal.ROUND_UNNECESSARY);
        /*DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
        String strVal = decimalFormat1.format(bigDecimal2);
        String strVa2 = decimalFormat1.format(bigDecimal3);
        String strVa3 = decimalFormat1.format(bigDecimal4);
        String strVa4 = decimalFormat1.format(bigDecimal5);
        String strVa5 = decimalFormat1.format(bigDecimal6);
        String strVa6 = decimalFormat1.format(bigDecimal7);+bigDecimal3+"::"+bigDecimal4+"::"+bigDecimal5+"::"+
                bigDecimal6+"::"+bigDecimal7+"::"+bigDecimal8+"::"
        String strVa7 = decimalFormat1.format(bigDecimal8);+bigDecimal9+"::"
        String strVa8 = decimalFormat1.format(bigDecimal9);*/
        System.out.println(bigDecimal2+"::"+bigDecimal3+","+bigDecimal5);
        String s2=null;
        double a=Double.parseDouble(Optional.ofNullable(s2).orElse(new BigDecimal("0.00").toString()));
        BigDecimal bigDecimal4=new BigDecimal(a).setScale(2,BigDecimal.ROUND_DOWN);
        System.out.println(bigDecimal4);
        System.out.println("------------------------------------------------------------------");
        List<Student> strings= Arrays.asList(
                new Student(10,"2010-01-02"),
                new Student(10,"1998-12-29"),
                new Student(8,"2020-02-12"),
                new Student(8,"2019-12-30"),
                new Student(10,"2008-05-28"),
                new Student(8,null),
                new Student(8,"2020-10-05"),
                new Student(10,null),
                new Student(10,""));
        System.out.println("---------------------------------------");
        Map<Integer,List<Student>> ends=strings.stream()
                .peek(stu->stu.setAge(stu.getAge()+5))
                .sorted((o1,o2)->Optional.ofNullable(o2.getName()).orElse("")
                        .compareTo(Optional.ofNullable(o1.getName()).orElse("0000-00-00")))
                .collect(Collectors.groupingBy(Student::getAge));
        ends.forEach((key,valu)->{
            System.out.println(key);
            System.out.println(valu);
        });
        /*ends.forEach(System.out::println);*/
        /*strings.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return Optional.ofNullable(o2.getName()).orElse("0000-00-00").compareTo(Optional.ofNullable(o1.getName()).orElse("0000-00-00"));
            }
        });
        strings.forEach(System.out::println);*/
        System.out.println("------------------------------------------------------------");
        String[] doubles={"12.50","1.50","7.79","6.11","18.10","-16"};
        double d=Arrays.stream(doubles).mapToDouble(daNum->Double.parseDouble(Optional.ofNullable(daNum).orElse("0.00"))).sum();
        System.out.println(d);
        BigDecimal bigDecimal6=new BigDecimal(d);
        DecimalFormat decimalFormat1=new DecimalFormat("0.00");
        System.out.println(decimalFormat1.format(bigDecimal6));


        BigDecimal stra=new BigDecimal(d).setScale(2,BigDecimal.ROUND_HALF_DOWN);
        System.out.println(stra);

        System.out.println(getListTest());
        String s3="a";
        System.out.println(s3.getClass());
        System.out.println(Character.isDigit(s3.charAt(0)));
        System.out.println(Integer.parseInt("9")+"输a出的数字是");

        Student student=new Student();
        String name="";
        Integer age=2;
        if(null!=name){
            student.setName(name);
        }
        student.setAge(age);
        System.out.println(student.toString());






    }



    public static List<Integer> getListTest(){
        return Collections.emptyList();
    }

    //@XStreamAlias 别名注解；
    //@XStreamAsAttribute//转换成属性，而不是子标签
    //@XStreamOmitField 忽略字段
    //@XStreamConverter注入转换器（转换器要求无参构造器）
    //@XStreamImplicit 忽略集合名称标签
    /**
     * 将Java类转化为string状态的xml代码
     * @param o 参数·
     * @return 返回数·
     */
    public static String toXml(Object o){
        XStream xStream=new XStream();
        //自动加载所有XStream注解的bean，与上面的区别是上面加载指定的bean
        xStream.autodetectAnnotations(true);
        //加载指定有XStream注解的bean
        xStream.processAnnotations(o.getClass());
        return xStream.toXML(o);
    }

    /**
     * 将xml格式转化为object
     * @param c 要转化成的类
     * @param datas xml参数
     * @return 返回转化结果
     */
    public static Object toBean(Class c,String datas){
        XStream xStream=new XStream(){
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypes(new Class[]{Student.class,Person.class,Item.class});
        xStream.processAnnotations(new Class[]{c});
        xStream.autodetectAnnotations(true);
        return xStream.fromXML(datas);
    }


    /**
     * webservice请求数据①
     * @param url 地址
     * @param method 方式
     * @param requestParam 请求参数
     * @param <T> 返回类型
     * @return 返回值
     */
    /*public <T> Object getClient(String url, String method, String requestParam){
        try {
            JaxWsDynamicClientFactory jax=JaxWsDynamicClientFactory.newInstance();
            Client client=jax.createClient(new URL(url));
            HTTPConduit httpConduit=(HTTPConduit) client.getConduit();
            HTTPClientPolicy factory=new HTTPClientPolicy();
            factory.setConnectionTimeout(10*10*600);
            factory.setReceiveTimeout(10*10*600);
            httpConduit.setClient(factory);

            Object[] o=client.invoke(method,requestParam);
            return toBean(Student.class,o[0].toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    /*public <T> Object getUrlWebService(String url, String method, String namespace,String requestParam) throws RemoteException {
        Service service=new Service();
        Call call;
        call=(Call) service.createCall();
        call.setTargetEndpointAddress(url);
        call.setOperationName(new QName(namespace,method));
        call.setTimeout(30000)
        call.addParameter("arg0",XMLType.XSD_STRING, ParameterMode._PARAM_IN);
        call.setReturnType(XMLType.XSD_STRING);
        String s=(String) call.invoke(new Object[]{requestParam});
        return s;
    }*/


}
