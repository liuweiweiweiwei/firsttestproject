package com.test.thread.newlw.net;


/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 15:54
 */
public class TestSericial {

    public static void main(String[] args) {
        //测试--根据需要的class传进去--即可实例化不同对象HessionSericial等
        IService service = SericialFactory.instance(HessionSericial.class);
        User u = new User("te",25);
        //序列化
        byte[] read = service.read(u);
        System.out.println(new String(read) + ":::" + read.length);
        //反序列化
        User write = service.write(read, User.class, null);
        System.out.println(write.toString());

        System.out.println(new String(new byte[]{10,3,77,105,99,16,18}));
    }
}
