package com.test.thread.mythreadpool;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 20:53
 * @description
 */
public interface RunnableQueue {
    void offer(Runnable runnable);

    Runnable take() throws InterruptedException;

    int size();
}
