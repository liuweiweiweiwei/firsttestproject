package com.test.lambda;

import java.util.LinkedList;
import java.util.List;

public class Spead {
    public static void main(String[] args) {
        int i=0;
        List<Integer> integerList=new LinkedList<>();
        for (int j = 0; j <10000 ; j++) {
            integerList.add(j);
        }

        //2
        long st1=System.currentTimeMillis();
        int num1=0;
        num1= integerList.stream().mapToInt(Integer::intValue).sum();
        System.out.println("最终得到的结果为："+num1);
        long en1=System.currentTimeMillis();
        System.out.println("串行最终时间为："+(en1-st1));
        //1
       /* long st=System.currentTimeMillis();
        int num=0;
        for (int j = 0; j <integerList.size() ; j++) {
            num+=integerList.get(j);
        }
        System.out.println("最终得到的结果为："+num);
        long en=System.currentTimeMillis();
        System.out.println("for最终时间为："+(en-st));*/
        //3
        long st2=System.currentTimeMillis();
        int num2=0;
        for (Integer i2:integerList
             ) {
            i2=i2.intValue();
            num2+=i2;
        }
        System.out.println("最终得到的结果为："+num2);
        long en2=System.currentTimeMillis();
        System.out.println("foreach最终时间为："+(en2-st2));
        //5
        long st3=System.currentTimeMillis();
        int num3=0;
        num3=integerList.parallelStream().mapToInt(Integer::intValue).sum();
        System.out.println("最终得到的结果为："+num3);
        long en3=System.currentTimeMillis();
        System.out.println("pall最终时间为："+(en3-st3));
    }
}
