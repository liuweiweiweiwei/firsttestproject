package com.test.thread.newlw.net;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 15:03
 */
public interface IService {
    /**
     * 序列化
     * @param t
     * @param <T>
     * @return
     */
    <T> byte[] read(T t);

    /**
     * 反序列化
     * @param bytes
     * @param tClass
     * @param name
     * @param <T>
     * @return
     */
    <T> T write(byte[] bytes,Class<T> tClass,String name);
}
