package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:26
 * @description
 */
public class SpringFactory implements IFactory{
    @Override
    public ICourse createCourse() {
        return new SpringSource();
    }

    @Override
    public INode createNode() {
        return new SpringNode();
    }

    @Override
    public IVideo createVideo() {
        return new SpringVideo();
    }
}
