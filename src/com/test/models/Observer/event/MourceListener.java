package com.test.models.Observer.event;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 15:13
 * @description
 */
public class MourceListener extends EventListener {

    public void click(){
        System.out.println("调用单及方法");
        this.trigger(CASE.CLICK_SIN);
    }

    public void on(){
        System.out.println("调用在方法");
        this.trigger(CASE.ON);
    }

    public void clickDouble(){
        System.out.println("调用双击方法");
        this.trigger(CASE.CLICK_TWO);
    }

    public void up(){
        System.out.println("调用上方法");
        this.trigger(CASE.UP);
    }
}
