package com.test.Files.newFIle;

import com.test.Files.FileUtil;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 19:18
 * @description
 */
public class newFile {

    public static void main(String[] args) {
        fileChannel();
        fileCopy();
        memoryFile();
    }

    /**
     * bufferchannel操作介绍
     */
    public static void fileChannel() {
        FileInputStream inputStream = null;
        try {
            //这是用文件处理
            inputStream = new FileInputStream(new File("F:\\tools\\testFile\\test.txt"));
            //这是用nio处理--创建文件操作管
            FileChannel channel = inputStream.getChannel();

            //分配缓冲区方法一：将字段放入进去-分配一个10个byte大小的空间留存后用【10b】
            ByteBuffer byteBuffer = ByteBuffer.allocate(32);
            //分配缓冲区方法二：
            byte[] buffer = new byte[10];
            ByteBuffer wrap = ByteBuffer.wrap(buffer);

            System.out.println("初始化->capacity:" + byteBuffer.capacity() + ";position:" + byteBuffer.position() + ";limit:" + byteBuffer.limit());

            //调用read
            channel.read(byteBuffer);
            System.out.println("read->capacity:" + byteBuffer.capacity() + ";position:" + byteBuffer.position() + ";limit:" + byteBuffer.limit());

            //调用flip--停止写入---
            byteBuffer.flip();
            System.out.println("flip->capacity:" + byteBuffer.capacity() + ";position:" + byteBuffer.position() + ";limit:" + byteBuffer.limit());

            //调用get方法
            while (byteBuffer.remaining() > 0) {
                byte b = byteBuffer.get();
            }
            System.out.println("调用get->capacity:" + byteBuffer.capacity() + ";position:" + byteBuffer.position() + ";limit:" + byteBuffer.limit());

            //调用clear
            byteBuffer.clear();
            System.out.println("clear->capacity:" + byteBuffer.capacity() + ";position:" + byteBuffer.position() + ";limit:" + byteBuffer.limit());

            //调用close管道
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            FileUtil.closeI(inputStream);
        }
    }

    /**
     * 复制数据
     */
    public static void fileCopy(){
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            //输入
            inputStream = new FileInputStream(new File("F:\\tools\\testFile\\test.txt"));
            FileChannel inputStreamChannel = inputStream.getChannel();

            //输出
            outputStream = new FileOutputStream("F:\\tools\\testFile\\testCopy.txt");
            FileChannel outputStreamChannel = outputStream.getChannel();

            //分配字节-使用的是allocateDirect而不是allocate
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(32);

            while (true){
                byteBuffer.clear();
                int t = inputStreamChannel.read(byteBuffer);

                if(t == -1){break;}
                byteBuffer.flip();
                outputStreamChannel.write(byteBuffer);
            }
            byteBuffer.clear();
            outputStreamChannel.close();
            inputStreamChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            FileUtil.closeO(outputStream);
            FileUtil.closeI(inputStream);
        }
    }

    /**
     * 基于内存直接操作数据
     */
    public static void memoryFile(){
        try {
            //写入random
            RandomAccessFile randomAccessFile = new RandomAccessFile("F:\\tools\\testFile\\test.txt","rw");

            FileChannel randomAccessFileChannel = randomAccessFile.getChannel();
            //读取内存中的位置并直接基于内存操作
            MappedByteBuffer mappedByteBuffer = randomAccessFileChannel.map(FileChannel.MapMode.READ_WRITE,
                    0,32);


            mappedByteBuffer.put(30, (byte) 'e');
            mappedByteBuffer.put(31, (byte) 'c');
            mappedByteBuffer.flip();
            randomAccessFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
