package com.test.thread.old;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/10/22 11:09
 * @description
 */
public class ThreadTwo {
    private static AtomicInteger count = new AtomicInteger(0);
    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();

    private static CountDownLatch countDownLatch = new CountDownLatch(10);

    public static void main(String[] args) throws InterruptedException {
//        for (int i = 0; i < 30; i++) {
//            new Thread(ThreadTwo::inc).start();
//        }
//        TimeUnit.SECONDS.sleep(2);
        System.out.println("结束:"+count);
        //wait-single
//        TimeUnit.SECONDS.sleep(1);
        new Thread(new ConditionDemo(lock,condition)).start();
        new Thread(new ConditionSingle(lock,condition)).start();
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                countDownLatch.countDown();
            }).start();
        }
        countDownLatch.await();
        System.out.println("countdownlatch-end"+countDownLatch.getCount());
    }
    public static void inc(){
        try {
//            synchronized (ThreadTwo.class) {
//                TimeUnit.SECONDS.sleep(5);
                count.incrementAndGet();
                System.out.println(count.get());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Reentranlock
     */
    static class AtomicDemo{
        private static int count = 0;
        static ReentrantLock lock = new ReentrantLock();
        public static void inc(){
            lock.lock();
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            count++;
            lock.unlock();
        }

        public static void main(String[] args) throws InterruptedException {
            for (int i = 0; i < 1000; i++) {
                new Thread(AtomicDemo::inc).start();
            }
            TimeUnit.SECONDS.sleep(3);
            System.out.println(count);
        }
    }

    static class LockDeam{
        static Map<String,Object> map = new HashMap<>();
        static ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
        static ReentrantReadWriteLock.WriteLock writeLock = rwl.writeLock();
        static ReentrantReadWriteLock.ReadLock readLock = rwl.readLock();
        public static final Object get(String key){
            System.out.println("获取数据...");

            readLock.lock();
            try {
                return map.get(key);
            }finally {
                readLock.unlock();
            }
        }
        public static final Object putN(String key,Object value){
            System.out.println("存储数据..."+key);
            return map.put(key, value);
        }
        public static final Object put(String key,Object value){
            System.out.println("存储数据..."+key);
            writeLock.lock();
            try{
                return map.put(key, value);
            }finally {
                writeLock.unlock();
            }
        }
        public static void main(String[] args) throws InterruptedException {
            int[] arr = new int[6];
            new Thread(()-> LockDeam.put(arr[0]+"","value"+arr[0])).start();
            new Thread(()-> LockDeam.put(arr[1]+"","value"+arr[0])).start();
            new Thread(()-> LockDeam.put(arr[2]+"","value"+arr[0])).start();
//            for (int i = 1; i < arr.length; i++) {
//                int finalI = i;
//                new Thread(()-> LockDeam.put(finalI+"","value"+finalI)).start();
//            }
            for (int i = 1; i < 100; i++) {
                int finalI = i;
                new Thread(()-> LockDeam.putN(finalI+"","value"+finalI)).start();
            }
            TimeUnit.SECONDS.sleep(3);
//            for (int i = 1; i < 100; i++) {
//                if(!LockDeam.map.containsKey(i+"")){
//                    System.out.println(i);
//                }
//            }
//            System.out.println(LockDeam.map.size());
        }
    }

    static class ConditionDemo implements Runnable{
        private Lock lock;
        private Condition condition;

        public ConditionDemo(Lock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            System.out.println("ConditionDemo-wait......");
            try {
                lock.lock();
                condition.await();
                System.out.println("ConditionDemo-Catrying");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
    }
    static class ConditionSingle implements Runnable{
        private Lock lock;
        private Condition condition;

        public ConditionSingle( Lock lock,Condition condition) {
            this.condition = condition;
            this.lock = lock;
        }


        @Override
        public void run() {
            System.out.println("ConditionSingle-single......");
            try {
                lock.lock();
                condition.signal();
                System.out.println("ConditionSingle-carry-end");
            }finally {
                lock.unlock();
            }
        }
    }







}
