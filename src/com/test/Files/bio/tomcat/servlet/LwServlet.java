package com.test.Files.bio.tomcat.servlet;

import com.test.Files.bio.tomcat.http.LwRequest;
import com.test.Files.bio.tomcat.http.LwResponse;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 22:35
 * @description
 */
public abstract class LwServlet {

    public void service(LwRequest request, LwResponse response) throws Exception {
        if("GET".equalsIgnoreCase(request.getMethod())){
            doGet(request, response);
        }else{
            doPost(request, response);
        }
    }

    protected abstract void doGet(LwRequest request,LwResponse response)throws Exception;

    protected abstract void doPost(LwRequest request,LwResponse response)throws Exception;
}
