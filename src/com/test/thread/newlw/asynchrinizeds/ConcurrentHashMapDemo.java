package com.test.thread.newlw.asynchrinizeds;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/23 14:17
 * @description
 */
public class ConcurrentHashMapDemo {

    static ConcurrentHashMap<String,Integer> map = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            map.put("key" + i, i);
        }

    }
}
