package com.test.tool.vo;

import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/8/5 9:51
 * @description
 */
public class Test1 {
    private String name;
    private Integer age;
    private List<String> content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}
