package com.test.java8;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class RestTemplates {
    public static RestTemplate restTemplate;
    public static void main(String[] args) {
        HttpHeaders httpHeaders=new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        Person person=new Person(18,"tom");
        HttpEntity<Person> httpEntity=new HttpEntity<Person>(person,httpHeaders);
        restTemplate=new RestTemplate();
        restTemplate.postForObject("localhost:8080",httpEntity,Student.class);

    }
}
