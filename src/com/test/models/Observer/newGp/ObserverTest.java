package com.test.models.Observer.newGp;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:34
 * @description
 */
public class ObserverTest {

    public static void main(String[] args) {
        Teacher teacher = new Teacher("提米");
        Teacher teacher1 = new Teacher("小李老师");
        Question question = new Question();
        question.setName("小红");
        question.setContent("今天是星期几？");
        LWer lWer = LWer.getInstance();
        lWer.addObserver(teacher);
        lWer.addObserver(teacher1);
        lWer.question(question);
    }
}
