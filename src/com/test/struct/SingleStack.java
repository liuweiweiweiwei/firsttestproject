package com.test.struct;

import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/3/25 8:39
 * @description 单调栈
 */
public class SingleStack {

    //设置单调递增的栈
    //1.栈为空或栈顶元素值大于入栈元素值
    //2.栈不为空栈顶元素大于当前元素->栈顶元素出栈、更新
    public Stack<Integer> getStack(int[] arr){
        Stack<Integer> stack=new Stack<>();
        for (int i = 0; i <arr.length ; i++) {
            if(stack.isEmpty()||stack.peek()>arr[i]){
                stack.add(arr[i]);
            }else {
                while (!stack.isEmpty()&&stack.peek()<arr[i]){
                    stack.pop();
                }
                stack.add(arr[i]);
            }
        }
        return stack;
    }
}
