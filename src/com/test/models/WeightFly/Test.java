package com.test.models.WeightFly;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/20 16:22
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Ticket ticket = FlyWeightFactory.getTicket("北京", "上海");
        ticket.showTicketInfo("环球！");
        Ticket ticket1 = FlyWeightFactory.getTicket("武汉", "天津");
        ticket1.showTicketInfo("内陆！");
        Ticket ticket2 = FlyWeightFactory.getTicket("北京", "上海");
        ticket2.showTicketInfo("环海！");
    }
}
