package com.test.autimatic.type;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/3 20:15
 * @description
 */
public class TanXinLeetcode {

    public static void main(String[] args) {
        System.out.println(canJump(new int[]{2,0,0}));
    }

    public static boolean canJump(int[] nums) {
        if(nums.length == 1){return true;}
        int max = nums[0];
        for (int i = 0; max >= i && i < nums.length; i++) {
            max = Math.max(max,nums[i] + i);
            if(max >= nums.length - 1){
                return true;
            }
        }
        return false;
    }
}
