package com.test.modelStyle.modelSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/10/9 16:51
 * @description
 */
public class ServerSocketA {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket=new ServerSocket(8765);
            System.out.println("进入服务器连接:请等待。。。。。。");
            Socket socket ;
            while (true){
                socket=serverSocket.accept();
                Thread thread=new Thread(new MyRunable(socket));
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static class MyRunable implements Runnable{
        Socket socket;

        public MyRunable() { }

        public MyRunable(Socket socket){
            this.socket=socket;
        }
        @Override
        public void run() {
            if(socket==null){
                return;
            }
            InputStream ip = null;
            OutputStream op = null;
            BufferedReader bf = null;
            PrintWriter pw = null;
            try {
                ip=socket.getInputStream();
                bf=new BufferedReader(new InputStreamReader(ip));
                StringBuffer stringBuffer=new StringBuffer();
                String info;
                while ((info=bf.readLine())!=null){
                    stringBuffer.append(info);
                }
                socket.shutdownInput();
                //返回客户端
                op=socket.getOutputStream();
                pw=new PrintWriter(op);
                pw.write(stringBuffer.toString());
                System.out.println("《服务器》：请求内容为-->"+stringBuffer.toString()+";请求结果为-->");
                //刷缓冲区，关闭流
                pw.flush();
                socket.shutdownOutput();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    pw.close();
                    op.close();
                    bf.close();
                    ip.close();
                }catch (Exception e){
                    e.getMessage();
                }
            }
        }
    }
}
