package com.test.thread.javaconcurrencyprogram;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/17 18:28
 * @description
 */
public class JavaInterrupt {
    public static void main(String[] args) {
        testInterrupt();
        Thread.currentThread().interrupt();
        System.out.println("main thread is interrupt :" + Thread.currentThread().isInterrupted());
        testJoin();
    }

    /**
     * test thread interrupt
     */
    private static void testInterrupt() {
        System.out.println("testInterrupt method start");
        System.out.println("1.main thread is interrupt :" + Thread.interrupted());
        Thread.currentThread().interrupt();
        System.out.println("2.main thread is interrupt :" + Thread.currentThread().isInterrupted());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            System.out.println("i will be interrupt still");
        }
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("test interrupt");
            try {
                TimeUnit.SECONDS.sleep(1);
                Thread.currentThread().interrupt();
            } catch (InterruptedException e) {
                System.out.println("3.inner method is sleep interrupt");
            }
        }
        System.out.println("end main thread is interrupt :" + Thread.currentThread().isInterrupted());
        System.out.println("testInterrupt method end");
    }

    /**
     * create thread
     * @param numThread thread num
     * @return thread
     */
    private static Thread createThread(int numThread) {
        return new Thread(() -> {
            for (int i = 1; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " # " + i);
            }
        }, "innerThread" + String.valueOf(numThread));
    }

    private static void threadSleep() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " : sleep is interrupt!");
        }
    }

    /**
     * test thread.Join()
     */
    private static void testJoin() {
        System.out.println("testJoin method start");
        List<Thread> tList = IntStream.range(1, 5).mapToObj(JavaInterrupt::createThread).collect(Collectors.toList());
        tList.forEach(Thread::start);
        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(thread.getName() + " : is interrupt!");
            }
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " # " + i);
            threadSleep();
        }
        System.out.println("testJoin method end");
    }
}
