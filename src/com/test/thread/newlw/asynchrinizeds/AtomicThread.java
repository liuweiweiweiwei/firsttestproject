package com.test.thread.newlw.asynchrinizeds;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/17 19:10
 * @description
 */
public class AtomicThread {

    private volatile static int cre = 0;

    public static void incre(){
        synchronized (AtomicThread.class) {
            try {
                TimeUnit.MILLISECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cre++;
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new Thread(()-> incre()).start();
        }
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("最终的cre = :"+cre);
    }
}
