package com.test.thread.newlw.example;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/16 20:49
 * @description 用户实体类---责任链模式
 */
public class User {
    private String name;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User(String name) {
        this.name = name;
    }
}
