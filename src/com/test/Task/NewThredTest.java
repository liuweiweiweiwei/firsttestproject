package com.test.Task;

import com.test.thread.old.UserThreadFactory;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/8 13:53
 * @description
 */
public class NewThredTest {

    public static void main(String[] args) {
        int index=0;
        for (int i = 0; i <10000 ; i++) {
            index=get(index);
        }
        System.out.println(index);

        String str="imas.asd_ed,";
        System.out.println(Arrays.asList(str.split(",",-1)));
        System.out.println(str.replace("code.",""));

        List<Integer> list=Arrays.asList(1,2,3,4,5,6);
        for (int i = 0; i < list.size(); ) {
            System.out.println("测试："+list.get(i++));
        }
        System.out.println("----------------分割线--------------------");
        String key1= UUID.randomUUID().toString();
        String key2= UUID.randomUUID().toString();
        for (int i = 0; i < 50; i++) {
            threadPoolExecutor.execute(new NewThread(key1));
            threadPoolExecutor.execute(new NewThread(key2));
            /*try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            //System.out.println("循环数据休眠2秒！");

        }
        while (true){
            if(map.get(key1)!=null&&map.get(key1).get()>45){
                System.out.println("数据一的数据到达数量值！");
                break;
            }
            if(map.get(key2)!=null&&map.get(key2).get()>45){
                System.out.println("数据二的数据到达数量值！");
                break;
            }
        }
        threadPoolExecutor.shutdown();
        System.out.println("最终的数据为："+key1+"value:"+map.get(key1).get());
        System.out.println("最终的数据为key："+key1+"value:"+map.get(key2).get());


    }


    public static ThreadPoolExecutor threadPoolExecutor= new ThreadPoolExecutor(2,2,10, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(200),new UserThreadFactory("data-count-map"),new ThreadPoolExecutor.CallerRunsPolicy());

    public static Map<String,AtomicInteger> map=new ConcurrentHashMap<>();
    static class NewThread implements Runnable{
        private String key;

        public NewThread(String key) {
            this.key = key;
        }

        @Override
        public void run() {
            AtomicInteger atomicInteger = map.get(key)==null?new AtomicInteger(0):map.get(key);
            atomicInteger.incrementAndGet();
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("当前线程是："+Thread.currentThread().getName()+"key:"+key+"计数是："+atomicInteger.get());
            map.put(key,atomicInteger);
        }
    }


    public static int get(int index){

        Random random=new Random();
        if(random.nextInt(100)==1){
            System.out.println("空值！");
            index++;
        }
        return index;
    }


}
