package com.test.Task.Helper;

import com.alibaba.fastjson.JSON;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 25338
 * @version 1.1
 * @date 2020/8/2 16:31
 */
public class TaskOne {
    public static void main(String[] args) {
        List<Institution> institutions = new ArrayList<>();
        List<Store> stores = new ArrayList<>();

        Institution institution1 = new Institution();
        institution1.setInstitutionId("01");
        institution1.setInstitutionName("爱康国宾");


        Institution institution2 = new Institution();
        institution2.setInstitutionId("02");
        institution2.setInstitutionName("美年大健康");

        institutions.add(institution1);
        institutions.add(institution2);
//---------------------------------------------------------------
        Store store1 = new Store();
        store1.setCityId("001");
        store1.setCityName("大同");
        store1.setProvinceId("0001");
        store1.setProvinceName("山西");
        store1.setInstitutionId("01");
        store1.setStoreId("0001");
        store1.setStoreName("爱康国宾大同店");

        Store store2 = new Store();
        store2.setCityId("002");
        store2.setCityName("太原");
        store2.setProvinceId("0001");
        store2.setProvinceName("山西");
        store2.setInstitutionId("01");
        store2.setStoreId("0002");
        store2.setStoreName("爱康国宾太原店");

        Store store3 = new Store();
        store3.setCityId("003");
        store3.setCityName("临西");
        store3.setProvinceId("0002");
        store3.setProvinceName("陕西");
        store3.setInstitutionId("01");
        store3.setStoreId("0003");
        store3.setStoreName("爱康国宾西安店");

        Store store4 = new Store();
        store4.setCityId("004");
        store4.setCityName("渭南");
        store4.setProvinceId("0002");
        store4.setProvinceName("陕西");
        store4.setInstitutionId("02");
        store4.setStoreId("0004");
        store4.setStoreName("美年大健康渭南店");

        Store store5 = new Store();
        store5.setCityId("005");
        store5.setCityName("西安");
        store5.setProvinceId("0002");
        store5.setProvinceName("陕西");
        store5.setInstitutionId("02");
        store5.setStoreId("0005");
        store5.setStoreName("美年大健康西安店");

        Store store6 = new Store();
        store6.setCityId("006");
        store6.setCityName("临汾");
        store6.setProvinceId("0001");
        store6.setProvinceName("山西");
        store6.setInstitutionId("01");
        store6.setStoreId("0006");
        store6.setStoreName("爱康国宾临汾店");

        stores.add(store1);
        stores.add(store2);
        stores.add(store3);
        stores.add(store4);
        stores.add(store5);
        stores.add(store6);

        //最终输出的三个类
        Bigduixiang bigduixiang=new Bigduixiang();
        List<Province> provinceList=new LinkedList<>();
        List<City> cityList=new LinkedList<>();
        List<Institution> institutionList=new LinkedList<>();

        //按照省份分组；key为省id；value为值
        Map<String, List<Store>> collect =stores.stream().collect(Collectors.groupingBy(Store::getProvinceId));
        //1.collect是根据省份组织后的
        collect.forEach((k,v)-> {
            Province province=new Province();
            province.setProvinceId(k);
            province.setProvinceName(v.get(0).getProvinceName());
            province.setStoreList(v);
            provinceList.add(province);
        });
        System.out.println("省份分类集合----------------------------------------------");
        //2.城市集合
         provinceList.forEach(province -> {
             Map<String, List<Store>> cityListMap = province.getStoreList().stream().collect(Collectors.groupingBy(Store::getCityId));
             List<City> middleCity=new LinkedList<>();
             cityListMap.forEach((k,v)->{
                 City city=new City();
                 city.setCityId(k);
                 city.setCityName(v.get(0).getCityName());
                 city.setStoreList(v);
                 middleCity.add(city);
             });
             province.setCityList(middleCity);//设置省份的cityList
             cityList.addAll(middleCity);//加入到city中
         });
        System.out.println("城市集合------------------------------------------------------------------");
        cityList.forEach(city -> {
            Map<String, List<Store>> instituMap = city.getStoreList().stream().collect(Collectors.groupingBy(Store::getInstitutionId));
            List<Institution> institutionListMiddle=new LinkedList<>();
            instituMap.forEach((k,v)->{
                Institution institution=new Institution();
                institution.setInstitutionId(k);
                institution.setInstitutionName(v.get(0).getInstitutionName());
                institution.setStoreList(v);
                institutionListMiddle.add(institution);
            });
            city.setInstitutionList(institutionListMiddle);
            institutionList.addAll(institutionListMiddle);
        });
        System.out.println("机构输出-------------------------------------------------------------------");
        institutionList.forEach(store->{
            System.out.println(store.getInstitutionName());
            System.out.println(store.getInstitutionId());
            System.out.println(store.getStoreList());
        });
        System.out.println("最终输出------------------------------------------------------------");
        System.out.println(JSON.toJSONString(provinceList).toString());
        System.out.println("------");
        System.out.println(JSON.toJSONString(cityList));
        System.out.println("------");
        System.out.println(JSON.toJSONString(institutionList));
        System.out.println("------");
        //最终获取
        bigduixiang.setProvinceList(provinceList);
    }
}
