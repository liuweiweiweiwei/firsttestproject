package com.test.models.Strategy.newa;

import org.springframework.beans.factory.config.PropertiesFactoryBean;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:12
 * @description  策略工厂--懒汉加安全单例
 */
public class PromotionStrategyFactory {

    private static Map<String,PromotionStrategy> factoryMap = new HashMap<>();

    private static final PromotionStrategy PROMOTION_STRATEGY = new EmptyStrategy();

    static {
        factoryMap.put(Promotion.GROUP,new GroupByStrategy());
        factoryMap.put(Promotion.CASH_BACK,new CashBackStrategy());
        factoryMap.put(Promotion.COUPOBN,new CouponStrategy());
    }

    private PromotionStrategyFactory() { }

    public static PromotionStrategy getInstance(String key){
        if(factoryMap.containsKey(key)){
            return factoryMap.get(key);
        }
        return PROMOTION_STRATEGY;
    }

    private interface Promotion{
        String GROUP = "GROUP";
        String CASH_BACK = "CASHBACK";
        String EMPTY = "EMPTY";
        String COUPOBN = "COUPON";
    }
}
