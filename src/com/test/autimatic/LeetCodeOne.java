package com.test.autimatic;


/**
 * @author 25338
 * 联系
 */
public class LeetCodeOne {
    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5};
        int end=minSubArrayLen(15,nums);
        System.out.println("最终得到的最小数组长度为："+end);
        //2.
        int[] nu={3,2,1,5,6,4};
        int maxnum=findKthLargest(nu,2);
        System.out.println("查询到的最大元素为："+maxnum);
    }
    /**
     * 最短子字符串
     */
    public static int minSubArrayLen(int s, int[] nums) {
        if(nums.length==0){
            return 0;
        }
        int max=nums.length;
        for (int i = 0; i <nums.length ; i++) {
            int num=0;
            int total=0;
            int j=i;
            while (total<s&&j<nums.length){
                total+=nums[j];
                j++;
                num++;
            }
            if(num==nums.length&&total<s){
                return 0;
            }
            num=total>=s?num:max;
            max=Math.min(max,num);
        }
        return max;
    }
    /**
     * 第k大的元素
     */
    public static int findKthLargest(int[] nums, int k) {
        int m=0;
        int n=0;
        int max = 0;
        while (m<k){
            max=0;
            for (int i = 0; i <nums.length-n ; i++) {
                max=Math.max(nums[i],max);
                if(max>nums[nums.length-n-1]) {
                    swap(nums, i, nums.length - n - 1);
                }
            }
            n++;
            m++;
        }
        return max;
    }
    public static void swap(int[] arr,int i,int j){
        int tem=arr[i];
        arr[i]=arr[j];
        arr[j]=tem;
    }

    public class ListNode {
          int val;
          ListNode next;
          ListNode() {}
          ListNode(int val) { this.val = val; }
          ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
    public ListNode swapPairs(ListNode head) {
        if(head==null||head.next==null){
            return head;
        }
        ListNode nodenextw=new ListNode(head.val);
        ListNode end=new ListNode(head.next.val,nodenextw);
        head=head.next.next;
        while (head!=null){
            if(head.next!=null) {
                ListNode nodenext = new ListNode(head.val);
                ListNode midend = new ListNode(head.next.val, nodenext);
                ListNode news=end;
                ListNode listNode = end;
                while (listNode.next!=null){
                    listNode=listNode.next;
                }
                listNode.next = midend;
                end = listNode;
            }else{
                ListNode midend = new ListNode(head.val,null);
                ListNode listNode = end;
                while (listNode.next!=null){
                    listNode=listNode.next;
                }
                listNode.next = midend;
                end = listNode;
                return end;
            }
            head=head.next.next;
        }
        return end;
    }
    
}
