package com.test.autimatic.type;

import com.test.MyLog.LwLog;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/23 18:19
 * @description
 */
public class NodeTest {
    public static void main(String[] args) {
        ListNode listNode11 = new ListNode(0);
        ListNode listNode12 = new ListNode(3);
        ListNode listNode13 = new ListNode(1);
        ListNode listNode14 = new ListNode(0);
        ListNode listNode15 = new ListNode(4);
        ListNode listNode16 = new ListNode(5);
        ListNode listNode17 = new ListNode(2);
        ListNode listNode18 = new ListNode(0);
        listNode11.next = listNode12;listNode12.next = listNode13;listNode13.next = listNode14;
        listNode14.next = listNode15;
        listNode15.next = listNode16;listNode16.next = listNode17;listNode17.next = listNode18;
//        ListNode reverseNode = reverseNode(listNode11);
//        LwLog.splitLogInfo("分割线");
//        ListNode reverseN = reverseNList(listNode11, 3);
//        LwLog.splitLogInfo("分割线");
        ListNode reList = reverseNtoMNode(listNode11, 2, 5);
        LwLog.splitLogInfo("分割线");
    }

    /**
     * reverse linkList
     * @param listNode head
     * @return reverseList
     */
    public static ListNode reverseNode(ListNode listNode) {
        if (listNode.next == null) {
            return listNode;
        }
        ListNode last = reverseNode(listNode.next);
        listNode.next.next = listNode;
        listNode.next = null;
        return last;
    }

    private static ListNode tempNode = null;

    /**
     * reverse before n node
     *
     * @param head head
     * @param n n
     * @return reverse end
     */
    public static ListNode reverseNList(ListNode head, int n) {
        if (n == 1) {
            tempNode = head.next;
            return head;
        }
        ListNode tempLast = reverseNList(head.next, n - 1);
        head.next.next = head;
        head.next = tempNode;
        return tempLast;
    }

    /**
     * reverse from m to n
     * @param node head
     * @param m from
     * @param n to
     * @return list
     */
    public static ListNode reverseNtoMNode(ListNode node, int m, int n) {
        if (m == 1) {
            return reverseNList(node, n);
        }
        node.next = reverseNtoMNode(node.next, m - 1,n - 1);
        return node;
    }

    private static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
