package com.test.thread.old;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/8/14 15:19
 * @description 定义工厂名称-回溯
 */
public class UserThreadFactory implements ThreadFactory {
    private final String factoryName;
    private final AtomicLong longAdder=new AtomicLong(1);

    public UserThreadFactory(String factoryName){
        this.factoryName="from userFactoryThread"+factoryName+"-name-";
    }


    @Override
    public Thread newThread(Runnable r) {
        String threadName=this.factoryName+longAdder.incrementAndGet();
        Thread thread=new Thread(null,r,threadName,1);
        return thread;
    }
}
