package com.test.boy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/2 15:13
 * @description
 */
public enum URL {
    //需要替换
    URL1("url","https://v.douyin.com/JT8YdXR/"),
    NEWURL("ur2","https://www.iesdouyin.com/web/api/v2/aweme/iteminfo/?item_ids="),
    URLEND("ur3","http");
    private String url;
    private String realAddress;

    URL(String url, String realAddress) {
        this.url = url;
        this.realAddress = realAddress;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRealAddress() {
        return realAddress;
    }

    public void setRealAddress(String realAddress) {
        this.realAddress = realAddress;
    }
}
