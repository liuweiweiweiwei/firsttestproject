package com.test.models.Strategy.payStrategy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 18:58
 * @description
 */
public class AliPay extends Pay {
    @Override
    public String getName() {
        return "支付宝";
    }

    @Override
    protected double queryBalance(String uid) {
        return 500;
    }
}
