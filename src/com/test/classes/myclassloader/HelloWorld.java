package com.test.classes.myclassloader;

/**
 * @author 25338
 * @date 2022/5/12 22:41
 * @since 1.0
 * description
 */
public class HelloWorld {
    static {
        System.out.println("this is initialized " + HelloWorld.class.getName());
    }

    public String welcome() {
        System.out.println("welcome to here!");
        return "LW";
    }
}
