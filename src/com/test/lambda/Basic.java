package com.test.lambda;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Basic {
    public static void main(String[] args) {

    }
    public static void getFiles(){
        Path path= Paths.get("E://fileTest//files//");
        //1
        File[] files=path.toFile().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(name.endsWith(".txt")){
                    return true;
                }
                return false;
            }
        });
        //或
        File[] files1=path.toFile().listFiles((File file,String name)->name.endsWith(".txt"));

        //2
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        //或
        Arrays.sort(files, Comparator.comparing(File::getName));

        //3
        ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(1,2,3, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(3),new ThreadPoolExecutor.CallerRunsPolicy());
        threadPoolExecutor.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("开始测试123456.");
                return null;
            }
        });
        //或runnable
        threadPoolExecutor.submit(()-> System.out.println("开始测试123"));
    }
    public static void getLambda(){
        File file= Paths.get("E://fileTest//files//").toFile();
        FileFilter filter=f->f.getName().endsWith(".txt");
        List<File> fileList= Arrays.stream(file.listFiles()).filter(file1 -> file1.getName().endsWith(".txt")).collect(Collectors.toList());
        FilenameFilter filenameFilter=(dir,name)->name.endsWith(".txt");
    }


}
