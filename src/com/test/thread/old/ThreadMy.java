package com.test.thread.old;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/10/21 9:00
 * @description-start
 */
public class ThreadMy {

    private PrintProcessor printProcessor;

    public ThreadMy(){
        SaveProcess saveProcess = new SaveProcess();
        saveProcess.start();
        //
        printProcessor = new PrintProcessor(saveProcess);
        printProcessor.start();
    }

    public static void main(String[] args) {
        ThreadMy threadMy = new ThreadMy();
        Req req = new Req("请求");
        threadMy.printProcessor.processRequest(req);
        req = new Req("中间");
        threadMy.printProcessor.processRequest(req);
        req = new Req("结束");
        threadMy.printProcessor.processRequest(req);
    }

    static class Req{
        private String name;

        public Req(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    interface RequestProcessor{
        void processRequest(Req req);
    }
    class PrintProcessor extends Thread implements RequestProcessor{

        LinkedBlockingQueue<Req> queue = new LinkedBlockingQueue<>();
        private final RequestProcessor processor;

        public PrintProcessor(RequestProcessor processor){
            this.processor = processor;
        }

        @Override
        public void run() {
            while (true){
                try {
                    System.out.println("start print......");
                    Req req = queue.take();
                    System.out.println("use req:"+req.getName());
                    //保存到saveProcess中
                    processor.processRequest(req);
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        @Override
        public void processRequest(Req req) {
            queue.add(req);
        }
    }

    class SaveProcess extends Thread implements RequestProcessor{
        LinkedBlockingQueue<Req> blockingQueue = new LinkedBlockingQueue<>();

        @Override
        public void processRequest(Req req) {
            blockingQueue.add(req);
        }

        @Override
        public void run() {
            while (true){
                try {
                    System.out.println("start save......");
                    //获取保存的阻塞队列里的数据
                    Req req = blockingQueue.take();
                    System.out.println("save req:" + req.getName());
                    TimeUnit.SECONDS.sleep(6);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
