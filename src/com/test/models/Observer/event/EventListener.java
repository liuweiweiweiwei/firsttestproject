package com.test.models.Observer.event;

import java.lang.reflect.Method;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:45
 * @description 监听器观察者
 */
public class EventListener {

    /**
     * jdk底层listener都是存到map
     */
    protected Map<String,EventL> lMap = new HashMap<>();

    /**
     * 添加元素
     * @param eventType
     * @param target
     */
    public void addListener(String eventType,Object target){
        try{
            this.addListener(eventType, target,
                    target.getClass().getMethod("on"+ toUpperCase(eventType),EventL.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addListener(String eventType, Object target, Method method) {
        lMap.put(eventType,new EventL(target,method));
    }

    /**
     * 触发，只要有动作就触发
     * @param eventL
     */
    private void trigger(EventL eventL){
        eventL.setSource(this);
        eventL.setTime(System.currentTimeMillis());
        try {
            //发起回调
            if(eventL.getCallback() == null){
                return;
            }
            eventL.getCallback().invoke(eventL.getTarget(),eventL);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 根据时间名称处罚
     * @param trigger
     */
    protected void trigger(String trigger){
        if(!this.lMap.containsKey(trigger)){
            return;
        }
        trigger(this.lMap.get(trigger).setTrigger(trigger));
    }

    private String toUpperCase(String eventType) {
        char[] chars = eventType.toCharArray();
        chars[0] &= (~(1 << 5));
        return new String(chars);
    }


    public interface CASE{
        String ON ="on";
        String UP ="up";
        String CLICK_TWO ="TwoClick";
        String CLICK_SIN ="click";
    }
}
