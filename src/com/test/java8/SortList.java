package com.test.java8;

import java.io.*;
import java.math.BigDecimal;
import java.util.Arrays;

public class SortList {

    public static void main(String[] args) {
        String param="测试1";
        getFile(param);
        analysisFile("E://abc.txt");
        Item item=new Item("肖勉",12,new BigDecimal("12.3"));
        putInputStreamData(item);
        getInputStream("E://a.txt");

        int[] a={12,7,9,2,20,16,15,25,2,0,12};
        test(a,0,a.length-1);
        Arrays.stream(a).forEach(System.out::println);
    }
    public static void test(int[] arr,int a0,int b0){
        int a=a0;
        int b=b0;
        if(a>=b){
            return;
        }
        boolean tran=false;
        while (a!=b){
            if(arr[a]>arr[b]){
                int t=arr[a];
                arr[a]=arr[b];
                arr[b]=t;
                tran= !tran;
            }
            if(tran){ a++; }else { b--; }
        }
        a--;
        b++;
        test(arr,a0,a);
        test(arr,b,b0);

    }

    public static void getBytesFile(byte[] bytes){
        ByteArrayInputStream byteArrayInputStream=null;
        ByteArrayOutputStream byteArrayOutputStream=null;
        OutputStream outputStream=null;
        byteArrayOutputStream=new ByteArrayOutputStream();
        try {
            outputStream=new ByteArrayOutputStream();
            byteArrayOutputStream.write(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void getFile(String param){
        File file=new File("E://abc.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(FileOutputStream fileInputStream=new FileOutputStream(file)) {
            fileInputStream.write(param.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysisFile(String fName){
        try (Reader fileWriter=new FileReader(fName)){
            CharArrayWriter writer=new CharArrayWriter();
            int a;
            while ((a=fileWriter.read())!=-1){
                writer.write(a);
            }
            System.out.println(writer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void putInputStreamData(Object ame){
        File file=new File("E://a.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(DataOutputStream dataInputStream=new DataOutputStream(new FileOutputStream(file))) {
            dataInputStream.write(ame.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getInputStream(String na){
        try (DataInputStream dataInputStream=new DataInputStream(new FileInputStream(na))){
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(dataInputStream));
            StringBuffer buffer=new StringBuffer();
            String s;
            while (null != (s=bufferedReader.readLine())){
                buffer.append(s);
            }
            System.out.println(buffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
