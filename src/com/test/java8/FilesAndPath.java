package com.test.java8;

import java.io.*;

public class FilesAndPath {
    public static void main(String[] args) {
        File file=new File("E://timg1.jpg");
        //Path path = null;
        try {
            InputStream inputStream=new FileInputStream("E://timg.jpg");
            OutputStream outputStream=new FileOutputStream("E://tim.gif");
            byte[] bytes=new byte[1024];
            StringBuilder builder=new StringBuilder("");
            //outputStream.write(s.getBytes());
            while (inputStream.read(bytes)!=-1) {
                outputStream.write(bytes);
            }
            /*Path path=Files.write(file.toPath(),builder.toString().getBytes());
            System.out.println(path.getFileName());
            List<String> stringList=Files.readAllLines(Paths.get("E://a.txt"));
            System.out.println(stringList.size());
            stringList.forEach(System.out::println);*/
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
