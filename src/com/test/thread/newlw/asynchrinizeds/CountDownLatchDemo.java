package com.test.thread.newlw.asynchrinizeds;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/22 19:09
 * @description
 */
public class CountDownLatchDemo {
    static CountDownLatch latch = new CountDownLatch(1);


    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            new CountDownOne().start();
        }
        TimeUnit.SECONDS.sleep(3);
        latch.countDown();
        System.out.println("...");
//        CountDownLatch latch = new CountDownLatch(3);
//        new Thread(()->{
//            System.out.println(Thread.currentThread().getName() + "：start！");try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            latch.countDown();
//        }).start();
//        new Thread(()->{
//            System.out.println(Thread.currentThread().getName() + "：start！");
//            latch.countDown();
//        }).start();
//        new Thread(()->{
//            System.out.println(Thread.currentThread().getName() + "：start！");
//            try {
//                TimeUnit.SECONDS.sleep(2);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            latch.countDown();
//        }).start();
//        latch.await();
    }

    public static class CountDownOne extends Thread{

        @Override
        public void run() {
            try {
                System.out.println("thread: " + Thread.currentThread().getName() + "start!");
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread: " + Thread.currentThread().getName() + "end");
        }
    }

    public static class SemaDemo extends Thread{

        private int car;
        //令牌--拿不到就会阻塞
        private Semaphore semaphore;

        public SemaDemo(int car,Semaphore semaphore){
            this.car = car;
            this.semaphore = semaphore;
        }

        public static void main(String[] args) {
            Semaphore semaphore1 = new Semaphore(5);
            for (int i = 0; i < 10; i++) {
                new SemaDemo(i,semaphore1).start();
            }
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();//获得一个令牌，如果拿不到就会阻塞
                System.out.println("第" + car + "号车牌：" + Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(2);
                System.out.println("第" + car + "开走");
                semaphore.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是的一组线程达到一组同步点前阻塞
     */
    public static class CycliBarryThree extends Thread{

        private CyclicBarrier barrier;
        private String path;

        public CycliBarryThree(CyclicBarrier barrier, String path) {
            this.barrier = barrier;
            this.path = path;
        }

        @Override
        public void run() {
            System.out.println("start thread:"+Thread.currentThread().getName() + path);
            try {//
                barrier.await();
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
            System.out.println("end thread:"+Thread.currentThread().getName() + path);
        }

    }

    public static class Demo extends Thread{

        public static void main(String[] args) {
            //成员数，线程
            CyclicBarrier barrier = new CyclicBarrier(3,new Demo());
            new Thread(new CycliBarryThree(barrier,"file刘巍")).start();
            new Thread(new CycliBarryThree(barrier,"file何淑静")).start();
            new Thread(new CycliBarryThree(barrier,"file李曼婷")).start();
            barrier.getParties();
        }
    }

}
