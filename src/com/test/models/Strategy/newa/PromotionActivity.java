package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:05
 * @description --->>>策略模式
 */
public class PromotionActivity {
    private PromotionStrategy promotionStrategy;

    public PromotionActivity(PromotionStrategy promotionStrategy) {
        this.promotionStrategy = promotionStrategy;
    }

    public void execute(){
        //执行策略
        this.promotionStrategy.doPromotion();
    }
}
