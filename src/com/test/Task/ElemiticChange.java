package com.test.Task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
//import com.ibm.mq.*;
import com.test.java8.Student;
import com.test.java8.TouLianThree;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import jodd.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

public class ElemiticChange {
    public static void main(String[] args) {
        Student u = new Student(18, "tom");
        System.out.println(u.toString());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("student", u);
        String s = "{ name:'tom'}";
        System.out.println(jsonObject.get("student"));
        Student student2 = JSON.parseObject(s, Student.class);
        System.out.println(student2);

        List<String> list = new LinkedList<>(Arrays.asList("tom", "jane", "12345"));
        String format = "%s给%s发送了一段密码，是：%s";
        String model = String.format(format, 1, 2, 3);
        System.out.println(model);
        Object o = list;
        System.out.println(o);
        list.add(0, "army");
        System.out.println(list.toString());

        Animal animal=new Animal();
        Cat cat=new Cat();
        cat.setAge(19);
        animal.setCat(cat);
        String result= TouLianThree.toXml(animal);
        System.out.println(result);
        System.out.println(cat.getResult());
        Cat cat1=new Cat();
        Chicken chicken=new Chicken();
        chicken.setAge(22);
        cat1.setResult(cat.getResult());
        setParams(chicken,cat1);
        System.out.println(cat1);

        System.out.println("--------------------------------------");
        boolean a= StringUtil.isAllBlank("sad",null,null);
        StringUtil.isBlank("sa");
        if(StringUtil.isBlank("kjh")||StringUtil.isBlank("sa")
                ||StringUtil.isBlank("jkh")){
            System.out.println("incloude null");
        }
        System.out.println(a);
        List<String> list1=Arrays.asList("123","asd","zae");

        Catas catas=new Catas(12,"tom","detail");
        Catas.Newparams newparams=new Catas.Newparams("test");
        Catas.Newparams newparams1=new Catas.Newparams("test");
        List<Catas.Newparams> list2=new ArrayList<>();
        list2.add(newparams);
        list2.add(newparams1);
        catas.setNewparams(newparams );
        String s1= TouLianThree.toXml(catas);
        System.out.println(s1);

//        boolean judges=org.apache.commons.lang3.StringUtils.isAnyEmpty("a","as","123","a","as","123",
//                "a","as","a","as","123","a","as","123","a","as","123","a","as","123");
//        System.out.println("测试结果为"+judges);

        getParam("model","123","456","789");

        List<Double> list3=Arrays.asList(1.0,2.1,3.1,4.5,5.8,6.9,7.2,8.1,9.9);
        double num=list3.stream().mapToDouble(Double::doubleValue).sum();
        System.out.println("结果只和："+num);
        System.out.println(list3.stream().reduce(0.6,Double::sum));
        System.out.println("--------分割线----------");
        testReduce();

        System.out.println(Double.parseDouble("-120.21"));
        try{
            List<Double> stringList=list3.stream()
                    .map(data-> data+=2)
                    .filter(da->Double.parseDouble(da+"")!=0)
                    .collect(Collectors.toList());
            System.out.println(stringList.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

        JSONObject jsonObject1=new JSONObject();
        List<String> stringList=new ArrayList<>();
        stringList.add("12312234342");
        jsonObject1.put("BBXR",stringList);
        System.out.println(jsonObject1.toJSONString());

    }

    public static void testReduce(){
        List<String> stringList=Arrays.asList("1","2","3");
        String ints=stringList.stream().reduce((t1,t2)->{
            System.out.println("t1:"+t1);
            System.out.println("t2:"+t2);
            t1+=t2;
            System.out.println("t1new:"+t1);
            System.out.println("-----------");
            return t1;
        }).get();
        System.out.println(ints);
        System.out.println("---------分割线-------");
        //2
        String str2=stringList.stream().reduce("0",(t1,t2)->{
            System.out.println("newt1:"+t1);
            System.out.println("newt2:"+t2);
            t1+=t2;
            System.out.println("t1new:"+t1);
            System.out.println("-----------");
            return t1;
        });
        //3
        List<String> strings=new LinkedList<>();
        List<String> stringList1=stringList.stream().
                reduce(strings,(t1,t2)->{
                    strings.add(t2);
                    System.out.println("t1:"+t1);
                    System.out.println("t2:"+t2);
                    System.out.println("t1new:"+t1);
                    System.out.println("-----------");
                    return t1;
                },(t1,t2)->null);
        System.out.println(stringList1.toString()+strings.toString());

    }



    public static void getParam(String model,String... strings){
        System.out.println(model+"::"+strings);
    }





    public static void setParams(Chicken chicken,Cat cat){
        cat.setResult(chicken.getResult());
        cat.setAge(chicken.getAge());
    }

    static int CCSID = 1381;
    static String queueString = "LOCALQUEUE";
//    static MQQueueManager manager;
//
//    public static void connect() throws MQException {
//        MQEnvironment.hostname = "localhost";
//        MQEnvironment.channel = "SERVERCONN";
//        MQEnvironment.port = 8927;
//        //MQEnvironment.CCSID = CCSID;
//        //MQ中拥有权限的用户名
//        //MQEnvironment.userID = "MUSR_MQADMIN";
//        //用户名对应的密码
//        MQEnvironment.password = "123456";
//
//        manager = new MQQueueManager("MyTest");
//    }
//
//    //读取ecm中消息数据
//    public void receiveMsg3() {
//        int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
//        MQQueue queue2 = null;
//        try {
//            queue2 = manager.accessQueue(queueString, openOptions,
//                    null, null, null);
//            System.out.println("该队列当前的深度为:" + queue2.getCurrentDepth());//队列内数据消息数量
//
//            MQMessage mqMessage = new MQMessage();// 要读的队列的消息
//            MQGetMessageOptions mqGetMessageOptions = new MQGetMessageOptions();//读取数据操作
//            queue2.get(mqMessage, mqGetMessageOptions);
//            mqMessage.getDataLength();//长度
//            String data = mqMessage.readUTF();//获取数据
//            Student student2 = JSON.parseObject(data, Student.class);//转化为对象
//            System.out.println(student2);
//            //mqMessage.readStringOfByteLength(mqMessage.getDataLength());//获取数据
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 查询保单授权码使用bas和csc请求参数
     * <cat name="馄饨"  age="12"></cat>  适用于一个xml属性包含多个节点
     */
    @XStreamAlias("Add")
    static class Add {
        @XStreamAsAttribute
        private Integer age;
        @XStreamAsAttribute
        private String name;

        public Add() {
        }

        public Add(Integer age, String name) {
            this.age = age;
            this.name = name;
        }
    }

    /**
     * 查询保单授权码使用bas和csc返回参数
     * <cat name="馄饨" age=1>
     * <result1>0|1|0|0|1|</result1>
     * </cat>
     */
    @XStreamAlias("catbng")
    static class Cat {
        @XStreamAlias("age")
        private Integer age;
        @XStreamAlias("name")
        private String name;
        @XStreamAlias("result1")
        private String result;

        public Cat() {
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public Integer getAge() {
            return age;
        }

        public String getName() {
            return name;
        }

        public String getResult() {
            return result;
        }

        public Cat(Integer age, String name, String result) {
            this.age = age;
            this.name = name;
            this.result = result;
        }


    }

    @XStreamAlias("Animal")
    static class Animal {
        @XStreamAlias("catbng")
        private Cat cat;
        private Add add;

        public void setCat(Cat cat) {
            this.cat = cat;
        }

        public void setAdd(Add add) {
            this.add = add;
        }

        public Cat getCat() {
            return cat;
        }

        public Add getAdd() {
            return add;
        }
    }

    @XStreamAlias("Catbng")
    public static class Catas {
        @XStreamAlias("age")
        private Integer age;
        @XStreamAlias("name")
        private String name;
        @XStreamAlias("result1")
        private String result;

        private Newparams newparams;


        public Catas() {
        }

        public Newparams getNewparams() {
            return newparams;
        }

        public void setNewparams(Newparams newparams) {
            this.newparams = newparams;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public Integer getAge() {
            return age;
        }

        public String getName() {
            return name;
        }

        public String getResult() {
            return result;
        }

        public Catas(Integer age, String name, String result) {
            this.age = age;
            this.name = name;
            this.result = result;
        }
        @XStreamAlias("param")
        public static class Newparams{
            @XStreamAlias("sex")
            private String test;

            public Newparams(String test) {
                this.test = test;
            }

            public Newparams() {
            }

            public String getTest() {
                return test;
            }

            public void setTest(String test) {
                this.test = test;
            }
        }

    }
}
