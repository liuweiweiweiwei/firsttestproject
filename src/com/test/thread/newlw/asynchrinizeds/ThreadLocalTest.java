package com.test.thread.newlw.asynchrinizeds;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/27 11:34
 * @description
 */
public class ThreadLocalTest {

    public static int num = 0;

    public static ThreadLocal<Integer> localNum = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };

    public static void main(String[] args) {
        Thread[] threads = new Thread[5];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(()->{
                num += 5;
                System.out.println(Thread.currentThread().getName() + "::" + num);
            });
        }
        for (Thread t:threads) {
            t.start();
        }

        Thread[] threads1 = new Thread[5];
        for (int i = 0; i < threads1.length; i++) {
            threads1[i] = new Thread(()->{
                Integer integer = localNum.get();
                localNum.set(integer + 5);
                System.out.println(Thread.currentThread().getName() + "::" + localNum.get());
            });
        }
        for (Thread t:threads1) {
            t.start();
        }
    }
}
