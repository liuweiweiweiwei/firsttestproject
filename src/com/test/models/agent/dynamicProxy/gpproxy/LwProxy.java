package com.test.models.agent.dynamicProxy.gpproxy;


import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:32
 * @description  用于生成源代码
 */
public class LwProxy {


    private static final String str = "\r\n";
    public static Object newProxyInstance(LwClassLoader classLoader, Class<?>[] interfaces,
                                          LwInvocationHandle h)throws IllegalArgumentException{
        try{
            //1、动态生成源代码.java文件
            String src = generate(interfaces);

//           System.out.println(src);
            //2、Java文件输出磁盘
            String filePath = LwProxy.class.getResource("").getPath();
           System.out.println(filePath);
            File f = new File(filePath + "$Proxy0.java");
            FileWriter fw = new FileWriter(f);
            fw.write(src);
            fw.flush();
            fw.close();

            //3、把生成的.java文件编译成.class文件
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            StandardJavaFileManager manage = compiler.getStandardFileManager(null,null,null);
            Iterable iterable = manage.getJavaFileObjects(f);

            JavaCompiler.CompilationTask task = compiler.getTask(null,manage,null,null,null,iterable);
            task.call();
            manage.close();

            //4、编译生成的.class文件加载到JVM中来
            Class proxyClass =  classLoader.findClass("$Proxy0");
            Constructor c = proxyClass.getConstructor(LwInvocationHandle.class);
            f.delete();

            //5、返回字节码重组以后的新的代理对象
            return c.newInstance(h);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static String generate(Class<?>[] interfaces) {
        //
        StringBuffer buffer = new StringBuffer();
        buffer.append("package com.test.models.agent.dynamicProxy.gpproxy;" + str);
        buffer.append("import com.test.models.agent.dynamicProxy.jdkProxy.Person;" + str);
        buffer.append("import java.lang.reflect.*;" + str);
        buffer.append("public class $Proxy0 implements ").append(interfaces[0].getName()).append(" {").append(str);
        //class类内内容
        buffer.append("LwInvocationHandle h;" + str);
        buffer.append("public $Proxy0(LwInvocationHandle h) throws Exception {" + str +
                "        this.h = h;" + str +
                "    }" + str);
        for (Method m: interfaces[0].getDeclaredMethods()) {
            buffer.append("public ").append(m.getReturnType().getName()).append(" ").append(m.getName()).append("(){").append(str);
                buffer.append("try {"+str);
                buffer.append("Method m = ").append(interfaces[0].getName()).append(".class.getMethod(\"").append(m.getName()).append("\" ,new Class[]{});").append(str);
                buffer.append("this.h.invoke(this,m,null);"+str);
                buffer.append("}catch(Throwable e){" + str);
                buffer.append("e.printStackTrace();"+ str);
                buffer.append("}" +str);
            buffer.append("}"+ str);
        }

        buffer.append("}" + str);
        return buffer.toString();
    }
}
