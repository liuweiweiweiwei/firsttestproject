package com.test.classes.packagesimple;

import com.test.classes.myclassloader.DestroyDumplicated;

/**
 * @author 25338
 * @date 2022/5/14 12:10
 * @since 1.0
 * description
 */
public class SimpleTest {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        DestroyDumplicated destroyDumplicated = new DestroyDumplicated("F:/file/java/");
        Class<?> aClass = destroyDumplicated.loadClass("com.test.classes.packagesimple.SImple");
        System.out.println(destroyDumplicated.getParent());
        Object o = aClass.newInstance();
        System.out.println(o);
    }
}
