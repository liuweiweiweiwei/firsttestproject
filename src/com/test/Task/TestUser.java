package com.test.Task;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/9/24 16:57
 * @description
 */
public class TestUser implements Comparable<TestUser>{
    private String name;

    private String age;

    private String id;

    public TestUser() {
    }

    public TestUser(String name, String age, String id) {
        this.name = name;
        this.age = age;
        this.id = id;
    }

    @Override
    public String toString() {
        return "TestUser{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(TestUser o) {
        if(o.getId().equals(id)){
            return 0;
        }else{
            return -1;
        }
    }


}
