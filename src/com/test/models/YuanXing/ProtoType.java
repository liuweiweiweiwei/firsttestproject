package com.test.models.YuanXing;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 14:39
 * @description 原型借口
 */
public interface ProtoType {

    ProtoType clone1();
}
