package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:02
 * @description
 */
public class CouponStrategy implements  PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("领取优惠券，然后进行课程价格减去100元！");
    }
}
