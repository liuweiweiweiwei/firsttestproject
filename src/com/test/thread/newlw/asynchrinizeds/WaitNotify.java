package com.test.thread.newlw.asynchrinizeds;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/20 10:35
 * @description
 */
public class WaitNotify {

    public static void main(String[] args) throws InterruptedException {
        Object o = new Object();
        new ThreadB(o).start();
        TimeUnit.SECONDS.sleep(3);
        System.out.println(Runtime.getRuntime().availableProcessors());
        new ThreadA(o).start();
    }


    public static class ThreadA extends Thread{

        private Object object;

        public ThreadA(Object object) {
            this.object = object;
        }

        @Override
        public void run() {
            synchronized (object) {
                System.out.println("threadA start！");
                object.notify();
                System.out.println("threadA end！");
            }
        }
    }

    public static class ThreadB extends Thread{
        private Object object;

        public ThreadB(Object object) {
            this.object = object;
        }

        @Override
        public void run() {
            synchronized (object){
                System.out.println("threadB start！");
                try {
                    object.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("threadB start！");
            }
        }
    }
}
