package com.test.redis.pubsub;

import redis.clients.jedis.JedisPubSub;

import java.util.Arrays;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/31 14:13
 * @description
 */
public class JedisListenerPubSub extends JedisPubSub {

    /**
     * 获取消息订阅处理
     * @param channel
     * @param message
     */
    @Override
    public void onMessage(String channel, String message) {
        System.out.println("onMessage:" +channel + " ::: " + message );
        super.onMessage(channel, message);
    }

    /**
     * 初始化时候处理
     * @param channel
     * @param subscribedChannels
     */
    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println("onSubscribe:" +channel + " ::: " + subscribedChannels );
        super.onSubscribe(channel, subscribedChannels);
    }

    /**
     * 取消发布订阅的时候处理
     * @param channel
     * @param subscribedChannels
     */
    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println("onUnsubscribe:" +channel + " ::: " + subscribedChannels );
        super.onUnsubscribe(channel, subscribedChannels);
    }


    @Override
    public void subscribe(String... channels) {
        System.out.println("subscribe:" + Arrays.toString(channels));
        super.subscribe(channels);
    }

    @Override
    public void psubscribe(String... patterns) {
        System.out.println("psubscribe:" + Arrays.toString(patterns));
        super.psubscribe(patterns);
    }
}
