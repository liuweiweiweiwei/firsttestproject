package com.test.autimatic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/22 14:00
 * @description 交并补
 */
public class HandInAndSupplement {

    public static class Element<V>{
        public V v;

        public Element(V v) {
            this.v = v;
        }
    }


    public static class NewSet<V>{
        //  数据对应的包装数据
        public HashMap<V, Element<V>> data;
        //  数据查找的父节点数据

        public HashMap<Element<V>,Element<V>> father;
        //  数据对应的长度数据

        public HashMap<Element<V>,Integer> sizeMap;

        /**
         * 初始化
         * @param list
         */
        public NewSet(List<V> list) {
            this.data = new HashMap<>();
            this.father = new HashMap<>();
            this.sizeMap = new HashMap<>();
            for (V v:list) {
                Element<V> vElement = new Element<>(v);
                data.put(v,vElement);
                father.put(vElement,vElement);
                sizeMap.put(vElement,1);
            }
        }

        /**
         * 查找头节点
         * @return
         */
        public Element<V> head(Element<V> a){
            //更新节点---都指向顶节点
            Stack<Element<V>> stack = new Stack<>();
            //获取根节点位置
            while (father.get(a) != a){
                stack.push(a);
                a = father.get(a);
            }
            while (!stack.isEmpty()){
                father.put(stack.pop(),a);
            }
            return a;
        }

        /**
         * 判断是否在同一个集合内
         * @param a
         * @param b
         * @return
         */
        public boolean isInSimpleList(V a,V b){
            if(data.get(a) != null && data.get(b) != null){
                return head(data.get(a)) == head(data.get(b));
            }
            return false;
        }

        /**
         * 合并操作
         * @param a
         * @param b
         */
        public void union(V a,V b){
            if(data.containsKey(a) && data.containsKey(b)){
                Element<V> aElement = head(data.get(a));
                Element<V> bElement = head(data.get(b));
                if(aElement != bElement){
                    Element<V> fElement = sizeMap.get(aElement) >= sizeMap.get(bElement) ? aElement : bElement;
                    Element<V> sElement = fElement == aElement ? bElement : aElement;
                    //更新父节点指向--更新数据量
                    father.put(sElement,fElement);
                    sizeMap.put(fElement,sizeMap.get(fElement) + sizeMap.get(sElement));
                    sizeMap.remove(sElement);
                }
            }
        }


    }


}
