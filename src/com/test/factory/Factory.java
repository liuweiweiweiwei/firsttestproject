package com.test.factory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 9:53
 * @description
 */
public abstract class Factory {


    public static String name(String head){
        return head+"Factory";
    }

    public abstract String getSomeThing();

}
