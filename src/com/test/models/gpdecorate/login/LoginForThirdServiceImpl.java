package com.test.models.gpdecorate.login;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 10:27
 * @description
 */
public class LoginForThirdServiceImpl implements ILoginForThirdService {
    private ILoginService service;

    public LoginForThirdServiceImpl(ILoginService service) {
        this.service = service;
    }

    @Override
    public String loginForQQ(String qq) {
        return null;
    }

    @Override
    public String loginForPhone(String phone) {
        return null;
    }

    @Override
    public String loginForWC(String id) {
        return null;
    }

    @Override
    public String login(String id) {
        return service.login(id);
    }

    @Override
    public String register(String id, String name) {
        return service.register(id, name);
    }
}
