package com.test.thread.javaconcurrencyprogram.jdkthread;

import com.test.MyLog.LwLog;
import org.apache.poi.ss.formula.functions.T;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.informix.jdbc.IfxTmpFile.e;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/5 20:53
 * @description
 */
public class JdkThread {
    private static class MyThreadFactory implements ThreadFactory{
        private AtomicInteger incre = new AtomicInteger();

        private boolean isDemo;

        public MyThreadFactory(boolean isDemo) {
            this.isDemo = isDemo;
        }

        @Override
        public Thread newThread(Runnable r) {
            incre.incrementAndGet();
            Thread thread = new Thread(r, "LW-THREAD-" + incre);
            thread.setDaemon(isDemo);
            return thread;
        }
    }
    private static final BlockingQueue<Runnable> BLOCKING_QUEUE = new LinkedBlockingQueue<>(100);
    public static final ExecutorService executorService = new ThreadPoolExecutor(
            1,
            1,
            30,
            TimeUnit.SECONDS,
            BLOCKING_QUEUE,
            new MyThreadFactory(true),
            new ThreadPoolExecutor.AbortPolicy());
    public static final ForkJoinPool FORK_JOIN_POOL = new ForkJoinPool(

    );

    public static void main(String[] args) throws Exception {
//        LwLog.splitLogInfo("分割线");
//        testFuture();
        LwLog.splitLogInfo("分割线");
        supplyRunAsync();
        LwLog.splitLogInfo("分割线");
//        runAsync();
//        sleepMy(3);
//        LwLog.splitLogInfo("分割线");
//        LwLog.outErr("第二阶段");
//        LwLog.splitLogInfo("分割线");
//        thenApplyAndAsync();
//        LwLog.splitLogInfo("分割线");
//        runAccept();
//        LwLog.splitLogInfo("分割线");
//        exceptionallyHandle();
//        LwLog.splitLogInfo("分割线");
//        whenCompleteMy();
//        LwLog.splitLogInfo("分割线");
//        handleMy();
//        LwLog.outErr("第三阶段");
//        LwLog.splitLogInfo("分割线");
//        combineAcceptAfterBoth();
//        LwLog.splitLogInfo("分割线");
//        editer();
//        LwLog.splitLogInfo("分割线");
//        compose();
    }

    /**
     * sleep
     * @param second s
     */
    private static void sleepMy(int second) {
        try {
            TimeUnit.SECONDS.sleep(second);
        } catch (InterruptedException e) {
            LwLog.errorInfo(Thread.currentThread().getName() + " : happend error!");
        }
    }

    /**
     * exception
     */
    private static void exception() {
        throw new RuntimeException("this is a run time exception!");
    }

    /**
     * carry future
     * @param t e
     * @param <T> e
     * @throws ExecutionException w
     * @throws InterruptedException e
     */
    private static <T extends Future> void carry(T t) throws ExecutionException, InterruptedException {
        // main 函数执行
        LwLog.logInfo(Thread.currentThread().getName() + " ： start,time : " + LocalDateTime.now());
        // 异步线程执行
//        LwLog.logInfo(Thread.currentThread().getName() + " future result : " + t.get());
        LwLog.logInfo(Thread.currentThread().getName() + " ： end,time : " + LocalDateTime.now());
    }

    /**
     * future submit
     */
    public static void testFuture() throws ExecutionException, InterruptedException {
        // 创建异步任务
        Future<String> future = executorService.submit(() -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start,time : " + LocalDateTime.now());
            sleepMy(1);
            /* exception(); */
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens,time : " + LocalDateTime.now());
            return "success";
        });
        carry(future);
    }

    /**
     * supply run async 带返回值的异步任务
     */
    public static void supplyRunAsync() throws ExecutionException, InterruptedException {
        // 默认 ForkJoinPool.commonPool
        ForkJoinPool pool = ForkJoinPool.commonPool();
        CompletableFuture.supplyAsync(()-> {
            System.out.println(Thread.currentThread());
            return "你好";
        }, pool).thenRun(() -> System.out.println("hello"));
        CompletableFuture<String> handle = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread());
            return "ASYNC_HANDLE";
        }, pool).handleAsync((s, e) ->  s + " some this");
        System.out.println(handle.get());
        // 创建异步执行任务有返回值
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start,time : " + LocalDateTime.now());
            sleepMy(1);
            /* exception(); */
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens,time : " + LocalDateTime.now());
            return 1;
        }, pool);
        System.out.println("end of method!");
        sleepMy(5);

    }

    /**
     * run async
     * @throws ExecutionException e
     * @throws InterruptedException e
     */
    public static void runAsync() throws ExecutionException, InterruptedException {
        CompletableFuture completableFuture = CompletableFuture.runAsync(() -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start,time : " + LocalDateTime.now());
            sleepMy(1);
            /* exception(); */
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens,time : " + LocalDateTime.now());
            //可设置默认的common pool 也可使用自己定义的线程池---单核机器使用
        }, executorService);
        carry(completableFuture);
    }

    // ------------------------------------异步回调-------------------------------------------------------------

    /**
     * common completable future
     * @return cf
     */
    private static <T> CompletableFuture<T> getCompletable(T t) {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        return CompletableFuture.supplyAsync(() -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start supplyAsync one,time : " + LocalDateTime.now());
            sleepMy(1);
            /* exception(); */
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens supplyAsync one,time : " + LocalDateTime.now());
            //可设置默认的common pool 也可使用自己定义的线程池---单核机器使用
            return t;
        }, forkJoinPool);
    }

    /**
     * then apply then apply async
     */
    public static void thenApplyAndAsync() throws ExecutionException, InterruptedException {
        CompletableFuture<String> cf = getCompletable("JOB-ONE");
        // thenApplyAsync 可以设置 executor 而thenApply则不能
        CompletableFuture<String> completableFuture = cf.thenApplyAsync((result) -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start job two,time : " + LocalDateTime.now());
            sleepMy(1);
            /* exception(); */
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens job two,time : " + LocalDateTime.now());
            //可设置默认的common pool 也可使用自己定义的线程池---单核机器使用
            return result + ": success";
        }, executorService);
        carry(cf);
        sleepMy(1);
        carry(completableFuture);
    }

    /**
     * then accept then run
     * then apply 有参数 有返回值 then accept 和接收上一个返回值作为参数，但是无返回值
     * then run 无参数无返回值
     */
    public static void runAccept() throws ExecutionException, InterruptedException {
        CompletableFuture<String> cf = getCompletable("JOB-ACCEPT");
        // 以cf结果为入参 传入 then apply中
        CompletableFuture completableFuture = cf.thenApply((result) -> {
            sleepMy(1);
            LwLog.logInfo(result);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start then apply 2,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens then apply 2,time : " + LocalDateTime.now());
            //可设置默认的common pool 也可使用自己定义的线程池---单核机器使用
            return result + ": ACCEPT success";
            // 以上一个任务执行结果为入参 传入 accept中 无返回值
        }).thenAccept((result) -> {
            sleepMy(1);
            LwLog.logInfo(result);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start then accept 3,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens then accept 3,time : " + LocalDateTime.now());
        }).thenRun(() -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start then run4,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens then run 4,time : " + LocalDateTime.now());
        });
        carry(cf);
        sleepMy(1);
        carry(completableFuture);
    }

    /**
     * when exceptionally 遇到异常返回处理
     */
    public static void exceptionallyHandle() throws ExecutionException, InterruptedException {
        CompletableFuture<String> completable = getCompletable("EXCEPTION-JOB");
        CompletableFuture<String> completableFuture = completable.exceptionally((param) -> {
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start exceptionally 2,time : " + LocalDateTime.now());
            param.printStackTrace();
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens exceptionally 2,time : " + LocalDateTime.now());
            return param + "：1.2d";
        });
        CompletableFuture<Void> voidCompletableFuture = completable.thenAccept((result) -> {
            sleepMy(1);
            LwLog.logInfo(result);
            LwLog.logInfo(Thread.currentThread().getName() + " ; start then accept 3,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens then accept 3,time : " + LocalDateTime.now());
        });
        sleepMy(1);
        carry(completableFuture);
        carry(voidCompletableFuture);
    }

    /**
     * when complete 当某个任务执行完成后回调，会将执行结果和异常返回，如果为繁盛异常，则一场为null
     */
    public static void whenCompleteMy() throws ExecutionException, InterruptedException {
        CompletableFuture<Double> completable = getCompletable(1.2);
        CompletableFuture<Double> completableFuture = completable.whenComplete((r, e) -> {
            sleepMy(1);
            LwLog.logInfo(String.format("result:%s;exception:%s", r, e));
            LwLog.logInfo(Thread.currentThread().getName() + " ; start when complete,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens when complete,time : " + LocalDateTime.now());
        });
        carry(completable);
        sleepMy(1);
        carry(completableFuture);
    }

    /**
     * handle 同whencomplete几乎一样，只不过handle又返回结果有返回值
     */
    public static void handleMy() throws ExecutionException, InterruptedException {
        CompletableFuture<Long> completable = getCompletable(123L);
        CompletableFuture<Long> handle = completable.handle((r, e) -> {
            sleepMy(1);
            LwLog.logInfo(String.format(Thread.currentThread().getName() + " result : %s;exception : %s", r, e));
            LwLog.logInfo(Thread.currentThread().getName() + " ; start handle,time : " + LocalDateTime.now());
            LwLog.logInfo(Thread.currentThread().getName() + " ; ens handle,time : " + LocalDateTime.now());
            return r + 1;
        });
        carry(completable);
        sleepMy(1);
        carry(handle);
    }

    /**
     * completableFuture 合并执行：
     * thenCombine会将两个异步方法执行完后才执行自己的任务，将异步方法返回值作为入参，且有返回值
     * thenAcceptBoth 同样将两个方法执行结果作为入参，但无返回值
     * thenAfterBoth 无入参，无返回值
     * 注意：两个任务只要有一个执行异常，异常信息都会作为指定任务的执行结果
     */
    public static void combineAcceptAfterBoth() throws ExecutionException, InterruptedException {
        CompletableFuture<String> jobOne = getCompletable("JOB_ONE");
        CompletableFuture<String> jobTwo = getCompletable("JOB_TWO");
        // 将job1 和job2的返回值作为入参， 且有返回值
        CompletableFuture<String> combine = jobOne.thenCombine(jobTwo, (r, e) -> {
            LwLog.logInfo(Thread.currentThread().getName() + " combine sync : start");
            LwLog.logInfo(String.format(Locale.ROOT, Thread.currentThread().getName()
                    + " combine result param: %s ; exception param: %s", r, e));
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " combine sync : end");
            return "COMBINE-" + r;
        });
        // job1 和 job2 的异步执行结果完成后,将结果作为方法入参传递给 afterBoth, 无返回值
        CompletableFuture acceptAfterBoth = jobOne.thenAcceptBoth(jobTwo, (r, e) -> {
            LwLog.logInfo(Thread.currentThread().getName() + " thenAcceptBoth sync : start");
            LwLog.logInfo(String.format(Locale.ROOT, Thread.currentThread().getName()
                    + " thenAcceptBoth result param: %s ; exception param: %s", r, e));
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " thenAcceptBoth sync : end");
        });
        // combine和thenaccept 执行完毕后 在执行下边completablefuture 无返回值
        CompletableFuture runAfterBoth = combine.runAfterBoth(acceptAfterBoth, () -> {
            LwLog.logInfo(Thread.currentThread().getName() + " runAfterBoth sync : start");
            sleepMy(1);
            LwLog.logInfo(Thread.currentThread().getName() + " runAfterBoth sync : end");
        });
        // 执行
        carry(jobOne);
        sleepMy(1);
        carry(runAfterBoth);
    }

    /**
     * applytoEditer:将已经执行完任务的执行结果作为方法入参，并有返回值
     * acceptEditer：将已经执行完任务的执行结果作为方法入参，无返回值,
     * runAfterEditer：无入参，无返回值
     */
    public static void editer() throws ExecutionException, InterruptedException {
        CompletableFuture<String> taskOne = getCompletable("TASK-ONE");
        CompletableFuture<String> taskTwo = getCompletable("TASK-TWO");
        CompletableFuture<String> applyToEditor = taskOne.applyToEither(taskTwo, r -> {
            LwLog.logInfo(Thread.currentThread().getName() + " applyToEditor  START ......");
            LwLog.logInfo(String.format(Locale.ROOT, Thread.currentThread().getName()
                    + " applyToEditor param result : %s", r));
            LwLog.logInfo(Thread.currentThread().getName() + " applyToEditor  END ......");
            return "applyToEditor-" + r;
        });
        CompletableFuture acceptEditor = taskOne.acceptEither(taskTwo,r -> {
            LwLog.logInfo(Thread.currentThread().getName() + " acceptEditor START ......");
            LwLog.logInfo(String.format(Locale.ROOT, Thread.currentThread().getName()
                    + " acceptEditor param result : %s", r));
            LwLog.logInfo(Thread.currentThread().getName() + " acceptEditor END ......");
        });
        CompletableFuture afterEditor = applyToEditor.runAfterEither(acceptEditor, () -> {
            LwLog.logInfo(Thread.currentThread().getName() + " afterEditor START ......");
            LwLog.logInfo(Thread.currentThread().getName() + " afterEditor param result");
            LwLog.logInfo(Thread.currentThread().getName() + " afterEditor END ......");
        });
        carry(taskOne);
        sleepMy(1);
        carry(afterEditor);
    }

    /**
     * compose 执行完某个任务后，将任务执行结果作为入参给指定方法，然后此方法返回一个新的实例
     * 然后执行这个新的任务
     */
    public static void compose() throws ExecutionException, InterruptedException {
        CompletableFuture<String> taskOne = getCompletable("TASK_ONE");
        CompletableFuture<String> compose = taskOne.thenCompose(r -> {
            LwLog.logInfo(Thread.currentThread().getName() + " compose ONE START ......");
            LwLog.logInfo(Thread.currentThread().getName() + " compose ONE param result + :: " + r);
            LwLog.logInfo(Thread.currentThread().getName() + " compose ONE END ......");
            return CompletableFuture.supplyAsync(() -> {
                LwLog.logInfo(Thread.currentThread().getName() + " compose supplyAsync ONE START ......");
                LwLog.logInfo(Thread.currentThread().getName() + " supplyAsync ONE param result");
                LwLog.logInfo(Thread.currentThread().getName() + " compose supplyAsync ONE END ......");
                return r + "-INNER";
            });
        });
        carry(taskOne);
        sleepMy(1);
        carry(compose);
    }
}
