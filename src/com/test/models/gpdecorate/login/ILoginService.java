package com.test.models.gpdecorate.login;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 10:24
 * @description 装饰器模式
 */
public interface ILoginService {

    String login(String id);

    String register(String id,String name);
}
