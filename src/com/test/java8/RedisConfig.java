package com.test.java8;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisConfig {

    public static Jedis getConnection(){
        //连接池配置
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
//        jedisPoolConfig.setMaxTotal(100);
//        jedisPoolConfig.setMaxIdle(10);
        //连接池jedisPoolConfig,"localhost",6379
        JedisPool jedisPool = new JedisPool();

        Jedis jedis=jedisPool.getResource();
        //jedis.auth("123456");
        return jedis;
    }
}
