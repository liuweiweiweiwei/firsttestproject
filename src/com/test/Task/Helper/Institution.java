package com.test.Task.Helper;

/**
 * @author 25338
 * @version 1.1
 * @date 2020/8/2 16:34
 */
import java.util.List;

public class Institution {
    private String institutionId;

    private String institutionName;

    private List<Store> storeList;

    @Override
    public String toString() {
        return "Institution{" +
                "institutionId='" + institutionId + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", storeList=" + storeList +
                '}';
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }


}
