package com.test.models.single;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 13:49
 * @description 额韩式单利
 */
public class HungerSingle {

    private HungerSingle(){}

    private static final HungerSingle HUNGER_SINGLE = new HungerSingle();

    public static HungerSingle getINstance(){return HUNGER_SINGLE;}
}
