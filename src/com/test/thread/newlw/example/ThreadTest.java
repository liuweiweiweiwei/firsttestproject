package com.test.thread.newlw.example;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/16 21:14
 * @description
 */
public class ThreadTest {



    public static void main(String[] args) throws InterruptedException {
//
//        SaveProcess saveProcess = new SaveProcess(null);
//        PrintProcess printProcess = new PrintProcess(saveProcess);
//        User user0 = new User("小明");
//        User user1 = new User("小蓝");
//        User user2 = new User("小紅");
//        printProcess.process(user0);
//        printProcess.process(user1);
//        printProcess.process(user2);
//        new Thread(printProcess).start();
//        new Thread(saveProcess).start();

        new Thread(()->{
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"time_wait_thread").start();

        new Thread(()->{
            while (true) {
                synchronized (ThreadTest.class) {
                    try {
                        ThreadTest.class.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        },"wait_thread").start();

        new Thread(new BlockThread(), "block_thread").start();
//        TimeUnit.SECONDS.sleep(2);
        new Thread(new BlockThread1(), "block_thread2").start();
    }


    public volatile static Object WAIT = new Object();
    public static class BlockThread extends Thread{
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + "进入run");
            synchronized (WAIT){
                System.out.println(Thread.currentThread().getName() + "进入sync");
                while (true){
                    try {
                        TimeUnit.SECONDS.sleep(5);
                        System.out.println(Thread.currentThread().getName() + "结束");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    public static class BlockThread1 extends Thread{
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + "进入run");
            synchronized (WAIT){

                WAIT.notify();
                System.out.println(Thread.currentThread().getName() + "进入sync");
            }
        }
    }
}
