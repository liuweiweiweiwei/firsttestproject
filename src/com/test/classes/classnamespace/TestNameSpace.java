package com.test.classes.classnamespace;

/**
 * @author 25338
 * @date 2022/5/14 11:30
 * @since 1.0
 * description
 */
public class TestNameSpace {
    private String name;
    private String sex;

    public TestNameSpace(String name, String sex) {
        this.name = name;
        this.sex = sex;
    }

    public TestNameSpace() {
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
