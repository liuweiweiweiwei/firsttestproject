package com.test.java8;

import java.util.ArrayDeque;
import java.util.Queue;

public class ThreadOne<T> {
    private Queue<T> list;
    private int size;

    public ThreadOne( int size) {
        this.size=size;
        this.list = new ArrayDeque<>(size);
    }

    public synchronized void addStr(T s) throws InterruptedException {
        while (list.size()==size){
            wait();
        }
        list.add(s);
        notifyAll();
    }

    public synchronized T remove() throws InterruptedException {
        while (list.size()==0){
            wait();
        }
        T t=list.poll();
        notifyAll();
        return t;
    }
}
