package com.test.java8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Optionals {
    //时间类型格式化
    public static final String Y_M_D_H_M_S="yyyy-MM-dd HH:mm:ss";
    //时间格式化2
    public static final String Y_M_D="yyyy-MM-dd";
    //最小时间
    public static final String MAX_TIME="23:59:59";
    //最大时间
    public static final String MIN_TIME="00:00:00";

    public static void main(String[] args) {
        List<Student> students= Arrays.asList(new Student[]{new Student(8,"肖勉"),
                new Student(18,"lisar"),new Student(22,"sam")});
        Optional.of(students).orElse(students).forEach(student -> System.out.println(student.getName()));
        List<Student> students2=students.stream().filter(student -> student.getAge()>10).collect(Collectors.toList());
        //students2.forEach(student -> System.out.println(student.toString()));
        Map<String,Integer> map=students.stream().filter(student -> "sam".equals(student.getName())).collect(Collectors.toMap(Student::getName,Student::getAge));
        Set<String> strings=map.keySet();
        Iterator<String> iterator=strings.iterator();
        while (iterator.hasNext()){
            String s=iterator.next();
            System.out.println(s+"::"+map.get(s).toString());
        }
        String ss=null;
        String s1=Optional.ofNullable(ss).orElse("::");
        System.out.println(s1);
        //-------------------------------------------------
        System.out.println(getMinTime());
        System.out.println(getMaxTime());
    }

    /**
     * 获取当天最小值
     * @return 最小值
     */
    public static LocalDateTime getMinTime(){
        String format = DateTimeFormatter.ofPattern(Y_M_D).format(LocalDate.now());
        String stringTime=format+" "+MIN_TIME;
        return LocalDateTime.parse(stringTime,DateTimeFormatter.ofPattern(Y_M_D_H_M_S));
    }

    /**
     * 获取当天最大值
     * @return 最大值
     */
    public static LocalDateTime getMaxTime(){
        String format = DateTimeFormatter.ofPattern(Y_M_D).format(LocalDate.now());
        String stringTime=format+" "+MAX_TIME;
        return LocalDateTime.parse(stringTime,DateTimeFormatter.ofPattern(Y_M_D_H_M_S));
    }

}
