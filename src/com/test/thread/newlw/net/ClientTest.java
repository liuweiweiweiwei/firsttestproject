package com.test.thread.newlw.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 14:46
 * @description
 */
public class ClientTest {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost",8790);
        User u = new User("刘巍",25);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(u);
        socket.close();
    }
}
