package com.test.models.factory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:35
 * @description 工厂
 */
public class CourseFactory {

    public MyCourse create(String name){
        if(!(name == null || "".equals(name.trim()))){
            try{
                return (MyCourse) Class.forName(name).newInstance();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

    public MyCourse createF(Class<?> name){
        if(null != name){
            try {
                return (MyCourse) name.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
