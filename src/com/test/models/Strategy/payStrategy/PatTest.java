package com.test.models.Strategy.payStrategy;

import java.util.Arrays;
import java.util.TreeMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 19:01
 * @description
 */
public class PatTest {

    public static void main(String[] args) {
        Order order = new Order("1","2021120701",260.5);
        MsgResult pay = order.pay(PayStrategy.WC_PAY);
        System.out.println(pay.toString());
        MsgResult msgResult = order.pay1(YinLianPay.class);
        System.out.println(msgResult.toString());
        MsgResult msgResult1 = order.pay2("com.test.models.Strategy.payStrategy.AliPay");
        System.out.println(msgResult1.toString());
    }
}
