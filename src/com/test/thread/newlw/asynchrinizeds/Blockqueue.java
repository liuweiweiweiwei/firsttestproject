package com.test.thread.newlw.asynchrinizeds;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/27 10:58
 * @description
 */
public class Blockqueue {

    public static ArrayBlockingQueue<String> arrayBlockingQueue = new ArrayBlockingQueue<String>(10);
    static {
        init();
    }
    private static void init(){
        new Thread(()->{
            while (true) {
                try {
                    String take = arrayBlockingQueue.take();
                    System.out.println("阻塞队列中取到数据：receive -> " + take);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void addBlock(String val) throws InterruptedException {
        System.out.println("addBlock -> " + val);
        arrayBlockingQueue.add("blockKey" + val);
        TimeUnit.SECONDS.sleep(1);
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            addBlock("testBlockQueue::" + i);
        }
    }
}
