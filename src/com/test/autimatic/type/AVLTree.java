package com.test.autimatic.type;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/17 16:53
 * @description
 */
public class AVLTree<T extends Comparable> {

    public static void main(String[] args) {
        AVLTree tree = new AVLTree();
        Tree<Integer> tree0 = new Tree<>(5);
        Tree<Integer> tree1 = new Tree<>(2);
        Tree<Integer> tree2 = new Tree<>(6);
        Tree<Integer> tree3 = new Tree<>(1);
        Tree<Integer> tree4 = new Tree<>(3);
        Tree<Integer> tree5 = new Tree<>(4);
        Tree<Integer> tree6 = new Tree<>(7);
        tree0.right = tree2;tree0.left = tree1;
        tree1.left = tree3;tree1.right = tree4;
        tree4.right = tree5;tree2.right = tree6;
        tree.afterIterator(tree0);
        System.out.println("-----------------------------");
        tree.middleIterator(tree0);
        System.out.println("-----------------------------");
        tree.beforeIterator(tree0);
        System.out.println("-----------------------------");
        Tree tree7 = tree.delTree(2, tree0);
        tree.middleIterator(tree7);
        System.out.println("-----------------------------");
        tree.addTree(tree0,8,tree0,1);
        tree.middleIterator(tree0);
        System.out.println("-----------------------------");

        //morris
        morrisMid(tree0);
        System.out.println();
    }


    /**
     * 后序
     * @param tree
     */
    public  void afterIterator(Tree<T> tree){
        if(tree == null){
            return;
        }
        afterIterator(tree.left);
        afterIterator(tree.right);
        System.out.print(tree.data + ",");
    }

    /**
     * 前序
     * @param tree
     */
    public  void beforeIterator(Tree<T> tree){
        if(tree == null){
            return;
        }
        System.out.print(tree.data + ",");
        beforeIterator(tree.left);
        beforeIterator(tree.right);
    }

    /**
     * 中序
     * @param tree
     */
    public  void middleIterator(Tree<T> tree){
        if(tree == null){
            return;
        }
        middleIterator(tree.left);
        System.out.print(tree.data + ",");
        middleIterator(tree.right);
    }

    public Tree addTree(Tree tree,T t,Tree parent,int height){
        if(tree == null){
            tree = new Tree(t);
            tree.parent = parent;
            tree.height = height;
            return tree;
        }
        int comp = tree.data.compareTo(t);
        if(comp > 0){
            tree.left =  addTree(tree.left,t,tree,height+1);
        }else {
            tree.right = addTree(tree.right, t, tree, height + 1);
        }
        return tree;
    }


    public <T extends Comparable> Tree delTree(T t, Tree node) {
        if (null == node) {
            return null;
        }
        int compare = node.data.compareTo(t);
        if (compare == 0) {
            node = delRightMin(node);
        } else if (compare > 0) {
            node.left = delTree(t, node.left);
        } else {
            node.right = delTree(t, node.right);
        }
        return node;
    }

    private <T extends Comparable> Tree delRightMin(Tree<T> tree) {
        //左右子树都为null直接删
        if (tree.right == null && tree.left == null) {
            return null;
        }
        //右子树空左子树替换
        if (tree.right == null) {
            tree.left.parent = tree.parent;
            return tree.left;
        }
        //左子树空右子树替换
        if (tree.left == null) {
            tree.right.parent = tree.parent;
            return tree.right;
        }
        //选择右子树最小节点替换
        Tree<T> temp = tree.right;
        Tree parent = tree.parent;
        Tree left = tree.left;
        while (temp.left != null) {
            temp = temp.left;
        }
        Tree<T> newTree = new Tree<>(temp.data);
        newTree.left = left;
        newTree.parent = parent;
        newTree.right = delTree(temp.data, temp);
        return newTree;
    }




    public static class Tree<T extends Comparable> {
        private Tree parent;
        private Tree left;
        private Tree right;
        private T data;
        private int height;

        public Tree(T data) {
            this.data = data;
        }

        public Tree() {
        }

        public Tree(Tree parent, Tree left, Tree right) {
            this.parent = parent;
            this.left = left;
            this.right = right;
        }

        public Tree getParent() {
            return parent;
        }

        public void setParent(Tree parent) {
            this.parent = parent;
        }

        public Tree getLeft() {
            return left;
        }

        public void setLeft(Tree left) {
            this.left = left;
        }

        public Tree getRight() {
            return right;
        }

        public void setRight(Tree right) {
            this.right = right;
        }
    }


    /**
     * morris遍历
     * @param tree
     */
    public static void morris(Tree<Integer> tree){
        if(tree == null){
            return;
        }
        Tree head = tree;
        Tree left = null;
        while (head != null){
            //获取左子树节点
            left = head.left;
            if(left != null) {
                //找出左子树中最右边的节点
                while (left.right != null && left.right != head) {
                    left = left.right;
                }
                if (left.right == null) {
                    System.out.print(head.data + ", ");
                    //让节点指向当前头结点--第一次操作
                    left.right = head;
                    //头结点指向做子树节点
                    head = head.left;
                    continue;
                } else {//还原操作---还原当前节点指向null-第二次操作
                    left.right = null;
                }
            }else{//没有左树
                System.out.print(head.data + ", ");
            }
            //遍历到右子树
            head = head.right;
        }
    }
    public static void morrisMid(Tree<Integer> tree){
        if(tree == null){
            return;
        }
        Tree head = tree;
        Tree left = null;
        while (head != null){
            //获取左子树节点
            left = head.left;
            if(left != null) {
                //找出左子树中最右边的节点
                while (left.right != null && left.right != head) {
                    left = left.right;
                }
                if (left.right == null) {
                    //让节点指向当前头结点--第一次操作
                    left.right = head;
                    //头结点指向做子树节点
                    head = head.left;
                    continue;
                } else {//还原操作---还原当前节点指向null-第二次操作
                    left.right = null;
                }
            }//没有左树
            System.out.print(head.data + ",");

            //遍历到右子树
            head = head.right;
        }
    }

}
