package com.test.models.template;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/15 14:33
 * @description 模
 */
public class TaskTemplate {
    public static void main(String[] args) {
        Task task=new Task();
        task.process();
    }
}

interface Template{
    void process();
}
abstract class TaskTem implements Template{
    abstract String init();
    abstract String start(String param);
    abstract String end(String param);
    @Override
    public final void process() {
        String result=init();
        String resultTemp=start(result);
        String end = end(resultTemp);
        System.out.println(end);
    }
}
class Task extends TaskTem{
    @Override
    String init() {
        System.out.println("初始化任务");
        return "dataTask";
    }
    @Override
    String start(String param) {
        System.out.println("任务开始");
        return param+":start";
    }
    @Override
    String end(String param) {
        System.out.println("任务完成！");
        return param+":end";
    }
}
