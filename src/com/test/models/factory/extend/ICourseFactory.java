package com.test.models.factory.extend;


import com.test.models.factory.MyCourse;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:56
 * @description
 */
public interface ICourseFactory {
    MyCourse createFactory();
}
