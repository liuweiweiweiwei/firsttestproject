package com.test.thread.javaconcurrencyprogram.threadBase;

import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/24 20:18
 * @description
 */
public interface FightQuery {
    /**
     * 获取飞行器
     * @return 飞行工具集合
     */
    List<String> getQuery();
}
