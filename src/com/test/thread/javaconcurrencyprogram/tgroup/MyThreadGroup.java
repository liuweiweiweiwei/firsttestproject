package com.test.thread.javaconcurrencyprogram.tgroup;

import com.test.MyLog.LwLog;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/2 21:17
 * @description
 */
public class MyThreadGroup {
    public static void main(String[] args) throws InterruptedException {
        // 当前group
        ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();

        // new group
        ThreadGroup threadGroup1 = new ThreadGroup("GENIE");

        // 判断
        System.out.println(threadGroup == threadGroup1.getParent());

        // 第二个
        ThreadGroup threadGroup2 = new ThreadGroup(threadGroup1, "TIANMING");
        System.out.println(threadGroup == threadGroup2.getParent());

        Thread cur = new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(3);
                    LwLog.logInfo(Thread.currentThread().getName() + " is running!");
                } catch (InterruptedException e) {
                    LwLog.errorInfo(Thread.currentThread().getName() + " has error!");
                    break;
                }
            }
        }, "THREAD_WEIZHUANG");
        cur.start();

        TimeUnit.SECONDS.sleep(5);
        Thread[] threads = new Thread[threadGroup.activeCount()];
        int resume = threadGroup.enumerate(threads);
        System.out.println(resume);
        cur.interrupt();
        TimeUnit.SECONDS.sleep(1);

        //two
        resume = threadGroup.enumerate(threads, false);
        System.out.println(resume);
    }


}
