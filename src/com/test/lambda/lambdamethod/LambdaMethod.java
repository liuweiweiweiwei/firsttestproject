package com.test.lambda.lambdamethod;

import com.test.MyLog.LwLog;
import net.sf.cglib.core.Local;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/7 23:06
 * @description
 */
public class LambdaMethod<T> {
    private List<Disable<T>> disableList = new ArrayList<>();

    private final List<Consumer<T>> consumerList = new ArrayList<>();

    interface MathOperate<T>{
        T add(T t, T t1);
    }

    interface MethodInter<T>{
        void accept();
    }

    interface Disable<T>{
        void disable();
    }

    protected MethodInter<String> method(String param) {
        return () -> System.out.println(param);
    }


    public Disable<T> disable(Disable<T> disable) {
        disableList.add(disable);
        return () -> disableList.remove(disable);
    }

    protected void notifyMethod(Disable<T> disable) {
        disableList.stream().filter(d -> d.equals(disable)).forEach(Disable::disable);
    }

    public static void main(String[] args) {
        MathOperate<Integer> mathOperate = (a, b) -> a + b;
        MathOperate<String> mathOperateStr = (a, b) -> a + b;
        MathOperate<Double> mathOperateDou = (a, b) -> a + b;

        Integer add = mathOperate.add(1, 2);
        Double add1 = mathOperateDou.add(1.1, 2.1);
        String add2 = mathOperateStr.add("one ", "two");
        System.out.println(String.format(Locale.ROOT, "add1 : %d ,add2 : %f, add3 : %s", add, add1, add2));

        LambdaMethod<String> lambdaMethod = new LambdaMethod<>();
        MethodInter<String> method = lambdaMethod.method("这是一个测试lambda的方法！");
        method.accept();

        Disable<String> disable = () -> System.out.println("第一个");
        Disable<String> disable1 = lambdaMethod.disable(disable);
        lambdaMethod.doSomeThing(() -> System.out.println("第二个"));
        LwLog.splitLogInfo("分割线");
        lambdaMethod.notifyMethod(disable);
        lambdaMethod.notifyMethod(disable);
        lambdaMethod.notifyMethod(disable);
        disable1.disable();
        LwLog.splitLogInfo("分割线");
        lambdaMethod.notifyMethod(disable);
        LwLog.splitLogInfo("分割线");
        System.out.println(78 * 6.5);
        LwLog.splitLogInfo("分割线");
        Disable<String> accept = lambdaMethod.accept((input) -> lambdaMethod.consumerTest("李猛"));
        lambdaMethod.accept((input) -> lambdaMethod.consumerTest("小红"));
        lambdaMethod.accept((input) -> lambdaMethod.consumerTest("小妹"));
        lambdaMethod.accept((input) -> lambdaMethod.consumerTest("小李"));
        System.out.println(lambdaMethod.consumerList.toString());
        lambdaMethod.consumerList.forEach((r) -> r.accept(null));
        LwLog.splitLogInfo("分割线");
        accept.disable();
        System.out.println(lambdaMethod.consumerList.toString());
        LwLog.splitLogInfo("分割线");
        lambdaMethod.consumerList.forEach((r) -> r.accept(null));
        LwLog.splitLogInfo("分割线");
        lambdaMethod.notifyD();
        System.out.println(lambdaMethod.consumerList.toString());
    }

    public void doSomeThing(Disable disable) {
        disable.disable();
        System.out.println("增在做什么！");
    }

    public void consumerTest(String input) {
        LwLog.logInfo(String.format(Locale.ROOT, "只是一个测试lambda的方法！%s", "He! " + input));
    }

    public Disable<T> accept(Consumer<T> consumer) {
        consumerList.add(consumer);
        return () -> consumerList.remove(consumer);
    }

    public void notifyD(){
        Iterator<Consumer<T>> iterator = consumerList.iterator();
        while (iterator.hasNext()) {
            Consumer<T> next = iterator.next();
            next.accept(null);
            iterator.remove();
        }
    }
}
