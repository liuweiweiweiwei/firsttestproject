package com.test.lambda;

import java.util.HashMap;
import java.util.Map;

public class JavaSpeed {
    public static enum Assces{
        TESTONE("tom",19),
        TESTTWO("jim",19),
        TESTTHREE("tim",19),
        TESTFOR("sim",19);
        private String name;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        Assces(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        Assces() {
        }
    }
    private static Map<String,Integer> MAP=new HashMap<>();
    static {
        MAP.put("tom",19);
        MAP.put("jim",19);
        MAP.put("tim",19);
        MAP.put("sim",19);
    }

    public static void main(String[] args) {
        long ti1=System.currentTimeMillis();
        for (int i = 0; i <10000000 ; i++) {
            Integer name=MAP.get("tom");
        }
        System.out.println(System.currentTimeMillis()-ti1);

        long ti2=System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            boolean judge=Assces.TESTFOR.getName().equals("tom");
        }
        System.out.println(System.currentTimeMillis()-ti2);
    }

    /**
     * 获取。。
     */
    public static void getTreeDeep(){

    }
}
