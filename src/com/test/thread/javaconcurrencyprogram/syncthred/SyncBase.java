package com.test.thread.javaconcurrencyprogram.syncthred;

import com.test.MyLog.LwLog;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/25 21:22
 * @description synchronized(this) == synchronized method  synchronized(class) == static synchronized method
 */
public class SyncBase {
    public static void main(String[] args) {
        new Thread(SyncBase::method3, "TM").start();
        new Thread(SyncBase::method4, "SY").start();
//        SyncBase syncThis = new SyncBase();
//        new Thread(syncThis::method1, "GENIE").start();
//        new Thread(syncThis::method2, "JINKE").start();
    }

    public synchronized void method1() {
        LwLog.logInfo(Thread.currentThread().getName() + " start ......");
        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            LwLog.errorInfo(Thread.currentThread().getName() + "hash error!");
        }
    }

    public void method2() {
        synchronized (this) {
            LwLog.logInfo(Thread.currentThread().getName() + " start ......");
            try {
                TimeUnit.MINUTES.sleep(3);
            } catch (InterruptedException e) {
                LwLog.errorInfo(Thread.currentThread().getName() + "hash error!");
            }
        }
    }

    public static synchronized void method3() {
        LwLog.logInfo(Thread.currentThread().getName() + " start ......");
        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            LwLog.errorInfo(Thread.currentThread().getName() + "hash error!");
        }
    }

    public static void method4() {
        synchronized (SyncBase.class) {
            LwLog.logInfo(Thread.currentThread().getName() + " start ......");
            try {
                TimeUnit.MINUTES.sleep(3);
            } catch (InterruptedException e) {
                LwLog.errorInfo(Thread.currentThread().getName() + "hash error!");
            }
        }
    }
}
