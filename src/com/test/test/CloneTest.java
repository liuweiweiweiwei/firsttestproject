package com.test.test;

import com.test.models.YuanXing.Client;
import com.test.models.YuanXing.CopyClone;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 14:49
 * @description
 */
public class CloneTest {

    public static void main(String[] args) {
        CopyClone copyClone = new CopyClone();
        copyClone.setName("小红");
        copyClone.setAge(18);
        copyClone.setTime(LocalDateTime.now());
        copyClone.setHobbys(Arrays.asList("足球","网球"));
        //克隆
        Client c = new Client(copyClone);
        CopyClone clone = (CopyClone) c.getProtoType();
        System.out.println(copyClone.toString());
        System.out.println(clone.toString());
        //克隆2
        CopyClone copyClone1 = (CopyClone) copyClone.clone();
        System.out.println(copyClone1);
        System.out.println(clone.getHobbys() == copyClone1.getHobbys());
        System.out.println(clone.getHobbys() == copyClone.getHobbys());

    }
}
