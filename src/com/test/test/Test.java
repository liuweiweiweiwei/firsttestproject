package com.test.test;

import com.alibaba.fastjson.JSON;

import java.beans.Expression;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/31 10:39
 * @description
 */
public class Test {
    public final static Map<String,User> map=new ConcurrentHashMap<>();
    public final static Map<String,String> map1=new ConcurrentHashMap<>();

    static {
        map.put("str1",new User("小李",18));
        map.put("str2",new User("小张",19));
        map.put("str3",new User("小网",14));
        map.put("str4",new User("小到",13));
        map.put("str5",new User("小李、红",16));
        map1.put("str1","12");
        map1.put("str2","13");
        map1.put("str3","14");
    }

    public static void main(String[] args) {
        User str5 = map.get("str5");
        User user=new User();
        user.setAge(17);
        user.setName("李红");
        user.setHead("小");
        user.setBody("小");
        String str1 = map1.get("str1");
        str1="15";
        System.out.println("end！"+ JSON.toJSONString(map)+map1);
        List<User> list=new ArrayList<>();
        list.add(new User("xs",12));
        list.add(new User("xq",10));
        list.add(new User("xa",11));
        User user11=new User();
        user11.setName("xd");
        User user1=new User("xiaola",12);
        User user2=new User();

        user2.setName("xiaohei");
        System.out.println(JSON.toJSONString(user2)+":"+JSON.toJSONString(user1)+JSON.toJSONString(list));
        List<User> userList=new ArrayList<>();
        userList.addAll(list);
        userList.remove(0);
        System.out.println(JSON.toJSONString(list)+":"+JSON.toJSONString(userList));

        User user3=new User(user);
        user3.setName("李兰红");
        user3.setBody("大");
        user3.setHead("大");
        User user4=user;
        /*user4.setName("李兰");
        user4.setBody("中");*/
        System.out.println(JSON.toJSONString(user)+":"+JSON.toJSONString(user3)+":"+JSON.toJSONString(user4));
        System.out.println("===");
        Node node1=new Node("1",null,1,0,1);
        Node node2=new Node("2",null,2,1,2);
        Node node3=new Node("3",null,3,2,3);
        Node node4=new Node("4",null,4,1,4);
        Node node5=new Node("5",null,5,3,5);
        Node node6=new Node("6",null,6,0,6);
        Node node7=new Node("7",null,7,6,7);
        Node node8=new Node("8",null,8,7,8);
        List<Node> nodeList= Arrays.asList(node1,node5,node3,node4,node2,node6,node7,node8);
        List<Node> node = new Test().get(nodeList);
        System.out.println(JSON.toJSONString(node));

        System.out.println(Arrays.toString("knaaa卡夹拉开车，莱克斯诺,发声方法；l clksdm:lkabc：kjanc".split(",|:|，|;|：|；")));

        Function f;
        Expression e;

    }

    private List<Node> get(List<Node> nodes1){
        List<Integer> nodes=new ArrayList<>();
        List<Node> nodeEnd=new ArrayList<>();
        Map<Integer,Node> nodeMap=new HashMap<>();
        nodes1.forEach(node -> {
            nodes.add(node.getId());
            nodeMap.put(node.getId(),new Node(node));
        });
        int len=nodes.size();
        for (int i = 0; i < len; i++) {
            Node nodeTemp=nodeMap.get(nodes.get(i));
            if(nodes.contains(nodeTemp.getParent())){
                Node node = nodeMap.get(nodeTemp.getParent());
                node.addNode(nodeTemp);
                nodeMap.put(nodeTemp.getParent(),node);
            }else {
                nodeEnd.add(nodeTemp);
            }
        }
        return nodeEnd;
    }




}
