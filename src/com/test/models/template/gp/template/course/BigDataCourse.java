package com.test.models.template.gp.template.course;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 13:52
 * @description
 */
public class BigDataCourse extends NetWorkCourse {

    private boolean needWork = false;

    public BigDataCourse(boolean needWork) {
        this.needWork = needWork;
    }

    @Override
    protected void checkWork() {
        System.out.println("大数据作业检查！");
    }

    @Override
    protected boolean work() {
        return this.needWork;
    }
}
