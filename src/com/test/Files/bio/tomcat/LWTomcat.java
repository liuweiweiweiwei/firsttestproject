package com.test.Files.bio.tomcat;

import com.test.Files.bio.tomcat.http.LwRequest;
import com.test.Files.bio.tomcat.http.LwResponse;
import com.test.Files.bio.tomcat.servlet.LwServlet;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 22:16
 * @description
 */
public class LWTomcat {
    //j2ee//打开Tomcat源码，全局搜索ServerSocket

    private int port = 8080;

    private Map<String,LwServlet> servletMapping = new HashMap<String,LwServlet>();

    private Properties webxml = new Properties();

    private ServerSocket serverSocket;

    public static void main(String[] args) {
        new LWTomcat().start();
    }


    private void init(){

        //加载web.xml文件,同时初始化 ServletMapping对象
        try{
            String WEB_INF = this.getClass().getResource("/").getPath();
            FileInputStream fis = new FileInputStream(WEB_INF + "web.properties");

            webxml.load(fis);

            for (Object k : webxml.keySet()) {

                String key = k.toString();
                if(key.endsWith(".url")){
                    String servletName = key.replaceAll("\\.url$", "");
                    String url = webxml.getProperty(key);
                    String className = webxml.getProperty(servletName + ".className");
                    LwServlet obj = (LwServlet) Class.forName(className).newInstance();
                    servletMapping.put(url, obj);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void start(){
        init();

        //Netty封装了NIO，Reactor模型，Boss，worker
        // Boss线程
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        // Worker线程
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            // Netty服务
            //ServetBootstrap   ServerSocketChannel
            ServerBootstrap server = new ServerBootstrap();
            // 链路式编程
            server.group(bossGroup, workerGroup)
                    // 主线程处理类,看到这样的写法，底层就是用反射
                    .channel(NioServerSocketChannel.class)
                    // 子线程处理类 , Handler
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        // 客户端初始化处理
                        @Override
                        protected void initChannel(SocketChannel client) throws Exception {
                            // 无锁化串行编程
                            //Netty对HTTP协议的封装，顺序有要求
                            // HttpResponseEncoder 编码器
                            client.pipeline().addLast(new HttpResponseEncoder());
                            // HttpRequestDecoder 解码器
                            client.pipeline().addLast(new HttpRequestDecoder());
                            // 业务逻辑处理
                            client.pipeline().addLast(new GPTomcatHandler());
                        }

                    })
                    // 针对主线程的配置 分配线程最大数量 128
                    .option(ChannelOption.SO_BACKLOG, 128)
                    // 针对子线程的配置 保持长连接
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // 启动服务器
            ChannelFuture f = server.bind(port).sync();
            System.out.println("GP Tomcat 已启动，监听的端口是：" + port);
            f.channel().closeFuture().sync();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            // 关闭线程池
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public class GPTomcatHandler extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            if (msg instanceof HttpRequest){
                HttpRequest req = (HttpRequest) msg;

                // 转交给我们自己的request实现
                LwRequest request = new LwRequest(ctx,req);
                // 转交给我们自己的response实现
                LwResponse response = new LwResponse(ctx,req);
                // 实际业务处理
                String url = request.getUrl();

                if(servletMapping.containsKey(url)){
                    servletMapping.get(url).service(request, response);
                }else{
                    response.write("404 - Not Found");
                }

            }
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

        }

    }

//    public void start() {
//        //servlet
//        init();
//        //request
//        try {
//            serverSocket = new ServerSocket(8080);
//            while (true) {
//                Socket accept = serverSocket.accept();
//                process(accept);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        //response
//    }
//
//    private void process(Socket accept) throws Exception{
//        InputStream inputStream = accept.getInputStream();
//        OutputStream outputStream = accept.getOutputStream();
//        LwRequest request = new LwRequest(inputStream);
//        LwResponse response = new LwResponse(outputStream);
//
//        String url = request.getUrl();
//        if(servletMapping.containsKey(url)){
//            servletMapping.get(url).service(request,response);
//        }else{
//            response.write("404 -- not found");
//        }
//
//        outputStream.flush();
//        outputStream.close();
//
//        inputStream.close();
//        accept.close();
//    }


//    private void init(){
//
//        //加载web.xml文件,同时初始化 ServletMapping对象
//        try{
//            String WEB_INF = this.getClass().getResource("/").getPath().substring(1);
//            System.out.println(WEB_INF);
//            FileInputStream fis = new FileInputStream(WEB_INF + "web.properties");
//
//            webxml.load(fis);
//
//            for (Object k : webxml.keySet()) {
//
//                String key = k.toString();
//                if(key.endsWith(".url")){
//                    String servletName = key.replaceAll("\\.url$", "");
//                    String url = webxml.getProperty(key);
//                    String className = webxml.getProperty(servletName + ".className");
//                    LwServlet obj = (LwServlet)Class.forName(className).newInstance();
//                    servletMapping.put(url, obj);
//                }
//            }
//
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//    }
    //1.配置好端口，默认8800--serverSocket-ip：localhost
    //2.配置web。xml---servlet-name servlet-class servlet-url-pattern
    // 继承于j2ee的servlet
    //3.读取配置，将url-pattern和servlet建立关系
    //map servletMapping
    //4.发送的数据就是字符串--字符串有一定的规定（http协议）
    //5.拿到url---找到servlet对应的class---》》》用反射进行实例化
    //6.调用实例化对象的方法---》》》执行具体的doGet和doPost方法--request对象inputstream，response对象outputstream



}
