package com.test.models.agent.dbsource;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:40
 * @description
 */
public class OrderServiceProxy implements IOrderService{

    public static ThreadLocal<SimpleDateFormat> simpleDateFormatThreadLocal =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy"));
    private IOrderService iOrderService;

    public OrderServiceProxy(IOrderService iOrderService) {
        this.iOrderService = iOrderService;
    }

    @Override
    public int createOrder(Order order) {
        long time = order.getTime();
        int year = Integer.parseInt(simpleDateFormatThreadLocal.get().format(new Date(time)));
        DynaticDataSourceEntity.set(year);
        System.out.println("静态代理对象自动分配到【" + year + "】中！");
        int order1 = this.iOrderService.createOrder(order);
        DynaticDataSourceEntity.restore();
        return order1;
    }

    @Override
    public void dealOrderService(Order order) {
        createOrder(order);
    }
}
