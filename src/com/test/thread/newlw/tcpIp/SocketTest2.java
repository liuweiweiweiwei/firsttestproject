package com.test.thread.newlw.tcpIp;

import java.io.IOException;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 11:07
 * @description
 */
public class SocketTest2 {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost",8012);
        new SocketTest.ClientOne(socket,"少羽").start();
    }
}
