package com.test.classes.packagesimple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @date 2022/5/14 11:45
 * @since 1.0
 * description
 */
public class SImple {
    private static byte[] buff = new byte[8];

    private static String str = "";

    private static List<String> simple = new ArrayList<>(16);

    static {
        buff[0] = 1;
        str = "simple";
        simple.add("element");
        System.out.println(buff[0]);
        System.out.println(str);
        System.out.println(simple.get(0));
    }
}
