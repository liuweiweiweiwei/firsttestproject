package com.test.models.adapt.gpnew.power;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 15:17
 * @description
 */
public interface DC5 {

    /**
     * 输出5v电压
     * @return
     */
    int outputDC5();
}
