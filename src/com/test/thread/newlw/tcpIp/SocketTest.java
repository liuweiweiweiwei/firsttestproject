package com.test.thread.newlw.tcpIp;

import com.sun.org.apache.xpath.internal.SourceTree;
import com.test.Files.FileUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/27 22:19
 * @description
 */
public class SocketTest {
    public static final String END = "bye";

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost",8012);
        new ClientOne(socket,"天明").start();
    }



    public static class ClientOne extends Thread{
        private Socket socket;
        private String name;

        public ClientOne(Socket socket) {
            this.socket = socket;
        }

        public ClientOne(Socket socket, String name) {
            this.socket = socket;
            this.name = name;
        }

        @Override
        public void run() {
            sendInfo();
        }

        private void sendInfo(){
            BufferedReader reader = null;
            PrintWriter writer = null;
            BufferedReader reader1 = null;
            try {
                //获取服务端信息
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //向服务端发送信息--并且输入信息
                writer = new PrintWriter(socket.getOutputStream(),true);
                writer.println(name + "进入直播间！");
                reader1 = new BufferedReader(new InputStreamReader(System.in));
                String clientInfo;
                while (!END.equals(clientInfo = reader.readLine())){
                    System.out.println("server:: " + clientInfo);
                    //写回到服务器

                    writer.println(name + " : " + reader1.readLine());
                }
                writer.println("hello 静静！");
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                FileUtil.closeOR(writer);
                FileUtil.closeIR(reader);
                FileUtil.closeIR(reader1);
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static class SocketAServer extends Thread{

        private Socket serverSocket;

        public SocketAServer(Socket serverSocket) {
            this.serverSocket = serverSocket;
        }

        @Override
        public void run() {
            socket();
        }

        private void socket(){
            BufferedReader reader = null;
            BufferedReader reader1 = null;
            PrintWriter writer = null;
            //监听ip：端口
            try {
                //获取客户端输入的信息（io阻塞）
                reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                //获取要输出的信息
                writer = new PrintWriter(serverSocket.getOutputStream(),true);
//                //输入信息
//                reader1 = new BufferedReader(new InputStreamReader(System.in));
                //输出获取信息
                String info;
                while (!SocketTest.END.equals((info = reader.readLine()))){
                    //写出客户端出过来的数据
                    System.out.println("client : say:--->>> " + info);
                    //写入客户端
                    writer.println(info);
                    writer.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                FileUtil.closeOR(writer);
                FileUtil.closeIR(reader);
                FileUtil.closeIR(reader1);
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
