package com.test.autimatic;

import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/10/15 20:22
 * @description
 */
public class LeetCodeThree {
    public static void main(String[] args) {
        System.out.println(backspaceCompare("a##c", "#a#c"));
        //2.
        ListNode listNode=new ListNode(1);
        ListNode listNode1=new ListNode(2);
        ListNode listNode2=new ListNode(3);
        ListNode listNode3=new ListNode(4);
        ListNode listNode4=new ListNode(5);
        ListNode listNode5=new ListNode(6);
        ListNode listNode6=new ListNode(7);
        listNode.next=listNode1;
        listNode1.next=listNode2;
        listNode2.next=listNode3;
        listNode3.next=listNode4;
        listNode4.next=listNode5;
        listNode5.next=listNode6;
        reorderList(listNode);
        while (listNode!=null){
            System.out.println(listNode.val);
            listNode=listNode.next;
        }
    }

    /**
     * 翻转数字
     * @param x
     * @return
     */
    public int reverse(int x) {
        long end=0;
        int tim=Math.abs(x);
        while (tim!=0){
            int yu=tim%10;
            tim=tim/10;
            if(end!=0||yu!=0){
                end=end*10+yu;
            }
        }
        if(end>Integer.MAX_VALUE||end<Integer.MIN_VALUE){
            return 0;
        }
        if(x<0){
            return -(int) end;
        }
        return (int) end;
    }

    /**给定 S 和 T 两个字符串，当它们分别被输入到空白的文本编辑器后，判断二者是否相等，并返回结果。 # 代表退格字符。
    注意：如果对空文本输入退格字符，文本继续为空。
    示例 1：
    输入：S = "ab#c", T = "ad#c"
    输出：true
    解释：S 和 T 都会变成 “ac”。
    示例 2：
    输入：S = "ab##", T = "c#d#"
    输出：true
    解释：S 和 T 都会变成 “”。
    示例 3：
    输入：S = "a##c", T = "#a#c"
    输出：true
    解释：S 和 T 都会变成 “c”。*/
    public static boolean backspaceCompare(String S, String T) {
        if(S==null&&T==null){
            return true;
        }
        if((S==null&&T!=null)||(T==null&&S!=null)){
            return false;
        }
        String endS=split(S);
        String endT=split(T);
        return endS.equals(endT);
    }


    public static String split(String S){
        while (S.contains("#")){
            while (S.startsWith("#")){
                S=S.substring(1);
            }
            //System.out.println(S);
            if(!S.contains("#")){
                return S;
            }
            int t=S.indexOf("#");
            StringBuilder stringBuilder=new StringBuilder();
            stringBuilder.append(S.substring(0,t-1));
            stringBuilder.append(S.substring(t==S.length()?t:t+1));
            S=stringBuilder.toString();
        }
        return S;
    }

    /**给定一个单链表 L：L0→L1→…→Ln-1→Ln ，
        将其重新排列后变为： L0→Ln→L1→Ln-1→L2→Ln-2→…
        你不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。
        示例 1:
        给定链表 1->2->3->4, 重新排列为 1->4->2->3.
        示例 2:
        给定链表 1->2->3->4->5, 重新排列为 1->5->2->4->3.
     */

    public static class ListNode {
         int val;
         ListNode next;
         ListNode() {}
         ListNode(int val) { this.val = val; }
         ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     }

    public static void reorderList(ListNode head) {
        Stack<Integer> stack=new Stack<>();
        ListNode newList=head;
        while (head!=null){
            stack.push(head.val);
            head=head.next;
        }
        System.out.println("statck:"+stack.size());
        ListNode list=newList;ListNode tem=newList;ListNode end=newList.next;
        int i=0;
        int m=stack.size()/2;
        int n=stack.size()%2;
        while (i<=m&&end!=null){
            ListNode node=new ListNode(stack.pop());
            System.out.println("mid:"+node.val);
            list.next=node;
            list.next.next=end;

            list=end;
            end=list.next;
            i++;
            if(i==m){
                head=tem;
                if(n==0){
                    list.next=null;
                }else {
                    end.next=null;
                }
                return;
            }

        }

    }

}
