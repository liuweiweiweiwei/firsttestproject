package com.test.autimatic.day;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/5 20:36
 * @description
 */
public class leet0505 {
    public static void main(String[] args) {
        System.out.println(numSubarrayProductLessThanK(new int[]{10, 5, 2, 6}, 100));
        System.out.println(isMiddleStr("AACCGGTT", "AACCGGTA"));
        System.out.println(longestPalindrome("aacabdkacaa"));
        System.out.println(subArraySun(new int[]{1, -2, 3, -2, 5}, 3));
    }

    /**
     * jisuan thank
     *
     * @param nums arr
     * @param k    k
     * @return num
     */
    public static int numSubarrayProductLessThanK(int[] nums, int k) {
        int result = 0;
        int product = 1;
        int len = nums.length;
        for (int i = 0, j = 0; i < len; i++) {
            while (j < len && (product *= nums[j]) < k) {
                j++;
            }
            result += (j - i);
            product /= nums[i];
            if (j > i && j < len) {
                product /= nums[j];
            }
            j = j == i ? i + 1 : j;
        }
        return result;
    }

    public static int minMutation(String start, String end, String[] bank) {
        if (Arrays.stream(bank).noneMatch(b -> b.equals(end))) {
            return -1;
        }
        int result = 0;
        Queue<String> queue = new ArrayDeque<String>() {{
            add(start);
        }};
        Set<String> set = new HashSet<>();
        while (!queue.isEmpty()) {
            if (queue.contains(end)) {
                return result;
            }
            set.addAll(queue);
            Queue<String> curQueue = new ArrayDeque<>(queue);
            queue.clear();
            while (!curQueue.isEmpty()) {
                String poll = curQueue.poll();
                Arrays.stream(bank).filter(b -> !set.contains(b) && isMiddle(poll, b)).forEach(queue::add);
            }
            result++;
        }
        return -1;
    }

    private static boolean isMiddle(String current, String target) {
        return IntStream.range(0, 8).filter(i -> current.charAt(i) == target.charAt(i)).count() == 7;
    }

    private static int isMiddleStr(String current, String target) {
        return (int) IntStream.range(0, 8).filter(i -> current.charAt(i) == target.charAt(i)).count();
    }

    public static List<Integer> findDuplicates(int[] nums) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            int curIndex = Math.abs(nums[i]);
            if (nums[curIndex - 1] > 0) {
                nums[curIndex - 1] = -nums[curIndex - 1];
            } else {
                list.add(-nums[curIndex - 1]);
            }
        }
        return list;
    }

    public static int findDuplicate(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int index = Math.abs(nums[i]);
            if (nums[index] < 0) {
                return index;
            } else {
                nums[index] = -nums[index];
            }
        }
        return -1;
    }

    public static String longestPalindrome(String s) {
        StringBuilder tempString = new StringBuilder("#");
        for (int i = 0; i < s.length(); i++) {
            tempString.append(s.charAt(i)).append('#');
        }
        List<Integer> midList = new ArrayList<>();
        int middle = 0;
        int round = 0;
        int start = 0;
        int end = 0;
        for (int i = 0; i < tempString.length(); i++) {
            int len = round > i ? Math.min(midList.get(2 * middle - i), round - i) : 0;
            while (i - len - 1 > -1 && i + len + 1 < tempString.length()
                    && (tempString.charAt(i - 1 - len) == tempString.charAt(i + 1 + len))) {
                len++;
            }
            midList.add(len);
            if (len * 2 + 1 > end - start) {
                start = i - len;
                end = i + len;
            }
            if (len + i > round) {
                round = i + len;
                middle = i;
            }
        }
        StringBuilder result = new StringBuilder();
        for (int i = start; i <= end; i++) {
            if (tempString.charAt(i) != '#') {
                result.append(tempString.charAt(i));
            }
        }
        return result.toString();
    }

    public static int minDeletionSize(String[] strs) {
        if (strs.length == 1) {
            return 0;
        }
        int result = 0;
        for (int i = 0; i < strs[0].length(); i++) {
            for (int j = 1; j < strs.length; j++) {
                if (strs[j].charAt(i) < strs[j - 1].charAt(i)) {
                    result++;
                    break;
                }
            }
        }
        return result;
    }

    public static int subarraySum(int[] nums, int k) {
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            int num = 0;
            for (int j = i; j >= 0; j--) {
                num += nums[j];
                if (num == k) {
                    result++;
                }
            }
        }
        return result;
    }

    public static int subArraySun(int[] arr, int k) {
        Map<Integer, Integer> numMap = new HashMap<Integer, Integer>(16){{put(0, 1);}};
        int startNum = 0, result = 0;
        for (int curNum : arr) {
            startNum += curNum;
            if (numMap.containsKey(startNum - k)) {
                result += numMap.get(startNum - k);
            }
            numMap.put(startNum, numMap.getOrDefault(startNum, 0) + 1);
        }
        return result;
    }
}