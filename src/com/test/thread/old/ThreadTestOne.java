package com.test.thread.old;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/8/10 17:29
 * @description
 */
public class ThreadTestOne {
    private static LongAdder atomicLong=new LongAdder();


    public static void main(String[] args) {
        MyRun myRun=new MyRun();
        for (int i = 0; i < 10; i++) {
            new Thread(myRun).start();
        }
    }




    public static class MyTestThread implements Callable {
        private int num;

        public MyTestThread(int num){
            this.num=num;
        }

        @Override
        public Object call() throws Exception {
            for (int i = 0; i <10 ; i++) {
                num--;
            }
            return num;
        }
    }

    public static class MyRun implements Runnable{
        private static int anInt=0;
        @Override
        public void run() {
            for (int i = 0; i <10 ; i++) {
                //anInt++;
                atomicLong.increment();
                System.out.print(anInt+","+atomicLong.intValue()+"，");
            }
        }
    }

    public static class Count{
        private static AtomicLong cou=new AtomicLong();
        public Count(int t){
            cou.compareAndSet(cou.get(),t);
        }
        public void creCount(){
            cou.getAndDecrement();
        }

        public AtomicLong getCou() {
            return cou;
        }
    }
}
