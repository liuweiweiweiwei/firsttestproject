package com.test.models.agent.dbsource;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:33
 * @description
 */
public class Order {
    private Object orderInfo;
    private String id;
    /**
     * 时间按年分库分表
     */
    private long time;

    public Object getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(Object orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
