package com.test.learn;

import java.io.File;
import java.io.IOException;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/2/26 8:54
 * @description
 */
public class IO {
    public static void main(String[] args) {
        IO io=new IO();
        io.getFile();
    }
    private static final String path="F:/fileDemo/text.txt";
    private static final String path1="F://fileDemo";

    /**
     * 定义
     */
    public void getFile(){
        File file=new File(path1);
        if(!file.exists()){
            file.mkdir();
            try {
                File file1=new File(path);
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
