package com.test.thread.mythreadpool;

import java.util.LinkedList;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 20:51
 * @description
 */
public class LinkThreadQueue implements RunnableQueue{
    private final int limit;

    private final DenyPolicy denyPolicy;

    private final RunnablePool runnablePool;

    private final LinkedList<Runnable> runnableLinkedList = new LinkedList<>();

    public LinkThreadQueue(int limit, DenyPolicy denyPolicy, RunnablePool runnablePool) {
        this.limit = limit;
        this.denyPolicy = denyPolicy;
        this.runnablePool = runnablePool;
    }

    @Override
    public void offer(Runnable runnable) {
        synchronized (runnableLinkedList) {
            if (runnableLinkedList.size() > limit) {
                denyPolicy.reject(runnable, runnablePool);
            } else {
                runnableLinkedList.addLast(runnable);
                runnableLinkedList.notifyAll();
            }
        }
    }

    @Override
    public Runnable take() throws InterruptedException{
        synchronized (runnableLinkedList) {
            while (runnableLinkedList.isEmpty()) {
                try {
                    runnableLinkedList.wait();
                } catch (InterruptedException e) {
                    throw e;
                }
            }
            return  runnableLinkedList.removeFirst();
        }
    }

    @Override
    public int size() {
        synchronized (runnableLinkedList) {
            return runnableLinkedList.size();
        }
    }
}
