package com.test.models.gpdecorate.v3;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:41
 * @description
 */
public class BatterCakeSuger extends BatterDecorator {

    public BatterCakeSuger(BatterCake batterCake) {
        super(batterCake);
    }

    @Override
    public String name() {
        return "又加了一根香肠";
    }

    @Override
    public int price() {
        return super.price() + 2;
    }
}
