package com.test.models.agent.dynamicProxy.gpproxy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:33
 * @description
 */
public class LwClassLoader extends ClassLoader{
    private File classPathFIle;

    public LwClassLoader() {
        this.classPathFIle = new File(LwClassLoader.class.getResource("").getPath());
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String s = LwClassLoader.class.getPackage().getName() + "." + name;
        if(classPathFIle != null){
            File file = new File(classPathFIle,name.replaceAll("\\.","/")+".class");
            if(file.exists()){
                FileInputStream inputStream = null;
                ByteArrayOutputStream outputStream = null;
                try{
                    inputStream = new FileInputStream(file);
                    outputStream = new ByteArrayOutputStream();
                    byte[] bytes = new byte[1024];
                    int len;
                    while ((len = inputStream.read(bytes)) != -1){
                        outputStream.write(bytes,0,len);
                    }
                    return defineClass(s,outputStream.toByteArray(),0,outputStream.size());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return super.findClass(name);
    }
}
