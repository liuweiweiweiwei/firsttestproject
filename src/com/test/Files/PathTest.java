package com.test.Files;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * pathapi方法使用测试
 */
public class PathTest {
    public static void main(String[] args) {
        // pathMethod();
        List<MyLoadClass> myLoadClassList= getFileWithName();
        System.out.println(myLoadClassList.get(0).getFileName()+myLoadClassList.size());
        byte[] bytes=myLoadClassList.get(0).getBody();
        File file=Paths.get("E://fileTest//changeFile//three.txt").toFile();
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(FileOutputStream fileOutputStream=new FileOutputStream(file)){
            fileOutputStream.write(bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void pathMethod() throws IOException {
        //方式1
        Path path= Paths.get("E://fileTest//one.txt");
        //方式二
        Path path1=Paths.get("E:","fileTest","fileTest2//two.txt");

        if(!path.toFile().exists()){
            path.toFile().createNewFile();
        }
        if(!path1.toFile().exists()){
            path1.toFile().createNewFile();
        }
        System.out.println("获取第一个文件路径名称"+path);
        System.out.println("获取第二个文件路径名称"+path1);
        try (BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile()),"UTF-8"))){
            StringBuilder readLine=new StringBuilder("");
            String word="";
            while ((word=reader.readLine())!=null){
                readLine.append(word);
            }
            System.out.println(readLine);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //2------------------
        File file=path1.toFile();
        try(FileInputStream inputStream=new FileInputStream(file)){

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<MyLoadClass> getFileWithName(){
        Path path=Paths.get("E://fileTest//files//");
        List<MyLoadClass> loadClassList=new LinkedList<>();
        try(Stream<Path> pathStream= Files.walk(path,1)) {
            pathStream.forEach(ps->{
                File file=ps.toFile();
                try {
                    if(!file.isDirectory()){
                        byte[] bytes=Files.readAllBytes(ps);
                        loadClassList.add(new MyLoadClass(file.getName(),bytes));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loadClassList;
    }

    static class MyLoadClass{
        private String fileName;
        private byte[] body;

        public MyLoadClass(String fileName, byte[] body) {
            this.fileName = fileName;
            this.body = body;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public byte[] getBody() {
            return body;
        }

        public void setBody(byte[] body) {
            this.body = body;
        }
    }
}
