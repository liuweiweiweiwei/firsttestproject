package com.test.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/3 15:06
 * @description
 */
public class Node {
    private int id;

    private String value;

    private List<Node> node;

    private int parent;

    private int length;

    public Node(String value, List<Node> node, int length,int parent,int id) {
        this.value = value;
        this.node = node;
        this.length = length;
        this.parent=parent;
        this.id=id;
    }

    public Node() {
    }
    public Node(Node node){
        this.value=node.value;
        this.parent=node.parent;
        this.node=node.node;
        this.length=node.length;
        this.id=node.id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Node> getNode() {
        return node;
    }

    public void setNode(List<Node> node) {
        this.node = node;
    }

    public void addNode(Node node){
        if(this.node==null)
            this.node=new ArrayList<>();
        this.node.add(node);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
