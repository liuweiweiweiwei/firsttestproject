package com.test.models.factory;

import com.test.models.factory.extend.ICourseFactory;
import com.test.models.factory.extend.SpringCourseFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:33
 * @description
 */
public class SimpleFactoryTest {

    public static void main(String[] args) {
        MyCourse course = new StudyCourse();
        course.record();
        CourseFactory test = new CourseFactory();
        MyCourse course1 = test.createF(StudyCourse.class);
        course1.record();
        MyCourse course2 = test.createF(SpringCourse.class);
        course2.record();
        ICourseFactory course3 = new SpringCourseFactory();
        course3.createFactory().record();
    }
}
