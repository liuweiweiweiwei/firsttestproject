package com.test.TraceProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicTrace {

    interface IService{
        void sayHello();
        String getToString();
    }
    static class RealService implements IService{
        @Override
        public void sayHello() {
            System.out.println("hi!");
        }

        @Override
        public String getToString() {
            //调用
            sayHello();
            return "操作";
        }
    }
    static class DynamicJDKTrace implements InvocationHandler{
        private Object object;
        public DynamicJDKTrace(Object obj){
            this.object=obj;
        }
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("JDK enter method before:"+method.getName());
            Object result=method.invoke(object,args);
            System.out.println("JDK leave method after:"+method.getName());
            return result;
        }
    }

    public static void main(String[] args) {
        IService iService=new RealService();
        IService proxyDynamic=(IService) Proxy.newProxyInstance(
                IService.class.getClassLoader(),
                new Class<?>[]{IService.class},
                new DynamicJDKTrace(iService)
        );
        proxyDynamic.getToString();
//        System.out.println(proxyDynamic);
    }
}
