package com.test.models.Strategy.payStrategy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 19:00
 * @description
 */
public class YinLianPay extends Pay {
    @Override
    public String getName() {
        return "银联支付";
    }

    @Override
    protected double queryBalance(String uid) {
        return 1000;
    }
}
