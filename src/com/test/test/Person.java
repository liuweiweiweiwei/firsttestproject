package com.test.test;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/2 17:18
 * @description
 */
public class Person {
    private String body;
    private String head;

    public Person(String body, String head) {
        this.body = body;
        this.head = head;
    }

    public Person() {
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
