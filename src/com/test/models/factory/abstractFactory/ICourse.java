package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 11:16
 * @description
 */
public interface ICourse {

    void operateCourse();
}
