package com.test.models.Observer.newGp;

import java.util.Observable;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:23
 * @description 观察者模式 ---jdk提供的
 */
public class LWer extends Observable {

    private LWer() {
    }

    private static LWer LWER = null;
    private String name = "CSDN";

    public static LWer getInstance() {
        if (LWER == null) {
            synchronized (LWer.class) {
                if (LWER == null) {
                    LWER = new LWer();
                }
            }
        }
        return LWER;
    }

    public String getName() {
        return name;
    }

    public void question(Question question) {
        System.out.println(question.getName() + ":提交了一个问题：<" + question.getContent() + ">在<" + this.name + ">上！");
        setChanged();
        notifyObservers(question);
    }
}
