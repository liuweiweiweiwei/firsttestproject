package com.test.models.Strategy.old;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 10:41
 * @description
 */
public interface Strategy {
    /**计算数值**/
    int getData();
}
