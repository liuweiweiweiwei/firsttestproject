package com.test.models.Strategy.payStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 19:03
 * @description
 */
public class PayStrategy {

    public static final String YL_PAY = "ylpay";
    public static final String WC_PAY = "wcpay";
    public static final String ALI_PAY = "alipay";
    public static final String JD_PAY = "jDpay";
    public static final AliPay DEFAULT_PAY = new AliPay();

    private static Map<String,Pay> payMap = new HashMap<>();

    static {
        payMap.put(YL_PAY,new YinLianPay());
        payMap.put(WC_PAY,new WeCharPay());
        payMap.put(ALI_PAY,new AliPay());
        payMap.put(JD_PAY,new JingDong());
    }

    public static Pay getInstance(String payKey){
        if(payMap.containsKey(payKey)){
            return payMap.get(payKey);
        }
        return DEFAULT_PAY;
    }

    public static Pay getInstance(Class<?> clazz){
        String name = clazz.getName();
        System.out.println(name);
        try {
            return (Pay) clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
