//package com.test.boy;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.http.impl.client.HttpClients;
//
//import java.io.IOException;
//import java.io.InputStream;
//
///**
// * @author 25338
// * @version 1.0
// * @date 2021/2/2 14:48
// * @description
// */
//public class ChangeUtil {
//
//
//    public static String getRequestUrl(String url) {
//        //类似浏览器
//        CloseableHttpClient client= HttpClients.createDefault();
//        String realUrl= "";
//        HttpGet httpGet=new HttpGet(url);
//        CloseableHttpResponse response=null;
//        try {
//            response=client.execute(httpGet);
//            if(response!=null){
//                realUrl=getUrl(response);
//            }
//        }catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if(client!=null){
//                try {
//                    client.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(response!=null){
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return realUrl;
//    }
//
//    public static String getUrl( CloseableHttpResponse response){
//
//        StringBuilder realUrl= new StringBuilder();
//        InputStream inputStream=null;
//        try {
////            inputStream = response..getContent();
//            byte[] a = new byte[1024];
//            int b = 0;
//            while ((b = inputStream.read(a)) != -1) {
//                String tempUrl = new String(a, 0, b);
//                realUrl.append(tempUrl);
//                a = new byte[1024];
//            }
//        }catch (Exception e){
//            System.out.println("解析返回连接出错："+e.getMessage());
//        }
//        return realUrl.toString();
//    }
//
//    public static String postRequestUrl(String url){
//        //类似浏览器
//        CloseableHttpClient client= HttpClientBuilder.create().build();
//        String realU="";
//        HttpPost httpPost=new HttpPost(url);
//        CloseableHttpResponse response=null;
//        try {
//            try {
//                response=client.execute(httpPost);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        }catch (Exception e) {
//            System.out.println("第一次重新获取连接出现问题："+e.getMessage());
//        }finally {
//            if(client!=null){
//                try {
//                    client.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(response!=null){
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return response.toString();
//    }
//
//    //获取第一步骤的url
//    public static String getUrlFrist(){
//        String realUrl="";
//        String response=postRequestUrl(URL.URL1.getRealAddress());
//        if(response!=null){
//            String resp=response;
//            realUrl=resp.substring(resp.indexOf("location: ")+1,resp.indexOf(", x-tt-logid:"));
//        }
//        return realUrl;
//    }
//
//
//    //获取真实访问地址
//    public static String getRealUrl(){
//        String endId="";
//        String realUrl=URL.NEWURL.getRealAddress();
//        String resu=getUrlFrist();
//        try {
//            //获取id
//            if (null != resu && !"".equals(resu)) {
//                endId = resu.substring(resu.indexOf("/video/")+7, resu.indexOf("/?region="));
//            }
//            //拼接访问地址
//            realUrl+=endId;
//
//        }catch (Exception e){
//            System.out.println("解析一层id出现异常："+e.getMessage());
//        }
//        return realUrl;
//    }
//
//    /**
//     * 第二次访问url并获取真实地址
//     */
//    public static String getLocation(){
//        String location="";
//        String twoUrl=getRealUrl();
//        try {
//            if (twoUrl != null && !"".equals(twoUrl)) {
//                String response = getRequestUrl(twoUrl);
//                System.out.println("最后：==========="+response);
//                JSONObject jsonObject=JSON.parseObject(response);
//                JSONArray jsonArray=JSON.parseArray(jsonObject.get("item_list").toString());
//                JSONObject jsonObject1=JSON.parseObject(jsonArray.get(0).toString());
//                JSONObject jsonObject2=JSON.parseObject(JSON.parseObject(jsonObject1.get("video").toString()).get("play_addr").toString());
//                JSONArray strings=(JSONArray) jsonObject2.get("url_list");
//                location=String.valueOf(strings.get(0));
//                location=location.replace("playwm","play");
//            }
//        }catch (Exception e){
//            System.out.println("json解析获取最终地址失败："+e.getMessage());
//            e.printStackTrace();
//        }
//        return location;
//    }
//
//
//
//
//    public static void main(String[] args) {
//        String end=getLocation();
//        System.out.println(end);
//    }
//}
