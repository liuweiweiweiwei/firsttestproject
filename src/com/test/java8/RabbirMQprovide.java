package com.test.java8;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbirMQprovide {

    public static final String EXCHANGE_NAME="exchage_basic";
    public static final String EXCHANGE_TYPE="direct";
    public static final String RABBITMQ_NAME="produce_people";
    public static void main(String[] args) {
        try {
            //创建连接
            Connection connection=ConnectionConfig.getConnection();
            //声明通道
            Channel channel=connection.createChannel();
            //声明队列
            //channel.queueDeclare(RABBITMQ_NAME,true,false,false,null);
            //声明交换机1
//            channel.exchangeDeclare(EXCHANGE_NAME,EXCHANGE_TYPE,false,false,null);
//            //声明交换机2
//            channel.exchangeDeclare("exchange_bin",EXCHANGE_TYPE,false,false,null);
//
//            //通过一台交换机像另一台传递·消息s1转到name中
//            channel.exchangeBind(EXCHANGE_NAME,"exchange_bin","key1");
            //声明队列
            //channel.queueDeclare(RABBITMQ_NAME,true,false,false,null);

            //发送信息 交换机，路由键，参数，消息体，
            //String msg="简单模式信息发送";
            String msg="123路由转接模式信息发送";

//            channel.basicPublish("exchange_bin","key",null,msg.getBytes());
//            System.out.println("生产消息："+msg);
//            channel.close();
//            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
    //12,56,2,6,7,9,23

}
