package com.test.models.agent.dynamicProxy.cglibProxy;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 13:21
 * @description
 */
public class CGlibMeiPo implements MethodInterceptor {


    public Object getInstance(Class<?> aclass)throws Exception{
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(aclass);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object invoke = methodProxy.invokeSuper(o,objects);
        after();
        return invoke;
    }

    public void before(){
        System.out.println("我是媒婆，我要给你找对象，已确定需求！");
        System.out.println("开始物色。。。。。");

    }

    public void after(){
        System.out.println("ok的话，准备彩礼！");
    }

}
