package com.test.thread.newlw.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 14:46
 * @description
 */
public class SeverTest {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8790);
        Socket accept = serverSocket.accept();
        ObjectInputStream inputStream = new ObjectInputStream(accept.getInputStream());
        try {
            User user = (User) inputStream.readObject();
            System.out.println(user.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        serverSocket.close();
    }
}
