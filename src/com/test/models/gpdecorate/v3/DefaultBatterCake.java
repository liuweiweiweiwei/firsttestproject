package com.test.models.gpdecorate.v3;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:32
 * @description 装饰器模式
 */
public class DefaultBatterCake extends BatterCake {

    public DefaultBatterCake() {
    }

    @Override
    protected String name() {
        return "煎饼";
    }

    @Override
    protected int price() {
        return 5;
    }
}
