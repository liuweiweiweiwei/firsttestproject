package com.test.thread.mythreadpool;

import io.netty.util.concurrent.DefaultThreadFactory;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 20:45
 * @description thread pool
 */
public class BasicThreadPool extends Thread implements RunnablePool{
    // 1. thread pool
    // 2. runable queue
    // 3. thread factory
    // 4. deny policy
    // 5. interRunnable

    @FunctionalInterface
    private interface ThreadFactory{
        Thread createThread(MyRunnable runnable);
    }

    private static class DefaultThreadFactory implements ThreadFactory{
        private static final AtomicInteger INTEGER = new AtomicInteger(1);

        private static final ThreadGroup THREAD_GROUP_MY = new ThreadGroup("THREAD_GROUP_"
                + INTEGER.getAndIncrement());

        private static final AtomicInteger COUNT = new AtomicInteger(0);
        @Override
        public Thread createThread(MyRunnable runnable) {
            return new Thread(THREAD_GROUP_MY, runnable, "THREAD_" + COUNT.getAndIncrement());
        }
    }

    private static class ThreadTask {
        Thread thread;
        MyRunnable runnable;

        public ThreadTask(Thread thread, MyRunnable runnable) {
            this.thread = thread;
            this.runnable = runnable;
        }
    }

    private final int initSize;

    private final int max;

    private final int coreSize;

    private int activitySize;

    private final LinkThreadQueue linkThreadQueue;

    private final ThreadFactory threadFactory;

    private volatile boolean isShutdown = false;

    private final long time;

    private final TimeUnit timeUnit;

    private static final ThreadFactory THREAD_FACTORY = new DefaultThreadFactory();

    private final Queue<ThreadTask> taskQueue = new ArrayDeque<>();

    public BasicThreadPool(int initSize, int max, int coreSize, ThreadFactory threadFactory,
                           int queueSize, long time, TimeUnit timeUnit, DenyPolicy denyPolicy) {
        this.initSize = initSize;
        this.max = max;
        this.coreSize = coreSize;
        this.threadFactory = threadFactory;
        this.linkThreadQueue = new LinkThreadQueue(queueSize, denyPolicy, this);
        this.time = time;
        this.timeUnit = timeUnit;
        this.init();
    }

    public BasicThreadPool(int initSize, int max, int coreSize, int queueSize) {
        this(initSize, max, coreSize, THREAD_FACTORY, queueSize,
                10, TimeUnit.SECONDS, new DenyPolicy.RunnerDenyPolicy());
    }

    private void init() {
        start();
        for (int i = 0; i < initSize; i++) {
            newThread();
        }
    }

    /**
     * 线程池维护
     */
    private void newThread() {
        MyRunnable runnable = new MyRunnable(linkThreadQueue);
        Thread thread = this.threadFactory.createThread(runnable);
        ThreadTask threadTask = new ThreadTask(thread, runnable);
        taskQueue.add(threadTask);
        this.activitySize++;
        thread.start();
    }

    /**
     * 校验shutdown
     */
    private void checkShutDown() {
        if (this.isShutdown) {
            throw new IllegalStateException("this thread pool is destroy!");
        }
    }

    /**
     * 随机一出任务
     */
    private void remoteThread() {
        ThreadTask threadTask = taskQueue.remove();
        threadTask.runnable.stop();
        this.activitySize--;
    }

    @Override
    public void execute(Runnable runnable) {
        checkShutDown();
        this.linkThreadQueue.offer(runnable);
    }

    @Override
    public void shutdown() {
        synchronized (this) {
            if (isShutdown) {return;}
            isShutdown = true;
            taskQueue.forEach(threadTask -> {
                threadTask.runnable.stop();
                threadTask.thread.interrupt();
            });
            this.interrupt();
        }
    }

    @Override
    public int getInitSize() {
        checkShutDown();
        return this.initSize;
    }

    @Override
    public int getMaxSize() {
        return max;
    }

    @Override
    public int getCoreSize() {
        checkShutDown();
        return this.coreSize;
    }

    @Override
    public int getQueueSize() {
        checkShutDown();
        return this.linkThreadQueue.size();
    }

    @Override
    public int getActivitySize() {
        synchronized (this) {
            return this.activitySize;
        }
    }

    @Override
    public boolean isShutdown() {
        return this.isShutdown;
    }

    @Override
    public void run() {
        while (!isShutdown && !isInterrupted()) {
            try {
                timeUnit.sleep(time);
            } catch (InterruptedException e) {
                isShutdown = true;
                break;
            }
            // 开始操作队列任务 添加
            synchronized (this) {
                if (isShutdown){
                    break;
                }
                if (linkThreadQueue.size() > 0 && activitySize < coreSize) {
                    for (int i = initSize; i < coreSize; i++) {
                        newThread();
                    }
                    // continue 不让线程直接到达最大线程数目 浪费资源
                    continue;
                }
                if (linkThreadQueue.size() > 0 && activitySize < max) {
                    for (int i = coreSize; i < max; i++) {
                        newThread();
                    }
                }
                // 如果任务队列没有任务，则回收
                if (linkThreadQueue.size() == 0 && activitySize > coreSize) {
                    for (int i = coreSize; i < activitySize; i++) {
                        remoteThread();
                    }
                }
            }
        }
    }
}
