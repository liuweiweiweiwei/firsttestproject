package com.test.Files;

public class Student {
    private int age;
    private String name;
    private boolean sa;
    private String detail;

    public Student() {
    }

    public Student(int age, String name, boolean sa, String detail) {
        this.age = age;
        this.name = name;
        this.sa = sa;
        this.detail = detail;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSa() {
        return sa;
    }

    public void setSa(boolean sa) {
        this.sa = sa;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", sa=" + sa +
                ", detail='" + detail + '\'' +
                '}';
    }
}
