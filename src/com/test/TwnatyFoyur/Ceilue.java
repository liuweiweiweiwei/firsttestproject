package com.test.TwnatyFoyur;

import sun.applet.Main;

import java.io.*;
import java.util.Properties;

/**
 * 通过更改文件不用更改代码，即可改变程序行为--策略模式
 */
public class Ceilue {
    public interface Iservice{
        public void action();
    }
    public static class confiGurationStrategyDemo{
        public static Iservice createIService(){
            try {
                Properties properties=new Properties();
                String configName="data/classload/config.properties";
                properties.load(new FileInputStream(configName));
                String name=properties.getProperty("service");
                Class<?> cla=Class.forName(name);
                return (Iservice)cla.newInstance();
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }

        public static void main(String[] args) {
            Iservice iservice=createIService();
            iservice.action();
        }
    }

    //自定义classloader-重写findclass
    public static class myClassLoader extends ClassLoader{
        private static final String BASE_URL="data/classload/";

        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            String urlName=name.replaceAll("\\.","/");
            urlName=BASE_URL+urlName+".class";
            try {
                byte[] classBytes=readFileTobyteArray(urlName);
                return defineClass(name,classBytes,0,classBytes.length);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }

        public static void main(String[] args) throws ClassNotFoundException {
            myClassLoader mcl=new myClassLoader();
            String name="data/classload/config.properties";
            Class<?> cla1=mcl.loadClass(name);
            myClassLoader myclass=new myClassLoader();
            Class<?> cla2=myclass.loadClass(name);
            if(cla1!=cla2){
                System.out.println("different class!!!");
            }
        }
    }
    public static byte[] readFileTobyteArray(String filename) throws FileNotFoundException {
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        try(InputStream inputStream=new FileInputStream(filename)) {
            copy(inputStream,outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }
    public static void copy(InputStream inputStream,OutputStream outputStream) throws IOException {
        byte[] bytes=new byte[4096];
        int j=0;
        while ((j=inputStream.read(bytes))!=-1){
            outputStream.write(bytes,0,j);
        }
    }

}
