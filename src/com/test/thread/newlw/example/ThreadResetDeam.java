package com.test.thread.newlw.example;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/17 9:34
 * @description
 */
public class ThreadResetDeam {

    private static volatile int i = 0;
    public static void main(String[] args) throws InterruptedException {
        //1.通过--复位Thread.interrupted();
        Thread threadOne = new Thread(() -> {
            while (true){
                if(Thread.currentThread().isInterrupted()){
                    System.out.println("当前线程："+Thread.currentThread().getName() + "已经被中断 before:"+Thread.currentThread().isInterrupted());
                    //恢复初始状态
                    Thread.interrupted();
                    System.out.println("当前线程："+Thread.currentThread().getName() + "已经恢复状态 after:"+Thread.currentThread().isInterrupted());
                    break;
                }
            }
        });
        threadOne.start();
        threadOne.interrupt();

        System.out.println("--------------------------------------");
        //2.通过异常复位
        Thread threadTwo = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()){
                try {
                    TimeUnit.SECONDS.sleep(1);//中断一个处于阻塞的线程会抛出异常
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
            System.out.println("i + = " + i);
        },"demo_thread");
        threadTwo.start();
        TimeUnit.SECONDS.sleep(1);
        threadTwo.interrupt();
        TimeUnit.SECONDS.sleep(3);
        System.out.println(threadTwo.isInterrupted());
    }
}
