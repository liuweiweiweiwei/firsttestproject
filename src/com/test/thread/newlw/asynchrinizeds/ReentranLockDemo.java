package com.test.thread.newlw.asynchrinizeds;

import javax.lang.model.element.VariableElement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/21 14:07
 * @description
 */
public class ReentranLockDemo {

    static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        lock.lock();//获得锁
        lock.unlock();//释放锁
    }
    public static class RWLock{

        /**
         * 读-读-》不互斥  2.写，读-写、读-》互斥 ---》读多写少场景
         */
        static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        /**
         * //读锁
         */
        static ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        /**
         * //写锁
         */
        static ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

        static Map<String,String> map = new HashMap<>();

        /**
         * 方法B
         * @param key
         * @return
         */
        public static final String get(String key){
            System.out.println("begin start read: ->");
            readLock.lock();//获得读锁
            try{
                return map.get(key);
            }finally {
                readLock.unlock();
            }
        }

        /**
         * 方法A
         * @param key
         * @param val
         * @return
         */
        public static final String put(String key,String val){
            System.out.println("begin start read: ->");
            writeLock.lock();//获得写锁阻塞
            try{
                return map.put(key, val);
            }finally {
                writeLock.unlock();
            }
        }


        public static void main(String[] args) {
            //

        }
    }

}
