package com.test.tool;

import com.test.tool.vo.User;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * @author 25338
 * excel导入mysql
 * 所需jar包---poi,poi-ooxml,xmlbean,commons-collections,commons-compress,poi-ooxml-schemas;
 */
public class TestMysqlInsert {
    private static final String USER_NAME="root";
    private static final String PASSWORD="123456";
    private static final String url="jdbc:mysql://localhost:3306/lw_log?serverTimezone=UTC&useSSL=true";


    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        //List<User> userList=getExcelTableInfo();
       /* List<User> userList= Arrays.asList(new User("小明",12),
                new User("小强",21),new User("小天",15),
                new User("小蓝",20),new User("小提示你",16),
                new User("小红",19),new User("小明",19),
                new User("小紫",18),new User("小光",22));*/
        //insertUser(userList);
//        insertLwUser();
//        System.out.println("成功！");
        //2.beanutils();
        User user = new User();
        Class<User> userClass = User.class;
        Field[] declaredFields = userClass.getDeclaredFields();
        for (Field f:declaredFields) {
            System.out.println(f.getName());
        }
        for (Method m:TestMysqlInsert.class.getDeclaredMethods()) {
            if(m.getName().equals("field")){
                System.out.println(m.getParameterTypes()[0].getSimpleName());
            }
        }

        TestMysqlInsert insert = new TestMysqlInsert();
        List<User> list = insert.selectList(User.class);
        for (User u:list) {
            System.out.println(u.toString());
        }
        List<Integer> listInt = new ArrayList<>();
        listInt.add(null);
        System.out.println(listInt.toString());

    }
    
    private String field(String nameField){
        if(nameField==null||nameField.length()<2){
            return null;
        }
        String field = "";
        if(nameField.startsWith("is")){
            field = nameField.substring(2);
        }else{
            field = nameField.substring(3);
        }
        char[] chars = field.toCharArray();
        chars[0] = Character.toLowerCase(chars[0]);
        return new String(chars);
    }

    /**
     * test
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection= DriverManager.getConnection(url,USER_NAME,PASSWORD);
        String sql="select * from opt_order_state";
        PreparedStatement preparedStatement=connection.prepareStatement(sql);
        ResultSet resultSet=preparedStatement.executeQuery();
        while (resultSet.next()){
            System.out.print(resultSet.getString("OPER_USER")+",");
            System.out.print(resultSet.getString("STATE")+",");
            System.out.println("--");
        }
    }

    /**
     *集合里面包括对象
     */
    public static List<User> getExcelTableInfo(){
        List<User> userList=new LinkedList<>();
        String pathFile="E:/test/text.xlsx";
        //excel转换类-获取文件对象
        try(XSSFWorkbook xssfWorkbook=new XSSFWorkbook(new FileInputStream(pathFile))) {
            //获取sheet对象类 hssfWorkbook=new HSSFWorkbook(new FileInputStream(pathFile))
            XSSFSheet hssfSheet=xssfWorkbook.getSheetAt(0);
            Object value=null;//值
            XSSFRow row=null;//横坐标
            XSSFCell cell=null;//纵坐标
            //循环列表数据
            for(int i=hssfSheet.getFirstRowNum()+1;i<=hssfSheet.getPhysicalNumberOfRows();i++){
                //获取到行数据------
                row=hssfSheet.getRow(i);
                if(row==null){
                    continue;
                }
                //将行数据存到对象中
                User user=new User();
                user.setName(row.getCell(1).toString());
                DecimalFormat decimalFormat=new DecimalFormat("0");
                user.setScope(Integer.parseInt(decimalFormat.format(Double.parseDouble(row.getCell(2)+""))));
                System.out.println("打印序号："+row.getCell(0));
                userList.add(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList;
    }

    /**
     * 插入数据
     * @param userList
     */
    public static void insertUser(List<User> userList){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection=DriverManager.getConnection(url,USER_NAME,PASSWORD);
            String sql="insert into User (name,scope) values (?,?)";
            PreparedStatement preparedStatement=connection.prepareStatement(sql);
            for (User user:userList) {
                preparedStatement.setString(1,user.getName());
                preparedStatement.setInt(2,user.getScope());
                preparedStatement.execute();
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    /**
     *  插入操作
     */
    public static void insertLwUser(){
        try {
            long start=System.currentTimeMillis();
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url, USER_NAME, PASSWORD);
            String SQL="INSERT INTO `lwlog_user` (`id`, `username`, `password`, `year`, `phone`, `sex`, `hobby`, `summary`, `headphoto`, `createtime`) " +
                    "VALUES ( ?, ?, 'MTIzNDU2', 10, ?, 0, ?, ?, ?, ?);";
            PreparedStatement preparedStatement=connection.prepareStatement(SQL);
            int p=0;
            for (int i = 1110000; i < 1120000; i++) {
                preparedStatement.setInt(1,i);
                preparedStatement.setString(2,"小明"+i);
                preparedStatement.setLong(3,130000000+i);
                preparedStatement.setString(4, UUID.randomUUID().toString());
                preparedStatement.setString(5,"篮球"+i);
                preparedStatement.setString(6,"目录10"+i);
                preparedStatement.setInt(7,i);
                preparedStatement.execute();
                //preparedStatement.addBatch();
                p++;
            }
            //preparedStatement.executeBatch();
            long end=System.currentTimeMillis();
        System.out.println("执行的时间为："+(end-start)/1000);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    public <T> List<T> selectList(Class<T> c){
        List<T> list = new ArrayList<>();
        PreparedStatement statement = null;
        Connection connection = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url,USER_NAME,PASSWORD);
            String sql = "select * from `user`";
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            change(resultSet,list,c);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            close(statement,connection);
        }
        return list;
    }

    /**
     *
     * @param resultSet
     * @param list
     * @param aClass
     * @param <T>
     * @return
     */
    public <T> void change(ResultSet resultSet,List<T> list,Class<?> aClass){
        Method[] declaredMethods = aClass.getDeclaredMethods();
        try {
            while (resultSet.next()) {
                //创建对象
                Object o1 = aClass.newInstance();
                //遍历方法
                for (Method m : declaredMethods) {
                    //方法可操作
                    m.setAccessible(true);
                    String name = m.getName();
                    //获取set方法
                    if (name.startsWith("set")) {
                        //获取属性名
                        String field = field(name);
                        //获取值
                        Object param = getData(m.getParameterTypes()[0].getSimpleName(),field,resultSet);
                        if(param != null) {
                            //赋值
                            m.invoke(o1, param);
                        }
                    }
                }
                list.add((T)o1);
            }
        } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据
     * @param type
     * @param name
     * @param resultSet
     * @return
     */
    private Object getData(String type, String name,ResultSet resultSet ){
        if(type == null){
            return null;
        }
        try {
            switch (type) {
                case "String":
                    return resultSet.getString(name);
                case "Integer":
                case "int":
                    return resultSet.getInt(name);
                case "Boolean":
                case "boolean":
                    return resultSet.getInt(name);
                case "Byte":
                case "byte":
                    return resultSet.getByte(name);
                case "Date":
                    return resultSet.getDate(name);
                case "Long":
                case "long":
                    return resultSet.getLong(name);
                case "Double":
                case "double":
                    return resultSet.getDouble(name);
                case "Float":
                case "float":
                    return resultSet.getFloat(name);
                default:break;
            }
        } catch (SQLException e) {
            return null;
        }
        return null;
    }

    public void close(PreparedStatement preparedStatement,Connection connection){
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(preparedStatement != null){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
