package com.test.models.gpdecorate.v1;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:14
 * @description 装饰者模式
 */
public class BatterCake {

    protected String name(){
        return "煎饼";
    }

    protected int price(){
        System.out.println("价格是：5元" );
        return 5;
    }
}
