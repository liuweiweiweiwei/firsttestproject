package com.test.models.template.tem2;

import com.alibaba.fastjson.JSON;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/17 14:59
 * @description
 */
public class Start {
    private static final Block<ItemBlock.Item> block=new ItemBlock();

    public static void main(String[] args) {
        ItemBlock.Item process = block.process(new Model());
        System.out.println(JSON.toJSONString(process));
    }
}
class Model{
    private int age;
    public Model() { }
    public Model(int age) { this.age = age; }
    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }
}
abstract class Block<T>{
    //流程
    public T process(Model model){
        T t=getInit();
        work(model,t);
        return t;
    }
    protected abstract T getInit();
    protected abstract void work(Model model,T o);
}
class ItemBlock extends Block<ItemBlock.Item>{


    @Override
    protected Item getInit() {
        return new Item();
    }

    @Override
    protected void work(Model model, Item o) {
        model.setAge(16);
        o.setName("信息");
        o.setFage(model.getAge());
    }


    public static class Item{
        private String name;
        private int fage;
        public int getFage() { return fage; }
        public void setFage(int fage) { this.fage = fage; }
        public Item() { }
        public String getName() { return name; }
        public void setName(String name) { this.name = name; }
        public Item(String name) { this.name = name; }
    }
}
