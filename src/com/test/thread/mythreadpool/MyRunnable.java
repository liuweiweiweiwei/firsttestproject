package com.test.thread.mythreadpool;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 21:02
 * @description runnable task
 */
public class MyRunnable implements Runnable{
    private final RunnableQueue runnableQueue;

    private volatile boolean running = true;

    public MyRunnable(RunnableQueue runnableQueue) {
        this.runnableQueue = runnableQueue;
    }

    @Override
    public void run() {
        while (running && !Thread.currentThread().isInterrupted()) {
            try{
                Runnable take = runnableQueue.take();
                take.run();
                TimeUnit.MILLISECONDS.sleep(1);
            } catch (InterruptedException e) {
                running = false;
                break;
            }
        }
    }

    public void stop() {
        running = false;
    }
}
