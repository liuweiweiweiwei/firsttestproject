package com.test.tool.vo;

import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/8/5 9:52
 * @description
 */
public class Test2 {
    private String name;
    private Integer age;
    private List<Integer> content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Integer> getContent() {
        return content;
    }

    public void setContent(List<Integer> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Test2{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", content=" + content +
                '}';
    }
}
