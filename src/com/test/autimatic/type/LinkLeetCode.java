package com.test.autimatic.type;

import java.util.LinkedList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/2 10:25
 * @description
 */
public class LinkLeetCode {
    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;listNode4.next = listNode5;
        reverseList(listNode1);
        System.out.println(rotateRight(listNode1,2));
        // --------
        ListNode listNode11 = new ListNode(0);
        ListNode listNode12 = new ListNode(3);
        ListNode listNode13 = new ListNode(1);
        ListNode listNode14 = new ListNode(0);
        ListNode listNode15 = new ListNode(4);
        ListNode listNode16 = new ListNode(5);
        ListNode listNode17 = new ListNode(2);
        ListNode listNode18 = new ListNode(0);
        listNode11.next = listNode12;listNode12.next = listNode13;listNode13.next = listNode14;
        listNode14.next = listNode15;
        listNode15.next = listNode16;listNode16.next = listNode17;listNode17.next = listNode18;
        System.out.println(mergeNodes(listNode11));
        int num = 1974;
        for (int i = 72; i < 83; i++) {
            num += i;
        }
        System.out.println(num);
    }


    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode();
        ListNode temp = new ListNode(0);
        result.next = temp;
        int isHigh = 0;
        while (l1 != null || l2 != null || isHigh == 1) {
            int first = 0;
            int second = 0;
            if (l1 != null) {
                first = l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                second = l2.val;
                l2 = l2.next;
            }
            int num = first + second + isHigh;
            isHigh = num > 9 ? 1 : 0;
            temp.next = new ListNode(num % 10);
            temp = temp.next;
        }
        return result.next.next;
    }

    public static ListNode deleteDuplicates(ListNode head) {
        ListNode res = new ListNode();
        ListNode listNode = new ListNode();
        listNode.next = res;
        boolean isSample = false;
        while (head != null) {
           while (head.next != null && head.val == head.next.val) {
               head.next = head.next.next;
               isSample = true;
           }
           if (!isSample) {
               res.next = new ListNode(head.val);
               res = res.next;
           }
           head = head.next;
           isSample = false;
        }
        return res.next;
    }

    public static ListNode mergeNodes(ListNode head) {
        ListNode next = head.next;
        ListNode tempHead = head;
        ListNode temp = tempHead;
        ListNode tempNext = new ListNode(0);
        while (next != null){
            if (next.val == 0) {
                tempHead.next = tempNext;
                tempHead = tempHead.next;
                tempNext = new ListNode(0);
            }else {
                tempNext.val += next.val;
            }
            next = next.next;
        }
        return temp.next;
    }


    public static class ListNode {
        int val;
        ListNode next;
        ListNode(){}
        ListNode(int x) { val = x; }
        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static ListNode rotateRight(ListNode head, int k) {
        if(head == null ){ return null; }
        ListNode tempHead = head;
        List<Integer> queue = new LinkedList<>();
        while (tempHead != null){
            queue.add(tempHead.val);
            tempHead = tempHead.next;
        }
        k = queue.size() - (k % queue.size());
        if(k == 0){return head;}
        int index = 0;
        ListNode temp = head;
        while (index < k){
            temp = temp.next;
            index++;
        }
        if(temp != null) {
            ListNode tail = temp;
            while (tail.next != null) {
                tail = tail.next;
            }
            ListNode newHaed = new ListNode(0);
            ListNode newHeadTemp = newHaed;
            index = 0;
            while (index < k) {
                newHeadTemp.next = new ListNode(queue.get(index));
                newHeadTemp = newHeadTemp.next;
                index++;
            }
            newHaed = newHaed.next;
            tail.next = newHaed;
        }
        return temp;
    }

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        if(n == 1 && head.next == null){ return null; }
        ListNode temp = new ListNode(-1,head);
        ListNode middle = head;
        int index = 1;
        while (true){
            if(middle.next == null){ temp.next = temp.next.next; break;}
            if(index >= n){ temp = temp.next; }
            middle = middle.next;
            index++;
        }
        if(n == index){ return head.next; }
        return head;
    }

    public static ListNode reverseList(ListNode head) {
        if(head == null){return null;}
        ListNode tempHead = new ListNode(head.val);
        while (head.next != null){
            ListNode next = new ListNode(head.next.val);
            head = head.next;
            next.next = tempHead;
            tempHead = next;
        }
        return tempHead;
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        int len = 0;
        ListNode lenNode = new ListNode();
        lenNode.next = head;
        while ((lenNode = lenNode.next) != null) {
            len++;
        }
        for (int i = 0; i < len; i += k) {
            head = reverseNtoM(head, i, k);
        }
        return head;
    }

    /**
     * Flip the first n linked list data
     */
    private ListNode curListNode;
    private ListNode reverseNode(ListNode listNode, int start) {
        if (start == 1) {
            curListNode = listNode.next;
            return listNode;
        }
        ListNode lastNode = reverseNode(listNode.next, start - 1);
        listNode.next.next = listNode;
        listNode.next = curListNode;
        return lastNode;
    }

    /**
     * 翻转从n到m个数据
     * @param head
     * @param from
     * @param to
     * @return list
     */
    private ListNode reverseNtoM(ListNode head, int from, int to) {
        if (from == 1) {
            return reverseNode(head, to);
        }
        head.next = reverseNtoM(head.next, from - 1, to);
        return head;
    }
}
