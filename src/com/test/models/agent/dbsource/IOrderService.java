package com.test.models.agent.dbsource;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:35
 * @description
 */
public interface IOrderService {

    int createOrder(Order order);

    void dealOrderService(Order order);
}
