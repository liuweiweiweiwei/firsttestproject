package com.test.classes;

/**
 * @author 25338
 * @date 2022/5/10 22:01
 * @since 1.0
 * description
 */
public class Singleton {
    private static Singleton singleton = new Singleton();

    private static int x = 0;

    private static int y;

    private Singleton(){x++;y++;}

    public static Singleton getInstance() {
        return singleton;
    }

    public static void main(String[] args) {
        System.out.println(x);
        System.out.println(y);
    }
}
