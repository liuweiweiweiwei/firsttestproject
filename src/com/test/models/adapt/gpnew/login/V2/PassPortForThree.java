package com.test.models.adapt.gpnew.login.V2;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 15:44
 * @description
 */
public class PassPortForThree extends OldLogin implements IPassPortForThree {

    @Override
    public String loginForPwd() {
        return super.loginForPwd();
    }

    @Override
    public String loginForQQ() {
        return getBeanFactory(LoginForQQAdapter.class,"id","name");
    }

    @Override
    public String loginForWc() {
        return null;
    }

    @Override
    public String loginForPhone() {
        return getBeanFactory(LoginForPhone.class,"id","name");
    }

    @Override
    public String loginForWb() {
        return null;
    }

    private String getBeanFactory(Class<?> clazz,String id,String name){
        try {
            LoginAdapter loginAdapter = (LoginAdapter) clazz.newInstance();
            if(loginAdapter.support(loginAdapter)){
                return loginAdapter.login(id,name);
            }else{
                System.out.println("不支持此种方式登录！");
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return "类型不匹配！系统错误！";
        }
        return "登陆失败！";
    }
}
