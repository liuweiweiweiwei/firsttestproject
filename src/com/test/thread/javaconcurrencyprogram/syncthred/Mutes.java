package com.test.thread.javaconcurrencyprogram.syncthred;

import com.test.MyLog.LwLog;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/25 21:10
 * @description
 */
public class Mutes {
    private static final Object MUTE = new Object();

    public void accessTask() {
        synchronized (MUTE) {
            try {
                TimeUnit.MINUTES.sleep(1);
            } catch (InterruptedException e) {
                LwLog.errorInfo(Thread.currentThread().getName() + " has a error!");
            }
        }
    }

    public static void main(String[] args) {
        final Mutes mutes = new Mutes();
        for (int i = 0; i < 5; i++) {
            new Thread(mutes::accessTask).start();
        }
    }
}
