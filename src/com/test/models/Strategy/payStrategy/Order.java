package com.test.models.Strategy.payStrategy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 18:50
 * @description
 */
public class Order {
    private String uid;
    private String orderId;
    private double amount;

    public Order(String uid, String orderId, double amount) {
        this.uid = uid;
        this.orderId = orderId;
        this.amount = amount;
    }

    public MsgResult pay(String payKey){
        System.out.println("支付信息");
        Pay pay = PayStrategy.getInstance(payKey);
        System.out.println("黄莺使用：" + pay.getName());
        return pay.pay(this.orderId,this.amount);
    }

    public MsgResult pay1(Class<?> clazz){
        System.out.println("工厂模式");
        Pay instance = PayStrategy.getInstance(clazz);
        System.out.println("欢迎使用：" + instance.getName());
        return instance.pay(this.orderId,this.amount);
    }

    public MsgResult pay2(String className){
        try {
            Class<?> aClass = Class.forName(className);
            return pay1(aClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
