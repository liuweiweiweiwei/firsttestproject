package com.test.models.gpdecorate.v2;


/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:19
 * @description 测试类
 */
public class BatterTest {

    public static void main(String[] args) {

        BatterCake batterCake = new DefaultBatterCake();
        System.out.println(batterCake.name() + "的价格是：" + batterCake.price() + "元！");
        batterCake = new BatterCakeEgg(batterCake);
        System.out.println(batterCake.name() + "的价格是：" + batterCake.price() + "元！");
        batterCake = new BatterCakeSauger(batterCake);
        System.out.println(batterCake.name() + "的价格是：" + batterCake.price() + "元！");


    }
}
