package com.test.java8;

public class ThreadNew extends Thread {
    private Counter counter;
    public ThreadNew(Counter counter){
        this.counter=counter;
    }

    @Override
    public void run() {
        for (int i = 0; i <1000 ; i++) {
            counter.add();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int num=1000;
        Thread thread[]=new Thread[num];
        Counter counter=new Counter();
        for (int i = 0; i <num; i++) {
            thread[i]=new ThreadNew(counter);
            thread[i].start();
        }
        for (int i = 0; i <num; i++) {
            thread[i].join();
        }
        System.out.println("最终输出结果为:"+counter.getCoun());
    }

     static class Counter{
        private int coun=0;
        public synchronized void add(){
            coun++;
        }
        public synchronized int getCoun(){
            return coun;
        }
    }
}
