package com.test.autimatic.weekDay;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/3 11:57
 * @description
 */
public class day0403 {
    public static void main(String[] args) {
        System.out.println(convertTime("11:00","11:01"));
        System.out.println(maximumCandies(new int[]{9,10,1,2,10,9,9,10,2,2}, 3));
    }

    /**
     * 给你两个字符串 current 和 correct ，表示两个 24 小时制时间 。
     24 小时制时间 按 "HH:MM" 进行格式化，其中 HH 在 00 和 23 之间，而 MM 在 00 和 59 之间。
     最早的 24 小时制时间为 00:00 ，最晚的是 23:59 。
     在一步操作中，你可以将 current 这个时间增加 1、5、15 或 60 分钟。你可以执行这一操作 任意 次数。
     返回将 current 转化为 correct 需要的 最少操作数 。
     * @param current current
     * @param correct correct
     * @return date
     */
    public static int convertTime(String current, String correct) {
        String[] dateCur = current.split(":");
        String[] dateCorect = correct.split(":");
        int curHour = Integer.parseInt(dateCur[0]);
        int curMini = Integer.parseInt(dateCur[1]);
        int corectHoue = Integer.parseInt(dateCorect[0]);
        int corectMini = Integer.parseInt(dateCorect[1]);
        int[] data = {15, 5, 1};
        int result = 0;
            result += (corectHoue - curHour);
            if (curMini > corectMini) {
                result--;
                corectMini += 60;
            }
            while (curMini < corectMini) {
                for (int i:data) {
                    if (corectMini >= curMini + i) {
                        result++;
                        curMini += i;
                        break;
                    }
                }
            }
        return result;
    }

    /**
     * 给你一个整数数组 matches 其中 matches[i] = [winneri, loseri] 表示在一场比赛中 winneri 击败了 loseri 。
     返回一个长度为 2 的列表 answer ：
     answer[0] 是所有 没有 输掉任何比赛的玩家列表。
     answer[1] 是所有恰好输掉 一场 比赛的玩家列表。
     两个列表中的值都应该按 递增 顺序返回。
     *
     * @param matches mathc
     * @return ans
     */
    public static List<List<Integer>> findWinners(int[][] matches) {
        Set<Integer> winMap = new HashSet<>();
        TreeMap<Integer, Integer> loseMap = new TreeMap<>();
        for (int[] arrs:matches) {
            if (!loseMap.containsKey(arrs[0])) {
                winMap.add(arrs[0]);
            }else {
                winMap.remove(arrs[0]);
            }
            if (winMap.contains(arrs[1])) {
                winMap.remove(arrs[1]);
            }
            int losePut = loseMap.getOrDefault(arrs[1], 0);
            loseMap.put(arrs[1], losePut + 1);
        }
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> win = new ArrayList<>();
        List<Integer> losed = new ArrayList<>();
        win.addAll(winMap);
        Collections.sort(win);
        loseMap.forEach((k, v) -> { if (v == 1) { losed.add(k); } });
        result.add(win);
        result.add(losed);
        return result;
    }

    /**
     * 给你一个 下标从 0 开始 的整数数组 candies 。数组中的每个元素表示大小为 candies[i] 的一堆糖果。
     * 你可以将每堆糖果分成任意数量的 子堆 ，但 无法 再将两堆合并到一起。
     另给你一个整数 k 。你需要将这些糖果分配给 k 个小孩，使每个小孩分到 相同 数量的糖果。
     每个小孩可以拿走 至多一堆 糖果，有些糖果可能会不被分配。
     返回每个小孩可以拿走的 最大糖果数目 。
     *
     * @param candies c
     * @param k k
     * @return re
     */
    public static int maximumCandies(int[] candies, long k) {
        long[] right = sum(candies);
        if (right[0] < k) { return 0; }
        int left = 1;
        int res = 0;
        int tempRight = Math.toIntExact(right[1]);
        while (left <= tempRight) {
            int mid = left + ((tempRight - left) / 2);
            if (isDevice(candies, k, mid)) {
                res = mid;
                left = mid + 1;
            }else {
                tempRight = mid;
            }
        }
        return Math.toIntExact(res);
    }

    /**
     * 获取糖果最小值
     *
     * @param candies c
     * @return min
     */
    private static long[] sum(int[] candies) {
        long candie = 0;
        int min = 0;
        for (int candieTemp:candies) {
            candie += candieTemp;
            min = Math.max(min, candieTemp);
        }
        return new long[]{candie, min};
    }

    /**
     * is can device
     *
     * @param candies candies
     * @param childNum childNum
     * @return bool
     */
    private static boolean isDevice(int[] candies, long childNum, int device) {
       long time = 0;
        for (int can:candies) {
            time += can / device;
            if (time >= childNum) {
                return true;
            }
        }
        return false;
    }
}
