package com.test.models.gpdecorate.v3;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:40
 * @description
 */
public class BatterCakeEgg extends BatterDecorator {
    public BatterCakeEgg(BatterCake batterCake) {
        super(batterCake);
    }

    @Override
    protected String name() {
        return "又加了一个鸡蛋的鸡蛋饼";
    }

    @Override
    protected int price() {
        return super.price() + 1;
    }
}
