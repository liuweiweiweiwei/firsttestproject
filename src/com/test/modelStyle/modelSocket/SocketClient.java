package com.test.modelStyle.modelSocket;

import java.io.*;
import java.net.Socket;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/10/9 17:26
 * @description
 */
public class SocketClient {

    public static void main(String[] args) {
        getConnect("小米");
        getConnect("小红");
        getConnect("小明");
    }


    public static void getConnect(String name){
        InputStream inputStream=null;
        BufferedReader bufferedReader=null;
        OutputStream outputStream=null;
        BufferedWriter bufferedWriter=null;
        Socket socket=null;
        try {
            socket=new Socket("localhost",8765);
            outputStream=socket.getOutputStream();
            bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream));
            bufferedWriter.write(name+"进入服务器！");
            bufferedWriter.flush();
            socket.shutdownOutput();

            //2.返回消息
            inputStream=socket.getInputStream();
            bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            String input;
            StringBuffer stringBuffer=new StringBuffer();
            while ((input=bufferedReader.readLine())!=null){
                stringBuffer.append(input);
            }
            System.out.println("从服务端接收的信息："+stringBuffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bufferedWriter.close();
                outputStream.close();
                bufferedReader.close();
                inputStream.close();
                if(null != socket)  {
                    socket.close();
                }
            }catch (IOException e){
                e.getMessage();
            }
        }
    }
}
