package com.test.models.Observer.event;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 15:16
 * @description
 */
public class MouceEventClick {

    public void onClick(EventL eventL) {
        System.out.println("-------------调用单机方法-------------\n" + eventL);
    }

    public void onTwoClick(EventL eventL) {
        System.out.println("-------------调用双击方法-------------\n" + eventL);
    }

    public void onUp(EventL eventL) {
        System.out.println("-------------调用上上方法-------------\n" + eventL);
    }

    public void onOn(EventL eventL) {
        System.out.println("-------------调用ON方法-------------\n" + eventL);
    }
}
