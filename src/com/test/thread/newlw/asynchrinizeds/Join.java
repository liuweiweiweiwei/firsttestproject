package com.test.thread.newlw.asynchrinizeds;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/20 19:53
 * @description
 */
public class Join {

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(()->{
            System.out.println("这是县城一！");
        });
        Thread thread2 = new Thread(()->{
            System.out.println("这是县城二！");
        });
        Thread thread3 = new Thread(()->{
            System.out.println("这是县城三！");
        });

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();
        thread3.start();
    }
}
