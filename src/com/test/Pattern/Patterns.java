package com.test.Pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 匹配
 */
public class Patterns {
    /**
     *    .*表示贪婪匹配例如：a.*c匹配abcabc会匹配一个bcab如果想去除贪婪匹配则需要 加个？
     */
    public static void findGroup(){
        String regx="(\\d{4})-(\\d{2})-(\\d{2})";
        Pattern pattern=Pattern.compile(regx);
        String string="today is 2020-06-16,yesterday is 2019-08-15";
        Matcher matcher=pattern.matcher(string);
        while (matcher.find()){
            System.out.println("enter method :"+matcher.group(1)+",month:"+matcher.group(2)+",day:"+matcher.group(3));
        }
    }

    public static void main(String[] args) {
        findGroup();
    }
}
