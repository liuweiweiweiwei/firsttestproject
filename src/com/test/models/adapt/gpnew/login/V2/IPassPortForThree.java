package com.test.models.adapt.gpnew.login.V2;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 15:33
 * @description
 */
public interface IPassPortForThree{

    String loginForQQ();


    String loginForWc();

    String loginForPhone();

    String loginForWb();
}
