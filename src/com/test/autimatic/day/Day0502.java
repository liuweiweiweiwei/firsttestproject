package com.test.autimatic.day;

import com.test.MyLog.LwLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/2 9:49
 * @description
 */
public class Day0502 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString("123 sd er".split(" ", 2)));
        System.out.println(findTheWinner(423, 12));
        int[][] generateMatrix = generateMatrix(3);
        LwLog.splitLogInfo("分割线");
        rotate(new int[][]{{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}});
        LwLog.splitLogInfo("分割线");
        List<Integer> row = getRow(5);
        LwLog.splitLogInfo("分割线");
    }

    public static String[] reorderLogFiles(String[] logs) {
        List<String> digitList = new ArrayList<>();
        List<String> charList = new ArrayList<>();
        for (String log : logs) {
            if (Character.isDigit(log.charAt(log.indexOf(" ") + 1))) {
                digitList.add(log);
            } else {
                charList.add(log);
            }
        }
        charList.sort((o1, o2) -> {
            String[] split1 = o1.split(" ", 2);
            String[] split2 = o2.split(" ", 2);
            if (split1[1].equals(split2[1])) {
                return split1[0].compareTo(split2[0]);
            }
            return split1[1].compareTo(split2[1]);
        });
        charList.addAll(digitList);
        return charList.toArray(new String[0]);
    }

    public int majorityElement(int[] nums) {
        int count = 0;
        int result = Integer.MAX_VALUE;
        for (int num : nums) {
            if (count == 0) {
                result = num;
            }
            count += (result == num) ? 1 : -1;
        }
        return result;
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {continue;}
            int two = i + 1;
            int three = nums.length - 1;
            while (three > two) {
                int num = nums[i] + nums[two] + nums[three];
                if (num == 0) {
                    result.add(Arrays.asList(nums[i], nums[two], nums[three]));
                    three--;
                    two++;
                } else if (num > 0) {
                    three--;
                } else {
                    two++;
                }
                while (three < nums.length - 1 && three > two && nums[three] == nums[three + 1]) {
                    three--;
                }
            }
        }
        return result;
    }

    public static int findTheWinner(int n, int k) {
        // 約瑟夫环 - f(n,k) = (f(n-1,k) + k) % i
//        int ans = 0;
//        for (int i = 2; i <= n; i++) {
//            ans = (ans + k) % i;
//        }
//        return ans + 1;

        boolean[] findBool = new boolean[n];
        int time = 1;
        int index = -1;
        while (time++ < n) {
            index = changeFind(findBool, index, k);
        }
        for (int i = 0; i < findBool.length; i++) {
            if (!findBool[i]) {
                return i + 1;
            }
        }
        return k + 1;
    }

    private static int changeFind(boolean[] find, int index, int len) {
        int time = 0;
        while (time < len) {
            index++;
            if (index >= find.length) {
                index = 0;
            }
            if (find[index]) {
                continue;
            }
            time++;
        }
        find[index] = true;
        return index;
    }

    public static int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int i = 0, j = 0, index = 1, xy = 1;
        int endx = n;
        int endy = n;
        int startx = 0;
        int starty = 0;
        while (index <= n * n) {
            result[i][j] = index++;
            switch (xy) {
                case 1:
                    if (j + 1 == endy) {
                        xy = 2;
                        i++;
                    } else {
                       j++;
                    }
                    break;
                case 2:
                    if (i + 1 == endx) {
                        xy = 3;
                        j--;
                    } else {
                        i++;
                    }
                    break;
                case 3:
                    if (j == starty) {
                        xy = 4;
                        i--;
                    } else {
                        j--;
                    }
                    break;
                case 4:
                    if (i == startx + 1) {
                        xy = 1;
                        starty++;
                        startx++;
                        endy--;
                        endx--;
                        j++;
                    } else {
                        i--;
                    }
                    break;
                default: break;
            }
        }
        return result;
    }

    public static void rotate(int[][] matrix) {
        int start = 0,end = matrix.length - 1;
        while (end - start > 0) {
            for (int i = 0; i < end; i++) {
                int temp = matrix[end - i][start];
                matrix[end - i][start] = matrix[end][end - i];
                matrix[end][end - i] = matrix[start + i][end];
                matrix[start + i][end] = matrix[start][start + i];
                matrix[start][start + i] = temp;
            }
            start++;
            end--;
        }
    }

    public static List<Integer> getRow(int rowIndex) {
        if (rowIndex == 0) {
            return Collections.singletonList(1);
        }
        if (rowIndex == 1) {
            return Arrays.asList(1,1);
        }
        Queue<Integer> queue = new LinkedList<>();
        queue.add(1);
        while (rowIndex-- > 0) {
            Queue<Integer> curqueue = new LinkedList<>();
            int start = queue.poll();
            curqueue.add(start);
            while (!queue.isEmpty()) {
                Integer poll = queue.poll();
                curqueue.add(poll + start);
                start = poll;
            }
            curqueue.add(1);
            queue = curqueue;
        }
        return new ArrayList<>(queue);
    }
}
