package com.test.thread.javaconcurrencyprogram.forkjointask;

import java.util.Locale;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/7 21:20
 * @description
 */
public class MyForkJoinTask {
    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        long start = System.currentTimeMillis();
        MyForkTask forkTask = new MyForkTask(0, 1000000000L);
        Long invoke = forkJoinPool.invoke(forkTask);
        long middle = System.currentTimeMillis();
        System.out.println((middle - start) / 1000);
        long num = 0L;
        for (long i = 0; i < 1000000000L; i++) {
            num += i;
        }
        long end = System.currentTimeMillis();
        System.out.println((end - middle) / 1000);
        System.out.println(String.format(Locale.ROOT, "forkJoin : %d \n num : %d", invoke, num));
    }

    private static class MyForkTask extends RecursiveTask<Long> {
        private static final long FORK_LONG = 500000000L;
        private long start;
        private long end;

        public MyForkTask(long start, long end) {
            super();
            this.start = start;
            this.end = end;
        }

        @Override
        protected Long compute() {
            long middle = end - start;
            if (middle <= FORK_LONG) {
                long num = 0L;
                for (long i = start; i < end; i++) {
                    num += i;
                }
                return num;
            } else {
                long mid = (end - start) / 2;
                MyForkTask left = new MyForkTask(start, mid);
                MyForkTask right = new MyForkTask(mid + 1, end);
                left.fork();
                right.fork();
                return left.join() + right.join();
            }
        }
    }
}
