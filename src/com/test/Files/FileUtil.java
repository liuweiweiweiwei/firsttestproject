package com.test.Files;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/30 10:27
 * @description
 */
public class FileUtil {

    /**]
     * 文件复制
     * @param sourceFile
     * @param targetFile
     */
    public static void copy(String sourceFile,String targetFile){
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        //创建文件
        mkdirsOrFile(targetFile,1);
        try{
            inputStream = new FileInputStream(sourceFile);
            outputStream = new FileOutputStream(targetFile);
            byte[] b = new byte[1024];
            int len;
            while ((len = inputStream.read(b))!= -1){
                outputStream.write(b,0,len);
            }
        }catch (Exception e){
            System.out.println("原文件-"+sourceFile + "复制到 -"+targetFile + "失败！");
            e.printStackTrace();
        }finally {
            closeO(outputStream);
            closeI(inputStream);
        }
    }

    public static boolean deleteFile(File file){
        if(!file.exists()){//如果文件不存在返回
            return false;
        }
        if(file.isFile()){
            return file.delete();
        }else{//删除文件夹内的文件
            File[] files = file.listFiles();
            if(files != null && files.length > 0) {
                for (File t : files) {
                    return deleteFile(t);
                }
            }//删除文件夹
            return file.delete();
        }
    }

    /**
     * 关闭
     * @param inputStream
     */
    public static void closeI(InputStream inputStream){
        if(inputStream != null){
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭流
     * @param reader
     */
    public static void closeIR(Reader reader){
        if(reader != null){
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭输出流
     * @param writer
     */
    public static void closeOR(Writer writer){
        if(writer != null){
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭
     * @param outputStream
     */
    public static void closeO(OutputStream outputStream){
        if(outputStream != null){
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * * zip压缩文件以及文件夹
     * @param source 要压缩的文件
     * @param target 腰锁喉的路径
     */
    public static void zipCompress(String source,String target){
        ZipOutputStream outputStream = null;
        File file = new File(source);
        //创建文件
        mkdirsOrFile(target,1);
        try{
            outputStream = new ZipOutputStream(new FileOutputStream(target));
            zipCom(outputStream,file);
            outputStream.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeO(outputStream);
        }
    }

    /**
     * 压缩操作
     * @param outputStream 压缩流
     * @param file 文件
     */
    private static void zipCom(ZipOutputStream outputStream,File file){
        if(!file.exists()){
            return;
        }//文件夹循环压缩
        if(file.isDirectory()){
            for (File f:file.listFiles()) {
                zipCom(outputStream,f);
            }
        }else{//文件直接压缩
            FileInputStream inputStream = null;
            try{//构建zipEntry对象
                outputStream.putNextEntry(new ZipEntry(file.getName()));
                inputStream = new FileInputStream(file);
                intOutStream(inputStream,outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {//关闭输入流
                closeI(inputStream);
            }
        }
    }

    /**
     * 文件流的读写操作
     * @param inputStream 输入流
     * @param outputStream 输出流
     */
    private static void intOutStream(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bytes = new byte[1024];
        int len;
        while ((len = inputStream.read(bytes)) != -1){
            outputStream.write(bytes,0,len);
        }
        outputStream.flush();
    }


    /**
     * 解压缩操作
     * @param source
     * @param target
     */
    public static void zipDecompression(String source,String target){
        ZipInputStream inputStream = null;
        try{
            inputStream = new ZipInputStream(new FileInputStream(source));
            zipDecom(inputStream,new File(target));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeI(inputStream);
        }
    }

    /**
     * 解压缩操作
     * @param inputStream
     * @param outFile
     */
    private static void zipDecom(ZipInputStream inputStream,File outFile){
        if(inputStream == null || !outFile.exists()){
            return;
        }
        try {
            ZipEntry nextEntry;
            while ((nextEntry = inputStream.getNextEntry()) != null){
                FileOutputStream outputStream = null;
                try {
                    //创建文件
                    String path = outFile.getPath() + File.separator + nextEntry.getName();
                    mkdirsOrFile(path, 1);
                    outputStream = new FileOutputStream(path);
                    intOutStream(inputStream, outputStream);
                }finally {
                    closeO(outputStream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 创建文件或者文件夹
     * @param path 文件路径
     * @param type 文件类型0：文件夹 1：文件
     */
    public static boolean mkdirsOrFile(String path,int type){
        File file = new File(path);
        if(file.exists()){
            return true;
        }
        if(0 == type){
            return file.mkdirs();
        }else{
            try {
                return file.createNewFile();
            } catch (IOException e) {
                System.out.println("文件夹："+path + "创建失败！");
                e.printStackTrace();
                return false;
            }
        }
    }


    /**
     * 测试
     * @param args
     */
    public static void main(String[] args) {
        //文件复制
//        copy("E:\\taikang\\software\\电销司庆活动需求规格说明书.docx","E:\\taikang\\software\\test\\电销司庆活动需求规格说明书.docx");
        //文件压缩
        zipCompress("E:\\taikang\\software","E:\\taikang\\file.zip");
        //文件解压
        zipDecompression("E:\\taikang\\file.zip","E:\\taikang\\testfile");
        System.out.println("操作成功！");
    }

}
