package com.test.java8;

import java.io.File;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Lambles {
    public static void main(String[] args) {
        File file=new File("E:\\MyDownloadsNew\\");
        File[] files=file.listFiles((File fil,String name)->name.endsWith(".e"));
        Arrays.asList(files).forEach(file1 -> System.out.println(file1.getName()));
        List<Student> students=Arrays.asList(new Student[]{new Student(8,"肖勉"),
                new Student(18,"lisar"),new Student(22,"sam")});
        List<Student> studentList=filter(students,t->t.getAge()>17);
        studentList.forEach(student -> System.out.println(student.toString()));


        //-------------------------------
        Map<String,Object> map= Arrays.asList(files).stream().collect(Collectors.toMap(f->f.getName(),ma->ma));

        Map<Integer,Student> map1=students.stream().filter(student -> student.getAge()>10).collect(Collectors.toMap(s->s.getAge(),m->m));

        Set<Integer> strings=map1.keySet();
        Iterator<Integer> iterator=strings.iterator();
        if(iterator.hasNext()){
            Integer key=iterator.next();
            System.out.println(key+"::"+map.get(key));
        }
        List<List<String>> lists=new LinkedList<>();
        List<String> stringList=Arrays.asList("tset","send");
        List<String> stringList1=Arrays.asList("tset1","send1");
        List<String> stringList2=Arrays.asList("tset2","send2");
        lists.add(stringList);
        lists.add(stringList1);
        lists.add(stringList2);
        List<String> listlist= lists.stream()
                .flatMap(st1->st1.stream().map(s->s+"###")).collect(Collectors.toList());
        listlist.forEach(System.out::println);
    }
    public static <E> List<E> filter(List<E> list, Predicate<E> predicate){
        List<E> list1=new ArrayList<>();
        for (E e:list
             ) {
            if(predicate.test(e)){
                list1.add(e);
            }
        }
        return list1;
    }


}
