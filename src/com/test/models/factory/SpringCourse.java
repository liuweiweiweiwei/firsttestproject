package com.test.models.factory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:45
 * @description
 */
public class SpringCourse implements MyCourse {
    @Override
    public void record() {
        System.out.println("spring 的超级课程！");
    }
}
