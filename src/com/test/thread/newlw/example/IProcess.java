package com.test.thread.newlw.example;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/16 20:50
 * @description 接口公共功能
 */
public interface IProcess {

    void process(User user);
}
