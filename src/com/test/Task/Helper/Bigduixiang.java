package com.test.Task.Helper;

/**
 * @author 25338
 * @version 1.1
 * @date 2020/8/2 16:40
 */
import java.util.List;

public class Bigduixiang {

    List<Province> provinceList;

    public List<Province> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<Province> provinceList) {
        this.provinceList = provinceList;
    }

    @Override
    public String toString() {
        return "Bigduixiang{" +
                "provinceList=" + provinceList +
                '}';
    }

}
