package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/8 19:31
 * @description
 */
public class GPStudy {

    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 6, 2, 3, 1, 8, 9, 0, 4, 5, 6, 2, 3, 5};
        System.out.println(Arrays.stream(arr).sum());
        System.out.println(getWin(arr));
        System.out.println(getWin2(arr));
        //飞马打象棋
        System.out.println(getHorse(7,7,6));
        System.out.println(getHorseDp(7,7,6));
        //走棋
        System.out.println(dpT1(8,3,6,5));
        System.out.println(dpTDp(8,3,6,5));
        System.out.println("------------------------------------");
        //money
        System.out.println(dpMoneyTime(new int[]{3,5,10},1600));
        System.out.println(domoneyProcess(new int[]{3,5,10},0,1600));
        System.out.println(dpMoneyMethod(new int[]{3,5,10},1600));
        System.out.println(dpMoneyMethod1(new int[]{3,5,10},1600));
        //thread
        System.out.println(4257218560L/1024/1024);
        //infected

    }

    /**
     * 亦或和为0的最优解法
     * @param arr  3,2,1,4,0,1,2,3,-1
     * @return
     */
    public static int orSum(int[] arr){
        //存储前缀信息
        Map<Integer,Integer> map = new HashMap<>(16);
        //根据相同前缀信息，求出前一个分割出0的位置的最大值
        int[] dp = new int[arr.length];
        int numOr = 0;map.put(0,-1);
        for (int i = 0; i < arr.length; i++) {
            numOr ^= arr[i];
            if(map.containsKey(numOr)){
                dp[i] = map.get(numOr) == -1 ? 1 : dp[map.get(numOr)] + 1;
            }
            if(i > 0){
                dp[i] = Math.max(dp[i - 1],dp[i]);
            }
            map.put(numOr,i);
        }
        return dp[arr.length - 1];
    }

    /**
     * 桶计数
     * @param arr
     * @return
     */
    public static int maxGap(int[] arr){
        //记录最终结果
        int result = 0;
        int min = Integer.MAX_VALUE;int max = Integer.MIN_VALUE;
        for (int i:arr) {
            max = Math.max(max,i);
            min = Math.min(min,i);
        }//将n个数放入n+1个桶
        if(max == min){return 0;}
        int[] minbucket = new int[arr.length + 1];
        int[] maxbucket = new int[arr.length + 1];
        boolean[] isbucket = new boolean[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            //计算每个数字应该放在哪个桶
            int index = (arr[i] - min) * arr.length / (max - min);
            minbucket[index] = isbucket[index] ? Math.min(minbucket[index], arr[i]) : arr[i];
            maxbucket[index] = isbucket[index] ? Math.max(maxbucket[index], arr[i]) : arr[i];
            isbucket[index] = true;
        }
        //遍历
        max = maxbucket[0];
        for (int i = 1; i < arr.length + 1; i++) {
            if(isbucket[i]){
                result = Math.max(minbucket[i] - max,result);
                max = maxbucket[i];
            }
        }
        return result;
    }

    public static int getInfectNum(int[][] data){
        int infectNum = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++) {
                if(data[i][j] == 1){
                    infectNum++;
                    infect(data,i,j);
                }
            }
        }
        return infectNum;
    }
    private static void infect(int[][] ints,int x,int y){
        if(x < 0 || y < 0 || x >= ints.length || y >= ints[0].length || ints[x][y] != 1){
            return;
        }
        ints[x][y] = 2;
        infect(ints,x + 1 ,y);
        infect(ints,x - 1, y);
        infect(ints,x - 1, y + 1);
        infect(ints,x, y - 1);
    }

    private static int domoneyProcess(int[] arr,int index,int num){
        if(index == arr.length){
            return num == 0 ? 1 : 0;
        }
        int way = 0;
        for (int i = 0; arr[index] * i <= num; i++) {
            way += domoneyProcess(arr,index + 1, num - arr[index] * i);
        }
        return way;
    }

    private static int dpMoneyMethod(int[] arr,int surplus){
        int[][] dp = new int[arr.length + 1][surplus + 1];
        dp[arr.length][0] = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j <= surplus; j++) {
                int way = 0;
                for (int h = 0; arr[i] * h <= j; h++) {
                    way += dp[i + 1][j - arr[i] * h];
                }
                dp[i][j] = way;
            }
        }
        return dp[0][surplus];
    }


    private static int dpMoneyMethod1(int[] arr,int surplus){
        int[][] dp = new int[arr.length + 1][surplus + 1];
        dp[arr.length][0] = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j <= surplus; j++) {
                dp[i][j] = dp[i + 1][j];
                if(j - arr[i] >= 0) {
                    dp[i][j] += dp[i][j - arr[i]];
                }
            }
        }
        return dp[0][surplus];
    }

    private static int dpMoneyTime(int[] money,int surplus){
        int[] dp = new int[surplus + 1];
        for (int i = 0; i <= surplus; i++) {
            dp[i] = i % money[0] == 0 ? 1 : 0;
        }
        for (int j = 1; j < money.length; j++) {
            for (int i = 1; i <= surplus; i++) {
                if(i - money[j] >= 0) {
                    dp[i] += dp[i - money[j]];
                }
            }
        }

        return dp[surplus];
    }

    private static int dpT1(int N,int m,int p,int step){
        if(m < 0 || m > N){
            return 0;
        }
        if(step == 0){
            return m == p ? 1 : 0;
        }
        return dpT1(N,m-1,p,step - 1)+dpT1(N, m+1, p ,step - 1);
    }

    private static int dpTDp(int N,int m,int p,int step){
        int[][] dp = new int[step+1][N+1];
        dp[0][p] = 1;
        for (int i = 1; i < step+1; i++) {
            for (int j = 0; j < N + 1; j++) {
                dp[i][j] = getInt(dp,i-1,j-1,N) + getInt(dp,i-1,j+1,N);
            }
        }
        return dp[step][m];
    }

    private static int getInt(int[][] dp,int i,int index,int N){
        if(index < 0 || index > N){
            return 0;
        }
        return dp[i][index];
    }



    private static int getHorse(int i,int j, int step){
        if(i < 0 || j < 0 || i > 8 || j > 9 || step < 0){
            return 0;
        }
        if(step == 0){
            return (i == 0 && j == 0) ? 1 : 0;
        }
        return  getHorse( i + 1, j + 2, step - 1) +
                getHorse( i + 2, j + 1, step - 1) +
                getHorse( i + 2, j - 1, step - 1) +
                getHorse( i + 1, j - 2, step - 1) +
                getHorse( i - 2, j - 1, step - 1) +
                getHorse( i - 1, j - 2, step - 1) +
                getHorse( i - 2, j + 1, step - 1) +
                getHorse( i - 1, j + 2, step - 1);
    }


    public static int getHorseDp(int i,int j,int step){
        int[][][] dp = new int[9][10][step + 1];
        dp[0][0][0] = 1;
        for (int k = 1; k <= step; k++) {
            for (int l = 0; l < 9; l++) {
                for (int m = 0; m < 10; m++) {
                    dp[l][m][k] = dp[l][m][k] +(
                            getValue(dp,l + 1,m + 2,k - 1) +
                            getValue(dp,l + 2,m + 1,k - 1) +
                            getValue(dp,l + 2,m - 1,k - 1) +
                            getValue(dp,l + 1,m - 2,k - 1) +
                            getValue(dp,l - 2,m - 1,k - 1) +
                            getValue(dp,l - 1,m - 2,k - 1) +
                            getValue(dp,l - 2,m + 1,k - 1) +
                            getValue(dp,l - 1,m + 2,k - 1));
                }
            }
        }
        return dp[i][j][step];
    }

    private static int getValue(int[][][] dp,int i,int j,int step){
        if(i < 0 || j < 0 || i > 8 || j > 9){
            return 0;
        }
        return dp[i][j][step];
    }

    /**
     * 获胜
     *
     * @return
     */
    private static int getWin2(int[] arr) {
        int[][] dpf = new int[arr.length][arr.length];
        int[][] dpl = new int[arr.length][arr.length];
        for (int i = 0; i < arr.length - 1; i++) {
            dpf[i][i] = arr[i];
        }
        int row = 0;
        int col = 1;
        while (col < arr.length) {
            int i = row;
            int j = col;
            while (i < arr.length && j < arr.length) {
                dpf[i][j] = Math.max(dpl[i + 1][j] + arr[i], dpl[i][j - 1] + arr[j]);
                dpl[i][j] = Math.min(dpf[i + 1][j], dpf[i][j - 1]);
                i++;
                j++;
            }
            col++;
        }
        return Math.max(dpf[0][arr.length - 1], dpl[0][arr.length - 1]);
    }

    private static int getWin(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int max = Math.max(getFirst(arr, 0, arr.length - 1), getLast(arr, 0, arr.length - 1));
        return max;
    }

    private static int getFirst(int[] arr, int i, int j) {
        if (i == j) {
            return arr[i];
        }
        return Math.max(arr[i] + getLast(arr, i + 1, j), arr[j] + getLast(arr, i, j - 1));
    }

    private static int getLast(int[] arr, int i, int j) {
        if (i == j) {
            return 0;
        }
        return Math.min(getFirst(arr, i + 1, j), getFirst(arr, i, j - 1));
    }

}
