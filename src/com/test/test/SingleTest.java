package com.test.test;


import com.test.models.single.LazySingle;
import com.test.models.single.ThreadLocalSingle;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 11:29
 * @description
 */
public class SingleTest {

    public static void main(String[] args) {
//        MyThread.executorService.execute(()->{
//            System.out.println("之心");
//            Object bean = SingleLemon.getBean("StaticSingle");
//            System.out.println(ThreadLocalSingle.getInstance());
//            System.out.println(bean);
//        });
//        MyThread.executorService.execute(()->{
//            System.out.println("之心1");
//            Object bean = SingleLemon.getBean("StaticSingle");
//            System.out.println(ThreadLocalSingle.getInstance());
//            System.out.println(bean);
//        });
//        MyThread.executorService.execute(()->{
//            System.out.println("之心2");
//            Object bean = SingleLemon.getBean("StaticSingle");
//
//            System.out.println(ThreadLocalSingle.getInstance());
//            System.out.println(bean);
//        });
        System.out.println(ThreadLocalSingle.getInstance());

        new Thread(()->{
            LazySingle lazySingle = LazySingle.getInstance();
            System.out.println(Thread.currentThread().getName()+ lazySingle);
        }).start();
        new Thread(()->{
            LazySingle lazySingle = LazySingle.getInstance();
            System.out.println(Thread.currentThread().getName()+ lazySingle);
        }).start();

        System.out.println("end");
    }
}
