package com.test.classes.myclassloader;

import java.lang.ClassLoader;
import java.nio.file.Path;

/**
 * @author 25338
 * @date 2022/5/14 10:44
 * @since 1.0
 * description
 */
public class DestroyDumplicated extends MyClassLoader{
    public DestroyDumplicated(String path) {
        super(path);
    }

    public DestroyDumplicated(ClassLoader parent, String path) {
        super(parent, path);
    }

    @SuppressWarnings("uncheck")
    @Override
    public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        synchronized (getClassLoadingLock(name)) {
            // 1.查询缓存
            Class<?> loadedClass = findLoadedClass(name);
            if (loadedClass == null) {
                // 2.系统类通过系统加载器加载
                if (name.startsWith("java.") || name.startsWith("javax.")) {
                    try {
                        loadedClass = getSystemClassLoader().loadClass(name);
                    } catch (Exception e) {
                        // to_do out
                    }
                } else {
                    // 3.自定义加载器加载
                    try {
                        loadedClass = findClass(name);
                    } catch (Exception e) {
                        // to_do out
                    }
                    // 4.如果还未加载到，则通过父类加载器或系统加载器加载
                    if (loadedClass == null) {
                        if (getParent() != null) {
                            loadedClass = getParent().loadClass(name);
                        } else {
                            loadedClass = getSystemClassLoader().loadClass(name);
                        }
                    }
                }
            }
            // 如果还未加载到，则抛出异常
            if (loadedClass == null) {
                throw new ClassNotFoundException(name + " class is not found");
            }
            if (resolve) {
                resolveClass(loadedClass);
            }
            return loadedClass;
        }
    }
}
