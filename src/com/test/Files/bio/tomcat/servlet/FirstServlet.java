package com.test.Files.bio.tomcat.servlet;

import com.test.Files.bio.tomcat.http.LwRequest;
import com.test.Files.bio.tomcat.http.LwResponse;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 22:27
 * @description
 */
public class FirstServlet extends LwServlet {
    @Override
    protected void doGet(LwRequest request, LwResponse response) throws Exception {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(LwRequest request, LwResponse response) throws Exception {
        response.write("this.is my first Method");
    }
}
