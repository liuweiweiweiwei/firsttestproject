package com.test.models.template.gp.template.course;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 13:55
 * @description
 */
public class NetworkTest {

    public static void main(String[] args) {
        System.out.println("课程：");
        NetWorkCourse workCourse = new JavaCourse();
        workCourse.createCourse();
        System.out.println("------next-----");
        NetWorkCourse bigCourse = new BigDataCourse(true);
        bigCourse.createCourse();
    }

}
