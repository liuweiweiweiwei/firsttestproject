package com.test.autimatic.type;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/9 11:17
 * @description
 */
public class Kmp {
    public static void main(String[] args) {
        System.out.println("KMP:" + rotateString("abcde", "cdeab"));
    }

    /**
     * 循环匹配
     *
     * @param s s
     * @param goal goal
     * @return true、false
     */
    public static boolean rotateString(String s, String goal) {
        if(s.length() != goal.length()) {return false;}
        int[][] kmp = kmp(goal);
        int start = 0;
        s += s;
        for (int i = 0; i < s.length(); i++) {
            start = kmp[start][s.charAt(i) - 'a'];
            if (start == goal.length()) {
                return true;
            }
        }
        return false;
    }

    /**
     * kmp
     *
     * @param target t
     * @return dp
     */
    private static int[][] kmp(String target) {
        int[][] dp = new int[target.length()][26];
        int shadow = 0;
        dp[0][target.charAt(0) - 'a'] = 1;
        for (int i = 1; i < target.length(); i++) {
            System.arraycopy(dp[shadow], 0, dp[i], 0, 26);
            shadow = dp[i][target.charAt(shadow) - 'a'];
            dp[i][target.charAt(i) - 'a'] = i + 1;
        }
        return dp;
    }
}
