package com.test.MyLog;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/23 18:23
 * @description
 */
public class LwLog {
    /**
     * pring split --------- + input + ----------
     * @param signal input
     */
    public static void splitLogInfo(String signal) {
        System.out.println("---------" + signal + "-----------");
    }

    /**
     * print input info
     * @param signal input
     */
    public static void logInfo(String signal) {
        System.out.println(signal);
    }

    /**
     * print input error
     * @param error input
     */
    public static void errorInfo(String error) {
        System.err.println(error);
    }

    /**
     * serr info
     * @param err
     */
    public static void outErr(String err) {
        System.err.println(err);
    }
}
