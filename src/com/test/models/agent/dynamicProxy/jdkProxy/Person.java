package com.test.models.agent.dynamicProxy.jdkProxy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:03
 * @description
 */
public interface Person {

    void findLove();
}
