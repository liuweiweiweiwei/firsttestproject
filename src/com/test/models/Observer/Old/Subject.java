package com.test.models.Observer.Old;


import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/10 15:30
 * 目标
 */
public class Subject {
    public static void main(String[] args) {
        //注册了
        Info info=new Info.InfoFactory().setData("第一个操作者").setDate("20210610").build();
        SubjectA subjectA=new SubjectInfo();
        subjectA.add(new ObserverPerson());
        subjectA.notifies(info);
    }

}

abstract class SubjectA {
    protected List<Observer> observerList;
    public SubjectA(){
        observerList=new ArrayList<>();
    }
    abstract void add(Observer observer);
    abstract void remove(Observer observer);
    public abstract void notifies(Info info);
}
class SubjectInfo extends SubjectA{

    @Override
    void add(Observer observer) {
        observerList.add(observer);
    }

    @Override
    void remove(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifies(Info info) {
        for (Object o:observerList) {
            ((Observer)o).EmailNotify(info);
        }
    }
}
/**
 * 数据类
 */
class Info{
    private String data;
    private String date;
    public Info(InfoFactory factory){
        this.data=factory.data;
        this.date=factory.date;
    }
    public Info() { }
    public String getData() { return data; }
    public void setData(String data) { this.data = data; }
    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }

    static class InfoFactory{
        private String data;
        private String date;

        public InfoFactory() { }
        public String getData() { return data; }
        public InfoFactory setData(String data) { this.data = data; return this;}
        public String getDate() { return date; }
        public InfoFactory setDate(String date) { this.date = date; return this;}
        public Info build(){ return new Info(this); }
    }
}

/**
 * 监听者
 */
interface Observer{
    void EmailNotify(Info info);
}

class ObserverPerson implements Observer{

    @Override
    public void EmailNotify(Info info) {
        //处理反应事件
        System.out.println("start send info cloud："+info.getData()+";时间是："+info.getDate());
    }
}