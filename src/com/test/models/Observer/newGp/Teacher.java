package com.test.models.Observer.newGp;

import java.util.Observable;
import java.util.Observer;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 14:29
 * @description
 */
public class Teacher implements Observer {

    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        Question arg1 = (Question) arg;
        LWer o1 = (LWer) o;
        System.out.println("--------------------------------------");
        System.out.println(this.name + "，你好!\n您收到了一个来自<<" + o1.getName() +">>的提问：希望你回答：问题是《" + arg1.getContent()+"》;"+
        "提问者是：" + arg1.getName());
    }
}
