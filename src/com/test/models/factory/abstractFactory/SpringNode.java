package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:28
 * @description
 */
public class SpringNode implements INode {
    @Override
    public void operateNode() {
        System.out.println("spring 的笔记操作！");
    }
}
