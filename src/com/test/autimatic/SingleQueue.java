package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/18 9:45
 * @description 单调队列
 */
public class SingleQueue {

    public static void main(String[] args) {
        SingleQueue queue = new SingleQueue();
        List<Integer> list = Arrays.asList(1,3,-1,-3,5);
        System.out.println(queue.queue(list,3));
    }

    /**
     * 遍历
     * @param list
     * @return
     */
    public List<Integer> queue(List<Integer> list,int k){
        List<Integer> end = new ArrayList<>();
        QueueMY<Integer> queueMY = new QueueMY<>();
        for (int i = 0; i < list.size(); i++) {
            if(i<k){
                queueMY.push(list.get(i));
                end.add(queueMY.max());
            }else{
                queueMY.push(list.get(i));
                end.add(queueMY.max());
                queueMY.pop(list.get(i-k+1));
            }
        }
        return end;
    }


    /**
     *  1 3 -1 -3 5
     * @param <T>
     */
    public static class QueueMY<T extends Comparable>{
        private Deque<T> queue;//数据队列

        public QueueMY() {
            this.queue = new LinkedList<>();
        }

        /**
         * 添加元素-队尾
         */
        public void push(T t){
            while (!queue.isEmpty() && queue.peekLast().compareTo(t)<0){
                queue.pollLast();
            }
            queue.addLast(t);
        }

        /**
         * 最大值元素
         * @return
         */
        public T max(){
            return queue.peekFirst();
        }

        /**
         * 删除元素-队尾azz
         */
        public T pop(T t){
            if(!queue.isEmpty()&&queue.peekLast().compareTo(t)==0) {
                return queue.pollLast();
            }else{
                return null;
            }
        }
    }

}
