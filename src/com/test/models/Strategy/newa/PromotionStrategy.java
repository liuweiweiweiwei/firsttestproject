package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:00
 * @description 策略的抽象
 */
public interface PromotionStrategy {
    /**
     * 优惠策略
     */
    void doPromotion();
}
