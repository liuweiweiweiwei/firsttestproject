package com.test.thread.old;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/26 16:31
 * @description
 */
public class MyThread {

    public static ExecutorService executorService = new ThreadPoolExecutor(5,5,20,TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(10),new ThreadPoolExecutor.AbortPolicy());


    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition condition = reentrantLock.newCondition();
        for (int i = 0; i < 2; i++) {
            if(i==0){
                new Thread(new ThreadOne(condition,reentrantLock)).start();
            }else{
                new Thread(new ThreadTwe(condition,reentrantLock)).start();
            }
        }
    }

    /**
     * 第一个线程
     */
    public static class ThreadOne implements Runnable {

        private Condition condition;
        private ReentrantLock reentrantLock;

        public ThreadOne(Condition condition) {
            this.condition = condition;
        }

        public ThreadOne(Condition condition, ReentrantLock reentrantLock) {
            this.condition = condition;
            this.reentrantLock = reentrantLock;
        }

        @Override
        public void run() {
            System.out.println("线程一开始执行......");
            try {
                reentrantLock.lock();
                condition.await();
                for (int i = 0; i < 50; i++) {
                    System.out.println(Thread.currentThread().getName() + ":" + Thread.currentThread().getId() + "运行-》" + i);
                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("线程一执行完毕......");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    /**
     * 第二个线程
     */
    public static class ThreadTwe implements Runnable {
        private Condition condition;
        private ReentrantLock reentrantLock;

        public ThreadTwe(Condition condition) {
            this.condition = condition;
        }

        public ThreadTwe(Condition condition, ReentrantLock reentrantLock) {
            this.condition = condition;
            this.reentrantLock = reentrantLock;
        }

        @Override
        public void run() {
            System.out.println("线程二开始执行......");
            try {
                reentrantLock.lock();
                for (int i = 0; i < 6; i++) {
                    System.out.println(Thread.currentThread().getName() + ":" + Thread.currentThread().getId() + "运行-》" + i);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                condition.signal();
                System.out.println("线程二执行完毕......");
            } finally {
                reentrantLock.unlock();
            }
        }
    }
}
