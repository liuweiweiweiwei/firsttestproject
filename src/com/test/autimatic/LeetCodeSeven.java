package com.test.autimatic;

import java.time.LocalDate;
import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 9:00
 * @description
 */
public class LeetCodeSeven {
    public static void main(String[] args) {
        //leetcode - 1
        System.out.println(isNStraightHand(new int[]{1, 2, 2, 2, 3, 3, 3, 4, 4}, 3));
        //leetcode - 2
        System.out.println(checkPerfectNumber(2));
        //leetcode - 3
        System.out.println(dayOfTheWeek(1, 1, 1971));
        //leetcode-4
        System.out.println(modifyString("?"));
        //leetcode - 5
        System.out.println(grayCode(3));
        //leetcode - 6
        System.out.println(slowestKey(new int[]{9, 29, 49, 50}, "cbcd"));
        // leetcode -7
        System.out.println(kSmallestPairs(new int[]{1, 1, 2}, new int[]{1, 2, 3}, 10));
        //leetcode - 8
        System.out.println(totalMoney(10));
        //
        System.out.println(new Random().nextInt(10));
        //leetcode - 9;
        System.out.println(countVowelPermutation(3));
        //leetcode - 10
        System.out.println(lengthOfLongestSubstring(" "));
        //
        System.out.println(removePalindromeSub("babababbababab"));
        //
        StockPrice price = new StockPrice();
        price.update(1,2);
        price.update(1,1);
        price.update(3,0);
        System.out.println(price.current());
        System.out.println(price.minimum());
        System.out.println(price.maximum());
        price.update(1,3);
        price.update(2,5);
        System.out.println(price.current());
        System.out.println(price.minimum());
        System.out.println(price.maximum());
        System.out.println("------------------------");
        System.out.println(countValidWords("he bought 2 pencils, 3 erasers, and 1  pencil-sharpener."));
        //leetcode   1,1},{2,1},{2,2},{1,2 ---1,5},{10,4},{4,3
        System.out.println(numberOfWeakCharacters(new int[][]{{7,9},{10,7},{6,9},{10,4},{7,5},{7,10}}));
        System.out.println(numberOfSteps(14));
        System.out.println(longestNiceSubstring("YazaAay"));
        System.out.println(reversePrefix("abcdefd",
                'd'));
    }

    public static int countGoodRectangles(int[][] rectangles) {
        int maxLen = -1;
        int num = 0;
        for (int[] temp : rectangles) {
            int tempMax = Math.min(temp[0],temp[1]);
            if(maxLen < tempMax){
                num = 1;
                maxLen = tempMax;
            }else if(maxLen == tempMax){
                num++;
            }
        }
        return num;
    }

    public static int numberOfMatches(int n) {
        int result = 0;
        while (n != 1){
            result += (n / 2);
            n = n % 2 == 0 ? n / 2 : n / 2 + 1;
        }
        return result;
    }

    public static String reversePrefix(String word, char ch) {
        char[] chars = word.toCharArray();
        int index = 0;
        boolean judge = false;
        for (char c : chars) {
            if(c == ch){ judge = true;break; }
            index++;
        }
        if(!judge){ return word; }
        for (int i = 0,j = index; i <= index / 2; i++,j--) {
            char temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
        }
        return new String(chars);
    }

    public static String longestNiceSubstring(String s) {
        int start = 0;
        int end = 0;
        for (int i = 0; i < s.length(); i++) {
            int l = 0;
            int r = 0;
            for (int j = i; j < s.length(); j++) {
                if('a' <= s.charAt(j) && s.charAt(j) <= 'z'){
                    l |= 1 << (s.charAt(j) - 'a');
                }else{
                    r |= 1 << (s.charAt(j) - 'A');
                }
                if(l == r && (end - start) < (j - i)){
                    start = i;
                    end = j;
                }
            }
        }
        if(end - start > 0){
            return s.substring(start,end + 1);
        }
        return "";
    }

    public static int numberOfSteps(int num) {
        int step = 0;
        while (num != 0){
            num = num % 2 == 0 ? num >> 1 : num ^ 1;
            step++;
        }
        return step;
    }

    public static int numberOfWeakCharacters(int[][] properties) {
        Arrays.sort(properties, (o1, o2) -> {
            if(o1[0] != o2[0]){
                return o1[0] - o2[0];
            }
            return o1[1] - o2[1];
        });
        int num = 0;
        int maxY = 0;
        for (int[] temp : properties) {
            if(temp[1] < maxY){
                num++;
            }else{
                maxY = temp[1];
            }
        }
        return num;
    }

    public static int countValidWords(String sentence) {
        if("!".equals(sentence) ||",".equals(sentence) ||".".equals(sentence)){
            return 1;
        }
        int num = 0;
        for (String temp : sentence.split(" ")) {
            if(!"".equals(temp)&&isSave(temp)){
                num++;
            }
        }
        return num;
    }

    private static boolean isSave(String s){
        int index = 0;
        s = s.trim();
        boolean start = false;
        for (int i = 0; i < s.length() - 1; i++) {
            char c = s.charAt(i);
            if(c == ' '){
                continue;
            }
            if(!start){
                if(c > 'z' || c < 'a'){
                    return false;
                }
                start = true;
            }
            if(c == '-' && ++index >= 2){
                return false;
            }
            if( (c > 'z' || c < 'a') && (c != '-')){
                return false;
            }
        }
        return (s.charAt(s.length() - 1) >= 'a' && s.charAt(s.length() - 1) <= 'z')
                || s.charAt(s.length() - 1) == '!' ||s.charAt(s.length() - 1) == ','
                || s.charAt(s.length() - 1) == '.' ;
    }

    static class DetectSquares {
        Map<Integer,Map<Integer,Integer>> mapMap;

        public DetectSquares() {
            mapMap = new HashMap<>(16);
        }

        public void add(int[] point) {
            Map<Integer, Integer> map = mapMap.get(point[1]);
            if(map == null){
                map = new HashMap<>(16);
            }
            map.put(point[0], map.getOrDefault(point[0],0) + 1);
            mapMap.put(point[1],map);
        }

        public int count(int[] point) {
            if (!mapMap.containsKey(point[1])) { return 0; }
            int count = 0;
            Map<Integer, Integer> map = mapMap.get(point[1]);
            Set<Map.Entry<Integer, Map<Integer, Integer>>> entries = mapMap.entrySet();
            for (Map.Entry<Integer,Map<Integer, Integer>> m : entries) {
                Integer key = m.getKey();
                Map<Integer, Integer> value = m.getValue();
                if(point[1] != key) {
                    int d = key - point[1];
                    count += value.getOrDefault(point[0],0) * value.getOrDefault(point[0] + d,0)
                            * map.getOrDefault(point[0] + d,0);
                    count += value.getOrDefault(point[0],0) * value.getOrDefault(point[0] - d,0)
                            * map.getOrDefault(point[0] - d,0);
                }
            }
            return count;
        }
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode end = new ListNode();
        ListNode temp = end;
        int tempInt = 0;
        while (l1 != null || l2 != null || tempInt == 1){
            int val = 0;
            if(l1 != null){
                val += l1.val;
                l1 = l1.next;
            }
            if(l2 != null){
                val += l2.val;
                l2 = l2.next;
            }
            if(tempInt == 1){
                val += tempInt;
            }
            temp.next = new ListNode(val > 9 ? val - 10 : val);
            tempInt = val > 9 ? 1 : 0;
            temp = temp.next;
        }
        return end.next;
    }

    static class StockPrice {

        TreeMap<Integer,Integer> newTime;
        TreeMap<Integer,Set<Integer>> newPrice;

        public StockPrice() {
            newTime = new TreeMap<>(Comparator.comparingInt(o -> o));
            newPrice = new TreeMap<>(Integer::compareTo);
        }

        public void update(int timestamp, int price) {
            Set<Integer> setVal = new HashSet<>(16);
            Integer priceOld = newTime.get(timestamp);
            if(priceOld != null){
                Set<Integer> tempVal = newPrice.get(priceOld);
                if(tempVal != null){
                    tempVal.remove(timestamp);
                }
                if(newPrice.get(priceOld).isEmpty()){
                    newPrice.remove(priceOld);
                }
            }
            if(newPrice.get(price) != null) {
                setVal = newPrice.get(price);
            }
            setVal.add(timestamp);
            newPrice.put(price,setVal);
            newTime.put(timestamp, price);
        }

        public int current() {
            return newTime.get(newTime.lastKey());
        }

        public int maximum() {
            return newPrice.lastKey();
        }

        public int minimum() {
            return newPrice.firstKey();
        }
    }

    public static int removePalindromeSub(String s) {
        for (int i = 0, j = s.length() - 1; i < s.length() / 2; i++,j--) {
            if(s.charAt(i) != s.charAt(j)){
                return 2;
            }
        }
        return 1;
    }

    private static int get(String s){
        StringBuilder builder = new StringBuilder();
        for (char c:s.toCharArray()) {
            builder.append("#").append(c);
        }
        builder.append("#");
        s = builder.toString();
        int start = 0;int end = 0;int mid = 0;int len = 0;
        List<Integer> temp = new ArrayList<>();
        temp.add(0);
        for (int i = 1; i < s.length(); i++) {
            int templen = 0;
            if(i <= len){
                templen = Math.min(temp.get(2 * mid - i), len - i);
            }
            while (i + templen + 1 < s.length() && i - templen - 1 >= 0
            &&s.charAt(i + templen + 1) == s.charAt(i - templen - 1)){templen++;}
            temp.add(templen);
            if(i + templen > len){
                len = i + templen;
                mid = i;
            }
            if(end - start < 2 * templen){
                start = i - templen;
                end = i + templen;
            }
        }
        if(end - start + 1 == s.length()){
            return 1;
        }
        return 2;
    }

    public static int lengthOfLongestSubstring(String s) {
        if(s.length() == 0){return 0;}
        final char temp = '0';
        Set<Integer> set = new HashSet<>(16);
        int max = 0;
        for (int i = 0, j = 0; j < s.length(); i++) {
            while (j < s.length() && !set.contains(s.charAt(j) - temp)){
                set.add(s.charAt(j) - temp);
                j++;
            }
            max = Math.max(j - i, max);
            set.remove(s.charAt(i) - temp);
        }
        return max;
    }

    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer,Integer> indexMap = new HashMap<>(16);
        for (int i = 0; i < nums.length; i++) {
            Integer start = indexMap.get(nums[i]);
            if(start != null && i - start <= k){
                return true;
            }
            indexMap.put(nums[i], i);
        }
        return false;
    }

    private static int countVowelPermutation(int n) {
        int mod = 1000000007;
//        long[][] dp = new long[n + 1][5];
//        // 计算初始值dp[0][] 可定都为因为没有长度，dp[1][]一个长度的字符串，以a e i o u 结尾所得到的数量就是1个
//        for (int i = 0; i < 5; i++) {
//            dp[1][i] = 1;
//        }
//        for (int i = 2; i <= n; i++) {
//            dp[i][0] = (dp[i - 1][4] + dp[i - 1][1] + dp[i - 1][2]) % mod; // a的数量
//            dp[i][1] = (dp[i - 1][0] + dp[i - 1][2]) % mod; // e的数量
//            dp[i][2] = (dp[i - 1][1] + dp[i - 1][3]) % mod; // i的数量
//            dp[i][3] = (dp[i - 1][2]) % mod; // o的数量
//            dp[i][4] = (dp[i - 1][2] + dp[i - 1][3]) % mod; // u的数量
//        }
//        return (int)((dp[n][0] + dp[n][1] + dp[n][2] + dp[n][3] + dp[n][4]) % mod);
        // 因为状态只和前一个数字有关联
        int a = 1,e = 1,i = 1,o = 1,u = 1,ta,te,ti,to,tu;
        while (--n > 0) {
            ta = e + i + u;
            te = a + i;
            ti = e + o;
            to = i;
            tu = i + o;
            a = ta % mod;
            e = te % mod;
            i = ti % mod;
            o = to % mod;
            u = tu % mod;
        }
        return (a + e + i + o + u) % mod;
    }

    public static int kthSmallest(int[][] matrix, int k) {
//        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((o1, o2) -> o2 - o1);
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[0].length; j++) {
//                if(priorityQueue.size() == k){
//                    Integer poll = priorityQueue.poll();
//                    priorityQueue.offer(Math.min(poll,matrix[i][j]));
//                }else{
//                    priorityQueue.offer(matrix[i][j]);
//                }
//            }
//        }
//        return priorityQueue.poll();
        PriorityQueue<int[]> priorityQueue = new PriorityQueue<>(Comparator.comparingInt(o -> matrix[o[0]][o[1]]));
        for (int i = 0; i < matrix.length; i++) {
            priorityQueue.add(new int[]{i,0});
        }
        int[] poll = new int[0];
        while (k != 0){
            poll =  priorityQueue.poll();
            if(matrix[0].length > poll[1] + 1) {
                priorityQueue.add(new int[]{poll[0], poll[1] + 1});
            }
            k--;
        }
        return matrix[poll[0]][poll[1]];
    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() { }
        ListNode(int val) { this.val = val; }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    static class Solution {
        int len;
        ListNode listNode;
        public Solution(ListNode head) {
            len = 0;
            listNode = head;
            ListNode temp = head;
            while (temp != null){
                temp = temp.next;
                len++;
            }
        }

        public int getRandom() {
            int site = new Random().nextInt(len);
            int index = 0;
            ListNode sitNode = listNode;
            while (sitNode != null){
                if(index == site){
                    return sitNode.val;
                }
                sitNode = sitNode.next;
                index++;
            }
            return -1;
        }
    }

    public static int totalMoney(int n) {
        int mod = n / 7;
        int tem = n % 7;
        int num = (28 * mod) + (mod * (mod - 1)) / 2 * 7;
        for (int i = mod + 1; tem != 0 && i < tem + mod + 1; i++) {
            num += i;
        }
        return num;
    }

    public static List<List<Integer>> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        if (k == 0 || nums1.length == 0 || nums2.length == 0) {
            return Collections.emptyList();
        }
        List<List<Integer>> listList = new ArrayList<>(16);
        int maxX = Math.min(nums1.length, 200);
        int maxY = Math.min(nums2.length, 200);
        TreeMap<int[], List<Integer>> map = new TreeMap<>((o1, o2) -> {
            if (o1[0] != o2[0]) {
                return o1[0] - o2[0];
            }
            return o1[1] - o2[1];
        });
        int index = 0;
        for (int i = 0; i < maxX; i++) {
            for (int j = 0; j < maxY; j++) {
                map.put(new int[]{nums1[i] + nums2[j], index++}, Arrays.asList(nums1[i], nums2[j]));
            }
        }
        for (int[] e : map.keySet()) {
            if (listList.size() == k) {
                return listList;
            }
            listList.add(map.get(e));
        }
        return listList;
    }

    public static boolean increasingTriplet(int[] nums) {
        int first = nums[0];
        int second = Integer.MAX_VALUE;
        for (int third : nums) {
            if (third > second) {
                return true;
            } else if (third > first) {
                second = third;
            } else {
                first = third;
            }
        }
        return false;
    }

    public static char slowestKey(int[] releaseTimes, String keysPressed) {
        int maxCount = releaseTimes[0];
        char result = keysPressed.charAt(0);
        for (int i = 1; i < releaseTimes.length; i++) {
            if (maxCount < (releaseTimes[i] - releaseTimes[i - 1]) || (maxCount == (releaseTimes[i] - releaseTimes[i - 1]) && (keysPressed.charAt(i) > result))) {
                result = keysPressed.charAt(i);
                maxCount = releaseTimes[i] - releaseTimes[i - 1];
            }
        }
        return result;
    }

    public static List<Integer> grayCode(int n) {
        List<Integer> start = new ArrayList<>(16);
        start.add(0);
        List<Integer> end = new ArrayList<>(16);
        end.add(1);
        if (n > 1) {
            int head = 3;
            int tail = 1;
            while (n - 1 > 0) {
                List<Integer> temp = new ArrayList<>(16);
                List<Integer> tS = new ArrayList<>(16);
                temp.addAll(start);
                temp.addAll(end);
                for (int t : start) {
                    tS.add(t + head);
                }
                for (int t : end) {
                    tS.add(t + tail);
                }
                start = temp;
                end = tS;
                head <<= 1;
                tail <<= 1;
                n--;
            }
        }
        start.addAll(end);
        return start;
    }

    public static int maxDepth(String s) {
        int depth = 0;
        int max = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                depth++;
                max = Math.max(max, depth);
            } else if (c == ')') {
                depth--;
            }
        }
        return max;
    }

    public static String modifyString(String s) {
        char[] chars = s.toCharArray();
        if (chars.length == 1) {
            return "a";
        }
        for (int i = 0; i < s.length(); i++) {
            if (chars[i] != '?') {
                continue;
            }
            for (char j = 'a'; j <= 'z'; j++) {
                if (i == 0) {
                    if (j != s.charAt(i + 1)) {
                        chars[i] = j;
                    }
                } else {
                    char temp = i < chars.length - 1 && s.charAt(i + 1) != '?' ? s.charAt(i + 1) : 'A';
                    if (!(chars[i - 1] == j || temp == j)) {
                        chars[i] = j;
                        break;
                    }
                }
            }
        }
        return new String(chars);
    }

    static String[] week = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public static String dayOfTheWeek(int day, int month, int year) {
        return week[LocalDate.of(year, month, day).getDayOfWeek().getValue() - 1];
    }

    public static int lastRemaining(int n) {
        int time = n, start = 1, end = n, jiange = 1, forword = 1;
        //利用两重判断--第一重判断删除的顺序---第二重判断当前剩余数量为奇数还是偶数决定起始位置--最终start == end
        while (start != end) {
            if ((forword & 1) == 1) {
                start = start + jiange;
                end = ((time & 1) == 1) ? end - jiange : end;
            } else {
                start = ((time & 1) == 1) ? start + jiange : start;
                end = end - jiange;
            }
            forword++;
            jiange <<= 1;
            time >>= 1;
        }
        return start;
    }

    public static int[][] construct2DArray(int[] original, int m, int n) {
        if (original.length != (m * n)) {
            return new int[0][0];
        }
        int[][] result = new int[m][n];
//        for (int i = 0,j = 0; j < original.length; i++,j += n) {
//            System.arraycopy(original,j,result[i],0,n);
//        }
        for (int i = 0; i < original.length; i++) {
            result[i / n][i % n] = original[i];
        }
        return result;
    }


    public static boolean checkPerfectNumber(int num) {
        if (num == 1) {
            return false;
        }
        int numTemp = 1;
        for (int i = 2; i < num / i; i++) {
            if (num % i == 0) {
                numTemp += (i + num / i);
            }
        }
        return numTemp == num;
    }

    private static void checkPerfect(int num, Set<Integer> set) {
        for (int i = 2; i < num / 2; i++) {
            if (num % i == 0) {
                int num1 = num / i;
                HashSet<Integer> set1 = new HashSet<>(set);
                if (!set.isEmpty()) {
                    for (int im : set) {
                        set1.add(im * i);
                        set1.add(im * num1);
                    }
                }
                set1.add(i);
                set1.add(num1);
                set = set1;
                checkPerfect(num1, set);
                return;
            }
        }

    }

    public static boolean isNStraightHand(int[] hand, int groupSize) {
        if (groupSize == 1) {
            return true;
        }
        Arrays.sort(hand);
        if (hand.length % groupSize != 0
                || (hand.length == groupSize && hand[1] - hand[0] > 1)
                || hand[hand.length - 1] == hand[0]) {
            return false;
        }
        int len = hand.length / groupSize;
        int[] temp = new int[len];
        int[] num = new int[len];
        Arrays.fill(temp, -1);
        int tempIndex = 0;
        for (int i = 0, j = 0; i < hand.length; i++) {
            tempIndex = (i > 0 && tempIndex < len && hand[i - 1] == hand[i]) ? ++tempIndex : j;
            if (tempIndex >= len || (temp[tempIndex] != -1 && (hand[i] - temp[tempIndex]) != 1)) {
                return false;
            }
            temp[tempIndex] = hand[i];
            num[tempIndex]++;
            if (num[tempIndex] == groupSize) {
                j++;
            }
        }
        return true;
    }
}
