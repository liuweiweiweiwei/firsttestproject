package com.test.autimatic;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/20 15:51
 * @description
 */
public class Rancher {

    public static void main(String[] args) {
        System.out.println(getPalindrome("12212"));
        //
        System.out.println(getWindow(new int[]{3,6,4,2,3,7}));
    }

    /**
     * 回文
     */
    public static int getPalindrome(String arr){
        StringBuilder builder = new StringBuilder();
        for (char c:arr.toCharArray()) {
            builder.append(c).append("#");
        }
        arr = builder.toString().substring(0,builder.length()-1);
        List<Integer> list = new ArrayList<>();
        int mid = 0;int right = 0;list.add(0);int start = -1;int end = -1;
        for (int i = 1; i < arr.length(); i++) {
            int arm = 1;
            if(right - mid >= i){
                //分两种情况在前一段之内或者超过半径长度
                arm = Math.min(list.get(2 * mid - i),right - i) + 1;
            }
            while (i - arm >= 0 && i + arm < arr.length() && arr.charAt(i - arm) == arr.charAt(i + arm)){arm++;}
            arm--;
            list.add(arm);
            if(i + arm >= right){
                right = i + arm;
                mid = i;
            }
            if(end - start < 2 * arm + 1){
                start = i - arm;
                end = i + arm;
            }
        }
        return (end - start) / 2 + 1;
    }


    public static List<Integer> getWindow(int[] window){
        List<Integer> list = new ArrayList<>();
        Deque<Integer> deque = new ArrayDeque<>();
        for (int i = 0,r = 0; i < window.length; i++) {
            while (!deque.isEmpty() && window[deque.peekLast()] < window[i]){
                deque.pollLast();
            }
            deque.addLast(i);
            if(i - r >= 2){
                list.add(window[deque.getFirst()]);
                deque.remove(r++);
            }
        }
        return list;
    }


}
