package com.test.Task;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/7/30 15:35
 * @description
 */
public class TestDemo {
    private String name;
    private Integer age;
    private String content;
    private String time;

    @Override
    public String toString() {
        return "TestDemo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public TestDemo() {
    }

    public TestDemo(String name, Integer age, String content, String time) {
        this.name = name;
        this.age = age;
        this.content = content;
        this.time = time;
    }

    public  String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public static final String USER_NAME="root";
    public static final String PASSWORD="123456";
    public static final String URL="jdbc:mysql://localhost:3306/lw_log?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=true";

    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection= DriverManager.getConnection(URL,USER_NAME,PASSWORD);
        PreparedStatement preparedStatement=null;
        for (int i = 2; i <20000 ; i++) {
            System.out.println("start"+i);
            String sql="INSERT INTO `lw_log`.`lwlog_language`" +
                    "( `log_id`, `content`, `time`, `user_id`) " +
                    "VALUES ( ?, ?, ?, ?);";
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setInt(1,i);
            preparedStatement.setString(2, "打篮球"+i);
            preparedStatement.setInt(3,112345678+i);
            preparedStatement.setInt(4,i);
            preparedStatement.execute();
            //TimeUnit.MILLISECONDS.sleep(80);
        }
        preparedStatement.close();
        connection.close();
        System.out.println("结束");
    }
}
