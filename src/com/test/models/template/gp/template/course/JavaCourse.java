package com.test.models.template.gp.template.course;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 13:51
 * @description
 */
public class JavaCourse extends NetWorkCourse {


    public JavaCourse() {
    }

    @Override
    protected boolean work() {
        return super.work();
    }

    @Override
    protected void checkWork() {
        System.out.println("检查java课程的作业！");
    }
}
