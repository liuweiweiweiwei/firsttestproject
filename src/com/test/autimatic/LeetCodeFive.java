package com.test.autimatic;


import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/12/14 19:47
 * @description
 */
public class LeetCodeFive {

    public static void main(String[] args) {
        //第一个
        int[] firsArr={1,2,8,8,8,2,1};
        int first=candy(firsArr);
        System.out.println(first);
        //第二个
        int[] g = {1,2,3};
        int[] s = {1,1};
        int second=findContentChildren(g,s);
        System.out.println("第二个"+second);
        //第三个
        int[] three={9,10,1,7,3};
        int threeEnd=lastStoneWeight(three);
        System.out.println("第三个："+threeEnd);
        //第四个
        String four="dcab";
        List<List<Integer>> pairs=new ArrayList<>();
        pairs.add(Arrays.asList(0,3));
        pairs.add(Arrays.asList(1,2));
        pairs.add(Arrays.asList(4,5));
        //String end=smallestStringWithSwaps(four,pairs);
        //System.out.println(end);
        System.out.println("---------four---------");
        //第五个
        int[] t1Five={2,1,5};
        List<Integer> list=getArrThree(t1Five,806);
        System.out.println("第五题：");
        list.forEach(System.out::print);
        System.out.println();
        System.out.println("-----five-----");
        //leetcode
        System.out.println(findNthDigit(1000000000));
        System.out.println("------six------");
        //leetcode-7
        System.out.println(largestSumAfterKNegations(new int[]{2,-3,-1,5,-4},2));
        System.out.println("--------seven---------");
        //leetcode-8
        System.out.println(superPow(78267,new int[]{1,7,7,4,3,1,7,0,1,4,4,9,2,8,5,0,0,9,3,1,2,5,9,6,0,9,9,0,9,6,0,5,3,7,9,8,8,9,8,2,5,4,1,9,3,8,0,5,9,5,6,1,1,8,9,3,7,8,5,8,5,5,3,0,4,3,1,5,4,1,7,9,6,8,8,9,8,0,6,7,8,3,1,1,1,0,6,8,1,1,6,6,9,1,8,5,6,9,0,0,1,7,1,7,7,2,8,5,4,4,5,2,9,6,5,0,8,1,0,9,5,8,7,6,0,6,1,8,7,2,9,8,1,0,7,9,4,7,6,9,2,3,1,3,9,9,6,8,0,8,9,7,7,7,3,9,5,5,7,4,9,8,3,0,1,2,1,5,0,8,4,4,3,8,9,3,7,5,3,9,4,4,9,3,3,2,4,8,9,3,3,8,2,8,1,3,2,2,8,4,2,5,0,6,3,0,9,0,5,4,1,1,8,0,4,2,5,8,2,4,2,7,5,4,7,6,9,0,8,9,6,1,4,7,7,9,7,8,1,4,4,3,6,4,5,2,6,0,1,1,5,3,8,0,9,8,8,0,0,6,1,6,9,6,5,8,7,4,8,9,9,2,4,7,7,9,9,5,2,2,6,9,7,7,9,8,5,9,8,5,5,0,3,5,8,9,5,7,3,4,6,4,6,2,3,5,2,3,1,4,5,9,3,3,6,4,1,3,3,2,0,0,4,4,7,2,3,3,9,8,7,8,5,5,0,8,3,4,1,4,0,9,5,5,4,4,9,7,7,4,1,8,7,5,2,4,9,7,9,1,7,8,9,2,4,1,1,7,6,4,3,6,5,0,2,1,4,3,9,2,0,0,2,9,8,4,5,7,3,5,8,2,3,9,5,9,1,8,8,9,2,3,7,0,4,1,1,8,7,0,2,7,3,4,6,1,0,3,8,5,8,9,8,4,8,3,5,1,1,4,2,5,9,0,5,3,1,7,4,8,9,6,7,2,3,5,5,3,9,6,9,9,5,7,3,5,2,9,9,5,5,1,0,6,3,8,0,5,5,6,5,6,4,5,1,7,0,6,3,9,4,4,9,1,3,4,7,7,5,8,2,0,9,2,7,3,0,9,0,7,7,7,4,1,2,5,1,3,3,6,4,8,2,5,9,5,0,8,2,5,6,4,8,8,8,7,3,1,8,5,0,5,2,4,8,5,1,1,0,7,9,6,5,1,2,6,6,4,7,0,9,5,6,9,3,7,8,8,8,6,5,8,3,8,5,4,5,8,5,7,5,7,3,2,8,7,1,7,1,8,7,3,3,6,2,9,3,3,9,3,1,5,1,5,5,8,1,2,7,8,9,2,5,4,5,4,2,6,1,3,6,0,6,9,6,1,0,1,4,0,4,5,5,8,2,2,6,3,4,3,4,3,8,9,7,5,5,9,1,8,5,9,9,1,8,7,2,1,1,8,1,5,6,8,5,8,0,2,4,4,7,8,9,5,9,8,0,5,0,3,5,5,2,6,8,3,4,1,4,7,1,7,2,7,5,8,8,7,2,2,3,9,2,2,7,3,2,9,0,2,3,6,9,7,2,8,0,8,1,6,5,2,3,0,2,0,0,0,9,2,2,2,3,6,6,0,9,1,0,0,3,5,8,3,2,0,3,5,1,4,1,6,8,7,6,0,9,8,0,1,0,4,5,6,0,2,8,2,5,0,2,8,5,2,3,0,2,6,7,3,0,0,2,1,9,0,1,9,9,2,0,1,6,7,7,9,9,6,1,4,8,5,5,6,7,0,6,1,7,3,5,9,3,9,0,5,9,2,4,8,6,6,2,2,3,9,3,5,7,4,1,6,9,8,2,6,9,0,0,8,5,7,7,0,6,0,5,7,4,9,6,0,7,8,4,3,9,8,8,7,4,1,5,6,0,9,4,1,9,4,9,4,1,8,6,7,8,2,5,2,3,3,4,3,3,1,6,4,1,6,1,5,7,8,1,9,7,6,0,8,0,1,4,4,0,1,1,8,3,8,3,8,3,9,1,6,0,7,1,3,3,4,9,3,5,2,4,2,0,7,3,3,8,7,7,8,8,0,9,3,1,2,2,4,3,3,3,6,1,6,9,6,2,0,1,7,5,6,2,5,3,5,0,3,2,7,2,3,0,3,6,1,7,8,7,0,4,0,6,7,6,6,3,9,8,5,8,3,3,0,9,6,7,1,9,2,1,3,5,1,6,3,4,3,4,1,6,8,4,2,5}));
        //leetcode-9
        System.out.println(' ' - '0');
    }

    public static String truncateSentence(String s, int k) {
        int time = 0;
        StringBuilder result = new StringBuilder();
        for (char c:s.toCharArray()) {
            if(c == ' '){
                time++;
            }
            if(time == k){
                return result.toString();
            }

            result.append(c);
        }
        return s;
    }

    private static final int MOD = 1337;
    public static int superPow(int a, int[] b) {
        int result = 1;
        //由于b的位数不确定且比较大会溢出，所以选用递归
//        for (int i = b.length - 1,j = 1; i >= 0; i--,j *= 10) {
//            //计算每个10阶位置的次方
//            result *= quickPow(a,b[i] * j);
//            result %=  MOD;
//        }
        result = recursion(a,b,b.length - 1) ;
        return result % MOD;
    }

    private static int recursion(int a,int[] b,int index){
        if(index < 0){ return 1; }
        //递归操作
        return (quickPow(recursion(a,b,index - 1),10 ) % MOD) * (quickPow(a,b[index]) % MOD);
    }
    /**
     * 求a的b次幂
     * @param a 要平方的数据
     * @param time 平方的次数
     * @return 结果
     */
    private static int quickPow(int a,int time){
        //设置结果
        int end = 1;
        //如果次数不为0
        while (time > 0){
            //如果此次计算次数为奇数，则让结果乘以一次目标值
            if((time & 1) == 1){
                end = (end %= MOD) * (a % MOD);
            }
            //将目标进行自乘以缩短成方次数--例如：
            // 目标值为5 time为4---5的4次方可以变为 5*5 的2次方time以log2进行递减a以自乘增加
            a = (a % MOD) * (a % MOD);
            time >>= 1;
        }
        return end % MOD;
    }

    public static boolean canConstruct(String ransomNote, String magazine) {
        //转化为数组
        int[] arrayNote = toArray(ransomNote);
        int[] arrayZine = toArray(magazine);
        for (int i = 0; i < 26; i++) {
            //比较数目多少
            if(arrayNote[i] > arrayZine[i]){
                return false;
            }
        }
        return true;
    }

    private static int[] toArray(String target){
        int[] arr = new int[26];
        for (char c:target.toCharArray()) {
            arr[c - 'a']++;
        }
        return arr;
    }

    public static int largestSumAfterKNegations(int[] nums, int k) {
        int num = 0;
        //统计绝对值最小
        //1.如果k>num.length
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if(k > 0 && nums[i] < 0){
                nums[i] = -nums[i];
                k--;
            }
            num += nums[i];
        }
        Arrays.sort(nums);
        return k % 2 == 0 ? num : (num - 2 * nums[0]);
    }

    public static String[] findRelativeRanks(int[] score) {
        if(score.length==0){return new String[0];}
        String[] ranks = new String[score.length];
        int[][] sort = new int[score.length][2];
        for (int i = 0; i < score.length; i++) {
            sort[i][0] = score[i];
            sort[i][1] = i;
        }
        Arrays.sort(sort, (o1,o2) -> o2[0] - o1[0]);
        ranks[sort[0][1]] = "Gold Medal";
        if(score.length > 1){ranks[sort[1][1]] = "Silver Medal"; }
        if(score.length > 2){ranks[sort[2][1]] = "Bronze Medal"; }
        for (int i = 3; i < score.length; i++) {
            ranks[sort[i][1]] = String.valueOf(i + 1);
        }
        return ranks;
    }

    public static int maxPower(String s) {
        int power = 1;
        int time = 1;
        for (int i = 1; i < s.length(); i++) {
            if(s.charAt(i) == s.charAt(i - 1)){
                time++;
            }else{
                power = time > power ? time : power;
                time = 1;
            }
        }
        return power > time ? power : time;
    }

    public static int findNthDigit(int n) {
        if(n < 10)return n;
        //初始化加一是为了引入0方便统计
        //初始化几个变量index表示当前阶段每位数字个数tempData表示当前阶段起始数值
        //nowNum表示当前数字阶段范围
        int index = 1;
        int tempData = 0;
        long nowNum = 9;
        while (true){
            if(n < nowNum){//当n小于当前阶段数目时候既可以计算
                String t = String.valueOf(n % index == 0 ? tempData + n / index - 1 : (tempData + n / index));
                return n % index == 0 ? Integer.parseInt(String.valueOf(t.charAt(t.length()-1)))
                        : Integer.parseInt(String.valueOf(t.charAt(n % index - 1)));
            }else if(n == nowNum){//当n等于当前阶段数目时候一定是9
                return 9;
            }
            //减去当前数目，index位数增加，当前起始值增加*10，当前最大值变化
            n -= nowNum;
            index++;
            tempData = tempData == 0 ? 10 : tempData * 10;
            nowNum = (long) 9 * index * tempData;
        }
    }

    /**
     * 给定一个非负整数 N，找出小于或等于 N 的最大的整数，同时这个整数需要满足其各个位数上的数字是单调递增。
     （当且仅当每个相邻位数上的数字 x 和 y 满足 x <= y 时，我们称这个整数是单调递增的。）
     示例 1:
     输入: N = 10
     输出: 9
     示例 2:
     输入: N = 1234
     输出: 1234
     示例 3:
     输入: N = 332
     输出: 299
     说明: N 是在 [0, 10^9] 范围内的一个整数。
     */
    public static int monotoneIncreasingDigits(int N) {
        int end=0;

        return end;
    }

    /**
     * 老师想给孩子们分发糖果，有 N个孩子站成了一条直线，老师会根据每个孩子的表现，预先给他们评分。
     你需要按照以下要求，帮助老师给这些孩子分发糖果：
     每个孩子至少分配到 1 个糖果。
     相邻的孩子中，评分高的孩子必须获得更多的糖果。
     那么这样下来，老师至少需要准备多少颗糖果呢？
     示例 1:
     输入: [1,0,2]
     输出: 5
     解释: 你可以分别给这三个孩子分发 2、1、2 颗糖果。
     示例 2:
     输入: [1,2,2]
     输出: 4
     解释: 你可以分别给这三个孩子分发 1、2、1 颗糖果。
     第三个孩子只得到 1 颗糖果，这已满足上述两个条件。
     */
    public static int candy(int[] ratings) {
        int len=ratings.length;
        if(len<2){
            return 1;
        }
        int[] tem=new int[len];
        tem[0]=ratings[0]>ratings[1]?2:1;
        int end=0;
        for (int i = 1; i < len; i++) {
            if(ratings[i]>ratings[i-1]){
                tem[i]=tem[i-1]+1;
            }else{
                if(tem[i-1]==1) {
                    int temIndex=i-1;
                    while (temIndex>=1&&ratings[temIndex-1]>=ratings[temIndex]){
                        if(ratings[temIndex-1]==ratings[temIndex]){
                            tem[temIndex] = tem[temIndex-1]-1;
                        }else{
                            if(temIndex-1>=1&&ratings[temIndex-2]>=ratings[temIndex-1]) {
                                tem[temIndex - 1]++;
                            }else{
                                tem[temIndex - 1]=tem[temIndex - 2];
                            }
                        }
                        temIndex--;
                    }
                }
                tem[i]=1;
            }
        }
        for (int i:tem) {
            end+=i;
        }
        return end;
    }

    /**
     * 假设你是一位很棒的家长，想要给你的孩子们一些小饼干。但是，每个孩子最多只能给一块饼干。
     对每个孩子 i，都有一个胃口值 g[i]，这是能让孩子们满足胃口的饼干的最小尺寸；并且每块饼干 j，都有一个尺寸 s[j]。
     如果 s[j] >= g[i]，我们可以将这个饼干 j 分配给孩子i这个孩子会得到满足。你的目标是尽可能满足越多数量的孩子,并输出这个最大数值。
     示例 1:
     输入: g = [1,2,3], s = [1,1]
     输出: 1
     解释:
     你有三个孩子和两块小饼干，3个孩子的胃口值分别是：1,2,3。
     虽然你有两块小饼干，由于他们的尺寸都是1，你只能让胃口值是1的孩子满足。
     所以你应该输出1。
     示例 2:
     输入: g = [1,2], s = [1,2,3]
     输出: 2
     解释:
     你有两个孩子和三块小饼干，2个孩子的胃口值分别是1,2。
     你拥有的饼干数量和尺寸都足以让所有孩子满足。
     所以你应该输出2.
     */
    public static int findContentChildren(int[] g, int[] s) {
        int gLen=g.length;
        int sLen=s.length;
        int end=0;
        if(gLen==0||sLen==0){
            return 0;
        }
        Arrays.sort(g);
        Arrays.sort(s);
        List<Integer> list=new ArrayList<>();
        for (int i = sLen-1; i >=0; i--) {
            list.add(s[i]);
        }
        for (int i = gLen-1; i >= 0; i--) {
            Iterator<Integer> integerIterator=list.iterator();
            while (integerIterator.hasNext()){
                int t=integerIterator.next();
                if(t>=g[i]){
                    end++;
                    integerIterator.remove();
                    break;
                }
            }
        }
        return end;
    }

    /**
     *
     给定一个整数数组 prices ，它的第 i 个元素 prices[i] 是一支给定的股票在第 i 天的价格。
     设计一个算法来计算你所能获取的最大利润。你最多可以完成 k 笔交易。
     注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
     示例 1：
     输入：k = 2, prices = [2,4,1]
     输出：2
     解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2 。
     示例 2：
     输入：k = 2, prices = [3,2,6,5,0,3]
     输出：7
     解释：在第 2 天 (股票价格 = 2) 的时候买入，在第 3 天 (股票价格 = 6) 的时候卖出, 这笔交易所能获得利润 = 6-2 = 4 。
     随后，在第 5 天 (股票价格 = 0) 的时候买入，在第 6 天 (股票价格 = 3) 的时候卖出, 这笔交易所能获得利润 = 3-0 = 3 。
     */
    public static int maxProfit(int k, int[] prices) {

        return 0;
    }

    /**
     * 有一堆石头，每块石头的重量都是正整数。
     每一回合，从中选出两块 最重的 石头，然后将它们一起粉碎。假设石头的重量分别为 x 和 y，且 x <= y。那么粉碎的可能结果如下：
     如果 x == y，那么两块石头都会被完全粉碎；
     如果 x != y，那么重量为 x 的石头将会完全粉碎，而重量为 y 的石头新重量为 y-x。
     最后，最多只会剩下一块石头。返回此石头的重量。如果没有石头剩下，就返回 0。
     示例：
     输入：[2,7,4,1,8,1]
     输出：1
     解释：
     先选出 7 和 8，得到 1，所以数组转换为 [2,4,1,1,1]，
     再选出 2 和 4，得到 2，所以数组转换为 [2,1,1,1]，
     接着是 2 和 1，得到 1，所以数组转换为 [1,1,1]，
     最后选出 1 和 1，得到 0，最终数组转换为 [1]，这就是最后剩下那块石头的重量。
     提示：
     1 <= stones.length <= 30
     1 <= stones[i] <= 1000
     */
    public static int lastStoneWeight(int[] stones) {
        if(stones.length<1){
            return 0;
        }else if(stones.length==1){
            return stones[0];
        }else if(stones.length==2){
            return Math.abs(stones[0]-stones[1]);
        }
        Arrays.sort(stones);
        int[] tem=stones;
        while (tem.length>1){
            int len=tem.length;
            int mid=Math.abs(tem[len-1]-tem[len-2]);
            tem=getNewInt(tem,mid,len-2);
        }
        if(tem.length==0){
            return 0;
        }
        return tem[0];
    }

    public static int[] getNewInt(int[] arr,int tem,int temi){
        int len=arr.length;
        if(tem==0){
            int[] arrNew=new int[len-2];
            for (int i = 0; i < len-2; i++) {
                arrNew[i]=arr[i];
            }
            return arrNew;
        }
        int[] arrNew=new int[len-1];
        int judge=0;
        int con=0;
        for (int i = 0; i < len-1; i++) {
            if(len-1==2){
                arrNew[0]=arr[0];
                arrNew[1]=tem;
            }else if(len-1==1){
                arrNew[0]=tem;
            }else {
                if ((arr[i] > tem||temi==i) && judge == 0) {
                    arrNew[i] = tem;
                    con = i;
                    judge = 1;
                } else {
                    arrNew[i] = arr[con];
                    con++;
                }
            }
        }
        return arrNew;
    }

    /**
     *
     给定一个区间的集合，找到需要移除区间的最小数量，使剩余区间互不重叠。
     注意:
     可以认为区间的终点总是大于它的起点。
     区间 [1,2] 和 [2,3] 的边界相互“接触”，但没有相互重叠。
     示例 1:
     输入: [ [1,2], [2,3], [3,4], [1,3] ]
     输出: 1
     解释: 移除 [1,3] 后，剩下的区间没有重叠。
     示例 2:
     输入: [ [1,2], [1,2], [1,2] ]
     输出: 2
     解释: 你需要移除两个 [1,2] 来使剩下的区间没有重叠。
     示例 3:
     输入: [ [1,2], [2,3] ]
     输出: 0
     解释: 你不需要移除任何区间，因为它们已经是无重叠的了。
     */
    public static int eraseOverlapIntervals(int[][] intervals) {
        int result=0;


        return result;
    }

    /**
     * 给你一个字符串 s，以及该字符串中的一些「索引对」数组 pairs，其中 pairs[i] = [a, b] 表示字符串中的两个索引（编号从 0 开始）。
     你可以 任意多次交换 在 pairs 中任意一对索引处的字符。
     返回在经过若干次交换后，s 可以变成的按字典序最小的字符串。
     示例 1:
     输入：s = "dcab", pairs = [[0,3],[1,2]]
     输出："bacd"
     解释：
     交换 s[0] 和 s[3], s = "bcad"
     交换 s[1] 和 s[2], s = "bacd"
     示例 2：
     输入：s = "dcab", pairs = [[0,3],[1,2],[0,2]]
     输出："abcd"
     解释：
     交换 s[0] 和 s[3], s = "bcad"
     交换 s[0] 和 s[2], s = "acbd"
     交换 s[1] 和 s[2], s = "abcd"
     示例 3：
     输入：s = "cba", pairs = [[0,1],[1,2]]
     输出："abc"
     解释：
     交换 s[0] 和 s[1], s = "bca"
     交换 s[1] 和 s[2], s = "bac"
     交换 s[0] 和 s[1], s = "abc"
     */
    public static String smallestStringWithSwaps(String s, List<List<Integer>> pairs) {
        List<List<Integer>> pairsNew=new ArrayList<>();
        Iterator<List<Integer>> listIterator=pairs.listIterator();
        int m=0;
        while (listIterator.hasNext()){
            int len=pairs.size();
            List<Integer> list=listIterator.next();
            int tlen=pairsNew.size();
            for (int i = m+1; i < len; i++) {
                int a1=pairs.get(i).get(0);
                int a2=pairs.get(i).get(1);
                if(list.contains(a1)||list.contains(a2)){
                    List<Integer> list1=new ArrayList<>();
                    list1.addAll(list);
                    list1.add(list.contains(pairs.get(i).get(0))?null:a1);
                    list1.add(list.contains(pairs.get(i).get(1))?null:a2);
                    pairsNew.add(list1);
                    listIterator.remove();
                }
            }
            if(pairsNew.size()==tlen){
                List<Integer> list1=new ArrayList<>();
                list1.addAll(list);
                pairsNew.add(list1);
            }
            m++;
        }
        pairs.forEach(pe->pe.forEach(System.out::println));
        for (List<Integer> l:pairsNew) {
            s=changeStrSite(s,l);
        }
        return s;
    }

    /**
     * 位置交换
     * @param str
     * @param list
     * @return
     */
    public static String changeStrSite(String str,List<Integer> list){
        char[] end=str.toCharArray();
        int len=list.size();
        char[] chars=new char[len];
        for (int i = 0; i < len; i++) {
            chars[i]=end[list.get(i)];
        }
        Arrays.sort(chars);
        for (int i = 0; i < len; i++) {
            end[list.get(i)]=chars[i];
        }
        return new String(end);
    }

    
    /**
     * 在本问题中, 树指的是一个连通且无环的无向图。
     输入一个图，该图由一个有着N个节点 (节点值不重复1, 2, ..., N) 的树及一条附加的边构成。
     附加的边的两个顶点包含在1到N中间，这条附加的边不属于树中已存在的边。
     结果图是一个以边组成的二维数组。每一个边的元素是一对[u, v] ，满足 u < v，表示连接顶点u 和v的无向图的边。
     返回一条可以删去的边，使得结果图是一个有着N个节点的树。如果有多个答案，则返回二维数组中最后出现的边。
     答案边 [u, v] 应满足相同的格式 u < v。
     示例 1：
     输入: [[1,2], [1,3], [2,3]]
     输出: [2,3]
     解释: 给定的无向图为:
     1
     / \
     2 - 3
     示例 2：
     输入: [[1,2], [2,3], [3,4], [1,4], [1,5]]
     输出: [1,4]
     解释: 给定的无向图为:
     5 - 1 - 2
     |   |
     4 - 3
     注意:
     输入的二维数组大小在 3 到 1000。
     二维数组中的整数在1到N之间，其中N是输入数组的大小。
     */
    public static int[] findRedundantConnection(int[][] edges) {
        int[] end=new int[2];

        return end;
    }

    /**
     * 对于非负整数 X 而言，X 的数组形式是每位数字按从左到右的顺序形成的数组。例如，如果 X = 1231，那么其数组形式为 [1,2,3,1]。
     给定非负整数 X 的数组形式 A，返回整数 X+K 的数组形式
     示例 1：
     输入：A = [1,2,0,0], K = 34
     输出：[1,2,3,4]
     解释：1200 + 34 = 1234
     示例 2：
     输入：A = [2,7,4], K = 181
     输出：[4,5,5]
     解释：274 + 181 = 455
     示例 3：
     输入：A = [2,1,5], K = 806
     输出：[1,0,2,1]
     解释：215 + 806 = 1021
     示例 4：
     输入：A = [9,9,9,9,9,9,9,9,9,9], K = 1
     输出：[1,0,0,0,0,0,0,0,0,0,0]
     解释：9999999999 + 1 = 10000000000
      
     提示：
     1 <= A.length <= 10000
     0 <= A[i] <= 9
     0 <= K <= 10000
     如果 A.length > 1，那么 A[0] != 0
     */
    public static List<Integer> getArrThree(int[] A,int K){
        List<Integer> end=new ArrayList<>();
        List<Integer> end1=new ArrayList<>();
        Queue<Integer> stack1=new ArrayDeque<>();
        Stack<Integer> stack2=new Stack<>();
        int nextChu=K%10;
        stack1.add(nextChu);
        K=K/10;
        while(K!=0){
            nextChu=K%10;
            stack1.add(nextChu);
            K=K/10;
        }
        for (int i = 0; i <A.length ; i++) {
            stack2.push(A[i]);
        }
        int stac=0;
        //处理
        while ((!stack1.isEmpty())||(!stack2.empty())||stac==1){
            int t1=stack1.isEmpty()?0:stack1.poll();
            int t2=stack2.empty()?0:stack2.pop();
            int tem=t1+t2+stac;
            if(tem>9){
                end.add(tem%10);
                stac=1;
            }else {
                end.add(tem);
                stac=0;
            }
        }
        for (int i = (end.size()-1); i >=0 ; i--) {
            end1.add(end.get(i));
        }
        return end1;
    }

}

