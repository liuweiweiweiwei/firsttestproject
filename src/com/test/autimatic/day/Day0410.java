package com.test.autimatic.day;

import com.sun.xml.internal.ws.util.Pool;

import java.io.Reader;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/10 12:23
 * @description
 */
public class Day0410 {
    public static void main(String[] args) {
        System.out.println(mostCommonWord("Bob. hIt, baLl", new String[]{"bob", "hit"}));
        System.out.println(lexicalOrder1(52));
        System.out.println(Arrays.deepToString(outerTrees(new int[][]{{1, 1}, {2, 2}, {2, 0}, {2, 4}, {3, 3}, {4, 2}})));
        Node construct = construct(new int[][]{{0, 1}, {1, 0}});
        System.out.println(construct);
    }

    public static int uniqueMorseRepresentations(String[] words) {
        String[] strings = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        Set<String> resultSet = new HashSet<>(words.length);
        for (String str : words) {
            StringBuilder builder = new StringBuilder();
            for (char c : str.toCharArray()) {
                builder.append(strings[c - 'a']);
            }
            resultSet.add(builder.toString());
        }
        return resultSet.size();
    }

    // This is the interface that allows for creating nested lists.
    // You should not implement it, or speculate about its implementation
    public static class NestedInteger {
        // Constructor initializes an empty nested list.
        public NestedInteger() {
        }

        // Constructor initializes a single integer.
        public NestedInteger(int value) {
        }

        // @return true if this NestedInteger holds a single integer, rather than a nested list.
        public boolean isInteger() {
            return false;
        }

        // @return the single integer that this NestedInteger holds, if it holds a single integer
        // Return null if this NestedInteger holds a nested list
        public Integer getInteger() {
            return 1;
        }

        // Set this NestedInteger to hold a single integer.
        public void setInteger(int value) {
        }

        // Set this NestedInteger to hold a nested list and adds a nested integer to it.
        public void add(NestedInteger ni) {
        }

        // @return the nested list that this NestedInteger holds, if it holds a nested list
        // Return empty list if this NestedInteger holds a single integer
        public List<NestedInteger> getList() {
            return Collections.emptyList();
        }
    }

    public static NestedInteger deserialize(String s) {
        if (s.length() > 0 && !s.startsWith("[")) {
            return new NestedInteger(Integer.parseInt(s));
        }
        Deque<NestedInteger> stack = new ArrayDeque<>();
        StringBuilder cutIntBuild = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char curChar = s.charAt(i);
            if (curChar == '[') {
                stack.push(new NestedInteger());
            } else if (curChar == ']' || curChar == ',') {
                if (Character.isDigit(s.charAt(i - 1))) {
                    stack.peek().add(new NestedInteger(Integer.parseInt(cutIntBuild.toString())));
                }
                cutIntBuild = new StringBuilder();
                if (curChar == ']' && stack.size() > 1) {
                    NestedInteger pop = stack.pop();
                    stack.peek().add(pop);
                    cutIntBuild = new StringBuilder();
                }
            } else {
                cutIntBuild.append(curChar);
            }
        }
        return stack.pop();
    }

    public static String mostCommonWord(String paragraph, String[] banned) {
        paragraph = paragraph.toLowerCase();
        String result = "";
        int maxLen = 0;
        Set<String> stringSet = new HashSet<>();
        stringSet.addAll(Arrays.stream(banned).collect(Collectors.toSet()));
        Map<String, Integer> mapWOrd = new HashMap<>();
        String[] split = paragraph.split("[ !?',;\\.]");
        Arrays.stream(split).filter(word -> !stringSet.contains(word) && !word.isEmpty())
                .forEach(word -> mapWOrd.put(word, mapWOrd.getOrDefault(word, 0) + 1));
        for (Map.Entry<String, Integer> en : mapWOrd.entrySet()) {
            Integer value = en.getValue();
            if (value > maxLen) {
                maxLen = value;
                result = en.getKey();
            }
        }
        return result;
    }

    /**
     * add 10 tree
     *
     * @param n n
     * @return list
     */
    public static List<Integer> lexicalOrder(int n) {
        List<Integer> result = new ArrayList<>(n);
        int start = 1;
        for (int i = 0; i < n; i++) {
            result.add(start);
            if (start * 10 <= n) {
                start *= 10;
            } else {
                while (start % 10 == 9 || start + 1 > n) {
                    start /= 10;
                }
                start++;
            }
        }
        return result;
    }

    static List<Integer> result = new ArrayList<>();

    private static void dfs(int cur, int n) {
        if (cur > n) {
            return;
        }
        result.add(cur);
        for (int i = 0; i <= 9; i++) {
            int tempCur = cur * 10 + i;
            if (tempCur > n) {
                break;
            }
            dfs(tempCur, n);
        }
    }

    public static List<Integer> lexicalOrder1(int n) {
        for (int i = 1; i <= 9; i++) {
            dfs(i, n);
        }
        return result;
    }

    public static int[][] outerTrees(int[][] trees) {
        TreeMap<Integer,int[]> treeMap = new TreeMap<>();
        for (int[] tree : trees) {
            int[] ints = treeMap.getOrDefault(tree[0], new int[]{-1, -1});
            if (ints[0] == -1) {
                ints = new int[2];
                ints[0] = ints[1] = tree[1];
            } else {
                ints[0] = Math.min(ints[0], tree[1]);
                ints[1] = Math.max(ints[1], tree[1]);
            }
            treeMap.put(tree[0], ints);
        }
        List<int[]> result = new ArrayList<>();
        for (Map.Entry<Integer, int[]> entry : treeMap.entrySet()) {
            Integer key = entry.getKey();
            int[] value = entry.getValue();
            result.add(new int[]{key, value[0]});
            if (value[0] != value[1]) {
                result.add(new int[]{key, value[1]});
            }
        }
        int[][] arrResult = new int[result.size()][2];
        for (int i = 0; i < result.size(); i++) {
            arrResult[i] = result.get(i);
        }
        return arrResult;
    }

    public static Node construct(int[][] grid) {
        return rootConstruct(grid, 0, 0, grid.length, grid.length);
    }

    private static Node rootConstruct(int[][] data, int i, int j, int ie, int je) {
        if (isConstruct(data, i, ie, j, je)) {
            return new Node(data[i][j] == 1, true);
        }
        Node res = new Node(
                false,
                false,
                rootConstruct(data, i, j, (ie + i)/2, (je + j)/2),
                rootConstruct(data, i, (je + j)/2, (ie + i)/2, je),
                rootConstruct(data, (ie + i)/2, j, ie, (je + j)/2),
                rootConstruct(data, (ie + i)/2, (je + j)/2, ie, je)
        );
        return res;
    }

    private static boolean isConstruct(int[][] data, int i, int ie, int j, int je) {
        for (int k = i; k < ie; k++) {
            for (int l = j; l < je; l++) {
                if (data[i][j] != data[k][l]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static class Node {
        public boolean val;
        public boolean isLeaf;
        public Node topLeft;
        public Node topRight;
        public Node bottomLeft;
        public Node bottomRight;


        public Node() {
            this.val = false;
            this.isLeaf = false;
            this.topLeft = null;
            this.topRight = null;
            this.bottomLeft = null;
            this.bottomRight = null;
        }

        public Node(boolean val, boolean isLeaf) {
            this.val = val;
            this.isLeaf = isLeaf;
            this.topLeft = null;
            this.topRight = null;
            this.bottomLeft = null;
            this.bottomRight = null;
        }

        public Node(boolean val, boolean isLeaf, Node topLeft, Node topRight, Node bottomLeft, Node bottomRight) {
            this.val = val;
            this.isLeaf = isLeaf;
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }
    }
}