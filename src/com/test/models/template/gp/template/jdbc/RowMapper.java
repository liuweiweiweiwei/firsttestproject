package com.test.models.template.gp.template.jdbc;

import java.sql.ResultSet;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/9 14:02
 * @description 做orm的模板接口
 */
public interface RowMapper<T> {

    T mapRow(ResultSet rs,int rowNum)throws Exception;
}
