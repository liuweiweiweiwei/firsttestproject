package com.test.models.factory.extend;

import com.test.models.factory.MyCourse;
import com.test.models.factory.StudyCourse;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:57
 * @description
 */
public class StudyCourseFactory implements  ICourseFactory {
    @Override
    public MyCourse createFactory() {
        return new StudyCourse();
    }
}
