package com.test.Pattern;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/10/19 10:12
 * @description
 */
public class PatternOne {
    private static Pattern pattern;
    public static void main(String[] args) {
        getThing();
        Map<String,String> map=new HashMap<>();
        map.put("test","测试");
        getComplite("这是一个测试${test}lksa",map);
    }

    public static void getThing(){
        //匹配手机号
        //pattern=Pattern.compile("^1[3|7|8|5]\\d{9}$");
        String phone="18456789078";
       // Matcher matcher=pattern.matcher(phone);
        //匹配mail
        pattern=Pattern.compile("^\\w+([+-.]\\w+)*@\\w+([-.])*\\.\\w+$");
        String mail="test123@baidu.com";
        //Matcher matcher=pattern.matcher(mail);
        //身份证
        pattern=Pattern.compile("^[0-9]{17}[0-9Xx]$");
        String ID="12345678901234569";
        //Matcher matcher=pattern.matcher(ID);
        //密码8-12 字母数字下划线
        pattern=Pattern.compile("^[A-Z][\\w&*$#@]{7,11}$");
        String pwd="A1234_897$";
        Matcher matcher=pattern.matcher(pwd);

        if(matcher.find()){
            System.out.println(matcher.group());
        }else {
            System.out.println("没有匹配！");
        }
    }
    private static Pattern complite=Pattern.compile("\\$\\{\\w+\\}");

    public static String getComplite(String s, Map<String,String> map){
        Matcher matcher=complite.matcher(s);
        StringBuffer stringBuffer=new StringBuffer();
        while (matcher.find()){
            String deal=matcher.group();
            String v=map.get(deal.substring(2,deal.length()-1));
            System.out.println(deal+":"+v);
            matcher.appendReplacement(stringBuffer,v==null?"":v);
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }


}
