package com.test.models.gpdecorate.v2;


/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:16
 * @description
 */
public class BatterCakeEgg extends BatterCake {

    private BatterCake batterCake;

    public BatterCakeEgg(BatterCake batterCake) {
        this.batterCake = batterCake;
    }

    @Override
    protected String name() {
        return "一个鸡蛋煎饼！";
    }

    @Override
    protected int price() {
        return batterCake.price() + 1;
    }
}
