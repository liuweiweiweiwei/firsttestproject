package com.test.models.WeightFly;

import java.util.Random;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/20 16:09
 * @description 享
 */
public class ContextTicket  implements Ticket{
    private String from;
    private String to;
    private double prices;

    private static final String TAG="CONTEXT";

    public ContextTicket(String from, String to, double prices) {
        this.from = from;
        this.to = to;
        this.prices = prices;
    }

    public ContextTicket() {
    }

    //private static final ContextTicket TICKET=null;

    private static class Inner{
        private static final ContextTicket TICKET=new ContextTicket();
    }

    public static ContextTicket Instance(){
        return Inner.TICKET;
    }

    @Override
    public void showTicketInfo(String pumb) {
        prices=new Random().nextDouble();
        System.out.println(TAG+":"+"Context->"+from+"--"+to+"--"+pumb+"--"+prices);
    }
}
