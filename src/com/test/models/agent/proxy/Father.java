package com.test.models.agent.proxy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:26
 * @description
 */
public class Father {

    private Son son;

    public Father(Son son) {
        this.son = son;
    }

    public void findLove(){
        System.out.println("父亲帮忙寻找对象！");
        this.son.find();
        System.out.println("双方父母同意，确定关系！");
    }
}
