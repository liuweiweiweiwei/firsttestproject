package com.test.java8;

import java.util.concurrent.TimeUnit;

public class TestThread extends Thread {
    private String ThreadName;
    TestThread(String threadName){
        setDaemon(true);
        this.ThreadName=threadName;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("当前执行线程为："+currentThread().getName()+"执行："+ThreadName);
    }
}
