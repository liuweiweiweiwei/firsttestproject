package com.test.autimatic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/11/17 16:12
 * @description
 */
public class IncreateStack {

    public static void main(String[] args) {
        IncreateStack stack = new IncreateStack();
        int[] ints = {2, 1, 2, 4, 3};
        System.out.println(Arrays.toString(stack.stackRound(ints)));
    }


    /**
     * 单调栈
     *
     * @return 返回下一个最大的数据--不包括环形数组
     */
    public List<Integer> stack(int[] arr) {
        Stack<Integer> stack = new Stack<>();//单调栈
        List<Integer> list = new ArrayList<>();//最终集合
        //遍历数据
        for (int i = arr.length - 1; i >= 0; i--) {
            //所有数据一次添加与删除
            while (!stack.isEmpty() && stack.peek() <= arr[i]) {
                stack.pop();
            }
            //添加最终集合内的数据
            list.add(stack.isEmpty() ? -1 : stack.peek());
            //将当前元素加入栈内
            stack.push(arr[i]);
        }
        //返回最终数据
        return list;
    }

    /**
     * 单调栈
     *
     * @return 返回下一个最大的数据--不包括环形数组
     */
    public int[] stackRound(int[] arr) {
        Stack<Integer> stack = new Stack<>();//单调栈
        int[] list = new int[arr.length];//最终集合
        //遍历数据
        for (int i = 2 * arr.length - 1; i >= 0; i--) {
            //所有数据一次添加与删除
            while (!stack.isEmpty() && stack.peek() <= arr[i % arr.length]) {
                stack.pop();
            }
            //添加最终集合内的数据
            list[i % arr.length] = stack.isEmpty() ? -1 : stack.peek();
            //将当前元素加入栈内
            stack.push(arr[i % arr.length]);
        }
        //返回最终数据
        return list;
    }

}
