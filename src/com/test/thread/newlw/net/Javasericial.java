package com.test.thread.newlw.net;


import com.test.Files.FileUtil;

import java.io.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 15:00
 */
public class Javasericial implements IService {

    @Override
    public <T> byte[] read(T t) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = null;
        try {
//            outputStream = new ObjectOutputStream(new FileOutputStream(new File("user.txt")));
            outputStream = new ObjectOutputStream(byteArrayOutputStream);
            outputStream.writeObject(t);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            FileUtil.closeO(outputStream);
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public <T> T write(byte[] bytes, Class<T> tClass,String name) {
        T t = null;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        try {
//            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(new File(name)));
            ObjectInputStream inputStream = new ObjectInputStream(byteArrayInputStream);
            t = (T)inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return t;
    }


}
