package com.test.thread.newlw.net;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 17:22
 */
public class SericialFactory {


    public static IService instance(Class z){
        if(z == null){return null;}
        IService o = null;
        try {
            o = (IService)z.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return o;
    }
}
