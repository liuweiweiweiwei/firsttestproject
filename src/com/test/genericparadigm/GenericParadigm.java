package com.test.genericparadigm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/8 22:22
 * @description
 */
public class GenericParadigm {
    public static void main(String[] args) {
        List<? super Animal> animalList = new ArrayList<>();
//        animalList.add(new Biological());
        animalList.add(new Dog());
        animalList.add(new Pig());
        animalList.add(new Animal());
        Object object = animalList.get(0);
        System.out.println(object.toString());
        List<? extends Plant> plantList = new ArrayList<>();
//        plantList.add(new Biological());
//        plantList.add(new Flower());
//        plantList.add(new Plant());
        plantList.add(null);
        Plant plant = plantList.get(0);
        List<Plant> plantList1 = new ArrayList<>();
        plantList1.add(new Flower());
        System.out.println(plantList1.get(0));
    }

    private static class Biological{}
    private static class Animal extends Biological{}
    private static class Plant extends Biological{}
    private static class Dog extends Animal{}
    private static class Flower extends Plant{}
    private static class Pig extends Animal{}
}
