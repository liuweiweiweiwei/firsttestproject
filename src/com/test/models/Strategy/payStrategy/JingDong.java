package com.test.models.Strategy.payStrategy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 18:59
 * @description
 */
public class JingDong extends Pay {
    @Override
    public String getName() {
        return "京东白条";
    }

    @Override
    protected double queryBalance(String uid) {
        return 300;
    }
}
