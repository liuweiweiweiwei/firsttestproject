package com.test.Task;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.UUID;

public class Java8Time {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        //party1:::
        LocalDateTime localDateTime=LocalDateTime.of(2020,9,10,12,30,20);
        LocalDateTime localDateTimeend=LocalDateTime.now();
        boolean isBefore=localDateTime.isBefore(localDateTimeend);
        String result=String.format("测试1：localDateTime 是在localDateTimeEnd 之前么？：%s",isBefore);
        System.out.println(result);
        //2:partyId
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection= DriverManager.getConnection(URL,USER_NAME,PASSWORD);
        PreparedStatement preparedStatement=null;
        for (int i = 1; i <60000 ; i++) {
            String str= UUID.randomUUID().toString().replace("-","");
            System.out.println("carry"+i);
            String sql="INSERT INTO `lw_log`.`lwlog_user`" +
                    "( `username`, `password`, `year`, `phone`, `sex`, `hobby`, `summary`, `headphoto`, `createtime`)" +
                    "VALUES ( ?, ?, ?, ?,?,?, ?, ?, ?)";
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setString(1,"xiaoming"+i);
            preparedStatement.setString(2,"MTIzNDU2");
            preparedStatement.setInt(3,i);
            preparedStatement.setInt(4,112345678+i);
            preparedStatement.setInt(5,i%2==0?0:1);
            preparedStatement.setString(6, str);
            preparedStatement.setString(7,"head"+i);
            preparedStatement.setString(8,"目录"+i);
            preparedStatement.setInt(9,12+i);
            preparedStatement.execute();
            String sql1="INSERT INTO `lw_log`.`lwlog_language`" +
                    "( `log_id`, `content`, `time`, `user_id`) " +
                    "VALUES ( ?, ?, ?, ?);";
            preparedStatement=connection.prepareStatement(sql1);
            preparedStatement.setInt(1,i);
            preparedStatement.setString(2, str);
            preparedStatement.setInt(3,112345678+i);
            preparedStatement.setInt(4,i);
            preparedStatement.execute();
            //TimeUnit.MILLISECONDS.sleep(80);
        }
        preparedStatement.close();
        connection.close();
        System.out.println("结束");
    }

    public static final String USER_NAME="root";
    public static final String PASSWORD="123456";
    public static final String URL="jdbc:mysql://localhost:3306/lw_log?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=true";
}
