package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 11:14
 * @description 所有子类实现此接口，不符合开闭原则
 */
public interface IFactory {

    ICourse createCourse();
    INode createNode();
    IVideo createVideo();

}
