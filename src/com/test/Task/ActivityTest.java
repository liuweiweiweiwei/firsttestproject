package com.test.Task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.test.classes.Type;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ActivityTest {
    private static AtomicLong atomicLong=new AtomicLong(100);

    public static void main(String[] args) throws Exception {

        String base64Before="http://www.app.com/hbsdcha&iuabdcajb/openid=12345&redirect_url=http://www.taikang.com";
        System.out.println("变更前："+base64Before);
        String baseMiddle=new String(Base64.getEncoder().encode(base64Before.getBytes()));
        System.out.println("加密后："+baseMiddle);
        String baseAfter=new String(Base64.getDecoder().decode(baseMiddle));
        System.out.println("解密后："+baseAfter);
        String uuid= UUID.randomUUID().toString();
        System.out.println("第一次数据："+uuid);
        System.out.println("第二次数据："+uuid.replaceAll("-",""));
        //
        int n=12,m=3;
        System.out.println("输出："+getSqrt(12));
        //
        String test="agentId=abcdefg&activityId=higklmn&channel=opqrst&userCode=uvwxyz&companyCode=jhhsadkchjvdhsj&redirectPageRout=kjbjkcabkjbckja";
        String agen=test.substring(test.indexOf("agentId=")+8,test.indexOf("&activityId"));
        String agen1=test.substring(test.indexOf("activityId=")+11,test.indexOf("&channel"));
        String agen2=test.substring(test.indexOf("channel=")+8,test.indexOf("&userCode"));
        String agen3=test.substring(test.indexOf("userCode=")+9,test.indexOf("&companyCode"));
        String agen4=test.substring(test.indexOf("companyCode=")+12,test.lastIndexOf("&"));
        String agen5=test.substring(test.indexOf("redirectPageRout=")+17,test.length());
        System.out.println(agen+"@@@"+agen1+"@@@"+agen2+"@@@"+agen3+"@@@"+agen4+"@@@"+agen5);

        List<String> stringList=Arrays.asList("agentId","activityId","channel","userCode","companyCode","redirectPageRout");
        Map<String,String> resul=getParam(test,stringList);
        resul.forEach((k,v)->{
            System.out.println(k+":"+v);
        });


        /*map.put("te","tes12");
        map.put("te1","te");
        map.put("te2","第一回 ");
        map.put("te13","te");*/
        boolean end=false;
        List<String> list=new ArrayList<>();
        for (String sd:list) {
            Map<String,String> map=new HashMap<>();
            end=getEquals(map);
        }

        System.out.println("判断两组字符串相等与否："+end);
        //3.
        List<TestDemo> demoList=Arrays.asList(
                new TestDemo("na",2,"con","ti"),
                new TestDemo("na1",2,"con3","ti3"),
                new TestDemo("na",2,"con2","ti2"));
        Map<String, List<TestDemo>> collect = demoList.stream().filter(de -> de.getAge() > 1)
                .collect(Collectors.groupingBy(TestDemo::getName));
        collect.forEach((k,v)->{
            System.out.println(k);
            System.out.println(v.get(0));
            System.out.println(v.size());
        });
        //匹配置换
        Map<String,String> stringMap=new HashMap<>();
        stringMap.put("name","小强");
        stringMap.put("age","18");
        String str="${name}的家的位置是河南，年龄是${age}";
        String endstr=stringFormat(str,stringMap);
        System.out.println(endstr);
        //3.定时
        //getTimeSend();
        //异步
        /*threadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                getTimeSend();
            }
        });
        getTimeSend();*/
        /////-------------------------------------------
        List<TestDemo> testDemoList=Arrays.asList(new TestDemo("1",2,"con1","tim1"),
                new TestDemo("2",2,"con2","tim23"),new TestDemo("1",2,"con5","tim8"),
                new TestDemo("3",2,"con3","tim2"),new TestDemo("1",2,"con6","tim7"),
                new TestDemo("2",2,"con4","tim4"),new TestDemo("3",2,"con7","tim6"));

        Map<String, List<TestDemo>> collect1 = testDemoList.stream().collect(Collectors.groupingBy(TestDemo::getName));

        collect1.forEach((k,v)->{
            System.out.println("key:"+k+":::::::value:"+v);
        });

        String bytes=new String(Base64.getEncoder().encode("13012345678".getBytes()));
        System.out.println(bytes);

        String bytes1=new String(Base64.getDecoder().decode(bytes));
        System.out.println(bytes1);

//        Pattern pattern=Pattern.compile("1[3897]\\d{9}");
//        Matcher matcher=pattern.matcher("11112345678");
//        System.out.println(matcher.find());

        String rep="c是啊w此处是cojc啊123c拆";
        String newRep=rep.replaceAll("[a-z]+","世界");
        System.out.println(rep+";"+newRep);

        LocalDateTime localDateTime=LocalDateTime.of(LocalDate.now(), LocalTime.now());

        int[] t={-1,-1,2,3,6,-1,3,1};
        int[] val=getListPrint(t,2);
        Arrays.stream(val).forEach(System.out::print);
        System.out.println("");

        //获取日期天数第一与最后
        //DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localTime=LocalDate.parse("1952-02-02");
        System.out.println(localTime);
        LocalDate localDate=getLastDayOfMonth(localTime);
        System.out.println(localDate);
        //活动模板2.封面3.公司介绍4.活动流程5.讲座介绍6.互动福利7.参会信息
        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime1=LocalDateTime.now();
        System.out.println(dateTimeFormatter.format(localDateTime1));
        //时分秒输出
        DateTimeFormatter dateTimeFormatter1=DateTimeFormatter.ofPattern("HH:mm:ss");
        System.out.println(dateTimeFormatter1.format(LocalTime.now()));
        //date与time
        Date date=new Date();
        System.out.println(date);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String dates=simpleDateFormat.format(date);
        System.out.println(dates);

        String na=null;
        String names=na+"";
        System.out.println(names);
        //3.
        String ss="02";
        System.out.println(getJudge(ss,"01","02","03"));
        //4.
        String ms=null;
        System.out.println(ms);
        //5.
        getNorepeat();
        //6.
        String testShareLink="http://www.baidu.com?aaa=vvv&jhg=安保&";
        Map<String,String> map=new TreeMap<>();

        map=shareUtilsDeal(testShareLink);
        System.out.println("redirectPageRout"+"<<<>>>"+map.get("redirectPageRout"));
        map.remove("redirectPageRout");
        if(map==null||map.isEmpty()){
            System.out.println("没有参数！");
            return;
        }
        map.forEach((k,v)->{
            System.out.println("输出k："+k+"<<<>>>"+"输出v："+v);
        });
        //7.
        getJson();
        //8.
        BigDecimal bigDecimal=new BigDecimal("1.25");
        BigDecimal bigDecimal1=bigDecimal.setScale(1,BigDecimal.ROUND_HALF_UP);
        System.out.println(bigDecimal.compareTo(bigDecimal1));
        System.out.println(bigDecimal1);
        Byte b=2;
        Short c=8;
        Integer a=010110;
        Long l=1L;
        HashSet<Integer> set=new HashSet<>();
        System.out.println(getmid());
        Arrays.sort(new int[]{1,2,5,4,3});
        Collections.sort(list);
        Type type=new Type("小明",6,"题目");
        System.out.println(JSON.toJSONString(type));
        //map,time
        Map<String,String> map1=new LinkedHashMap<>();
        map1.put("feedbackB","5");
        map1.put("feedbackD","1");
        map1.put("feedbackA","1");
        map1.put("feedbackE","2");
        map1.put("feedbackC","4");
        map1.forEach((k,v)-> System.out.println(k+":"+v));
        LocalDateTime localDateTime2=LocalDateTime.now();
        long second=localDateTime2.toEpochSecond(ZoneOffset.of("+8"));
        long st = LocalDateTime.parse("2020-11-23 12:30:30",dateTimeFormatter).toEpochSecond(ZoneOffset.of("+8"));
        System.out.println(second);
        System.out.println(second-st);

        //时间sql类转换
        SimpleDateFormat simpleDateFormat1=new SimpleDateFormat("HH:mm:ss");
        Date date1=simpleDateFormat1.parse("12:30:20");
        Time time=new Time(date1.getTime());
        System.out.println(time);
        //
        List<Integer> ls=new LinkedList<>();
        ls.addAll(Arrays.asList(1,1,1,1,1,1));
        ls.add(1,2);
        ls.add(2,3);
        ls.add(4,4);
        ls.forEach(System.out::println);
        String s ="你好赢得身高多少感受感受";
        byte[] wm=s.getBytes();
        System.out.println(s+"::"+wm);
        String strm="";
        for (int i = 0; i < wm.length; i++) {
            strm+=wm[i];
        }
        byte[] bytes2=new byte[strm.length()];
        String[] ssss=strm.split("-");
        System.out.println(ssss.length);
        for (int i = 0; i < ssss.length-1; i++) {
            System.out.println(ssss[i+1]);
            bytes2[i]=Byte.valueOf("-"+ssss[i+1]);
        }
        System.out.println(strm+"::"+new String(bytes2));

        char[] chars=s.toCharArray();
        String ss1="";
        for (int i = 0; i < chars.length; i++) {
            ss1+=chars[i];
        }
        System.out.println(ss1);
        //11.分配的初始值
        int[] ints111=new int[2];
        Arrays.stream(ints111).forEach(System.out::println);
        ints111[0]=1;ints111[1]=1;
        if(ints111[0]==1&&ints111[1]==1){
            System.out.println(112);
        }
        //12.set不重复
        Set<String> stringSet=new HashSet<>();
        stringSet.add("1");
        System.out.println("1>>>>>>>>>>>>>"+stringSet.size());
        stringSet.stream().forEach(System.out::println);
        stringSet.add("2");
        System.out.println("2>>>>>>>>>>>>>+stringSet.size()"+stringSet.size());
        stringSet.stream().forEach(System.out::println);
        stringSet.add("1");
        System.out.println("3>>>>>>>>>>>>>"+stringSet.size());
        stringSet.stream().forEach(System.out::println);
        String strin="uuid-1234567";
        System.out.println(strin.intern());
        Stack<Integer> stack=new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        while (!stack.empty()){
            System.out.println(stack.pop());
        }
        Queue<Integer> stack1=new ArrayDeque<>();
        stack1.add(1);
        stack1.add(2);
        stack1.add(3);
        while (!stack1.isEmpty()){
            System.out.println(stack1.poll());
        }

        Integer aaa=1900;
        int bbb=1900;
        System.out.println("aaa==bbb?"+(aaa==bbb));
        ArrayList arrayList;LinkedList linkedList;HashMap map2;

        Chicken chicken=new Chicken();
        if(chicken!=null){
            System.out.println("...");
        }else{
            System.out.println("+++");
        }
        List<String> list1= Arrays.asList(null,null,null);
        System.out.println(list1.toString());
        List<Integer> list2=new ArrayList<>(Arrays.asList(11,22,32,42,52));
        System.out.println(list2.toString());
        list2.remove(0);
        //System.out.println(list2.toString());
        list2.remove(0);
        System.out.println(list2.toString());

        //testADDall
        List<Integer> list3=Collections.emptyList();
        List<Integer> list4=new ArrayList<>();
        List<Integer> list5=Collections.emptyList();
        List<Integer> alllist=new ArrayList<>();
        alllist.addAll(list3);
        alllist.addAll(list4);
        alllist.addAll(list5);
        if(alllist==null||alllist.size()==0) {
            System.out.println("testADDall结果为:null");
        }else{
            System.out.println("not null！");
        }
        System.out.println(Arrays.binarySearch(new int[]{1,2,3,4,5},6));
        EnumSet<ENU> enumSet=EnumSet.allOf(ENU.class);
        System.out.println(enumSet);
        Random random=new Random();
        System.out.println(random.nextInt(80));
        List<String> list6 = Arrays.asList(("绍齐, 博文, 梓晨, 胤祥, 瑞霖, 明哲, 天翊, 凯瑞, 健雄, 耀杰, 潇然, 子涵, 越彬, " +
                "钰轩, 智辉, 致远, 俊驰, 雨泽, 烨磊, 晟睿, 文昊, 修洁, 黎昕, 远航, 旭尧, 鸿涛, 伟祺, 荣轩, 越泽, 浩宇, 瑾瑜, " +
                "皓轩, 擎苍, 擎宇, 志泽, 子轩, 睿渊, 弘文, 哲瀚, 雨泽, 楷瑞, 建辉, 晋鹏, 天磊, 绍辉, 泽洋, 鑫磊, 鹏煊, 昊强," +
                " 伟宸, 博超, 君浩, 子骞, 鹏涛, 炎彬, 鹤轩, 越彬, 风华, 靖琪, 明辉, 伟诚, 明轩, 健柏, 修杰, 志泽, 弘文, 峻熙, " +
                "嘉懿, 煜城, 懿轩, 烨伟, 苑博, 伟泽, 熠彤, 鸿煊, 博涛, 烨霖, 烨华, 煜祺, 智宸, 正豪, 昊然, 明杰, 立诚, 立轩, " +
                "立辉, 峻熙, 弘文, 熠彤, 鸿煊, 烨霖, 哲瀚, 鑫鹏, 昊天, 思聪, 展鹏, 笑愚, 志强, 炫明, 雪松, 思源, 智渊, 思淼, " +
                "晓啸, 天宇, 浩然, 文轩, 鹭洋, 振家, 乐驹, 晓博, 文博, 昊焱, 立果, 金鑫, 锦程, 嘉熙, 鹏飞, 子默, 思远, 浩轩, " +
                "语堂, 聪健, 明, 文, 果, 思, 鹏, 驰, 涛, 琪, 浩, 航, 彬").split(","));
        System.out.println(list6.size());
        File file=new File("E:\\app\\chenge.log");
        Long length = file.length();
        System.out.println(length);
        Arrays.sort(new int[]{1,4,5,2,3});
        //getZFinfo("E:\\app\\XM\\bug_install.txt","E:\\app\\XM\\install.zip");
        System.out.println(File.separator);
        Path path=file.toPath();
        testProperties();
        bundle();
    }
    enum ENU{
        TSTR,MSTR,RSTR,ZSTR;
    }

    public static void testProperties(){
        //测试properties和bundle
        Properties properties=new Properties();
        String pathRedis = ActivityTest.class.getResource("").toString();
        System.out.println(pathRedis);
        pathRedis += "properties/config.properties";
        pathRedis = pathRedis.replace("file:/" ,"");
        InputStream inputStream = null;
        try{
            File file = new File(pathRedis);
            inputStream = new FileInputStream(file);
            properties.load(inputStream);
        }catch (Exception e){
            e.printStackTrace();
        }
        //打印
        System.out.println(properties.getProperty("redis.host")+":host:"+properties.getProperty("redis.user")+":name:"+
                properties.getProperty("redis.port")+":port:"+properties.getProperty("redis.password")+":password");
    }

    public static void bundle(){
        ResourceBundle bundle = ResourceBundle.getBundle("properties/config",Locale.getDefault());
        Integer port = Integer.parseInt(bundle.getString("redis.port"));
        System.out.println(bundle.getString("redis.host")+":host:"+bundle.getString("redis.user")+
                ":name:"+ port +":port:"+bundle.getString("redis.password")+":password");
    }



    public static int get(int[] arr){
        int end=0;
        int[] arrm={5,8,2,5,7,15,3,7,4,0,9,6};
        //a表示第i天是否有数据，有股则a[i][j]=a[i-1][1]/a[i-1][0]+arrm[i]
        int[][] a=new int[arrm.length][2];
        return end;
    }

    public static void getCodeList(String f) throws IOException {
        BufferedWriter bufferedWriter=null;
        try (BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(new FileInputStream("a.txt")))){
            bufferedWriter=new BufferedWriter(new OutputStreamWriter(new FileOutputStream("b.txt")));
            String line="";
            while ((line=bufferedReader.readLine())!=null){
                //匹配


                //添加
                bufferedWriter.write(line+",");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            bufferedWriter.close();
        }
    }

    /**
     * a1
     * @param map
     * @return
     */
    public static List<String> getListFromMap(Map<String,String> map){
        Set<String> strings = map.keySet();
        List<String> stringList=strings.stream().sorted().collect(Collectors.toList());
        return stringList;
    }
    public static void getZFinfo(String param,String param2){
        ZipOutputStream zipOutputStream=null;
        InputStream reader=null;
        FileOutputStream fileOutputStream=null;
        try {
            File file=new File(param);
            fileOutputStream=new FileOutputStream(param2);
            reader=new FileInputStream(file);
            zipOutputStream=new ZipOutputStream(fileOutputStream);
            byte[] bytes=new byte[1024];
            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            while ((reader.read(bytes))!=-1){
                zipOutputStream.write(bytes);
                bytes=new byte[1024];
            }
            zipOutputStream.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(zipOutputStream!=null) {
                    zipOutputStream.closeEntry();
                    zipOutputStream.close();
                }
                fileOutputStream.close();
                if(reader!=null)reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getmid(){
        int[] ints={12,8,15,7};
        int m=0;
        for (int i = 0; i < ints.length; i++) {
            for (int j = i+1; j < ints.length; j++) {
                if(ints[i]>ints[j]){
                    m++;
                }
            }
        }

        return String.valueOf(m);
    }

    /**
     * 处理url返回一个map集合
     * @param url string的url
     * @return treemap
     */
    public static TreeMap<String,String> shareUtilsDeal(String url){
        TreeMap<String,String> treeMap=new TreeMap<>();
        //中间存储的url，以及中间节点的k，v;;;对应参数？，=，&分别等于wen,deng,at
        String temUrl=url;
        String k;
        String v;
        int wen;
        int deng;
        int at;
        //进入循环处理后续参数
        while (temUrl.contains("&")||temUrl.contains("=")){
            wen=temUrl.indexOf("?");
            deng=temUrl.indexOf("=");
            //处理k
            k=temUrl.contains("?")?temUrl.substring(wen+1,deng): temUrl.substring(0,deng);
            //url地址
            temUrl=temUrl.substring(deng+1);
            at=temUrl.indexOf("&");
            //处理v
            v=temUrl.contains("&")?temUrl.substring(0,at):temUrl;
            //存储map集合中
            treeMap.put(k,v);
            if(!temUrl.contains("&")){
                return treeMap;
            }
            //处理地址
            temUrl=temUrl.substring(at+1);
        }
        return treeMap;
    }

    public static void getJson(){
        JSONArray array=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","timi1");
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("name","timi2");
        JSONObject jsonObject2=new JSONObject();
        jsonObject2.put("name","timi3");
        array.add(jsonObject);
        array.add(jsonObject1);
        array.add(jsonObject2);
        System.out.println(JSON.toJSONString(array));
        JSONObject object=(JSONObject)array.get(0);
        System.out.println(object.get("name"));
        String sa="[{\"name\":\"timi1\"},{\"name\":\"timi2\"},{\"name\":\"timi3\"}]";
        JSONArray jsonArray= JSONArray.parseArray(sa);
        JSONObject object1=JSONObject.parseObject(jsonArray.get(0).toString());
        String mot=object1.getString("age");
        if(mot==null){
            System.out.println("1234567");
        }
    }

    public static List<String> getList(){
        List<String> list=new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            list.add(UUID.randomUUID().toString());
            if(i%10000==0){
                System.out.println(list.get(i));
            }
        }
        return list;
    }

    //并集
    public static List<Integer> getMergeList(List<Integer> list,List<Integer> list2){
        List<Integer> listEnd=new LinkedList<>();
        listEnd.addAll(list);
        list2.forEach(da->{
            if(!listEnd.contains(da)){
                listEnd.add(da);
            }
        });
        return listEnd;
    }

    public static boolean getJudge(String s,String... strings){
        for (String s1:strings) {
            if(s.equals(s1)){
                return true;
            }
        }
        return false;
    }

    public static LocalDate getLastDayOfMonth(LocalDate localDate){
        int month=localDate.getMonthValue();
        int year=localDate.getYear();
        int day=31;
        LocalDate localDateEnd;
        switch (month){
            case 1:day=31;break;
            case 2:day=(year%4==0&&year%100!=0)||(year%400==0)?29:28;break;
            case 3:day=31;break;
            case 4:day=30;break;
            case 5:day=31;break;
            case 6:day=30;break;
            case 7:day=31;break;
            case 8:day=31;break;
            case 9:day=30;break;
            case 10:day=31;break;
            case 11:day=30;break;
            case 12:day=31;break;
            default:break;
        }
        localDateEnd=LocalDate.of(year,month,day);
        return localDateEnd;
    }

    public static void getNorepeat(){
        List<TestUser> users=Arrays.asList(new TestUser("abc","13","1239"),
                new TestUser("def","13","12345"),new TestUser("abh","13","1253"),
                new TestUser("abc1","13","123"),new TestUser("abd","13","1263"));
        List<TestUser> usersNew=new LinkedList<TestUser>();
        for (int i = 0; i <users.size() ; i++) {
            boolean ta=false;
            for (int j = i+1; j <users.size()-i-1 ; j++) {
                if(users.get(i).compareTo(users.get(j))==0){
                    ta=true;
                    break;
                }
            }
            if (!ta){
                usersNew.add(users.get(i));
            }
        }
        usersNew.forEach(d-> System.out.println(d.toString()));
    }

    public static int getMaxGroup(int[] arr){
        int num=0;
        int tem=arr[0];
        for(int i=1;i<arr.length;i++){
            tem+=arr[i];
            tem=Math.max(arr[i],tem);
            num= Math.max(tem,num);
        }
        return num;
    }

    public static int[] getListPrint(int[] param,int k){
        int[] result=new int[param.length];
        int m=0;
        Map<String,Integer> maps=new HashMap<>();
        for (int i:param) {
            maps.put(i+"",maps.get(""+i)==null?1:maps.get(""+i)+1);
        }
        List<Integer> list=maps.entrySet().stream().map(da->da.getValue()).sorted().collect(Collectors.toList());
        Integer integer=list.get(list.size()-k);

        Iterator<String> iterator=maps.keySet().iterator();
        while(iterator.hasNext()){
            String kt=iterator.next();
            Integer v=maps.get(kt);
            System.out.println("k,v"+kt+":"+v);
            if(v>=integer){
                result[m++]=Integer.parseInt(kt);
            }
        }
        return result;
    }

    private static int getSqrt( int n){
        if(n==2){
            return 1;
        }else if(n<2){
            return 0;
        }else if(n==3){
            return 2;
        }
        int[] sqrt=new int[n*n];
        sqrt[0]=0;
        sqrt[1]=1;
        sqrt[2]=2;
        sqrt[3]=3;
        for (int i = 4; i <=n ; i++) {
            int max=0;
            for (int j = 1; j < i/2+1; j++) {
                max=sqrt[i]=Math.max(sqrt[j]*sqrt[i-j],max);
            }
        }
        return sqrt[n];
    }

    private static boolean getEquals(Map<String,String> map){
        Boolean[] judge={true};
        map.forEach((k,v)->{
            if(k!=null&&v!=null) {
                if (!k.trim().equals(v.trim())) {
                    judge[0] = false;
                }
            }
        });
        return judge[0];
    }

    /**
     * 获取base64加密数据拼接字符串处理获取数据
     * @param url
     * @param stringList
     * @return
     */
    private static Map<String,String> getParam(String url,List<String> stringList){
        Map<String,String> map=new HashMap<>();
        for (int i = 0; i <stringList.size() ; i++) {
            String bas=stringList.get(i);
            String v=url.substring(url.indexOf(bas+"=")+bas.length()+1,
                    i+1<stringList.size()?url.indexOf("&"+stringList.get(i+1)):url.length());
            map.put(bas,v);
        }
        return map;
    }

    public static <T> T getObj(String data,Object o){
        JSONObject jsonObject=new JSONObject();
        return JSON.parseObject(data, (Class<T>) o.getClass());
    }

    public static final Pattern PATTERN=Pattern.compile("\\$\\{\\w+\\}");

    public static String stringFormat(String str,Map<String,String> params){
        Matcher matcher=PATTERN.matcher(str);
        StringBuffer stringBuffer=new StringBuffer();
        while (matcher.find()){
            String st=matcher.group();
            System.out.println(st);
            String va=params.get(st.substring(2,st.length()-1));
            System.out.println(va);
            matcher.appendReplacement(stringBuffer,va==null?"":va);
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    public static void getTimeSend(){
        for (int i = 0; i <100 ; i++) {
            System.out.println("开始跑任务："+i);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(
            1,1,3,TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(3),new ThreadPoolExecutor.CallerRunsPolicy());
}
