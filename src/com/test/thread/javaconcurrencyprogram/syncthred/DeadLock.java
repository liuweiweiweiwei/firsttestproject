package com.test.thread.javaconcurrencyprogram.syncthred;

import com.test.MyLog.LwLog;

import java.util.HashMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/25 21:38
 * @description
 */
public class DeadLock {
    // question one
    private final Object READ_LOCK = new Object();
    private final Object WRITE_LOCK= new Object();

    public void read() {
        synchronized (READ_LOCK) {
            LwLog.logInfo(Thread.currentThread().getName() + " get READ_LOCK !");
            synchronized (WRITE_LOCK) {
                LwLog.logInfo(Thread.currentThread().getName() + " get WRITE_LOCK !");
            }
            LwLog.logInfo(Thread.currentThread().getName() + "  WRITE_LOCK release !");
        }
        LwLog.logInfo(Thread.currentThread().getName() + "  READ_LOCK release !");
    }

    public void write() {
        synchronized (WRITE_LOCK) {
            LwLog.logInfo(Thread.currentThread().getName() + " get WRITE_LOCK !");
            synchronized (READ_LOCK) {
                LwLog.logInfo(Thread.currentThread().getName() + " get READ_LOCK !");
            }
            LwLog.logInfo(Thread.currentThread().getName() + "  READ_LOCK release !");
        }
        LwLog.logInfo(Thread.currentThread().getName() + "  WRITE_LOCK release !");
    }

    public static void main(String[] args) {
        final DeadLock deadLock = new DeadLock();
        new Thread(()->{
            while (true) {
                deadLock.read();
            }
        }, "START_READ").start();

        new Thread(()->{
            while (true) {
                deadLock.write();
            }
        }, "START_WRITE").start();
    }

    /**
     *  test two
     */
    private static class DeadMap{
        // not safety
        private final HashMap<String, String> hashMap = new HashMap<>();

        public void add(String k, String v) {
            hashMap.put(k, v);
        }

        public static void main(String[] args) {
            final DeadMap map = new DeadMap();
            for (int i = 0; i < 5; i++) {
                new Thread(() -> {
                    for (int j = 0; j < 10; j++) {
                        map.add(String.valueOf(j), String.valueOf(j));
                    }
                }, i + "TM").start();
            }
        }
    }
}
