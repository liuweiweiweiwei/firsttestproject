package com.test.autimatic.type;

import java.util.Arrays;

/**
 * bulong
 * @author 25338
 * @version 1.0
 * @date 2021/11/23 14:33
 */
public class BuLong {
    public static void main(String[] args) {

        //bit 32 位的数---比特320以内的===》32*10
        int[] a = new int[10];
        Arrays.fill(a,1);

        //获取第i个数据
        int i = 64;

        //取数据
        int numIndex = 112 / 32;
        int index = 112 % 32;

        //获取状态---0/1
        int state = (a[numIndex] >> index & 1);

        //将此位置上的数据变为1
        a[numIndex] = a[numIndex] | (1 << index);

        //将此位置换位0
        a[numIndex] = a[numIndex] & (~ (1<<index));


        //所有的取值
        int status = (a[i/32] >> (i%32) & 1);

        System.out.println(state);
        System.out.println(status);

        //比较
        int aM = 13,bM = -15;
        int cM = aM - bM;
        int temM = (cM >> 31 & 1);//取正负1：为负数0：为正数
        int temM2 = (temM ^ 1);//取反1:-》0 ；；；0 -》1
        System.out.println(temM * bM + temM2 * aM);

        //亦或--无进位相加
        int ja = 31;int jb = 5;
        System.out.println(add(ja,jb));
        //秋雨--相减
        System.out.println(desic(ja ,jb));
        //相乘
        System.out.println(cheng(ja, jb));
        //除法
        System.out.println(devide(ja, jb));
    }

    /**
     * 加法
     * @param ja
     * @param jb
     * @return
     */
    private static int add(int ja, int jb){
        int tempJ;
        while ((tempJ = ((ja & jb ) << 1)) != 0){
            ja = ja ^ jb;
            jb = tempJ;
        }
        return ja ^ jb;
    }

    /**
     * 减法
     * @param a
     * @param b
     * @return
     */
    private static int desic(int a, int b){
        return add(a, add(~b, 1));
    }

    /**
     * 乘法
     * @param a
     * @param b
     * @return
     */
    private static int cheng(int a,int b){
        int c = 0;
        while (a != 0){
            if((a & 1) == 1){
                c = add(c, b);
            }
            b <<= 1;
            a >>= 1;
        }
        return c;
    }

    /**
     * 除法操作
     * @param a
     * @param b
     * @return
     */
    private static int devide(int a,int b){
        if(b == 0){
            System.out.println("除数不能为0！");
            return -1;
        }
        int x = a < 0 ? -a : a;
        int y = b < 0 ? -b : b;
        int end = 0;
        for (int i = 31; i >= 0 ; i = desic(i,1)) {
            if(y <= (x >> i)){
                end = add(end, (1 << i));
                x = desic(x, y << i);
            }
        }
        return end;
    }
}
