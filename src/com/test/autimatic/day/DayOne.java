package com.test.autimatic.day;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/3/27 13:02
 * @description
 */
public class DayOne {
    public static void main(String[] args) {
        System.out.println(findKthNumber(100, 10));
        System.out.println(canReorderDoubled(new int[]{2,4,0,0,8,1}));
    }

    public int[] missingRolls(int[] rolls, int mean, int n) {
        int[] result = new int[n];
        Arrays.fill(result, -1);
        int num = (rolls.length + n ) * mean;
        num -= Arrays.stream(rolls).sum();
        int yu = num / n;
        int chu = num % n;
        if (yu > 6 || (yu == 6 && num % n != 0)) {
            return result;
        }
        for (int z = 0; z < n - 1; z++) {
            result[z] = yu + (chu != 0 ? 1 : 0);
            if (chu != 0) {chu--;}
        }
        return result;
    }

    public static int findKthNumber(int n, int k) {
        long index = 1;
        long result = 1;
        while (index < k) {
            long step = getOperateNum(result, n);
            if (index + step > k) {
                k--;
                result *= 10;
            }else {
                k -= step;
                result++;
            }
        }
        return Math.toIntExact(result);
    }

    private static long getOperateNum(long index, int n) {
        long next= index + 1;
        long step = 0;
        while (index <= n) {
            step += Math.min(n + 1, next) - index;
            index *= 10;
            next = next * 10;
        }
        return Math.toIntExact(step);
    }

    /**
     * 给定一个长度为偶数的整数数组 arr，只有对 arr 进行重组后可以满足 “对于每个 0 <= i < len(arr) / 2，
     * 都有 arr[2 * i + 1] = 2 * arr[2 * i]” 时，返回 true；否则，返回 false。
     * @param arr
     * @return
     */
    public static boolean canReorderDoubled(int[] arr) {
        Arrays.sort(arr);
        Map<Integer, Integer> map = new HashMap<>();
        for (int ar: arr) {
            Integer value = map.getOrDefault(ar, 0);
            map.put(ar, value + 1);
        }
        int num = 0, mid = 0;
        for (int i:arr) {
            Integer v = map.getOrDefault(i, 0);
            map.put(i , v - 1);
            if (v > 0) {
                if (i > 0) {
                    Integer v1 = map.getOrDefault(2 * i, 0);
                    if (v1 > 0) { num++; }
                    map.put(2 * i, v1 - 1);
                }else if (i == 0) {
                    mid++;
                }else {
                    if (i % 2 != 0) {
                        return false;
                    }
                    Integer v1 = map.getOrDefault(i / 2 ,0);
                    if (v1 > 0) { num++; }
                    map.put(i / 2, v1 - 1);
                }
            }
        }
        return 2 * num + mid == arr.length;
    }
}
