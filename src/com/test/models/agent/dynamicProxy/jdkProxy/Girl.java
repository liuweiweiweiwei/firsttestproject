package com.test.models.agent.dynamicProxy.jdkProxy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:02
 * @description
 */
public class Girl implements Person{


    @Override
    public void findLove() {
        System.out.println("女生寻找对象：高富帅！");
    }
}
