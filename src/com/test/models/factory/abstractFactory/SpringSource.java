package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:26
 * @description
 */
public class SpringSource implements ICourse {
    @Override
    public void operateCourse() {
        System.out.println("spring 的 课程操作！");
    }
}
