package com.test.thread.newlw.net;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 17:24
 */
public class HessionSericial implements IService {

    @Override
    public <T> byte[] read(T t) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        HessianOutput output = new HessianOutput(outputStream);
        try {
            output.writeObject(t);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    @Override
    public <T> T write(byte[] bytes, Class<T> tClass, String name) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        HessianInput input = new HessianInput(inputStream);
        try {
            return (T)input.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
