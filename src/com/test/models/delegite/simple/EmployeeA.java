package com.test.models.delegite.simple;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 16:36
 * @description
 */
public class EmployeeA implements IWorkers {
    @Override
    public void work() {
        System.out.println("我是员工！A：我擅长加密工作，执行commond！");
    }
}
