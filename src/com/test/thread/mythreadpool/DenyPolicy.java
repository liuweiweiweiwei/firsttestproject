package com.test.thread.mythreadpool;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 20:54
 * description DenyPolicy
 */
@FunctionalInterface
public interface DenyPolicy {
    void reject(Runnable runnable, RunnablePool runnablePool);

    class DiscardDenyPolicy implements DenyPolicy {
        @Override
        public void reject(Runnable runnable, RunnablePool runnablePool) {
            // to_do do last add queue
        }
    }

    class ExceptionDenyPolicy implements DenyPolicy {
        @Override
        public void reject(Runnable runnable, RunnablePool runnablePool) {
            throw new DenyPolicyException(runnable + " is happy error ! ");
        }
    }

    class DenyPolicyException extends RuntimeException{
        public DenyPolicyException(String message) {
            super(message);
        }
    }

    class RunnerDenyPolicy implements DenyPolicy {
        @Override
        public void reject(Runnable runnable, RunnablePool runnablePool) {
            if (!runnablePool.isShutdown()) {
                runnable.run();
            }
        }
    }
}
