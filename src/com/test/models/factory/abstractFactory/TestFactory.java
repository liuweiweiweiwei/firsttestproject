package com.test.models.factory.abstractFactory;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 13:29
 * @description
 */
public class TestFactory {

    public static void main(String[] args) {
        //方式一
        CourseFactory factory = new CourseFactory();
        IVideo beanByClass = factory.getBeanByClass(JavaVideo.class);
        System.out.println("方式一");
        beanByClass.operateVideo();
        //方拾贰
        ICourse beanCourseByName = factory.getBeanCourseByName("com.test.models.factory.abstractFactory.JavaCourse");
        System.out.println("方式二");
        beanCourseByName.operateCourse();
        //方式三
        System.out.println("方式三");
        IFactory iFactory = new SpringFactory();
        iFactory.createNode().operateNode();
    }
}
