package com.test.models.single;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 13:51
 * @description 懒汉史丹利
 */
public class LazySingle {

    private LazySingle(){}

    private static LazySingle LAZY_SINGLE = null;

    public static LazySingle getInstance(){
        if(null == LAZY_SINGLE) {
            synchronized (LazySingle.class) {
                if(LAZY_SINGLE == null) {
                    LAZY_SINGLE = new LazySingle();
                }
            }
        }
        return LAZY_SINGLE;
    }
}
