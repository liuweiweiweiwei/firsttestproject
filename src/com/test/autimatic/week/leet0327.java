package com.test.autimatic.week;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/3/27 10:32
 * @description
 */
public class leet0327 {

    public static void main(String[] args) {
        System.out.println(minDeletion(new int[]{2, 6, 2, 5, 8, 9, 7, 2, 2, 5, 6, 2, 2, 0, 6, 8, 7, 3, 9, 2, 1, 1, 3, 2, 6, 2, 4, 6, 5, 8, 4, 8, 7, 0, 4,
                8, 7, 8, 4, 1, 1, 4, 0, 1, 5, 7, 7, 5, 9, 7, 5, 5, 8, 6, 4, 3, 6, 5, 1, 6, 7, 6, 9, 9, 6, 8, 6, 0, 9, 5, 6, 7, 6, 9, 5, 5, 7, 3, 0, 0, 5, 5, 4, 8, 3, 9, 3,
                4, 1, 7, 9, 3, 1, 8, 8, 9, 1, 6, 0, 0}));
        System.out.println(findMinHeightTrees(4, new int[][]{{1, 0}, {1, 2}, {1, 3}}));
        System.out.println(reachingPoints(1, 1, 2, 2));
    }

    /**
     * 辗转相除
     *
     * @param sx x
     * @param sy y
     * @param tx tx
     * @param ty ty
     * @return isReaching
     */
    public static boolean reachingPoints(int sx, int sy, int tx, int ty) {
        while (sx < tx && sy < ty && tx != ty) {
            if (tx > ty) {
                tx %= ty;
            } else {
                ty %= tx;
            }
        }
        if (sx == tx && sy == ty) {
            return true;
        } else if (sx == tx && sy < ty) {
            return (ty - sy) % sx == 0;
        } else {
            return sy == ty && sx < tx && (tx - sx) % sy == 0;
        }
    }

    static class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    /**
     * 多查束层序遍历
     *
     * @param root 根节点
     * @return 集合数据
     */
    public static List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> listList = new ArrayList<>();
        if (root == null) {
            return listList;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            List<Integer> curList = new ArrayList<>();
            Queue<Node> curQueue = new LinkedList<>();
            while (!queue.isEmpty()) {
                Node poll = queue.poll();
                curList.add(poll.val);
                for (Node n : poll.children) {
                    curQueue.offer(n);
                }
            }
            listList.add(curList);
            queue = curQueue;
        }
        return listList;
    }

    static int minHeight = Integer.MAX_VALUE;
    static Set<Integer> minHeightTreesResult = new HashSet<>();

    /**
     * 310 最小高度树
     *
     * @param n     h
     * @param edges arr
     * @return height
     */
    public static List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> res = new ArrayList<>();
        /*如果只有一个节点，那么他就是最小高度树*/
        if (n == 1) {
            res.add(0);
            return res;
        }
        /*建立各个节点的出度表*/
        int[] degree = new int[n];
        /*建立图关系，在每个节点的list中存储相连节点*/
        List<List<Integer>> map = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            map.add(new ArrayList<>());
        }
        for (int[] edge : edges) {
            degree[edge[0]]++;
            degree[edge[1]]++;
            /*出度++*/
            map.get(edge[0]).add(edge[1]);
            /*添加相邻节点*/
            map.get(edge[1]).add(edge[0]);
        }
        /*建立队列*/
        Queue<Integer> queue = new LinkedList<>();
        /*把所有出度为1的节点，也就是叶子节点入队*/
        for (int i = 0; i < n; i++) {
            if (degree[i] == 1) {
                queue.offer(i);
            }
        }
        /*循环条件当然是经典的不空判断*/
        while (!queue.isEmpty()) {
            res = new ArrayList<>();
            /*这个地方注意，我们每层循环都要new一个新的结果集合，
            这样最后保存的就是最终的最小高度树了*/
            int size = queue.size();
            /*这是每一层的节点的数量*/
            for (int i = 0; i < size; i++) {
                int cur = queue.poll();
                res.add(cur);
                /*把当前节点加入结果集，不要有疑问，为什么当前只是叶子节点为什么要加入结果集呢?
                因为我们每次循环都会新建一个list，所以最后保存的就是最后一个状态下的叶子节点，
                这也是很多题解里面所说的剪掉叶子节点的部分，你可以想象一下图，每层遍历完，
                都会把该层（也就是叶子节点层）这一层从队列中移除掉，
                不就相当于把原来的图给剪掉一圈叶子节点，形成一个缩小的新的图吗*/
                List<Integer> neighbors = map.get(cur);
                /*这里就是经典的bfs了，把当前节点的相邻接点都拿出来，
                * 把它们的出度都减1，因为当前节点已经不存在了，所以，
                * 它的相邻节点们就有可能变成叶子节点*/
                for (int neighbor : neighbors) {
                    degree[neighbor]--;
                    if (degree[neighbor] == 1) {
                        /*如果是叶子节点我们就入队*/
                        queue.offer(neighbor);
                    }
                }
            }
        }
        return res;
        /*返回最后一次保存的list*/
    }

    public List<List<Integer>> findDifference(int[] nums1, int[] nums2) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> listF = new ArrayList<>();
        List<Integer> listS = new ArrayList<>();
        Set<Integer> setF = new HashSet<>();
        Set<Integer> setS = new HashSet<>();
        Arrays.stream(nums1).forEach(setF::add);
        Arrays.stream(nums2).forEach(setS::add);
        setF.forEach(n1 -> {
            if (!setS.contains(n1)) {
                listF.add(n1);
            }
        });
        setS.forEach(n2 -> {
            if (!setF.contains(n2)) {
                listS.add(n2);
            }
        });
        result.add(listF);
        result.add(listS);
        return result;
    }

    public static int minDeletion(int[] nums) {
        int result = 0;
        for (int i = 0; i < nums.length; i += 2) {
            while (i + 1 < nums.length && nums[i] == nums[i + 1]) {
                result++;
                i++;
            }
        }
        if ((nums.length - result) % 2 != 0) {
            return result + 1;
        }
        return result;
    }

    public long[] kthPalindrome(int[] queries, int intLength) {
        long[] result = new long[queries.length];
        int num = getnum(intLength);
        for (int i = 0; i < queries.length; i++) {
            if (queries[i] > num) {
                result[i] = -1;
            } else {
                result[i] = getLongNum(intLength % 2 == 0 ? 0 : 1, queries[i], num);
            }
        }
        return result;
    }

    /**
     * get num
     *
     * @param intLen l
     * @return n
     */
    private static int getnum(int intLen) {
        if (intLen == 1) {
            return 9;
        }
        int num = 9;
        int bei = intLen / 2;
        if (intLen % 2 == 0) {
            while (bei > 1) {
                num *= 10;
            }
        } else {
            while (bei >= 1) {
                num *= 10;
            }
        }
        return num;
    }

    static String[] re = {"00", "11", "22", "33", "44", "55", "66", "77", "88", "99"};
    static long[] ro = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    /**
     * get long num
     *
     * @return l
     */
    private static long getLongNum(int sin, int index, int num) {
        if (sin == 1) {
            return (long) index;
        }
        if (sin == 0) {
            long[] re = {11, 22, 33, 44, 55, 66, 77, 88, 99};
            return re[index - 1];
        }
        StringBuilder re = getLongStr(num, index, num / 90, sin);
        return Long.parseLong(re.toString());
    }

    private static StringBuilder getLongStr(int num, int index, int len, int sin) {
        StringBuilder builder = new StringBuilder();
        int startEnd = index / (num / 9) + 1;
        builder.append(startEnd);
        back(builder, ((long) index) % 10, len, sin);
        builder.append(startEnd);
        return builder;
    }

    private static void back(StringBuilder builder, long index, long size, int sin) {
        if (index <= 10) {
            builder.append(sin == 0 ? re[(int) index] : ro[(int) index]);
            return;
        }
        long num = index / size;
        builder.append(num);
        back(builder, index % size, size / 10, sin);
        builder.append(num);
    }
}
