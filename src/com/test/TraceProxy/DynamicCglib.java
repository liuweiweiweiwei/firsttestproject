package com.test.TraceProxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * cglib不仅可以代理接口还可以代理类sdk智能代理接口
 */
public class DynamicCglib {

    public static class RealService{
        private void sayHello(){
            System.out.println("hello !");
        }
    }
    static class InterDynamicCglib implements MethodInterceptor{
        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            System.out.println("enter method before:"+method.getName());
            Object result=methodProxy.invokeSuper(o,objects);
            System.out.println("leave method after:"+method.getName());
            return result;
        }
    }
    public static <T> T getProxy(Class<?> cls){
        Enhancer enhancer=new Enhancer();
        enhancer.setSuperclass(cls);
        enhancer.setCallback(new InterDynamicCglib());
        return (T)enhancer.create();
    }

    public static void main(String[] args) {
        RealService realService=getProxy(RealService.class);
        realService.sayHello();
    }
}
