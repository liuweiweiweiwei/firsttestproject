package com.test.autimatic;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.*;

/**
 * @author 25338
 * 通用知识
 */
public class CommonKnowledge {
    public static void main(String[] args) {
        //1.
        List<String> stringList= Arrays.asList("米的","佩奇","杰瑞","汤姆","米奇");
        unmodifiableList(stringList);
        //stringList.add("题目");
        stringList.forEach(System.out::println);
        //2
        List stringList1 = checkedList(stringList, String.class);
       // stringList1.add(15);
        //3
        List<String> stringList2 = Collections.synchronizedList(stringList);
        //5
        getUnEquals();
    }
    public static void getUnEquals(){
        int x0=0;
        int N=1001;
        for (int i = 0; i < N; i++) {
            x0=(x0^i);
            System.out.println(x0+"::::::");
        }

    }

}
