package com.test.autimatic.week;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/17 10:31
 * @description
 */
public class leet0417 {
    public static void main(String[] args) {
        System.out.println(digitSum("00000000", 3));
    }

    public static String digitSum(String s, int k) {
        String result = s;
        while (result.length() > k) {
            result = createNewDigit(result, k);
        }
        return result;
    }

    private static String createNewDigit(String s, int k) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i += k) {
            int curInt = 0;
            for (int j = i; j < k + i && j < s.length(); j++) {
                curInt += Integer.parseInt(s.charAt(j) + "");
            }
            result.append(String.valueOf(curInt));
        }
        return result.toString();
    }

    public static int minimumRounds(int[] tasks) {
        Map<Integer, Integer> taskMap = new HashMap<>();
        for (int i : tasks) {
            int value = taskMap.getOrDefault(i, 0);
            taskMap.put(i, value + 1);
        }
        if (taskMap.containsValue(1)) {
            return -1;
        }
        int result = 0;
        for (int k : taskMap.keySet()) {
            Integer val = taskMap.get(k);
            result += (val % 3 == 0 ? val / 3 : val / 3 + 1);
        }
        return result;
    }

    public static int maxTrailingZeros(int[][] grid) {
        int result = 0;
        int[][][] dp = new int[grid.length][grid[0].length][3];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                getZeroNum(grid[i][j], dp[i][j]);
            }
        }
        for (int i = 0; i < grid.length; i++) {
            int[] curArr = new int[3];
            for (int j = 0; j < grid[0].length; j++) {
                for (int k = 0; k < grid.length; k++) {
                    curArr[0] = dp[k][j][0];
                    curArr[1] = dp[k][j][1];
                    curArr[2] = dp[k][j][2];
                }
            }
            result = Math.max(result, curArr[2] + Math.min(curArr[0], curArr[1]));
        }
        return result;
    }

    /**
     * 计算0 数目
     * @param a
     * @param arrNum
     * @return
     */
    private static void getZeroNum(int a, int[] arrNum) {
        int curA = a;
        if (a % 10 == 0) {
            while (curA % 10 == 0) {
                curA /= 10;
                arrNum[2]++;
            }
        } else if (a % 5 == 0) {
            while (curA % 5 == 0) {
                curA /= 5;
                arrNum[1]++;
            }
        } else if (a % 2 == 0) {
            while (curA % 2 == 0) {
                curA /= 2;
                arrNum[0]++;
            }
        } 
    }
}
