package com.test.classes.myclassloader;

import com.test.MyLog.LwLog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author 25338
 * @date 2022/5/12 22:17
 * @since 1.0
 * description
 */
public class MyClassLoader extends ClassLoader{
    private static final Path DEFAULT_PATH = Paths.get("F:/file/java/");

    private final Path path;

    public MyClassLoader() {
        super();
        this.path = DEFAULT_PATH;
    }

    public MyClassLoader(String path) {
        this.path = Paths.get(path);
    }

    public MyClassLoader(ClassLoader parent, String path) {
        super(parent);
        this.path = Paths.get(path);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bytes = readClassByte(name);
        if (bytes == null || bytes.length == 0) {
            throw  new RuntimeException("get class file byte failed : " + name);
        }
        return this.defineClass(name, bytes, 0, bytes.length);
    }

    /**
     * 将 class 读入内存
     * @return bytes
     */
    private byte[] readClassByte(String name) {
        String classpath = name.substring(name.lastIndexOf(".") + 1);
        Path fullPath = path.resolve(Paths.get(classpath + ".class"));
        if (!fullPath.toFile().exists()) {
            throw new RuntimeException(name + " class not exit !");
        }
        try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            Files.copy(fullPath, outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            LwLog.errorInfo("readClassByte has IOException : " + e.getMessage());
        }
        return null;
    }

    @Override
    public String toString() {
        return "MyClassLoader{" + "path=" + path + '}';
    }
}
