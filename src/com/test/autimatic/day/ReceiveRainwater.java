package com.test.autimatic.day;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/4/1 21:14
 * @description 接雨水
 */
public class ReceiveRainwater {
    public static void main(String[] args) {
        System.out.println();

    }

    /**
     * 快速幂
     *
     * @param a 基础知识
     * @param b 数组
     * @return 幂次方
     */
    public static int counting(int a, int[] b) {
        int result = 1;
        for (int bb : b) {
            result = (int)((long)countBase(result, 10, 1337) * (long)countBase(a, bb, 1337) % 1337);
        }
        return result;
    }

    /**
     * base 快速幂
     *
     * @param a a
     * @param num num
     * @param mod mod
     * @return re
     */
    private static int countBase(int a, int num, int mod) {
        long temp = a % mod;
        int result = 1;
        while (num > 0) {
            if (num % 2 != 0) {
                result = (int)((long)result * temp % mod);
                num--;
            }
            num /= 2;
            temp = (temp  * temp ) % mod;
        }
        return result;
    }

    /**
     * 二分吃香蕉
     *
     * @param piles piles
     * @param h h
     * @return num
     */
    public static int minEatingSpeed(int[] piles, int h) {
        int left = 1;
        int right = max(piles);
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (eatFinish(piles, h, mid)) {
                right = mid - 1;
            }else {
                left = mid + 1;
            }
        }
        return left;
    }

    /**
     * 获取最大
     *
     * @param piles p
     * @return re
     */
    private static int max(int[] piles) {
        return Arrays.stream(piles).max().orElse(0);
    }

    /**
     * 是否能吃完
     *
     * @param piles p
     * @return re
     */
    private static boolean eatFinish(int[] piles, int time, int quantitEatenAtOneTime) {
        int timeResult = 0;
        for (int i = 0; i < piles.length; i++) {
            timeResult += (piles[i] / quantitEatenAtOneTime + (piles[i] % quantitEatenAtOneTime == 0 ? 0 : 1));
        }
        return time >= timeResult;
    }
}
