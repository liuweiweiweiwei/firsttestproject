package com.test.models.agent.dynamicProxy.jdkProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:04
 * @description jdk的动态代理InvocationHandle
 */
public class JdkMeiPo implements InvocationHandler {

    private Person person;

    public Object getInstance(Person person)throws Exception{
        this.person = person;
        //反射获取对象
        Class<? extends Person> aClass = this.person.getClass();
        return Proxy.newProxyInstance(aClass.getClassLoader(),aClass.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object invoke = method.invoke(this.person, args);
        after();
        return invoke;
    }

    public void before(){
        System.out.println("我是媒婆，我要给你找对象，已确定需求！");
        System.out.println("开始物色。。。。。");

    }

    public void after(){
        System.out.println("ok的话，准备彩礼！");
    }

}
