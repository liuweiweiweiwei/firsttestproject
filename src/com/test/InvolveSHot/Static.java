package com.test.InvolveSHot;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class Static {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<Date> dateClass=Date.class;
        String[][] strings=new String[1][2];
        Class<? extends String[][]> sClass=strings.getClass();

        try {
            Class<?> cla=Class.forName("java.util.HashMap");
            System.out.println(cla.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        List<String> stringList= Arrays.asList("肖勉","tom","jane","sine");
        Class<?> clas=stringList.getClass();
        System.out.println(clas.getModifiers()+";;;;;;");
       // System.out.println(clas.getAnnotation());注解
        for (Field f:clas.getDeclaredFields()
             ) {
            f.setAccessible(true);
            System.out.println(f.getModifiers());

            try {
                System.out.println(f.getName()+":"+f.get(stringList));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Class<?> cla=Integer.class;
        try {
            Method method=cla.getMethod("parseInt",new Class[]{ String.class});
            System.out.println(method.invoke(null,"123"));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<Integer,String> map= HashMap.class.newInstance();
        map.put(123,"tom");
        System.out.println(map.get(123));
        Constructor<StringBuilder> constructor=StringBuilder.class.getConstructor(new Class[]{int.class});
        StringBuilder stringBuilder=constructor.newInstance(100);
        System.out.println(stringBuilder.toString());
    }
}
