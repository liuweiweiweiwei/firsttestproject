package com.test.thread.newlw.tcpIp;

import com.test.Files.FileUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/27 22:45
 * @description
 */
public class SocketTest1 {

    static ExecutorService executorService = new ThreadPoolExecutor(4,4, 60,TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100),new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) throws IOException {
        //bio模型
        ServerSocket serverSocket = new ServerSocket(8012);
        while (true) {
            //连接阻塞
            Socket accept = serverSocket.accept();
//            new SocketTest.SocketAServer(accept).start();
            executorService.execute(new SocketTest.SocketAServer(accept));
            System.out.println("结束！");

        }
    }

}
