package com.test.java8;

import java.util.concurrent.TimeUnit;

public class ThreadMain {

    public static void main(String[] args) {
        ThreadOne<String> threadOne=new ThreadOne<>(5);
        new produce(threadOne).start();
        new receive(threadOne).start();
    }





    static class produce extends Thread{
        private ThreadOne<String> threadOne;

        produce(ThreadOne<String> threadOne){
            this.threadOne=threadOne;
        }

        @Override
        public void run() {
            int t=0;
            while (true){
                try {
                    threadOne.addStr(t+"");
                    t++;
                    System.out.println("队列新加入信息："+t);
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    static class receive extends Thread{
        private ThreadOne<String> threadOne;
        receive(ThreadOne<String> threadOne){
            this.threadOne=threadOne;
        }
        @Override
        public void run() {
            while (true){
                try {
                    String po=threadOne.remove();
                    System.out.println("队列删除信息："+po);
                    TimeUnit.SECONDS.sleep(8);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
