package com.test.models.build;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/19 10:55
 * @description
 */
public class BuildModel {
    private int age;
    private String hobby;
    private String name;
    public BuildModel() {
    }

    public BuildModel(BuildFactory buildFactory) {
        this.age = buildFactory.age;
        this.hobby = buildFactory.hobby;
        this.name = buildFactory.name;
    }

    @Override
    public String toString() {
        return "BuildModel{" +
                "age=" + age +
                ", hobby='" + hobby + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    static class BuildFactory{
        private int age;
        private String hobby;
        private String name;

        public BuildFactory(int age, String hobby, String name) {
            this.age = age;
            this.hobby = hobby;
            this.name = name;
        }

        public BuildFactory() { }

        public int getAge() {
            return age;
        }

        public BuildFactory setAge(int age) {
            this.age = age;
            return this;
        }

        public String getHobby() {
            return hobby;
        }

        public BuildFactory setHobby(String hobby) {
            this.hobby = hobby;
            return this;
        }

        public String getName() {
            return name;
        }

        public BuildFactory setName(String name) {
            this.name = name;
            return this;
        }

        public BuildModel build(){
            return new BuildModel(this);
        }
    }


}
