package com.test.models.agent.dynamicProxy.gpproxy;

import java.lang.reflect.Method;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 15:32
 * @description
 */
public interface LwInvocationHandle {

    Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
