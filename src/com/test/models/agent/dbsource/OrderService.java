package com.test.models.agent.dbsource;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 14:35
 * @description
 */
public class OrderService implements IOrderService {

    private OrderDao orderDao;

    public OrderService() {
        this.orderDao = new OrderDao();
    }

    @Override
    public int createOrder(Order order) {
        System.out.println("调用orderDao创建订单！");
        return orderDao.createOrder(order);
    }

    @Override
    public void dealOrderService(Order order) {
        System.out.println("调用dealOrder方法处理订单业务！");
        createOrder(order);
    }


}
