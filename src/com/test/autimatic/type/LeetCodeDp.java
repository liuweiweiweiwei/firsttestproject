package com.test.autimatic.type;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 */
public class LeetCodeDp {
    /*public static void main(String[] args) {
        //1.
        int[] num={7,8,8,10};
        boolean judge=stoneGame(num);
        System.out.println("最终胜利的是："+judge);
        //2.
        int[][] pathNet=new int[3][3];
        pathNet[0]=new int[]{1,3,1};
        pathNet[1]=new int[]{1,5,1};
        pathNet[2]=new int[]{4,2,1};
        int nuPath=getPath(pathNet);
        System.out.println("获取到的最终路径长为："+nuPath);
        //3.
        List<List<Integer>> lists=new ArrayList<>();
        List<Integer> list1=new ArrayList<>();
        list1.add(2);
        List<Integer> list2=new ArrayList<>();
        list2.add(3);
        list2.add(4);
        List<Integer> list3=new ArrayList<>();
        list3.add(6);
        list3.add(5);
        list3.add(7);
        List<Integer> list4=new ArrayList<>();
        list4.add(4);
        list4.add(1);
        list4.add(8);
        list4.add(3);
        lists.add(list1);
        lists.add(list2);
        lists.add(list3);
        lists.add(list4);
        int san=minimumTotal(lists);
        System.out.println("最终结果："+san);
        //3.
        int[] te={7,1,5,3,6,4};
        int end=maxProfit(te);
        System.out.println("输出："+end);
        //4.双指针第一题
        int[] leetTwoPointOneParam={1,8,6,2,5,4,8,3,7};
        int twoPointOneAnswer = maxArea(leetTwoPointOneParam);
        System.out.println("leetcode双指针第一题答案是："+twoPointOneAnswer);

    }*/
    /**
     * 亚历克斯和李用几堆石子在做游戏。偶数堆石子排成一行，每堆都有正整数颗石子 piles[i] 。
     游戏以谁手中的石子最多来决出胜负。石子的总数是奇数，所以没有平局。
     亚历克斯和李轮流进行，亚历克斯先开始。 每回合，玩家从行的开始或结束处取走整堆石头。 这种情况一直持续到没有更多的石子堆为止，此时手中石子最多的玩家获胜。
     假设亚历克斯和李都发挥出最佳水平，当亚历克斯赢得比赛时返回 true ，当李赢得比赛时返回 false 。
     *
     *
     *输入：[5,3,4,5]
     输出：true
     解释：
     亚历克斯先开始，只能拿前 5 颗或后 5 颗石子 。
     假设他取了前 5 颗，这一行就变成了 [3,4,5] 。
     如果李拿走前 3 颗，那么剩下的是 [4,5]，亚历克斯拿走后 5 颗赢得 10 分。
     如果李拿走后 5 颗，那么剩下的是 [3,4]，亚历克斯拿走后 4 颗赢得 9 分。
     这表明，取前 5 颗石子对亚历克斯来说是一个胜利的举动，所以我们返回 true 。
     */
    public static boolean stoneGame(int[] piles) {
        int i=0;
        int j=piles.length-1;
        int yakesi=0;
        int li=0;
        int[] dpo=new int[piles.length/2+1];
        int[] dpt=new int[piles.length/2+1];
        dpo[0]=0;
        dpt[0]=0;
        int m=2;
        while (i<=j){
            //计算i，+1，与j+j-1的数据以及，i+j的数据大小；
            dpo[m]=dpo[m-1]+piles[i]>=piles[j]?piles[i++]:piles[j--];


            yakesi+=piles[i]>=piles[j]?piles[i++]:piles[j--];
            li+=(piles[i]>=piles[j])?piles[i++]:piles[j--];
        }
        return yakesi>li?true:false;
    }


    /**
     * 给定一个包含非负整数的 m x n 网格，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
     说明：每次只能向下或者向右移动一步。
     示例:
     输入:
     [
     [1,3,1],
     [1,5,1],
     [4,2,1]
     ]
     输出: 7
     解释: 因为路径 1→3→1→1→1 的总和最小。
     */
    public static int getPath(int[][] net){
        //有两种情况1.贴近边缘，2.在内部
        int[][] dp=new int[net.length][net[0].length];
        for (int i = 0; i <net[0].length ; i++) {
            dp[0][i]=net[0][i];
            dp[i][0]=net[i][0];
        }
        for (int i = 1; i <net[0].length ; i++) {
            for (int j = 1; j <net.length ; j++) {
                if(i-1==0){
                    dp[0][j]=dp[i-1][j]+net[i-1][j];
                }else if(j-1==0){
                    dp[i][0]=dp[i][j-1]+net[i][0];
                }else{
                    dp[i][j]=Math.min(dp[i][j-1]+net[i][j],dp[i-1][j]+net[i][j]);
                }
            }
        }
        return dp[net.length-1][net[0].length-1];
    }

    /**
     * 三角形最小路径之和
     * @param triangle
     * @return
     *
     */
    public static int minimumTotal(List<List<Integer>> triangle) {
        if(triangle==null){
            return 0;
        }
        int[] dp=new int[triangle.size()+1];
        for (int i = triangle.size()-1; i >=0 ; i--) {
            for (int j = 0; j <i+1 ; j++) {
                dp[j]=Math.min(dp[j+1],dp[j])+triangle.get(i).get(j);
            }
        }
        return dp[0];
    }

    /**
     *  买卖股票的最好价格
     */
    public static int maxProfit(int[] prices) {
        int max=0;
        int min=0;
        int midmin=1;
        int midmax=1;
        for (int i = 0; i <prices.length ; i++) {
            if(prices[midmax]>prices[i]){
                max=prices[i];
                midmax=i;
            }else if(prices[midmin]<prices[i]){
                min=prices[i];
                midmin=i;
            }
        }
        return max-min;
    }

    /**
     * 最大面积
     * @param height 高度
     * @return 返回值
     */
    public static int maxArea(int[] height) {
        if(height.length<2)return 0;
        int max=0;
        int l=0;
        int r=height.length-1;
        while (r>l){
            if(height[l]>height[r]){
                max=Math.max(height[r]*(r-l),max);
                r--;
            }else{
                max=Math.max(height[l]*(r-l),max);
                l++;
            }
        }
        return max;
    }

    public static int nthUglyNumber(int n) {
        int[] dp=new int[n+1];dp[0]=1;
        int two=1;int three=1;int five=1;
        for (int i = 1; i <n ; i++) {
            int temTwo=two*2;int temThree=three*3;int temFive=five*5;
            dp[i]=Math.min(Math.min(temTwo,temThree),temFive);
            if(dp[i]==temTwo)two++;
            if(dp[i]==temThree)three++;
            if(dp[i]==temFive)five++;
        }
        return dp[n];
    }

    /**
     *
     * @param nums
     * @param queries
     * @return
     */
    public static int[] maximizeXor(int[] nums, int[][] queries) {
        Arrays.sort(nums);
        int numQ = queries.length;
        int[][] newQueries = new int[numQ][3];
        for (int i = 0; i < numQ; ++i) {
            newQueries[i][0] = queries[i][0];
            newQueries[i][1] = queries[i][1];
            newQueries[i][2] = i;
        }
        Arrays.sort(newQueries, new Comparator<int[]>() {
            @Override
            public int compare(int[] query1, int[] query2) {
                return query1[1] - query2[1];
            }
        });

        int[] ans = new int[numQ];
        Trie trie = new Trie();
        int idx = 0, n = nums.length;
        for (int[] query : newQueries) {
            int x = query[0], m = query[1], qid = query[2];
            while (idx < n && nums[idx] <= m) {
                trie.insert(nums[idx]);
                ++idx;
            }
            if (idx == 0) { // 字典树为空
                ans[qid] = -1;
            } else {
                ans[qid] = trie.getMaxXor(x);
            }
        }
        return ans;
    }
}

class Trie {
    static final int L = 30;
    Trie[] children = new Trie[2];

    public void insert(int val) {
        Trie node = this;
        for (int i = L - 1; i >= 0; --i) {
            int bit = (val >> i) & 1;
            if (node.children[bit] == null) {
                node.children[bit] = new Trie();
            }
            node = node.children[bit];
        }
    }

    public int getMaxXor(int val) {
        int ans = 0;
        Trie node = this;
        for (int i = L - 1; i >= 0; --i) {
            int bit = (val >> i) & 1;
            if (node.children[bit ^ 1] != null) {
                ans |= 1 << i;
                bit ^= 1;
            }
            node = node.children[bit];
        }
        return ans;
    }

    static class Tclass extends Thread{
        private int ind;
        private String name;
        private String fileName;

        public Tclass(int ind,String name,String filename){
            this.ind=ind;
            this.name=name;
            this.fileName=filename;
        }
        @Override
        public void run() {
            try {
            File file=new File(fileName);
            if(!file.exists()){
                file.createNewFile();
            }
            FileChannel fileChannel=new FileOutputStream(fileName).getChannel();
            for (int i = 0; i < 1000000; i++) {
               // System.out.println("运行："+name+":::"+get(i)+":::"+Thread.currentThread().getName());
                fileChannel.write(ByteBuffer.wrap(("" + get(i) + "|*|"+(i%20==0?"\n":"")).getBytes()));
            }
            fileChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int get(int index){
        return index+1;
    }

    public static ThreadPoolExecutor executor=new ThreadPoolExecutor(4,4,2, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100),new ThreadPoolExecutor.DiscardPolicy());
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            executor.execute(new Tclass(i,"name"+i,"E:\\writeEfile\\txt\\name"+i+".txt"));
        }
        executor.shutdown();
    }



}
