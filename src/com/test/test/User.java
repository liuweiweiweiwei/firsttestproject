package com.test.test;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/31 10:39
 * @description
 */
public class User extends Person{
    private String name;
    private int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public User(User user){
        setAge(user.getAge());
        setName(user.getName());
        setBody(user.getBody());
        setHead(user.getHead());
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
