package com.test.autimatic;

import com.alibaba.fastjson.support.springfox.SwaggerJsonSerializer;

import java.util.Stack;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/9 21:55
 * @description
 */
public class JiaJian {

    public static void main(String[] args) {
        System.out.println(calculate("3+2*2"));
    }

    public static int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        int num = 0;
        char c = '+';
        int index = 0;
        for (char isNum : s.toCharArray()) {
            index++;
            if(isNum == ' '){
                continue;
            }
            if(isDigit(isNum) || index == s.length()){
                if(index == s.length()){
                    num = num * 10 + (isNum - '0');
                }
                switch (c){
                    case '+':stack.push(num);break;
                    case '-':stack.push(-num);break;
                    case '*':
                        Integer pop = stack.pop();
                        stack.push(pop * num);
                        break;
                    case '/':
                        Integer pop1 = stack.pop();
                        stack.push(pop1 / num);
                        break;
                    default:break;
                }
                num = 0;
                c = isNum;
            }else{
                num = num * 10 + (isNum - '0');
            }

        }
        int result = 0;

        while (!stack.isEmpty()){
            result += stack.pop();
        }
        return result;
    }

    private static boolean isDigit(char c){
        return c == '+' || c == '-' || c == '*' || c == '/';
    }
}
