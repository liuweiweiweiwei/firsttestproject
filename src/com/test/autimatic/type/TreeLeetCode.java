package com.test.autimatic.type;

import com.ibm.mq.jms.admin.AP;
import com.sun.jmx.remote.internal.ArrayQueue;
import com.test.MyLog.LwLog;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/3 11:42
 * @description
 */
public class TreeLeetCode {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        TreeNode left = new TreeNode(1);
        TreeNode right = new TreeNode(8);
        TreeNode rightl = new TreeNode(7);
        TreeNode rightr = new TreeNode(9);
        TreeNode leftL = new TreeNode(3);
        TreeNode leftr = new TreeNode(2);
        TreeNode leftLl = new TreeNode(0);
        TreeNode leftLr = new TreeNode(6);
        root.left = left;root.right = right;left.left = leftL; left.right = leftr;
        right.left = rightl; right.right= rightr; leftL.right = leftLr;
//        leftL.left = leftLl;
        System.out.println(isValidBST(root));
        LwLog.splitLogInfo("分割线");
        getRootVal(root, new LinkedList<>());
        LwLog.splitLogInfo("分割线");
    }

    public class Codec {

        // Encodes a tree to a single string.
        public String serialize(TreeNode root) {
            StringBuilder builder = new StringBuilder();
            serializeStr(root, builder);
            return builder.toString().substring(0,builder.length());
        }

        private void serializeStr(TreeNode treeNode, StringBuilder string) {
            if (treeNode == null) {
                return;
            }
            string.append(treeNode.val).append(",");
            serializeStr(treeNode.left, string);
            serializeStr(treeNode.right, string);
        }

        // Decodes your encoded data to tree.
        public TreeNode deserialize(String data) {
            String[] strings = data.split(",");
            return deserializeList(new ArrayList<>(Arrays.asList(strings)));
        }

        private TreeNode deserializeList(List<String> str) {
            if (str.size() == 0) {
                return null;
            }
            TreeNode root = new TreeNode(Integer.parseInt(str.remove(0)));
            root.left = deserializeList(str);
            root.right= deserializeList(str);
            return root;
        }
    }

    public static List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
        List<Integer> result = new ArrayList<>();
        Queue<Integer> queue1 = new LinkedList<>();
        Queue<Integer> queue2 = new LinkedList<>();
        getRootVal(root1,queue1);
        getRootVal(root2,queue2);
        while (!queue1.isEmpty() && !queue2.isEmpty()) {
            result.add(queue1.peek() > queue2.peek() ? queue2.poll() : queue1.poll());
        }
        while (queue1.isEmpty() && !queue2.isEmpty()) {
            result.add(queue2.poll());
        }
        while (!queue1.isEmpty() && queue2.isEmpty()) {
            result.add(queue1.poll());
        }
        return result;
    }

    private static void getRootVal(TreeNode root, Queue<Integer> integerQueue) {
        if (root == null) {
            return;
        }
        getRootVal(root.left, integerQueue);
        integerQueue.add(root.val);
        getRootVal(root.right, integerQueue);
        System.out.println(root.val);
    }

    public static String tree2str(TreeNode root) {
        if (root == null) { return ""; }
        StringBuilder builder = new StringBuilder();
        builder.append(root.val);
        getTreeString(root, builder);
        return builder.toString().substring(String.valueOf(root.val).length() + 1, builder.length() - 1);
    }

    private static void getTreeString(TreeNode treeNode, StringBuilder result) {
        if (treeNode == null) {
            return;
        }
        result.append('(').append(treeNode.val);
        getTreeString(treeNode.left, result);
        if (treeNode.left == null && treeNode.right != null) {
            result.append("()");
        }
        getTreeString(treeNode.right, result);
        result.append(')');
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() { }
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static boolean isValidBST(TreeNode root) {
        return isValidTemp(root);
    }

    private static boolean isValidTemp(TreeNode root) {
        if(root == null){ return true; }
        if(root.left != null) {
            if (root.left.val >= root.val) {
                return false;
            }
            if (root.left.left != null && root.left.left.val >= root.left.val) {
                return false;
            }
            if(root.left.right != null ){
                if (root.left.right.val <= root.left.val || root.left.right.val >= root.val){
                    return false;
                }
                if(root.left.right.right != null && (root.val <= root.left.right.right.val)){
                    return false;
                }
            }

        }
        if(root.right != null) {
            if (root.right.val <= root.val) {
                return false;
            }
            if (root.right.right != null && root.right.right.val <= root.right.val) {
                return false;
            }
            if(root.right.left != null){
               if(root.right.left.val >= root.right.val || root.right.left.val <= root.val){
                    return false;
                }
                if(root.right.left.left != null && (root.val >= root.right.left.left.val)){
                   return false;
                }
            }
        }
        return isValidTemp(root.left) && isValidTemp(root.right);
    }




    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p != null && q != null) {
            if(p.val != q.val){
                return false;
            }
            return isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
        } else {
            return false;
        }
    }

}
