package com.test.models.WeightFly;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/20 16:16
 * @description
 */
public class FlyWeightFactory {

    private final static Map<String,Ticket> ticketMap=new ConcurrentHashMap<>();
    private static final String TAG="FLY_WEIGHT_FACTORY";

    public static Ticket getTicket(String from,String to){
        String key=from+"_"+to;
        if(ticketMap.containsKey(key)){
            System.out.println("缓存数据！");
            return ticketMap.get(key);
        }else{
            System.out.println("新建数据！");
            Ticket ticket=new ContextTicket(from,to,0);
            ticketMap.put(key,ticket);
            return ticket;
        }
    }
}
