package com.test.reflect;

import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/8 22:42
 * @description
 */
public class ArrayReflect<E> {
    private Object[] elementData;
    private final int DEFAULT_SIZE = 2;
    private int size;

    public ArrayReflect() {
        elementData = new Object[DEFAULT_SIZE];
    }

    public void add(E e) {
        if (++size >= elementData.length) {
            int len = (int)(elementData.length * 1.5);
            elementData = Arrays.copyOf(elementData, len);
        }
        elementData[size - 1] = e;
    }

    @Override
    public String toString() {
        return "ArrayReflect{" + "elementData=" + Arrays.toString(elementData)
                + ", DEFAULT_SIZE=" + DEFAULT_SIZE + ", size=" + size + '}';
    }

    public static void main(String[] args) {
        ArrayReflect<Integer> arrayReflect = new ArrayReflect<>();
        arrayReflect.add(1);
        arrayReflect.add(2);
        arrayReflect.add(3);
        Integer[] integers = arrayReflect.toArray(Integer.class);
        System.out.println(Arrays.toString(integers));
        System.out.println(arrayReflect.toString());
    }

    public E[] toArray(Class<E> eClass) {
        Object instance = Array.newInstance(eClass, size);
        System.arraycopy(elementData, 0, instance, 0, size);
        return (E[])instance;
    }
}
