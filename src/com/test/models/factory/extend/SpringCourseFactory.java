package com.test.models.factory.extend;


import com.test.models.factory.MyCourse;
import com.test.models.factory.SpringCourse;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/6 10:58
 * @description
 */
public class SpringCourseFactory implements ICourseFactory{
    @Override
    public MyCourse createFactory() {
        return new SpringCourse();
    }
}
