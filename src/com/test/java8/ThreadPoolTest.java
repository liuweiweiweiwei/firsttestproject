package com.test.java8;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolTest {
    public static void main(String[] args) {
        //初始化线程池
        ThreadPoolExecutor executor=new ThreadPoolExecutor(1,2,2, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(2),new ThreadPoolExecutor.DiscardPolicy());
        //测试1 AbortPolicy,将队列和池内执行完毕，继续增加抛出异常
        // 2 CallerRunsPolicy 将任务放入池，池满放入等待队列然后执行下一个进入线程，
        // 3 DiscardOldestPolicy 4 DiscardPolicy
        for (int i = 0; i < 7; i++) {
            System.out.println("开始执行任务。。。"+i);
            executor.execute(new TestThread("任务"+i));
            Iterator iterator=executor.getQueue().iterator();
            while (iterator.hasNext()){
                TestThread thread=(TestThread)iterator.next();
                System.out.println("队列中任务："+thread.getName());
            }
        }



    }

}
