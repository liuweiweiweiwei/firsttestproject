package com.test.classes;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @author 25338
 * @date 2022/5/10 22:46
 * @since 1.0
 * description
 */
public class ThreadClass {
    static {
        try {
            System.out.println("the threadclass static code will be invoke!");
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        IntStream.range(0,5).forEach(i -> new Thread(ThreadClass::new));
    }
}
