package com.test.models.Strategy.payStrategy;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 18:54
 * @description 支付抽象
 */
public abstract class Pay {

    public abstract String getName();

    protected abstract double queryBalance(String uid);

    public MsgResult pay(String id,double amount){
        if(queryBalance(id) < amount){
            return new MsgResult(500,"支付失败","余额不足！");
        }else {
            return new MsgResult(200,"支付成功！","交易完成！");
        }
    }
}
