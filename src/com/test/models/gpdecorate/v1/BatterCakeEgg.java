package com.test.models.gpdecorate.v1;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:16
 * @description
 */
public class BatterCakeEgg extends BatterCake {

    @Override
    protected String name() {
        return "一个鸡蛋煎饼！";
    }

    @Override
    protected int price() {
        System.out.println("加个鸡蛋加了一块钱");
        return super.price() + 1;
    }
}
