package com.test.models.delegite.mvc;

import com.test.models.delegite.mvc.controller.AController;
import com.test.models.delegite.mvc.controller.BController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 16:45
 * @description
 */
public class DispatcherServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //完成调度
        doDispatch(req,resp);
    }

    private void doDispatch(HttpServletRequest req, HttpServletResponse resp) {
        String uri = req.getRequestURI();
        String mid = req.getParameter("mid");

        if("aWord".equals(uri)){
            AController aController = new AController();
            aController.aWord();
        }else if("bWork".equals(uri)){
            BController bController = new BController();
            bController.bWord();
        }
        //......

    }
}
