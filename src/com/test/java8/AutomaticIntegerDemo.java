package com.test.java8;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AutomaticIntegerDemo {
    private static AtomicInteger atomicInteger=new AtomicInteger(0);
    static class Visit extends Thread{
        @Override
        public void run() {
            for (int i = 0; i < 1000; i++) {
                atomicInteger.getAndIncrement();
            }
        }
    }

    /*public static void main(String[] args) throws InterruptedException {
        Thread[] threads=new Thread[100];
        for (int i = 0; i <100 ; i++) {
            threads[i]=new Visit();
            threads[i].start();
            threads[i].join();
        }
        *//*for (int i = 0; i <100 ; i++) {
            threads[i].join();
        }*//*
        System.out.println(atomicInteger.get());
    }*/

    static class Accounter{
        private Lock lock=new ReentrantLock();
        private volatile double money=0L;

        public Accounter(double money){
            this.money=money;
        }
        public void addMoney(double incre){
            lock.lock();
            try {
                money+=incre;
            }finally {
                lock.unlock();
            }
        }
        public double getMoney(){
            return money;
        }
        public void reduceMoney(double rmoney){
            lock.lock();
            try {
                money-=rmoney;
            }finally {
                lock.unlock();
            }
        }
        public boolean getLock(){
            return lock.tryLock();
        }
        public boolean getLockCon(TimeUnit unit,int num){
            try {
                return lock.tryLock(num,unit);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        }
        void setLock(){
            lock.lock();
        }
        void setLockDown(){
            lock.unlock();
        }
    }
    static class AccountMon{
        public static class NoSuchEnoughMonry extends Exception{}
        public static boolean tryTranFer(Accounter form,Accounter to,double moneys) throws NoSuchEnoughMonry {
            if (form.getLock()) {
                try {
                    if(to.getLock()) {
                        try {
                            if (form.getMoney() >= moneys) {
                                form.reduceMoney(moneys);
                                to.addMoney(moneys);
                            } else {
                                throw new NoSuchEnoughMonry();
                            }
                            return true;
                        } finally {
                            to.setLockDown();
                        }
                    }
                } finally {
                    form.setLockDown();
                }
            }
            return false;
        }
        public static void Tranfer(Accounter from,Accounter to,double money) throws NoSuchEnoughMonry {
            boolean success=false;
            do{
                success=tryTranFer(from, to, money);
                if(!success){
                    Thread.yield();
                }
            }while (!success);
        }
    }

    public static void main(String[] args) {
        Random random=new Random();
        Accounter[] accounters=new Accounter[10];
        Thread[] threads=new Thread[100];
        double nu=0;
        for (int i = 0; i <10 ; i++) {//创建10个用户
            accounters[i]=new Accounter(random.nextInt(100000));
            System.out.println(accounters[i].getMoney());
            nu+=accounters[i].getMoney();
        }
        System.out.println("总数量："+nu);
        //-------------------------------------------
        System.out.println("结束后。。。。。。。。。。。。。。。");
        //-------------------------------------------
        for (int i = 0; i < 100; i++) {
            threads[i]=new Thread(){
                @Override
                public void run() {
                    try {
                        int from=random.nextInt(10);
                        int to=random.nextInt(10);
                        if(from!=to) {
                           // System.out.println(accounters[from].getMoney()+";"+accounters[to].getMoney());
                            AccountMon.Tranfer(accounters[from], accounters[to], 10.5);
                        }
                    } catch (AccountMon.NoSuchEnoughMonry noSuchEnoughMonry) {
                        noSuchEnoughMonry.printStackTrace();
                    }
                }
            };
            threads[i].start();
        }
        double num=0;
        for (int i = 0; i < 10; i++) {
            System.out.println(accounters[i].getMoney());
            num+=accounters[i].getMoney();
        }
        System.out.println("总数："+num);
    }


}
