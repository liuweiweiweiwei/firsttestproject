package com.test.redis.pubsub;

import redis.clients.jedis.Jedis;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/31 14:16
 */
public class ListenerTest {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.175.1.196",6379);
        final JedisListenerPubSub pubSub = new JedisListenerPubSub();
        //回阻塞
        jedis.subscribe(pubSub,new String[]{"tw*"});
    }

}
