package com.test.models.delegite.simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 16:34
 * @description
 */
public class Leader {

    private Map<String,IWorkers> workersMap = new HashMap<>();

    public Leader() {
        workersMap.put("A",new EmployeeA());
        workersMap.put("B",new EmployeeB());
    }

    public void word(String commond){
        System.out.println("lead的接收领导力--->干活！");
        workersMap.get(commond).work();
    }
}
