package com.test.models.YuanXing;

import java.io.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 14:38
 * @description 原型模式
 */
public class CopyClone implements ProtoType,Serializable{

    private String name;
    private int age;
    private LocalDateTime time;
    private List<String> hobbys;

    public List<String> getHobbys() {
        return hobbys;
    }

    public void setHobbys(List<String> hobbys) {
        this.hobbys = hobbys;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "CopyClone{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", time=" + time +
                ", hobbys=" + hobbys +
                '}';
    }

    /**
     * 申客隆
     * @return
     */
    @Override
    public Object clone(){
        try {
            //读入数据
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(this);
            //写数据
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            CopyClone object = (CopyClone) objectInputStream.readObject();
            return object;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ProtoType clone1() {
        CopyClone copyClone = new CopyClone();
        copyClone.setAge(this.getAge());
        copyClone.setName(this.getName());
        copyClone.setTime(this.getTime());
        copyClone.setHobbys(this.getHobbys());
        return copyClone;
    }
}
