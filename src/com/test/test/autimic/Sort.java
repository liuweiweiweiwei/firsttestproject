package com.test.test.autimic;

import java.util.Arrays;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/8 13:41
 * @description
 */
public class Sort {

    public static void main(String[] args) {
        int[] arr = {2,3,1,4,2,3,1,6,7,8,9,5,4,12,3,4,453,32,34,23,0,45,23,1,23,4,25,43,545,34,34,6,345,4,523,4,23,4,23,4,2,5,43,23,433,4,64,56,5,6,78,9,5,6};
        System.out.println(Arrays.toString(arr));
        threeQuickSort(arr,0,arr.length-1);
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println("------------next------------");
        int[] arr1 = {2,3,1,4,2,3,1,6,7,8,9,5,4,12,3,4,453,32,34,23,0,45,23,1,23,4,25,43,545,34,34,6,345,4,523,4,23,4,23,4,2,5,43,23,433,4,64,56,5,6,78,9,5,6};
        mergeSort(arr1,0,arr1.length - 1,new int[arr1.length]);
        System.out.println(Arrays.toString(arr1));
        System.out.println("------------next------------");
        int[] arr2 = {2,3,1,4,2,3,1,6,7,8,9,5,4,12,3,4,453,32,34,23,0,45,23,1,23,4,25,43,545,34,34,6,345,4,523,4,23,4,23,4,2,5,43,23,433,4,64,56,5,6,78,9,5,6};
        heapSort(arr2);
        System.out.println(Arrays.toString(arr2));
        System.out.println("------------next------------");
    }


    /**
     * 快速排序
     * @param arr
     */
    public static void threeQuickSort(int[] arr, int i, int j){
        if(i >= j){return;}
        //声明起始位置---以及标准值
        int start = i + 1;int end =j;int mid = i;int stand = arr[mid];
        //当start == end的时候还有此时的数据没有交换 === 交换到最后 i，mid - 1；end + 1，j；
        while (start <= end){
            if(arr[start] > stand){
                swap(arr, start, end--);
            }else if(arr[start] < stand){
                swap(arr, start++, mid++);
            }else{
                start++;
            }
        }
        threeQuickSort(arr, i, mid - 1);
        threeQuickSort(arr, end + 1, j);
    }

    /**
     * 归并排序
     */
    public static void mergeSort(int[] arr, int i, int j,int[] tempArr){
        if(i >= j){return;}
        int mid = i + (j - i)/2;
        mergeSort(arr, i, mid, tempArr);
        mergeSort(arr, mid + 1, j, tempArr);
        int start = i;int end = mid + 1;int index = i;
        System.arraycopy(arr,i,tempArr,i,j - i + 1);
        while (index <= j){
            if(start > mid){
                arr[index++] = tempArr[end++];
            }else if(end > j){
                arr[index++] = tempArr[start++];
            }else if(tempArr[start] > tempArr[end]){
                arr[index++] = tempArr[end++];
            }else{
                arr[index++] = tempArr[start++];
            }
        }
    }

    public static void heapSort(int[] arr){
        //上浮-构造大项堆
        for (int i = 1; i < arr.length; i++) {
            swim(arr, i);
        }
        //下沉--交换
        int t = arr.length - 1;
        while (t > 0){
            swap(arr, t, 0);
            sink(arr, 0, --t);
        }
    }

    /**
     * 上浮操作
     * @param arr
     * @param index
     */
    private static void swim(int[] arr,int index){
        while (index >= 0){
            int tempHead = (index & 1) == 1 ? (index - 1)/2 : (index - 2)/2;
            if(tempHead < 0 || arr[tempHead] > arr[index]){break;}
            swap(arr,index,tempHead);
            index = tempHead;
        }
    }

    /**
     * 下沉操作
     * @param arr
     * @param index
     * @param len
     */
    private static void sink(int[] arr,int index, int len){
        int temp;
        while ((temp = (2 * index + 1)) < len){
            if(temp + 1 < len && arr[temp] < arr[temp + 1]){temp++;}
            if(arr[index] > arr[temp]){break;}
            swap(arr, index, temp);
            index = temp;
        }
    }


    /**
     * 交換
     * @param arr
     * @param a
     * @param b
     */
    private static void swap(int[] arr,int a, int b){
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }



}
