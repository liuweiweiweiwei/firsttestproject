package com.test.thread.mythreadpool;

import com.test.MyLog.LwLog;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/9 22:33
 * @description
 */
public class TestThreadPool {
    public static void main(String[] args) throws InterruptedException {
        final RunnablePool pool = new BasicThreadPool(
                2, Runtime.getRuntime().availableProcessors(), 6,100);
        for (int i = 0; i < 20; i++) {
            pool.execute(() -> {
                try {
                    TimeUnit.SECONDS.sleep(10);
                    System.out.println(Thread.currentThread().getName() + " is running!");
                } catch (InterruptedException e) {
                    LwLog.errorInfo(e.getMessage() + " : " + Thread.currentThread().getName());
                }
            });
        }
        for (;;) {
            TimeUnit.SECONDS.sleep(8);
            LwLog.logInfo(String.format(Locale.ROOT, "activitySize : %s" , pool.getActivitySize()));
            LwLog.logInfo(String.format(Locale.ROOT, "queueSize : %s" , pool.getQueueSize()));
            LwLog.logInfo(String.format(Locale.ROOT, "coreSize : %s", pool.getCoreSize()));
            LwLog.logInfo(String.format(Locale.ROOT, "maxSize : %s", pool.getMaxSize()));
        }
    }
}
