package com.test.models.gpdecorate.v2;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:18
 * @description
 */
public class BatterCakeSauger extends BatterCake {

    private BatterCake batterCake;

    public BatterCakeSauger(BatterCake batterCake) {
        this.batterCake = batterCake;
    }
    @Override
    protected String name() {
        return "加了个香肠的煎饼！";
    }

    @Override
    protected int price() {
        return batterCake.price() + 2;
    }
}
