package com.test.autimatic;

import org.apache.poi.ss.formula.functions.T;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/5 10:26
 * @description
 */
public class LeetCodeEight {
    public static void main(String[] args) {
        System.out.println(getMaximumGold(new int[][]{{0, 6, 0}, {5, 8, 7}, {0, 9, 0}}));
        System.out.println(sumOfUnique(new int[]{1, 2, 3, 2}));
        System.out.println(Arrays.toString(sortEvenOdd(new int[]{4, 1, 2, 3})));
        System.out.println(smallestNumber(310));
        System.out.println(Integer.toBinaryString(3 & 1023));
        System.out.println(minimumDifference(new int[]{87063, 61094, 44530, 21297, 95857, 93551, 9918}, 6));
        System.out.println(minimumOperations(new int[]{1,1}));
        System.out.println(minimumRemoval(new int[]{1,5,6,4}));
        System.out.println(countPairs(new int[]{5,5,9,2,5,5,9,2,2,5,5,6,2,2,5,2,5,4,3},7));
        System.out.println(goodDaysToRobBank(new int[]{5,3,3,3,5,6,2},2));
        System.out.println(digArtifacts(6,new int[][]{{0,2,0,5},{0,1,1,1},{3,0,3,3},{4,4,4,4},{2,1,2,4}},
        new int[][]{{0,2},{0,3},{0,4},{2,0},{2,1},{2,2},{2,5},{3,0},{3,1},{3,3},{3,4},{4,0},{4,3},{4,5},{5,0},{5,1},{5,2},{5,4},{5,5}}));

        int[] arr = {52,98,7,10,27,1,33,17,14,70,79,41,37,83,58,69,52,14,66,7,36,32,39,69,65,64,45,90,34,68,44,51,36,49,71,54,63,76,73,76,67,26,54,76,89,92,89,69,26,55,93,89,15,3,54,91,21,93,78,29,79,59,14,80,70,29,5,80,93,69,29,22,3,6,2,36,31,3,22,96,32,25,97,82,78,10,83,46,98,30,1,93,89};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(maximumTop(new int[]{
                62,35,90,68,67,70,25,27,13,87,88,47,98,65,94,42,17,56,67,30,53,32,89,97,64,26,98,20,71,19,49,7,17,15,79,9,77,87,14,91,4,54,6,94,29,40,44,0,99,57,49,19,95,12,77,67,40,77,96,22,68,19,36,91,31,31,6,42,15,46,31,71,49,56,47,70
        },17));

        System.out.println("count"+countCollisions("SSRSSRLLRSLLRSRSSRLRRRRLLRRLSSRR"));
        System.out.println(Arrays.toString(maximumBobPoints(9, new int[]{1,1,0,1,0,0,2,1,0,1,2,0})));
    }

    public static int[] maximumBobPoints(int numArrows, int[] aliceArrows) {
        IntPoint[] intPoints = new IntPoint[12];
        for (int i = 0; i < 12; i++) {
            intPoints[i] = new IntPoint(i, aliceArrows[i]);
        }
        Arrays.sort(intPoints, (o1, o2) -> {
            if (o1.num != o2.num) {
                return o2.num - o1.num;
            }
            return o2.index - o1.index;
        });
        int[] result = new int[12];
        int num = numArrows;
        for (IntPoint intPoint: intPoints) {
            if (num <= intPoint.num) {
                result[intPoint.index] = num;
                break;
            }else {
                result[intPoint.index] = intPoint.num + 1;
                num -= intPoint.num + 1;
            }
        }
        for (int i = 11; i > 0; i--) {
            if (num < result[i]) {
                result[i] = num;
                break;
            }else {
                num -= result[i];
            }
        }
        return result;
    }

    static class IntPoint{
        private int index;
        private int num;

        public IntPoint() {
        }

        public IntPoint(int index, int num) {
            this.index = index;
            this.num = num;
        }
    }

    public static int countCollisions(String directions) {
        int result = 0;
        int end = directions.length() - 1,start = 0;
        for (int i = directions.length() - 1; i >= 0 ; i--) {
            if (directions.charAt(i) == 'R'){
                end--;
            }else {
                break;
            }
        }
        for (int i = 0; i < directions.length() ; i++) {
            if (directions.charAt(i) == 'L'){
                start++;
            }else {
                break;
            }
        }
        directions = directions.substring(start,end + 1);
        for (int i = 1; i < directions.length(); i++) {
            if (directions.charAt(i) != 'S') {
                result++;
            }
        }
        return result;
    }

    public static int countHillValley(int[] nums) {
        int result = 0;
        int pre = nums[0];
        for (int i = 1; i < nums.length -1; i++) {
            if (nums[i] == nums[i + 1]) {
                continue;
            }
            if ((nums[i] > pre && nums[i] > nums[i + 1]) || (nums[i] < pre && nums[i] < nums[i + 1])) {
                result++;
                pre = nums[i];
            }
        }
//        result = nums[nums.length - 1] == nums[nums.length - 2] ? result : result + 1;
        return result;
    }

    public static int maximumTop(int[] nums, int k) {
        if (k == 0 || (k == 1 && nums.length <=1)) {
            return -1;
        }
        if (k == 1) {
            return nums[1];
        }
        Map<Integer,Integer> map = new HashMap<>(16);
        for (int key:nums) {
            Integer val = map.getOrDefault(key, 0);
            map.put(key, val + 1);
        }
        int max = -1;
        int end = Math.min(k - 1, nums.length);
        for (int i = 0; i < end; i++) {
            Integer integer = map.get(nums[i]);
            if (integer == 1) {
                map.remove(nums[i]);
                max = Math.max(max, nums[i]);
            }else {
                map.put(nums[i], integer - 1);
            }
        }
        if (k < nums.length) {max = Math.max(nums[k], max);}
        int time = k;
        for (int i = 0; i < end; i++) {
            if ( nums[i] > max) {
//                if (time % 2 == 1) {
                    max = nums[i];
//                }
            }
            time--;
        }
        return max;
    }

    public static int digArtifacts(int n, int[][] artifacts, int[][] dig) {
        HashSet<String> set = new HashSet<>(16);
        for (int[] digArr : dig) {
            set.add(digArr[0] + ":" + digArr[1]);
        }
        int result = 0;
        for (int[] arti : artifacts) {
            if (isCon(arti, set)) {
                result++;
            }
        }
        return result;
    }

    private static boolean isCon(int[] arti, HashSet<String> set){
        int rowM = Math.max(arti[0], arti[2]);
        int row = Math.min(arti[0], arti[2]);
        int colM = Math.max(arti[1], arti[3]);
        int col = Math.min(arti[1], arti[3]);
        for (int i = row; i <= rowM; i++) {
            for (int j = col; j <= colM; j++) {
                if (!set.contains(i + ":" + j)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static List<Integer> findKDistantIndices(int[] nums, int key, int k) {
        List<Integer> result = new ArrayList<>();
        int tempEnd = 0;
        for (int i = 0; i < nums.length && tempEnd < nums.length; i++) {
            if (nums[i] == key) {
                int start = Math.max(tempEnd,i - k);
                int end = Math.min(nums.length, i + k);
                while (start < end) {
                    result.add(start++);
                }
                tempEnd = end;
            }
        }
        return result;
    }

    public static List<Integer> goodDaysToRobBank(int[] security, int time) {
        List<Integer> result = new ArrayList<>();
        if (time == 0) {
            for (int i = 0; i < security.length; i++) {
                result.add(i);
            }
            return result;
        }
        int[] dp1 = new int[security.length + 1];
        int[] dp2 = new int[security.length + 1];
        for (int i = 1; i < security.length; i++) {
            if (security[i] <= security[i - 1]) {
                dp1[i] = dp1[i - 1] + 1;
            }
            if (security[security.length - i] >= security[security.length - i - 1]) {
                dp2[security.length - i - 1] = dp2[security.length - i] + 1;
            }
        }
        for (int i = time - 1; i < dp1.length; i++) {
            if (dp1[i] >= time && dp2[i] >= time) {
                result.add(i);
            }
        }
        return result;
    }

    public static int countPairs(int[] nums, int k) {
        Map<Integer,List<Integer>> listMap = new HashMap<>(16);
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            List<Integer> listsTemp = listMap.getOrDefault(nums[i], new ArrayList<>(16));
            if (listsTemp.size() > 0) {
                if (i % k == 0) {
                    result += listsTemp.size();
                } else{
                    for (int lt : listsTemp) {
                        if (lt * i % k == 0) {
                            result += 1;
                        }
                    }
                }
            }
            listsTemp.add(i);
            listMap.put(nums[i],listsTemp);
        }
        return result;
    }

    public static boolean isOneBitCharacter(int[] bits) {
        for (int i = 0; i < bits.length; ) {
            if (i == bits.length - 1) {
                return true;
            }
            i = bits[i] == 1 ? i + 2 : i + 1;
        }
        return false;
    }

    public static List<Integer> pancakeSort(int[] arr) {
        List<Integer> result = new ArrayList<>();
        for (int i = arr.length - 1, index = arr.length; i > 0; i--,index--) {
            if (arr[i] == index) {
                continue;
            }
            int temp = -1;
            for (int j = i; j >= 0; j--) {
                if (arr[j] == index) {
                    temp = j + 1;
                    break;
                }
            }
            swap(arr, temp);
            swap(arr, index);
            if (temp != 1) {result.add(temp);}
            result.add(index);
        }
        return result;
    }

    private static void swap(int[] arr, int end) {
        for (int i = 0,j = end - 1; i < j; i++, j--) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }

    public int findCenter(int[][] edges) {
        Set<Integer> result = new HashSet<>(16);
        for (int[] temp1 : edges) {
            if (result.contains(temp1[0])) {
                return temp1[0];
            }
            if (result.contains(temp1[1])) {
                return temp1[1];
            }
            result.add(temp1[0]);
            result.add(temp1[1]);
        }
        return -1;
    }

    



    public static List<Integer> luckyNumbers (int[][] matrix) {
        List<Integer> list = new ArrayList<>(16);
        Set<Integer> rowMax = new HashSet<>();
        for (int[] team : matrix) {
            int max = 1000001;
            for (int i : team) {
                max = Math.min(max, i);
            }
            rowMax.add(max);
        }
        for (int i = 0; i < matrix[0].length; i++) {
            int min = -1;
            for (int[] aMatrix : matrix) {
                min = Math.max(min, aMatrix[i]);
            }
            if(rowMax.contains(min)){
                list.add(min);
            }
        }
        return list;
    }

    public static int singleNonDuplicate(int[] nums) {
        int result = 0;
        for (int temp : nums) {
            result ^= temp;
        }
        return result;
    }

    public static long minimumRemoval(int[] beans) {
        long minResult = 0;
        Arrays.sort(beans);
        for (int i:beans) {
            minResult += i;
        }
        int size = beans.length;
        long minNums = minResult;
        for (int i = 0; i < beans.length; i++) {
            minNums = minResult - ((long)beans[i] * ((size - i))) < minNums ? minResult - ((long)beans[i] * ((size - i))) : minNums;
        }
        return minNums;
    }

    public static int minimumOperations(int[] nums) {
        if (nums.length == 1 || (nums.length == 2 && nums[0] != nums[1])) {
            return 0;
        }
        Map<Integer,Integer> map1 = new HashMap<>(16);
        Map<Integer,Integer> map2 = new HashMap<>(16);
        boolean simple = true;
        int[][] max1 = new int[2][2];
        int[][] max2 = new int[2][2];
        for (int i = 0; i < nums.length; i++) {
            if(i != nums.length - 1) {simple = simple && nums[0] == nums[i];}
            if (i % 2 == 0){
                Integer val = map1.getOrDefault(nums[i], 0) + 1;
                map1.put(nums[i], val);

            }else{
                Integer val = map2.getOrDefault(nums[i], 0) + 1;
                map2.put(nums[i], val);

            }
        }
        if (simple) {
            return nums.length / 2;
        }
        map1.forEach((k,v)->{
            if (max1[0][1] < v) {
                if (max1[0][0] != max1[1][0]){
                    max1[1][1] = max1[0][1];
                    max1[1][0] = max1[0][0];
                }
                max1[0][1] = v;
                max1[0][0] = k;
            }else if (v > max1[1][1]){
                max1[1][1] = v;
                max1[1][0] = k;
            }
        });
        map2.forEach((k,v)->{
            if (max2[0][1] < v) {
                if (max2[0][0] != max2[1][0]){
                    max2[1][1] = max2[0][1];
                    max2[1][0] = max2[0][0];
                }
                max2[0][1] = v;
                max2[0][0] = k;
            }else if (v > max2[1][1]){
                max2[1][1] = v;
                max2[1][0] = k;
            }
        });
        if (max1[0][0] == max2[0][0]){
            return nums.length - Math.max(max1[0][1] + max2[1][1], max1[1][1] + max2[0][1]);
        }
        return nums.length - max1[0][1] - max2[0][1];
    }

    public static int countOperations(int num1, int num2) {
        int step = 0;
        while (num1 != 0 && num2 != 0) {
            if (num1 >= num2) {
                num1 = num1 - num2;
            }else {
                num2 = num2 - num1;
            }
            step++;
        }
        return step;
    }

    public static int maxNumberOfBalloons(String text) {
        int[] result = new int[5];
        for (char temp : text.toCharArray()) {
            switch (temp){
                case 'b':result[0]++;break;
                case 'a':result[1]++;break;
                case 'l':result[2]++;break;
                case 'o':result[3]++;break;
                case 'n':result[4]++;break;
                default:break;
            }
        }
        return Math.min(Math.min(Math.min(result[2]/2,result[3]/2),result[4]),Math.min(result[0],result[1]));
    }

    public static int minimumDifference(int[] nums, int k) {
        if(k == 1){return 0;}
        Arrays.sort(nums);
        int min = Integer.MAX_VALUE;
        for (int i = k; i <= nums.length; i++) {
            min = Math.min(min, nums[i - 1] - nums[i - k]);
        }
        return min;
    }

    public static int minimumTime(String s) {
        return 1;
    }

    class Bitset {
        int[] data;
        int one;
        int real;

        public Bitset(int size) {
            real = size;
            data = new int[size % 10 == 0 ? size / 10 : size / 10 + 1];
            one = 0;
        }

        public void fix(int idx) {
            int i1 = idx / 10;
            int i2 = idx % 10 + 1;
            char[] chars = new char[10];
            Arrays.fill(chars,'0');
            chars[i2] = '1';
            int temp = Integer.parseInt(new String(chars),2);
            if((data[i1] & temp) == 0){
                one++;
            }
            data[i1] |= temp;
        }

        public void unfix(int idx) {
            int i1 = idx / 10;
            int i2 = idx % 10 + 1;
            char[] chars = new char[10];
            Arrays.fill(chars,'1');
            chars[i2] = '0';
            int temp = Integer.parseInt(new String(chars),2);
            if((data[i1] | temp) != temp){
                one--;
            }
            data[i1] &= temp;
        }

        public void flip() {
            for (int i = 0; i < data.length; i++) {
                data[i] = (1023 & (~data[i]));
            }
            one = real - one;
        }

        public boolean all() {
            return one == real;
        }

        public boolean one() {
            return one > 0;
        }

        public int count() {
            return one;
        }

        public int countKDifference(int[] nums, int k) {
            int result = 0;
            for (int i = 0; i < nums.length; i++) {
                for (int j = i; j < nums.length; j++) {
                    if(Math.abs(nums[i] - nums[j]) == k){result++;}
                }
            }
            return result;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            int yu = real % 10;
            int index = 0;
            for (int i:data) {
                String binaryString = Integer.toBinaryString(i & 1023);
                if(!"0".equals(binaryString)){
                    for (int j = 0; j < 10 - binaryString.length(); j++) {
                        builder.append("0");
                    }
                }
                if(++index == data.length){
                    for (int j = 0; j < yu ; j++) {
                        builder.append(binaryString.charAt(j));
                    }
                    break;
                }
                builder.append(binaryString);
            }
            return builder.toString();
        }
    }


    public static long smallestNumber(long num) {
        if(num == 0){return 0;}
        boolean fix = num > 0;
        StringBuilder builder = new StringBuilder();
        String end = String.valueOf(num);
        int[] site = new int[10];
        if(!fix){
            end = end.substring(1);
        }
        for (char c : end.toCharArray()) {
            site[c - '0']++;
        }
        if(fix){
            if(site[0] > 0){
                int index = 1;
                while (site[index] == 0){
                    index++;
                }
                site[index]--;
                builder.append(index);
            }
            for (int i = 0; i <= 9; i++) {
                while (site[i]-- > 0){
                    builder.append(i);
                }
            }
        }else{
            for (int i = 9; i >= 0; i--) {
                while (site[i]-- > 0){
                    builder.append(i);
                }
            }
        }
        return fix ? Long.parseLong(builder.toString()) : -Long.parseLong(builder.toString());
    }

    public static int[] sortEvenOdd(int[] nums) {
        if(nums.length <= 2){return nums;}
        int len = nums.length / 2;
        Integer[] nums1 = new Integer[nums.length % 2 == 0 ? len : len + 1];
        Integer[] nums2 = new Integer[len];
        for (int i = 0,a = 0, b = 0; i < nums.length; i++) {
            if(i % 2 == 0){
                nums1[a++] = nums[i];
            }else{
                nums2[b++] = nums[i];
            }
        }
        Arrays.sort(nums1);
        Arrays.sort(nums2, Collections.reverseOrder());
        int index = 0;int i = 0;int j = 0;
        while (index < nums.length){
            nums[index] = index % 2 == 0 ? nums1[i++] : nums2[j++];
            index++;
        }
        return nums;
    }

    public static int sumOfUnique(int[] nums) {
        Map<Integer,Integer> simple = new HashMap<>(16);
        int num = 0;
        for (int temp : nums) {
            if(!simple.containsKey(temp)){
                num += temp;
                simple.put(temp,1);
                continue;
            }
            if(simple.getOrDefault(temp,0) == 1){
                num -= temp;
                simple.put(temp,0);
            }
        }
        return num;
    }

    private static int max = 0;
    public static int getMaximumGold(int[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if(grid[i][j] != 0){
                    Set<String> set = new HashSet<>(4);
                    set.add(i + "" + j);
                    getIt(set,grid,i,j,grid.length,grid[0].length,grid[i][j]);
                }
            }
        }
        return max;
    }

    private static void getIt(Set<String> forword,int[][] grid,int i,int j,int x,int y,int num){
        if(grid[i][j] == 0){
            max = Math.max(max,num);
            return;
        }
        // 上,下,左,右
        if(i > 0 && !forword.contains(i - 1 + "" + j) && grid[i - 1][j] != 0){
//            set.add(i - 1 + "" + j);
            num += grid[i - 1][j];
            forword.add(i - 1 + "" + j);
            getIt(forword, grid, i - 1, j, x, y, num);
            num -= grid[i - 1][j];
            forword.remove(i - 1 + "" + j);
        }
        if(i + 1 < x  && !forword.contains(i + 1 + "" + j) && grid[i + 1][j] != 0){
//            set.add(i + 1 + "" + j);
            num += grid[i + 1][j];
            forword.add(i + 1 + "" + j);
            getIt(forword, grid, i + 1, j, x, y, num);
            num -= grid[i + 1][j];
            forword.remove(i + 1 + "" + j);
        }
        if(j > 0 && !forword.contains(i + "" + (j- 1)) && grid[i][j - 1] != 0){
//            set.add(i + "" + (j- 1));
            num += grid[i][j - 1];
            forword.add(i + "" + (j- 1));
            getIt(forword, grid, i, j - 1, x, y, num);
            num -= grid[i][j - 1];
            forword.remove(i + "" + (j- 1));
        }
        if(j + 1 < y  && !forword.contains(i + "" + (j + 1)) && grid[i][j + 1] != 0){
//            set.add(i + "" + (j + 1));
            num += grid[i][j + 1];
            forword.add(i + "" + (j + 1));
            getIt(forword, grid, i, j + 1, x, y, num);
            num -= grid[i][j + 1];
            forword.remove(i + "" + (j + 1));
        }
        max = Math.max(max,num);
    }
}
