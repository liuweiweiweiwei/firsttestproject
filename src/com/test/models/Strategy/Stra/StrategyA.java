package com.test.models.Strategy.Stra;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/6/17 15:27
 * @description
 */
public class StrategyA {
    public static void main(String[] args) throws Exception {
        Modify modify=new Modify();
        String moneyNum = modify.getMoneyNum(Test.doller, 100);
        System.out.println(moneyNum);
    }
}

interface Money {
    String getNum(int num)throws Exception;
}
class Doller implements Money{
    @Override
    public String getNum(int num) {
        return String.valueOf(num*6.9);
    }
}
class Point implements Money{
    @Override
    public String getNum(int num) {
        return String.valueOf(num*10);
    }
}
class Modify{
    public static Map<String,Money> moneyMap=new HashMap<>();
    static {
        moneyMap.put("doller",new Doller());
        moneyMap.put("point",new Point());
    }

    public String getMoneyNum(Enum key,int data)throws Exception{
        Money money = moneyMap.get(key.name());
        if(null==money)
            return null;
        return money.getNum(data);
    }
}
enum Test{
    doller("doller"),point("point");
    Test(String name) { this.name = name; }
    private String name;
    public final String getName() { return name; }
}