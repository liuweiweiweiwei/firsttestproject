package com.test.autimatic.type;

import java.util.*;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/2/20 13:01
 * @description
 */
public class LeetCodeString {

    public static void main(String[] args) {
        System.out.println(minimumTime(new int[]{39,82,69,37,78,14,93,36,66,61,13,58,57,12,70,14,67,75,91,1,34,68,73,50,13,40,81,21,79,12,35,18,71,43,5,50,37,16,15,6,61,7,87,43,27,62,95,45,82,100,15,74,33,95,38,88,91,47,22,82,51,19,10,24,87,38,5,91,10,36,56,86,48,92,10,26,63,2,50,88,9,83,20,42,59,55,8,15,48,25
        },4187));
//        System.out.println(minimalKSum(new int[]{96,44,99,25,61,84,88,18,19,33,60,86,52,19,32,47,35,50,94,17,29,98,22,21,72,100,40,84
//        },35));
        System.out.println(minimalKSum(new int[]{339919839},670399));
//        System.out.println(minimalKSum(new int[]{53,41,90,33,84,26,50,32,63,47,66,43,29,88,71,28,83}
//        ,76));
    }

    public List<String> cellsInRange(String s) {
        List<String> result = new ArrayList<>();
        int mini,maxi;
        char minc,maxc;
        if (s.charAt(0) > s.charAt(3)) {
            minc = s.charAt(3);
            maxc = s.charAt(0);
        } else {
            minc = s.charAt(0);
            maxc = s.charAt(3);
        }
        if (s.charAt(1) > s.charAt(4)) {
            mini = Integer.parseInt(s.charAt(4) + "");
            maxi = Integer.parseInt(s.charAt(1) + "");
        }else {
            mini = Integer.parseInt(s.charAt(1) + "");
            maxi = Integer.parseInt(s.charAt(4) + "");
        }
        for (char i = minc; i <= maxc; i++) {
            for (int j = mini; j <= maxi ; j++) {
                result.add(i + "" + j);
            }
        }
        return result;
    }


    public static long minimalKSum(int[] nums, int k) {
        Arrays.sort(nums);
        long num = 0;
        int index = nums[0] - 1;
        int tempEnd = nums[0] > k ? k : nums[0] - 1;
        int tempNum = nums[0] > k ? k : nums[0] - 1;
        if (nums[0] != 1) {
            num += num(1, tempEnd, tempNum);
        }
        for (int i = 1; i < nums.length && index < k; i++) {
            if ((nums[i] == nums[i - 1]) || (nums[i] == nums[i - 1] + 1)) {
                continue;
            }
            int end = nums[i] - 1;
            int temp = nums[i] - nums[i - 1] - 1;
            if (k - index < nums[i] - nums[i - 1]) {
                temp = k - index;
                end = nums[i - 1] + k - index;
            }
            index += temp;
            num += num(nums[i - 1] + 1, end, temp);
        }
        if (index < k) {
            num += num(nums[nums.length - 1] + 1, nums[nums.length - 1] + (k - index), k - index );
        }
        return num;
    }

    private static long num(long start, long end, long num) {
        return  (long)((start + end) * (num) / 2);
    }

    public int findLUSlength(String a, String b) {
        if (a.length() != b.length()) {
            return Math.max(a.length(), b.length());
        }
        if (a.equals(b)) {
            return -1;
        }
        return a.length();
    }

    public static long minimumTime(int[] time, int totalTrips) {
        Arrays.sort(time);
        if (time.length >= totalTrips) {
            int[] temp = new int[totalTrips];
            System.arraycopy(time,0,temp,0,totalTrips);
            time = temp;
        }
        for (long i = time[0]; i <= time[0] * totalTrips; i++) {
            int tempNum = 0;
            for (int ti : time) {
                tempNum += (i / ti);
            }
            if (tempNum >= totalTrips) {
                return i;
            }
        }
        return -1;
    }

    public int minSteps(String s, String t) {
        int[] sArr = new int[26];
        int[] tArr = new int[26];
        for (char tempS : s.toCharArray()) {
            sArr[tempS - 'a']++;
        }
        for (char tempT : t.toCharArray()) {
            tArr[tempT - 'a']++;
        }
        int result = 0;
        for (int i = 0; i < 26; i++) {
            result += Math.abs(sArr[i] - tArr[i]);
        }
        return result;
    }

    public int prefixCount(String[] words, String pref) {
        int result = 0;
        for (String str : words) {
            if (str.startsWith(pref)) {
                result++;
            }
        }
        return result;
    }

    public String optimalDivision(int[] nums) {
        if (nums.length == 1) {
            return String.valueOf(nums[0]);
        }
        if (nums.length == 2) {
            return String.valueOf(nums[0] + "/" + nums[1]);
        }
        StringBuilder builder = new StringBuilder();
        builder.append(nums[0]).append("/(");
        for (int i = 1; i < nums.length; i++) {
            builder.append(nums[i]);
            if (i != nums.length - 1) {
                builder.append("/");
            }
        }
        builder.append(")");
        return builder.toString();
    }

    public String reverseOnlyLetters(String s) {
        char[] chars = s.toCharArray();
        for (int i = 0, j = chars.length - 1; i < j; i++, j--) {
            while (i < chars.length && !(judgeLetter(chars[i]))) {
                i++;
            }
            while (j >= 0 && !(judgeLetter(chars[j]))) {
                j--;
            }
            if (i < j){
                char temp = chars[i];
                chars[i] = chars[j];
                chars[j] = temp;
            }
        }
        return new String(chars);
    }

    private boolean judgeLetter(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    private static void add(StringBuilder builder, char c, int time){
        for (int i = 0; i < time; i++) {
            builder.append(c);
        }
    }
}
