package com.test.autimatic.week;

import java.util.*;
import java.util.StringJoiner;

/**
 * @author 25338
 * @version 1.0
 * @date 2022/5/1 10:32
 * @description
 */
public class leetcodeweek0501 {
    public static void main(String[] args) {
        System.out.println(minimumCardPickup(new int[]{3, 4, 2, 3, 4, 7}));
        System.out.println(countDistinct(new int[]{1,2,3,4},4, 1));
    }

    public static String removeDigit(String number, char digit) {
        StringBuilder result = new StringBuilder();
        int index = -1;
        for (int i = 0; i < number.length(); i++) {
            char cur = number.charAt(i);
            if (cur == digit) {
                index = i;
                if (i == number.length() - 1 || cur < number.charAt(i + 1)) {
                    result.append(number.substring(i + 1));
                    return result.toString();
                }
            }
            result.append(cur);
        }
        return number.substring(0, index) + number.substring(index + 1);
    }

    public static int minimumCardPickup(int[] cards) {
        Map<Integer, Integer> numMap = new HashMap<>();
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < cards.length; i++) {
            Integer value = numMap.getOrDefault(cards[i], i);
            if (value != i) {
                result = Math.min(result, i - value + 1);
            }
            numMap.put(cards[i], i);
        }
        return result == Integer.MAX_VALUE ? -1 : result;
    }

    public static int countDistinct(int[] nums, int k, int p) {
        Set<String> resultSet = new HashSet<>();
        List<Integer> beforeIndex = new ArrayList<>();
        int kNum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % p == 0) {
                kNum++;
                beforeIndex.add(i);
            }
            if (kNum > k) {
                Integer remove = beforeIndex.remove(0);
                getCountNum(resultSet, nums, remove,i);
                getAnother(resultSet, nums, remove, i);
            } else if (i == nums.length - 1) {
                Integer remove = beforeIndex.remove(0);
                getCountNum(resultSet, nums, remove,i);
            }
        }
        return resultSet.size();
    }

    private static void getCountNum(Set<String> allSet, int[] data, int start, int end) {
        for (int i = 1; i <= end - start; i++) {
            for (int j = start; j <= end; j++) {
                allSet.add(countDistStr(data, j, i + j > end ? end : (i == end - start) ? i + j : j + i - 1));
            }
        }
    }

    private static void getAnother(Set<String> allSet, int[] data, int start, int end) {
        String builder = String.valueOf(data[end]);
        for (int i = end - 1; i > start; i--) {
            builder = String.format("%d:%s", data[i], builder);
            allSet.add(builder);
        }
    }

    private static String countDistStr(int[] data, int s, int e) {
        StringJoiner resultStr = new StringJoiner(":");
        for (int i = s; i <= e; i++) {
            resultStr.add(String.valueOf(data[i]));
        }
        return resultStr.toString();
    }
}
