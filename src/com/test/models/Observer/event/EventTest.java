package com.test.models.Observer.event;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/14 15:15
 * @description
 */
public class EventTest {

    public static void main(String[] args) {
        System.out.println('@'-'0');
        MourceListener mourceListener = new MourceListener();
        MouceEventClick click = new MouceEventClick();
        mourceListener.addListener(EventListener.CASE.CLICK_SIN,click);
        mourceListener.addListener(EventListener.CASE.CLICK_TWO,click);
        mourceListener.addListener(EventListener.CASE.ON,click);
        mourceListener.addListener(EventListener.CASE.UP,click);
        mourceListener.click();
        mourceListener.clickDouble();
        mourceListener.on();
        mourceListener.up();
    }
}
