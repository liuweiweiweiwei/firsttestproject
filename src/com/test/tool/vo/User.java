package com.test.tool.vo;

import java.time.LocalDateTime;

/**
 * excel转mysql数据类
 * @author 25338
 */
public class User extends Parent{
    private String name;
    private Integer scope;
    private LocalDateTime time;

    public User(String name, Integer scope) {
        this.name = name;
        this.scope = scope;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return super.toString()+"User{" +
                "name='" + name + '\'' +
                ", scope=" + scope +
                '}';
    }
}
class Parent{
    private String parentName;
    private int parentAge;
    private boolean judge;

    public boolean isJudge() {
        return judge;
    }

    public void setJudge(boolean judge) {
        this.judge = judge;
    }

    public Parent(String parentName, int parentAge) {
        this.parentName = parentName;
        this.parentAge = parentAge;
    }

    public Parent() {
    }

    @Override
    public String toString() {
        return "Parent{" +
                "parentName='" + parentName + '\'' +
                ", parentAge=" + parentAge +
                ", judge=" + judge +
                '}';
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public int getParentAge() {
        return parentAge;
    }

    public void setParentAge(int parentAge) {
        this.parentAge = parentAge;
    }
}
