package com.test.models.Strategy.newa;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/7 17:04
 * @description
 */
public class GroupByStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("拼团操作！减少付钱！！！满20人成团，享受价格！");
    }
}
