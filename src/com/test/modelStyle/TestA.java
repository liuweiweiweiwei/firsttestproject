package com.test.modelStyle;

/**
 * @author 25338
 * @version 1.0
 * @date 2020/10/9 14:05
 * @description BeanFactory。 Spring中的BeanFactory就是简单工厂模式的体现，
 * 根据传入一个唯一的标识来获得Bean对象，但是否是在传入参数后创建还是传入参
 * 数前创建这个要根据具体情况来定。
 */
public class TestA {

}
