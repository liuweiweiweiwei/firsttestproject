package com.test.models.adapt.old;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/5/20 15:27
 * @description 适
 */
public class AdaptModel {


    public static void main(String[] args) {
        Phone phone=new Phone();
        phone.charging(new AdaptPower(new Model()));
    }
}

/**
 * 原始
 */
class Model {
    public int info() {
        int power = 220;
        System.out.println("电压为:" + power);
        return power;
    }
}
/**
 * 奥利给
 */
interface Adapt{
    int info5();
}
/**
 * 奥利给1
 */
class AdaptPower implements Adapt{
    private Model model;

    public AdaptPower(Model model){this.model=model;}
//    @Override
//    public int info() {
//        return super.info();
//    }

    @Override
    public int info5() {
        int power=5;
        if(model!=null){
            power=model.info()/44;
        }
        System.out.println("电压为:"+power);
        return power;
    }
}
class Phone{
    public void charging(AdaptPower adaptPower){
        if(adaptPower!=null&&adaptPower.info5()<=5){
            System.out.println("电压为正常！");
        }else{
            System.out.println("Power不正常请检查！");
        }
    }
}