package com.test.status;

public class StaticSingle {
    private StaticSingle(){}
    private static class Static{
        private static StaticSingle staticSingle=new StaticSingle();
    }
    public static StaticSingle getInstance(){
        return Static.staticSingle;
    }

    private Object readResolver(){
        return Static.staticSingle;
    }
}
