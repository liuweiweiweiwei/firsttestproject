package com.test.models.gpdecorate.v2;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/13 22:14
 * @description 装饰者模式
 */
public abstract class BatterCake {

    protected abstract String name();

    protected abstract int price();
}
