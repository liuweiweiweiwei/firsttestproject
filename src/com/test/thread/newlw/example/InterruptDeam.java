package com.test.thread.newlw.example;

import java.util.concurrent.TimeUnit;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/17 9:23
 * @description
 */
public class InterruptDeam {

    private static volatile int i = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                i++;
            }
            System.out.println("i = " + i);
        });
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        //打断线程
        thread.interrupt();
    }
}
