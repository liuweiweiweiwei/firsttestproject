package com.test.thread.newlw.net;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/28 16:04
 */
public class FastJsonSericial implements IService {

    static {
        //配置--transient可以序列化
        JSON.DEFAULT_GENERATE_FEATURE = SerializerFeature.config(
                JSON.DEFAULT_GENERATE_FEATURE, SerializerFeature.SkipTransientField,false
        );
    }

    @Override
    public <T> byte[] read(T t) {
        return JSON.toJSONString(t).getBytes();
    }

    @Override
    public <T> T write(byte[] bytes, Class<T> tClass, String name) {
        return JSON.parseObject(new String(bytes),tClass);
    }
}
