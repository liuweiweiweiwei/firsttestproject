package com.test.status;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/1 11:23
 * @description
 */
public class SingleLemon {

    private SingleLemon() {
    }

    private static Map<String, Object> ioc = new ConcurrentHashMap<>();

    public static Object getBean(String className) {
        synchronized (ioc) {
            if (!ioc.containsKey(className)) {
                Object o = null;
                try {
                    Class aClass = Class.forName(className);
                    Constructor declaredConstructor = aClass.getDeclaredConstructor(null);
                    declaredConstructor.setAccessible(true);
                    o = declaredConstructor.newInstance();
                    ioc.put(className, o);
                    declaredConstructor.setAccessible(false);
                    return o;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return ioc.get(className);
        }
    }
}
