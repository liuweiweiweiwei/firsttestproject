package com.test.classes.myclassloader;

import com.test.MyLog.LwLog;

import java.lang.ClassLoader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 25338
 * @date 2022/5/12 22:46
 * @since 1.0
 * description
 */
public class TestClassLoader {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException,
            InstantiationException, NoSuchMethodException, InvocationTargetException {
        // 通过类加载器加载
        MyClassLoader myClassLoader = new MyClassLoader();
        Class<?> loadClass = myClassLoader.loadClass("com.test.classes.myclassloader.HelloWorld");
        System.out.println(loadClass.getClassLoader());
        Object o = loadClass.newInstance();
        System.out.println(o);
        Method method = loadClass.getMethod("welcome");
        String result = (String) method.invoke(o);
        System.out.println(result);
        LwLog.splitLogInfo("分割线");
        new TestClassLoader().setParentClassLoader();
    }

    /**
     * 绕过根类加载器
     */
    public void setParentClassLoader() throws ClassNotFoundException, IllegalAccessException,
        InstantiationException, NoSuchMethodException, InvocationTargetException {
        ClassLoader parent = TestClassLoader.class.getClassLoader().getParent();
//        MyClassLoader myClassLoader = new MyClassLoader(parent, "F:/file/java/");
        MyClassLoader myClassLoader = new MyClassLoader(null, "F:/file/java/");
        Class<?> aClass = myClassLoader.loadClass("com.test.classes.myclassloader.HelloWorld");
        System.out.println(aClass);
        System.out.println(aClass.getClassLoader());
        Object o = aClass.newInstance();
        Method welcome = aClass.getMethod("welcome");
        Object invoke = welcome.invoke(o);
        System.out.println(invoke.toString());
    }
}
