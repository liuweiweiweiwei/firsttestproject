package com.test.TraceProxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @date 20211020
 */
public class Aop {

    @interface Aspect{
        Class<?>[] value();
    }
    static class IserviceAImpl implements DynamicFloat.IService {
        @Override
        public void sayHello() {
            System.out.println("hi!A");
        }

        @Override
        public String getName() {
            return "IserviceA";
        }
    }
    static class IserviceBImpl implements DynamicFloat.IService {
        @Override
        public void sayHello() {
            System.out.println("hello ! B");
        }

        @Override
        public String getName() {
            return "IserviceB";
        }
    }

    /**
     * 调用前后
     */
    @Aspect({IserviceAImpl.class,IserviceBImpl.class})
    static class ServiceLogAspect{
        public static void before(Object o, Method method,Object[] args){
            System.out.println("enter method-1 class :"+method.getDeclaringClass().getSimpleName() +"::"+method.getName());
        }
        public static void after(Object o,Method method,Object[] args,Object result){
            System.out.println("leaving method-2 :"+method.getDeclaringClass().getSimpleName() +"::"+method.getName()+"::result:"+result);
        }
    }
    @Aspect({IserviceBImpl.class,IserviceAImpl.class})
    static class ServiceException{
        public static void exception(Object o,Method m,Object[] args,Throwable throwable){
            System.err.println("exception when-3 getMethod:"+m.getName()+","+ Arrays.toString(args));
        }
    }

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        CGLIBContainer container = new CGLIBContainer();
        DynamicFloat.IService instance = CGLIBContainer.createInstance(IserviceAImpl.class);
        instance.getName();
        for (Class c:CGLIBContainer.interPointMap.keySet()) {
            System.out.println("k"+c.getName());
            System.out.println("v"+CGLIBContainer.interPointMap.get(c));
        }
        DynamicFloat.IService instance1 = CGLIBContainer.getInterstance(IserviceAImpl.class);
        instance1.sayHello();
        for (Class c:CGLIBContainer.interPointMap.keySet()) {
            System.out.println("k"+c.getName());
            System.out.println("v"+CGLIBContainer.interPointMap.get(c));
        }
        System.out.println(CGLIBContainer.interPointMap.toString());
    }
    /**
     * container容器
     */
    static class CGLIBContainer{
        //容器变量
        static Map<Class<?>,Map<InterPointer,List<Method>>> interPointMap = new HashMap<>();
        //指定对象
        static Class<?>[] aClass = new Class<?>[]{ServiceLogAspect.class,ServiceException.class};
        {
            try {
                init();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        public void init() throws NoSuchMethodException {
            for (Class<?> cla:aClass) {
                System.out.println(cla.getSimpleName());
                Aspect aspect=cla.getAnnotation(Aspect.class);
                if(aspect!=null){
                    Method before=cla.getMethod("before", Object.class,Method.class,Object[].class);
                    Method after=cla.getMethod("after", Object.class,Method.class,Object[].class);
                    Method exception=cla.getMethod("exception", Object.class,Method.class,Object[].class);

                    Class<?>[] clas=aspect.value();
                    for (Class<?> c:clas) {
                        addintercepted(c,InterPointer.BEFORE,before);
                        addintercepted(c,InterPointer.AFTER,after);
                        addintercepted(c,InterPointer.EXCEPTION,exception);
                    }
                }
            }
        }
        private static void addintercepted(Class<?> cla,InterPointer interPointer,Method method){
            if(method==null){
                return;
            }
            Map<InterPointer, List<Method>> map = interPointMap.computeIfAbsent(cla, k -> new HashMap<>());
            List<Method> list = map.computeIfAbsent(interPointer, k -> new LinkedList<>());
            list.add(method);
        }
        public static <T> T createInstance(Class<T> cla) throws IllegalAccessException, InstantiationException {
            if(interPointMap.containsKey(cla)){
                return cla.newInstance();
            }
            Enhancer enhancer=new Enhancer();
            enhancer.setSuperclass(cla);
            enhancer.setCallback(new AspectInterpoint());
            return (T)enhancer.create();
        }

        static List<Method> getInterCeptMethod(Class<?> clas,InterPointer interPointer){
            Map<InterPointer,List<Method>> map = interPointMap.get(clas);
            if(map==null){
                return Collections.emptyList();
            }
            List<Method> list=map.get(interPointer);
            if(list==null){
                return Collections.emptyList();
            }
            return list;
        }

        enum InterPointer{
            BEFORE,AFTER,EXCEPTION
        }
        static class AspectInterpoint implements MethodInterceptor{
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                //执行befor方法
                List<Method> methodList=getInterCeptMethod(o.getClass().getSuperclass(),InterPointer.BEFORE);
                for (Method od:methodList) {
                    od.invoke(null, o,method,objects);
                    System.out.println("before");
                }
                //调用原始方法
                try {
                    Object result=methodProxy.invokeSuper(o,objects);
                    //调用after
                    List<Method> methodList1=getInterCeptMethod(o.getClass().getSuperclass(),InterPointer.AFTER);
                    for (Method od:methodList1) {
                        od.invoke(null, o,method,objects);
                        System.out.println("end");
                    }
                    return result;
                }catch (Throwable e){
                    List<Method> methodList1Excep=getInterCeptMethod(o.getClass().getSuperclass(),InterPointer.EXCEPTION);
                    for (Method od:methodList1Excep) {
                        od.invoke(null, o,method,objects);
                        System.out.println("throw");
                    }
                    throw e;
                }
            }
        }
        //创建实例
        public static <T> T getInterstance(Class<T> cla){
            try {
                T obj=createInstance(cla);
                Field[] fields=cla.getDeclaredFields();
                for (Field fle:fields) {
                    if(!fle.isAccessible()){
                        fle.setAccessible(true);
                    }
                    Class<?> c=fle.getType();
                    fle.set(obj,getInterstance(c));
                }
                return obj;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
     }

}
