package com.test.Files.bio.tomcat.http;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author 25338
 * @version 1.0
 * @date 2021/12/30 22:26
 * @description
 */
public class LwRequest {

    private ChannelHandlerContext context;
    private HttpRequest request;

    private InputStream inputStream;
    private String url;
    private String method;

    public LwRequest(InputStream inputStream) {
        this.inputStream = inputStream;
        String content = "";
        try {
            byte[] buffer = new byte[1024];
            int b;
            while ((b = inputStream.read(buffer)) != -1) {
                content = new String(buffer,0,b);
            }
            System.out.println(content);
            //解析数据
            String cont = content.split("\\n")[0];
            String[] split = cont.split("\\s");
            this.method = split[0];
            this.url = split[1].split("\\?")[0];
            System.out.println("-------------");
            System.out.println(method + " ;" + url);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public LwRequest(ChannelHandlerContext context, HttpRequest request) {
        this.context = context;
        this.request = request;
    }

    public String getUrl() {
        return request.uri();
//        return url;
    }
    public String getMethod() {
        return request.method().name().toString();
//        return method;
    }

    public Map<String,List<String>> getParameters(){
        QueryStringDecoder decoder = new QueryStringDecoder(request.uri());
        return decoder.parameters();
    }

    @Override
    public String toString() {
        return super.toString();
    }
    public String getparameter(String name){
        Map<String, List<String>> parameters = getParameters();
        List<String> list = parameters.get(name);
        return list == null ? null : list.get(0);
    }
}
